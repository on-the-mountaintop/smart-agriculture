#See https://aka.ms/customizecontainer to learn how to customize your debug container and how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
ARG BUILD_CONFIGURATION=Release
WORKDIR /src
COPY ["I_Archives.Read.API/I_Archives.Read.API.csproj", "I_Archives.Read.API/"]
COPY ["I_Archives.Infrastructure/I_Archives.Infrastructure.csproj", "I_Archives.Infrastructure/"]
COPY ["I_Archives.Domains/I_Archives.Domains.csproj", "I_Archives.Domains/"]
COPY ["CommonClass/CommonClass.csproj", "CommonClass/"]
RUN dotnet restore "./I_Archives.Read.API/I_Archives.Read.API.csproj"
COPY . .
WORKDIR "/src/I_Archives.Read.API"
RUN dotnet build "./I_Archives.Read.API.csproj" -c $BUILD_CONFIGURATION -o /app/build

FROM build AS publish
ARG BUILD_CONFIGURATION=Release
RUN dotnet publish "./I_Archives.Read.API.csproj" -c $BUILD_CONFIGURATION -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "I_Archives.Read.API.dll"]
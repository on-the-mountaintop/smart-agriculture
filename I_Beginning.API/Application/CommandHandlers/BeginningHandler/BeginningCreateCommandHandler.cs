﻿using AutoMapper;
using I_Beginning.API.Application.Command.BeginningCommand;
using I_Beginning.Domains.BeginningManagement;
using I_Beginning.Infrastructure.Interface;
using MediatR;

namespace I_Beginning.API.Application.CommandHandlers.BeginningHandler
{
    public class BeginningCreateCommandHandler : IRequestHandler<BeginningCreateCommand, int>
    {
        private readonly IBeginningRepository beginningRepository;
        private readonly IMapper mapper;
        public BeginningCreateCommandHandler(IBeginningRepository beginningRepository, IMapper mapper)
        {
            this.beginningRepository = beginningRepository;
            this.mapper = mapper;
        }
        /// <summary>
        /// 新增期初信息
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<int> Handle(BeginningCreateCommand request, CancellationToken cancellationToken)
        {
            var res = mapper.Map<Beginning>(request);
            return await beginningRepository.AddAsync(res);
        }
    }
}

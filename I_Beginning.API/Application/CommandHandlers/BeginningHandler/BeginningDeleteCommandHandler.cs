﻿using I_Beginning.API.Application.Command.BeginningCommand;
using I_Beginning.Infrastructure.Interface;
using MediatR;

namespace I_Beginning.API.Application.CommandHandlers.BeginningHandler
{
    public class BeginningDeleteCommandHandler : IRequestHandler<BeginningDeleteCommand, int>
    {
        private readonly IBeginningRepository beginningRepository;
        public BeginningDeleteCommandHandler(IBeginningRepository beginningRepository)
        {
            this.beginningRepository = beginningRepository;
        }
        /// <summary>
        /// 逻辑删除期初信息
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<int> Handle(BeginningDeleteCommand request, CancellationToken cancellationToken)
        {
            var res = await beginningRepository.GetModelAsync(request.BeginningId);
            res.IsDel = false;
            return await beginningRepository.StateDelete(res);
        }
    }
}

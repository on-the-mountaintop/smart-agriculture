﻿using I_Beginning.Domains;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace I_Beginning.API.Application.Command.BeginningCommand
{
    public class BeginningCreateCommand: AuditField, IRequest<int>
    {
        /// <summary>
        /// 羊只耳号
        /// </summary>
        [StringLength(40)]
        public string? EarNumber { get; set; }
        /// <summary>
        /// 栋舍
        /// </summary>
        [StringLength(40)]
        public string? Buildings { get; set; }
        /// <summary>
        /// 栏位
        /// </summary>
        [StringLength(40)]
        public string? Columns { get; set; }
        /// <summary>
        /// 只数
        /// </summary>
        public int SheepNumber { get; set; }
        /// <summary>
        /// 重量（公斤）
        /// </summary>
        public decimal Weight { get; set; }
        /// <summary>
        /// 种公 种母 后裔标识列
        /// </summary>
        public int Code { get; set; }
    }
}

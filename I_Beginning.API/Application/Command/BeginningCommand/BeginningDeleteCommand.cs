﻿using MediatR;

namespace I_Beginning.API.Application.Command.BeginningCommand
{
    public class BeginningDeleteCommand : IRequest<int>
    {
        /// <summary>
        /// 期初ID
        /// </summary>
        public int BeginningId { get; set; }
    }
}

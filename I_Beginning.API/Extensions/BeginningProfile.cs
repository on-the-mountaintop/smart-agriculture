﻿using AutoMapper;
using I_Beginning.API.Application.Command.BeginningCommand;
using I_Beginning.Domains.BeginningManagement;

namespace I_Beginning.API.Extensions
{
    public class BeginningProfile: Profile
    {
        public BeginningProfile() 
        {
            //新增
            CreateMap<BeginningCreateCommand, Beginning>().ReverseMap();
            //修改
            CreateMap<BeginningUpdateCommand, Beginning>().ReverseMap();
        }
    }
}

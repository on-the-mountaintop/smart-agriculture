using I_Beginning.API.Extensions;
using I_Beginning.Infrastructure;
using I_Beginning.Infrastructure.Impl;
using I_Beginning.Infrastructure.Interface;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
//添加jwt授权验证
builder.Services.AddJWT(builder);
//注入数据库上下文
builder.Services.AddDbContext<EFDbContext>(x => x.UseSqlServer(builder.Configuration.GetConnectionString("Conser")));
//注入中介者模式
builder.Services.AddMediatR(Assembly.GetExecutingAssembly());

//注入AutoMapper
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
//注入IBeginningRepository接口实现
builder.Services.AddScoped<IBeginningRepository, BeginningRepository>();
var app = builder.Build();

// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
//    app.UseSwagger();
//    app.UseSwaggerUI();
//}
app.UseSwagger();
app.UseSwaggerUI();

app.UseAuthentication();//身份认证

app.UseAuthorization();//授权


app.MapControllers();

app.Run();

﻿using I_Beginning.API.Application.Command.BeginningCommand;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Beginning.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BeginningController : ControllerBase
    {
        private readonly IMediator mediator;
        public BeginningController(IMediator mediator)
        {
            this.mediator = mediator;
        }
        /// <summary>
        /// 新增期初信息
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<int> BeginninCreate(BeginningCreateCommand command)
        {
            return await mediator.Send(command);
        }
        /// <summary>
        /// 修改期初信息
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<int> BeginninUpdate(BeginningUpdateCommand  command)
        {
            return await mediator.Send(command);
        }
        /// <summary>
        /// 逻辑删除期初信息
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<int> BeginninDelete(BeginningDeleteCommand command)
        {
            return await mediator.Send(command);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Beginning.Domains.BeginningManagement
{
    public class Page<T>
    {
        public List<T>? Data { get; set; }
        /// <summary>
        /// 数据总数
        /// </summary>
        public int total { get; set; }
        /// <summary>
        /// 总页数
        /// </summary>
        public int pageCount { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Beginning.Domains.BeginningManagement
{
    /// <summary>
    /// 期初表
    /// </summary>
    [Table("Beginning")]
    public class Beginning: AuditField
    {
        /// <summary>
        /// 期初ID
        /// </summary>
        [Key]
        public int BeginningId { get; set; }
        /// <summary>
        /// 羊只耳号
        /// </summary>
        [StringLength(40)]
        public string? EarNumber { get; set; }
        /// <summary>
        /// 栋舍
        /// </summary>
        [StringLength(40)]
        public string? Buildings { get; set; }
        /// <summary>
        /// 栏位
        /// </summary>
        [StringLength(40)]
        public string? Columns { get; set; }
        /// <summary>
        /// 只数
        /// </summary>
        public int SheepNumber { get; set; }
        /// <summary>
        /// 重量（公斤）
        /// </summary>
        public decimal Weight { get; set; }
        /// <summary>
        /// 种公 种母 后裔标识列
        /// </summary>
        public int Code { get; set; }
    }
}

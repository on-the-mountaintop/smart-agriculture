﻿using CommonClass;
using I_Produce.Domains.DTO;
using I_Produce.Domains.Entity;
using I_Produce.Read.API.Application.Commands.PregnancyPregnancyTestCommand;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Produce.Read.API.Controllers
{
	/// <summary>
	/// 种母妊检API接口(读)
	/// </summary>
	[Route("api/[controller]/[action]")]
	[ApiController]
	public class PregnancyPregnancyTestReadController : ControllerBase
	{
		private readonly IMediator _mediator;

		/// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="mediator"></param>
		public PregnancyPregnancyTestReadController(IMediator mediator)
		{
			_mediator = mediator;
		}

		/// <summary>
		/// 种母妊检的查询分页显示功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpGet]
		public async Task<ApiResult<PagerResult<PregnancyPregnancyTest>>> GetPregnancyPregnancyTestList([FromQuery] PregnancyPregnancyTestQueryCommand command)
		{
			return await _mediator.Send(command);
		}

		/// <summary>
		/// 种母妊检的回显数据功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpGet]
		public async Task<ApiResult<PregnancyPregnancyTest>> GetPregnancyPregnancyTestInfo([FromQuery] PregnancyPregnancyTestQueryInfoCommand command)
		{
			return await _mediator.Send(command);
		}

		/// <summary>
		/// 获取妊检方式
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		public IActionResult GetPregnancyTestMethod()
		{
			var list = new List<object>();

			foreach (var item in Enum.GetValues<PregnancyTestMethod>())
			{
				list.Add(new
				{
					value = item,
					name = item.ToString()
				});
			}

			return Ok(list);
		}

		/// <summary>
		/// 获取妊检结果
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		public IActionResult GetPregnancyTestResult()
		{
			var list = new List<object>();

			foreach (var item in Enum.GetValues<PregnancyTestResult>())
			{
				list.Add(new
				{
					value = item,
					name = item.ToString()
				});
			}

			return Ok(list);
		}
	}
}

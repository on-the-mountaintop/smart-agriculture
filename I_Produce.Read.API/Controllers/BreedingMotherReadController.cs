﻿using CommonClass;
using I_Produce.Domains.DTO;
using I_Produce.Domains.Entity;
using I_Produce.Read.API.Application.Commands.BreedingMotherCommand;
using I_Produce.Read.API.Application.Commands.GermlineHistoryRecordsCommand;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Produce.Read.API.Controllers
{
	/// <summary>
	/// 种母配种API接口(读)
	/// </summary>
	[Route("api/[controller]/[action]")]
	[ApiController]
	public class BreedingMotherReadController : ControllerBase
	{
		private readonly IMediator _mediator;


		/// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="mediator"></param>
		public BreedingMotherReadController(IMediator mediator)
		{
			_mediator = mediator;
		}

		/// <summary>
		/// 种母配种的查询分页显示功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpGet]
		public async Task<ApiResult<PagerResult<BreedingMother>>> GetBreedingMotherList([FromQuery] BreedingMotherQueryCommand command)
		{
			return await _mediator.Send(command);
		}

		/// <summary>
		/// 种母配种的回显数据功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpGet]
		public async Task<ApiResult<BreedingMother>> GetBreedingMotherInfo([FromQuery] BreedingMotherQueryInfoCommand command)
		{
			return await _mediator.Send(command);
		}

		/// <summary>
		/// 获取配种方式
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		public IActionResult GetBreedingPattern()
		{
			var list = new List<object>();

			foreach (var item in Enum.GetValues<BreedingPattern>())
			{
				list.Add(new
				{
					value = item,
					name = item.ToString()
				});
			}

			return Ok(list);
		}

		/// <summary>
		/// 获取种母发情类型
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		public IActionResult GetSpeciesEstrusType()
		{
			var list = new List<object>();

			foreach (var item in Enum.GetValues<SpeciesEstrusType>())
			{
				list.Add(new
				{
					value = item,
					name = item.ToString()
				});
			}

			return Ok(list);
		}

		/// <summary>
		/// 获取繁殖状态
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		public IActionResult GetReproductiveState()
		{
			var list = new List<object>();

			foreach (var item in Enum.GetValues<ReproductiveState>())
			{
				list.Add(new
				{
					value = item,
					name = item.ToString()
				});
			}

			return Ok(list);
		}
	}
}

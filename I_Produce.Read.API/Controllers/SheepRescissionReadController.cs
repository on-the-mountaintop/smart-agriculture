﻿using CommonClass;
using I_Produce.Domains.DTO;
using I_Produce.Domains.Entity;
using I_Produce.Read.API.Application.Commands.SheepRescissionCommand;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Produce.Read.API.Controllers
{
	/// <summary>
	/// 羊只转舍API接口(读)
	/// </summary>
	[Route("api/[controller]/[action]")]
	[ApiController]
	public class SheepRescissionReadController : ControllerBase
	{
		private readonly IMediator _mediator;

		/// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="mediator"></param>
		public SheepRescissionReadController(IMediator mediator)
		{
			_mediator = mediator;
		}

		/// <summary>
		/// 羊只转舍的查询分页显示功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpGet]
		public async Task<ApiResult<PagerResult<SheepRescission>>> GetSheepRescissionList([FromQuery] SheepRescissionQueryCommand command)
		{
			return await _mediator.Send(command);
		}

		/// <summary>
		/// 羊只转舍的回显数据功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpGet]
		public async Task<ApiResult<SheepRescission>> GetSheepRescissionInfo([FromQuery] SheepRescissionQueryInfoCommand command)
		{
			return await _mediator.Send(command);
		}

		/// <summary>
		/// 羊只转舍显示查询耳号功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpGet]
		public async Task<List<SheepRescission>> GetSheepResList([FromQuery] SheepRescissionListCommand command)
		{
			return await _mediator.Send(command);
		}

		/// <summary>
		/// 获取转舍类型
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		public IActionResult GetRoundType()
		{
			var list = new List<object>();

			foreach (var item in Enum.GetValues<RoundType>())
			{
				list.Add(new
				{
					value = item,
					name = item.ToString()
				});
			}

			return Ok(list);
		}
	}
}

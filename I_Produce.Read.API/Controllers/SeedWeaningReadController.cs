﻿using CommonClass;
using I_Produce.Domains.DTO;
using I_Produce.Domains.Entity;
using I_Produce.Read.API.Application.Commands.SeedWeaningCommand;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Produce.Read.API.Controllers
{
	/// <summary>
	/// 种母断奶API接口(读)
	/// </summary>
	[Route("api/[controller]/[action]")]
	[ApiController]
	public class SeedWeaningReadController : ControllerBase
	{
		private readonly IMediator _mediator;

		/// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="mediator"></param>
		public SeedWeaningReadController(IMediator mediator)
		{
			_mediator = mediator;
		}

		/// <summary>
		/// 种母断奶的查询分页显示功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpGet]
		public async Task<ApiResult<PagerResult<SeedWeaning>>> GetSeedWeaningList([FromQuery] SeedWeaningQueryCommand command)
		{
			return await _mediator.Send(command);
		}

		/// <summary>
		/// 种母断奶的回显数据功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpGet]
		public async Task<ApiResult<SeedWeaning>> GetSeedWeaningInfo([FromQuery] SeedWeaningQueryInfoCommand command)
		{
			return await _mediator.Send(command);
		}
	}
}

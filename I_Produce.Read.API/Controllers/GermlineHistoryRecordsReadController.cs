﻿using CommonClass;
using I_Produce.Domains.DTO;
using I_Produce.Domains.Entity;
using I_Produce.Read.API.Application.Commands.GermlineHistoryRecordsCommand;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace I_Produce.Read.API.Controllers
{
    /// <summary>
    /// 种母历史记录API接口(读)
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class GermlineHistoryRecordsReadController : ControllerBase
    {

        private readonly IMediator _mediator;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="mediator"></param>
        public GermlineHistoryRecordsReadController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// 种母历史记录的查询分页显示功能
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult<PagerResult<GermlineHistoryRecords>>> GetGermlineHistoryRecordsList([FromQuery] GermlineHistoryRecordsQueryCommand command)
        {
            return await _mediator.Send(command);
        }
        
        /// <summary>
        /// 种母历史记录的显示查询功能
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<GermlineHistoryRecords>> GetGermlineHistoryList([FromQuery] GermlineHistoryRecordsListCommand command)
        {
            return await _mediator.Send(command);
        }

        /// <summary>
        /// 种母历史记录的回显数据功能
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ApiResult<GermlineHistoryRecords>> GetGermlineHistoryRecordsInfo([FromQuery] GermlineHistoryRecordsQueryInfoCommand command)
        {
             return await _mediator.Send(command);
        }
    }
}

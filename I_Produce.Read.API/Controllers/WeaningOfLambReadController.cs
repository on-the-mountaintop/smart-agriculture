﻿using CommonClass;
using I_Produce.Domains.DTO;
using I_Produce.Domains.Entity;
using I_Produce.Read.API.Application.Commands.WeaningOfLambCommand;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Produce.Read.API.Controllers
{
	/// <summary>
	/// 羔羊断奶API接口(读)
	/// </summary>
	[Route("api/[controller]/[action]")]
	[ApiController]
	public class WeaningOfLambReadController : ControllerBase
	{
		private readonly IMediator _mediator;

		/// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="mediator"></param>
		public WeaningOfLambReadController(IMediator mediator)
		{
			_mediator = mediator;
		}

		/// <summary>
		/// 羔羊断奶的查询分页显示功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpGet]
		public async Task<ApiResult<PagerResult<WeaningOfLamb>>> GetWeaningOfLambList([FromQuery] WeaningOfLambQueryCommand command)
		{
			return await _mediator.Send(command);
		}

		/// <summary>
		/// 羔羊断奶的回显数据功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpGet]
		public async Task<ApiResult<WeaningOfLamb>> GetWeaningOfLambInfo([FromQuery] WeaningOfLambQueryInfoCommand command)
		{
			return await _mediator.Send(command);
		}
	}
}

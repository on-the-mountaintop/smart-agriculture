﻿using CommonClass;
using I_Produce.Domains.DTO;
using I_Produce.Domains.Entity;
using I_Produce.Read.API.Application.Commands.ProgenyTransferCommand;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Produce.Read.API.Controllers
{
	/// <summary>
	/// 后裔转种API接口(读)
	/// </summary>
	[Route("api/[controller]/[action]")]
	[ApiController]
	public class ProgenyTransferReadController : ControllerBase
	{
		private readonly IMediator _mediator;

		/// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="mediator"></param>
		public ProgenyTransferReadController(IMediator mediator)
		{
			_mediator = mediator;
		}

		/// <summary>
		/// 后裔转种的查询分页显示功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpGet]
		public async Task<ApiResult<PagerResult<ProgenyTransfer>>> GetProgenyTransferList([FromQuery] ProgenyTransferQueryCommand command)
		{
			return await _mediator.Send(command);
		}

		/// <summary>
		/// 后裔转种的回显数据功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpGet]
		public async Task<ApiResult<ProgenyTransfer>> GetProgenyTransferInfo([FromQuery] ProgenyTransferQueryInfoCommand command)
		{
			return await _mediator.Send(command);
		}
	}
}

﻿using CommonClass;
using I_Produce.Domains.DTO;
using I_Produce.Domains.Entity;
using I_Produce.Read.API.Application.Commands.SeedBirthCommand;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Produce.Read.API.Controllers
{
	/// <summary>
	/// 种母分娩API接口(读)
	/// </summary>
	[Route("api/[controller]/[action]")]
	[ApiController]
	public class SeedBirthReadController : ControllerBase
	{
		private readonly IMediator _mediator;

		/// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="mediator"></param>
		public SeedBirthReadController(IMediator mediator)
		{
			_mediator = mediator;
		}

		/// <summary>
		/// 种母分娩的查询分页显示功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpGet]
		public async Task<ApiResult<PagerResult<SeedBirth>>> GetSeedBirthList([FromQuery] SeedBirthQueryCommand command)
		{
			return await _mediator.Send(command);
		}

		/// <summary>
		/// 种母分娩的回显数据功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpGet]
		public async Task<ApiResult<SeedBirth>> GetSeedBirthInfo([FromQuery] SeedBirthQueryInfoCommand command)
		{
			return await _mediator.Send(command);
		}

		/// <summary>
		/// 获取分娩难易
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		public IActionResult GetDifficultyOfDelivery()
		{
			var list = new List<object>();

			foreach (var item in Enum.GetValues<DifficultyOfDelivery>())
			{
				list.Add(new
				{
					value = item,
					name = item.ToString()
				});
			}

			return Ok(list);
		}

		/// <summary>
		/// 获取肥羊批次
		/// </summary>
		/// <returns></returns>
		[HttpGet]
		public IActionResult GetFatSheepLot()
		{
			var list = new List<object>();

			foreach (var item in Enum.GetValues<FatSheepLot>())
			{
				list.Add(new
				{
					value = item,
					name = item.ToString()
				});
			}

			return Ok(list);
		}
	}
}

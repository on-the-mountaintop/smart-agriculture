﻿using I_Produce.Read.API.Application.Commands.ProduceCountCommand;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Produce.Read.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ProduceCountController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ProduceCountController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// 种母存栏记录
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public Task<List<int>> GetBreedingHenStock([FromQuery] BreedingHenStockCommand command)
        {
            return _mediator.Send(command);
        }
    }
}

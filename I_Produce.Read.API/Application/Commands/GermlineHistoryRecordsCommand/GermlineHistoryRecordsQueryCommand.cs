﻿using CommonClass;
using I_Produce.Domains.DTO;
using I_Produce.Domains.Entity;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace I_Produce.Read.API.Application.Commands.GermlineHistoryRecordsCommand
{
    public class GermlineHistoryRecordsQueryCommand : IRequest<ApiResult<PagerResult<GermlineHistoryRecords>>>
    {
        /// <summary>
        /// 开始配种日期
        /// </summary>
        public string? sDateOfBreeding { get; set; }

        /// <summary>
        /// 结束配种日期
        /// </summary>
        public string? eDateOfBreeding { get; set; }

        /// <summary>
        /// 种母耳号
        /// </summary>
        public string? motherEarHorn { get; set; }

        /// <summary>
        /// 页码
        /// </summary>
        public int pageSize { get; set; }

        /// <summary>
        /// 页序号
        /// </summary>
        public int pageIndex { get; set; }
    }
}

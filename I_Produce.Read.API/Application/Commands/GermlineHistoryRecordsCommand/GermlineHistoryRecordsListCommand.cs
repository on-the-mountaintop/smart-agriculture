﻿using I_Produce.Domains.Entity;
using MediatR;

namespace I_Produce.Read.API.Application.Commands.GermlineHistoryRecordsCommand
{
	public class GermlineHistoryRecordsListCommand : IRequest<List<GermlineHistoryRecords>>
	{
		public string? motherEarHorn { get; set; }
	}
}

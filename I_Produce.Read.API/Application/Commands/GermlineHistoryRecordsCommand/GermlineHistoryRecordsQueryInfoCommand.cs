﻿using CommonClass;
using I_Produce.Domains.Entity;
using MediatR;

namespace I_Produce.Read.API.Application.Commands.GermlineHistoryRecordsCommand
{
	public class GermlineHistoryRecordsQueryInfoCommand : IRequest<ApiResult<GermlineHistoryRecords>>
    {
        public int id { get; set; }
    }
}

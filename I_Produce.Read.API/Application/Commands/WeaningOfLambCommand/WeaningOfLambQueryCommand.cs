﻿using CommonClass;
using I_Produce.Domains.DTO;
using I_Produce.Domains.Entity;
using MediatR;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace I_Produce.Read.API.Application.Commands.WeaningOfLambCommand
{
	public class WeaningOfLambQueryCommand : IRequest<ApiResult<PagerResult<WeaningOfLamb>>>
	{
		/// <summary>
		/// 断奶日期
		/// </summary>
		public string? sWeaningDate { get; set; }

		/// <summary>
		/// 断奶日期
		/// </summary>
		public string? eWeaningDate { get; set; }

		/// <summary>
		/// 羔羊耳号
		/// </summary>
		public string? lambsEar { get; set; }

		/// <summary>
		/// 断奶日龄
		/// </summary>
		public string? sWeaningAge { get; set; }

		/// <summary>
		/// 断奶日龄
		/// </summary>
		public string? eWeaningAge { get; set; }

		/// <summary>
		/// 断奶重
		/// </summary>
		public string? sWeaningWeight { get; set; }		
		
		/// <summary>
		/// 断奶重
		/// </summary>
		public string? eWeaningWeight { get; set; }

		/// <summary>
		/// 哺乳种母耳号
		/// </summary>
		public string? lamTheMotheLactatingSpeciesbsEar { get; set; }

		/// <summary>
		/// 页码
		/// </summary>
		public int pageSize { get; set; }

		/// <summary>
		/// 页序号
		/// </summary>
		public int pageIndex { get; set; }
	}
}

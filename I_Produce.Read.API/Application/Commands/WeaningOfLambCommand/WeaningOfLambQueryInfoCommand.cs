﻿using CommonClass;
using I_Produce.Domains.Entity;
using MediatR;

namespace I_Produce.Read.API.Application.Commands.WeaningOfLambCommand
{
	public class WeaningOfLambQueryInfoCommand : IRequest<ApiResult<WeaningOfLamb>>
	{
		public int id { get; set; }
	}
}

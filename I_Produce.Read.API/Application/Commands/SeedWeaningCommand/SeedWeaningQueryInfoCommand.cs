﻿using CommonClass;
using I_Produce.Domains.Entity;
using MediatR;

namespace I_Produce.Read.API.Application.Commands.SeedWeaningCommand
{
	public class SeedWeaningQueryInfoCommand : IRequest<ApiResult<SeedWeaning>>
	{
        public int id { get; set; }
    }
}

﻿using CommonClass;
using I_Produce.Domains.DTO;
using I_Produce.Domains.Entity;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace I_Produce.Read.API.Application.Commands.SeedWeaningCommand
{
	public class SeedWeaningQueryCommand : IRequest<ApiResult<PagerResult<SeedWeaning>>>
	{

		/// <summary>
		/// 断奶日期
		/// </summary>
		public string? sWeaningDate { get; set; }

		/// <summary>
		/// 断奶日期
		/// </summary>
		public string? eWeaningDate { get; set; }

		/// <summary>
		/// 耳号
		/// </summary>
		public string? auricle { get; set; }

		/// <summary>
		/// 转入栋舍
		/// </summary>
		public string? transferBuilding { get; set; }

		/// <summary>
		/// 负责人
		/// </summary>
		public string? personCharge { get; set; }

		/// <summary>
		/// 页码
		/// </summary>
		public int pageSize { get; set; }

		/// <summary>
		/// 页序号
		/// </summary>
		public int pageIndex { get; set; }
	}
}

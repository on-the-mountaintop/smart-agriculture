﻿using CommonClass;
using I_Produce.Domains.DTO;
using I_Produce.Domains.Entity;
using MediatR;

namespace I_Produce.Read.API.Application.Commands.SheepRescissionCommand
{
	public class SheepRescissionQueryCommand : IRequest<ApiResult<PagerResult<SheepRescission>>>
    {
        /// <summary>
        /// 开始转舍日期
        /// </summary>
        public string? sSurrenderDate { get; set; }

        /// <summary>
        /// 结束转舍日期
        /// </summary>
        public string? eSurrenderDate { get; set; }

        /// <summary>
        /// 羊只耳号
        /// </summary>
        public string? visualEarSignal { get; set; }

        /// <summary>
        /// 转舍类型
        /// </summary>
        public string? roundType { get; set; }

        /// <summary>
        /// 转出栋舍
        /// </summary>
        public string? turnBuilding { get; set; }

        /// <summary>
        /// 转出栏位
        /// </summary>
        public string? rolloutField { get; set; }

        /// <summary>
        /// 转入栋舍
        /// </summary>
        public string? transferBuilding { get; set; }

        /// <summary>
        /// 转入栏位
        /// </summary>
        public string? driveField { get; set; }

        /// <summary>
        /// 页码
        /// </summary>
        public int pageSize { get; set; }

        /// <summary>
        /// 页序号
        /// </summary>
        public int pageIndex { get; set; }
    }
}

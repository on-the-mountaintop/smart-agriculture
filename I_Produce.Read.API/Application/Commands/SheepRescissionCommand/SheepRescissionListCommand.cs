﻿using I_Produce.Domains.Entity;
using MediatR;

namespace I_Produce.Read.API.Application.Commands.SheepRescissionCommand
{
	public class SheepRescissionListCommand : IRequest<List<SheepRescission>>
	{
        public string? visualEarSignal { get; set; }
    }
}

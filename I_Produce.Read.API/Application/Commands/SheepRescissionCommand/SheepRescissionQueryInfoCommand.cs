﻿using CommonClass;
using I_Produce.Domains.Entity;
using MediatR;

namespace I_Produce.Read.API.Application.Commands.SheepRescissionCommand
{
    public class SheepRescissionQueryInfoCommand : IRequest<ApiResult<SheepRescission>>
    {
        public int id { get; set; }
    }
}

﻿using CommonClass;
using I_Produce.Domains.Entity;
using MediatR;

namespace I_Produce.Read.API.Application.Commands.ProgenyTransferCommand
{
	public class ProgenyTransferQueryInfoCommand : IRequest<ApiResult<ProgenyTransfer>>
	{
		public int id { get; set; }
	}
}

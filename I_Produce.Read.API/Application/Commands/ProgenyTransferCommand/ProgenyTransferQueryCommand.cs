﻿using CommonClass;
using I_Produce.Domains.DTO;
using I_Produce.Domains.Entity;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace I_Produce.Read.API.Application.Commands.ProgenyTransferCommand
{
	public class ProgenyTransferQueryCommand : IRequest<ApiResult<PagerResult<ProgenyTransfer>>>
	{
		/// <summary>
		/// 转种日期
		/// </summary>
		public string? sDateConversion { get; set; }

		/// <summary>
		/// 转种日期
		/// </summary>
		public string? eDateConversion { get; set; }

		/// <summary>
		/// 育成羊耳号
		/// </summary>
		public string? growSheepEars { get; set; }

		/// <summary>
		/// 页码
		/// </summary>
		public int pageSize { get; set; }

		/// <summary>
		/// 页序号
		/// </summary>
		public int pageIndex { get; set; }
	}
}

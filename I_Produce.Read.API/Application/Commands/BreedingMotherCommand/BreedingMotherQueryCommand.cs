﻿using CommonClass;
using I_Produce.Domains.DTO;
using I_Produce.Domains.Entity;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace I_Produce.Read.API.Application.Commands.BreedingMotherCommand
{
	public class BreedingMotherQueryCommand : IRequest<ApiResult<PagerResult<BreedingMother>>>
	{
		/// <summary>
		/// 配种日期
		/// </summary>
		public string? sDateOfBreeding { get; set; }

		/// <summary>
		/// 配种日期
		/// </summary>
		public string? eDateOfBreeding { get; set; }

		/// <summary>
		/// 耳号
		/// </summary>
		public string? auricle { get; set; }

		/// <summary>
		/// 负责人
		/// </summary>
		public string? personCharge { get; set; }

		/// <summary>
		/// 页码
		/// </summary>
		public int pageSize { get; set; }

		/// <summary>
		/// 页序号
		/// </summary>
		public int pageIndex { get; set; }
	}
}

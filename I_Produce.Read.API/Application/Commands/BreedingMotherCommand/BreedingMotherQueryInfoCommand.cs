﻿using CommonClass;
using I_Produce.Domains.Entity;
using MediatR;

namespace I_Produce.Read.API.Application.Commands.BreedingMotherCommand
{
	public class BreedingMotherQueryInfoCommand : IRequest<ApiResult<BreedingMother>>
	{
        public int id { get; set; }
    }
}

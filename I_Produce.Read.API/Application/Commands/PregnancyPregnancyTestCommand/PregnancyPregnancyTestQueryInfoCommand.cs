﻿using CommonClass;
using I_Produce.Domains.DTO;
using I_Produce.Domains.Entity;
using MediatR;

namespace I_Produce.Read.API.Application.Commands.PregnancyPregnancyTestCommand
{
	public class PregnancyPregnancyTestQueryInfoCommand : IRequest<ApiResult<PregnancyPregnancyTest>>
	{
        public int id { get; set; }
    }
}

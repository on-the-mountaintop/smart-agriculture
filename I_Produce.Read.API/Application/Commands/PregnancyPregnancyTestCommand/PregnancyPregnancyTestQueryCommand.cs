﻿using CommonClass;
using I_Produce.Domains.DTO;
using I_Produce.Domains.Entity;
using MediatR;

namespace I_Produce.Read.API.Application.Commands.PregnancyPregnancyTestCommand
{
	public class PregnancyPregnancyTestQueryCommand : IRequest<ApiResult<PagerResult<PregnancyPregnancyTest>>>
	{
		/// <summary>
		/// 妊检日期
		/// </summary>
		public string? sPregnancyTestDate { get; set; }

		/// <summary>
		/// 妊检日期
		/// </summary>
		public string? ePregnancyTestDate { get; set; }

		/// <summary>
		/// 耳号
		/// </summary>
		public string? auricle { get; set; }

		/// <summary>
		/// 妊检结果
		/// </summary>
		public string? pregnancyTestResult { get; set; }

		/// <summary>
		/// 同胎数
		/// </summary>
		public string? parityNumber { get; set; }

		/// <summary>
		/// 负责人
		/// </summary>
		public string? personCharge { get; set; }

		/// <summary>
		/// 页码
		/// </summary>
		public int pageSize { get; set; }

		/// <summary>
		/// 页序号
		/// </summary>
		public int pageIndex { get; set; }
	}
}

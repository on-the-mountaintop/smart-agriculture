﻿using CommonClass;
using I_Produce.Domains.DTO;
using I_Produce.Domains.Entity;
using MediatR;

namespace I_Produce.Read.API.Application.Commands.SeedBirthCommand
{
	public class SeedBirthQueryCommand : IRequest<ApiResult<PagerResult<SeedBirth>>>
	{
		/// <summary>
		/// 分娩日期
		/// </summary>
		public string? sDeliveryDate { get; set; }

		/// <summary>
		/// 分娩日期
		/// </summary>
		public string? eDeliveryDate { get; set; }

		/// <summary>
		/// 种母耳号
		/// </summary>
		public string? motherEarHorn { get; set; }

		/// <summary>
		/// 当前栋舍
		/// </summary>
		public string? currentBuilding { get; set; }

		/// <summary>
		/// 转入栋舍
		/// </summary>
		public string? transferBuilding { get; set; }

		/// <summary>
		/// 窝号
		/// </summary>
		public string? broodNumber { get; set; }

		/// <summary>
		/// 负责人
		/// </summary>
		public string? personCharge { get; set; }

		/// <summary>
		/// 页码
		/// </summary>
		public int pageSize { get; set; }

		/// <summary>
		/// 页序号
		/// </summary>
		public int pageIndex { get; set; }
	}
}

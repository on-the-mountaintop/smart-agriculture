﻿using CommonClass;
using I_Produce.Domains.Entity;
using MediatR;

namespace I_Produce.Read.API.Application.Commands.SeedBirthCommand
{
	public class SeedBirthQueryInfoCommand : IRequest<ApiResult<SeedBirth>>
	{
        public int id { get; set; }
    }
}

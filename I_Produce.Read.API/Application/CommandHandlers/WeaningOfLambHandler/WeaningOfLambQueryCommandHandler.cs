﻿using CommonClass;
using I_Produce.Domains.DTO;
using I_Produce.Domains.Entity;
using I_Produce.Infrastructure.Interface;
using I_Produce.Read.API.Application.Commands.WeaningOfLambCommand;
using MediatR;
using Newtonsoft.Json;

namespace I_Produce.Read.API.Application.CommandHandlers.WeaningOfLambHandler
{
	public class WeaningOfLambQueryCommandHandler : IRequestHandler<WeaningOfLambQueryCommand, ApiResult<PagerResult<WeaningOfLamb>>>
	{
		private readonly IWeaningOfLambRepository _repository;
		private readonly Cross_services_Helper _httpClient;

		public WeaningOfLambQueryCommandHandler(IWeaningOfLambRepository repository, Cross_services_Helper httpClient)
		{
			_repository = repository;
			_httpClient = httpClient;
		}

		public async Task<ApiResult<PagerResult<WeaningOfLamb>>> Handle(WeaningOfLambQueryCommand request, CancellationToken cancellationToken)
		{
			var list = _repository.All().Result.Where(x => !x.IsDel);

			if (!string.IsNullOrEmpty(request.sWeaningDate))
			{
				list = list.Where(x => x.WeaningDate >= DateTime.Parse(request.sWeaningDate));
			}

			if (!string.IsNullOrEmpty(request.eWeaningDate))
			{
				list = list.Where(x => x.WeaningDate < DateTime.Parse(request.eWeaningDate).AddDays(1));
			}

			if (!string.IsNullOrEmpty(request.lambsEar))
			{
				list = list.Where(x => x.LambsEar.Contains(request.lambsEar));
			}

			if (!string.IsNullOrEmpty(request.sWeaningAge))
			{
				list = list.Where(x => x.WeaningAge >= Convert.ToInt32(request.sWeaningAge));
			}

			if (!string.IsNullOrEmpty(request.eWeaningAge))
			{
				list = list.Where(x => x.WeaningAge <= Convert.ToInt32(request.eWeaningAge));
			}

			if (!string.IsNullOrEmpty(request.sWeaningWeight))
			{
				list = list.Where(x => x.WeaningWeight.Equals(Convert.ToDecimal(request.sWeaningWeight)));
			}

			if (!string.IsNullOrEmpty(request.eWeaningWeight))
			{
				list = list.Where(x => x.WeaningWeight.Equals(Convert.ToDecimal(request.eWeaningWeight)));
			}

			if (!string.IsNullOrEmpty(request.lamTheMotheLactatingSpeciesbsEar))
			{
				list = list.Where(x => x.LamTheMotheLactatingSpeciesbsEar.Contains(request.lamTheMotheLactatingSpeciesbsEar));
			}

			var total = list.Count();
			var count = (int)Math.Ceiling(total * 1.0 / request.pageSize);

			list = list.OrderByDescending(x => x.Id).Skip(request.pageSize * (request.pageIndex - 1)).Take(request.pageSize);

			foreach (var item in list)
			{
				var lambsEar = await _httpClient.Get("http://10.31.56.22:8000/ArchivesGetRout/api/Archives/ArchivesGetByEarNumberInfo?EarNumber=" + item.LambsEar);
				var SheepBir = JsonConvert.DeserializeObject<WeaningDateDTO>(lambsEar).SheepBir;

				TimeSpan timeSpan = Convert.ToDateTime(item.WeaningDate) - Convert.ToDateTime(SheepBir);

				item.WeaningAge = timeSpan.Days;
			}

			return new ApiResult<PagerResult<WeaningOfLamb>>
			{
				Code = 200,
				msg = "成功",
				Data = new PagerResult<WeaningOfLamb>
				{
					TotalCount = total,
					PageCount = count,
					Datas = list.ToList()
				}
			};
		}
	}
}

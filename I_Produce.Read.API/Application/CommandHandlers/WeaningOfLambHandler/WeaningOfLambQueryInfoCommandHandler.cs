﻿using CommonClass;
using I_Produce.Domains.Entity;
using I_Produce.Infrastructure.Interface;
using I_Produce.Read.API.Application.Commands.WeaningOfLambCommand;
using MediatR;

namespace I_Produce.Read.API.Application.CommandHandlers.WeaningOfLambHandler
{
	public class WeaningOfLambQueryInfoCommandHandler : IRequestHandler<WeaningOfLambQueryInfoCommand, ApiResult<WeaningOfLamb>>
	{
		private readonly IWeaningOfLambRepository _repository;

		public WeaningOfLambQueryInfoCommandHandler(IWeaningOfLambRepository repository)
		{
			_repository = repository;
		}

		public async Task<ApiResult<WeaningOfLamb>> Handle(WeaningOfLambQueryInfoCommand request, CancellationToken cancellationToken)
		{
			var result = await _repository.GetByIdAsync(request.id);

			return new ApiResult<WeaningOfLamb>
			{
				Code = 200,
				msg = "成功",
				Data = result
			};
		}
	}
}

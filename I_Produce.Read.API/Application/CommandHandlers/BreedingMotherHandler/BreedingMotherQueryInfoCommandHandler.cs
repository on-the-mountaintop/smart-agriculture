﻿using CommonClass;
using I_Produce.Domains.Entity;
using I_Produce.Infrastructure.Impl;
using I_Produce.Infrastructure.Interface;
using I_Produce.Read.API.Application.Commands.BreedingMotherCommand;
using MediatR;

namespace I_Produce.Read.API.Application.CommandHandlers.BreedingMotherHandler
{
	public class BreedingMotherQueryInfoCommandHandler : IRequestHandler<BreedingMotherQueryInfoCommand, ApiResult<BreedingMother>>
	{

		private readonly IBreedingMotherRepository _repository;

		public BreedingMotherQueryInfoCommandHandler(IBreedingMotherRepository repository)
		{
			_repository = repository;
		}

		public async Task<ApiResult<BreedingMother>> Handle(BreedingMotherQueryInfoCommand request, CancellationToken cancellationToken)
		{
			var result = await _repository.GetByIdAsync(request.id);

			return new ApiResult<BreedingMother>
			{
				Code = 200,
				msg = "成功",
				Data = result
			};
		}
	}
}

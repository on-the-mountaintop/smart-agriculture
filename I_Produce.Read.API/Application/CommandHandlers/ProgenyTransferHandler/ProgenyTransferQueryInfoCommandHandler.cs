﻿using CommonClass;
using I_Produce.Domains.Entity;
using I_Produce.Infrastructure.Interface;
using I_Produce.Read.API.Application.Commands.ProgenyTransferCommand;
using MediatR;

namespace I_Produce.Read.API.Application.CommandHandlers.ProgenyTransferHandler
{
	public class ProgenyTransferQueryInfoCommandHandler : IRequestHandler<ProgenyTransferQueryInfoCommand, ApiResult<ProgenyTransfer>>
	{
		private readonly IProgenyTransferRepository _repository;

		public ProgenyTransferQueryInfoCommandHandler(IProgenyTransferRepository repository)
		{
			_repository = repository;
		}

		public async Task<ApiResult<ProgenyTransfer>> Handle(ProgenyTransferQueryInfoCommand request, CancellationToken cancellationToken)
		{
			var result = await _repository.GetByIdAsync(request.id);

			return new ApiResult<ProgenyTransfer>
			{
				Code = 200,
				msg = "成功",
				Data = result
			};
		}
	}
}

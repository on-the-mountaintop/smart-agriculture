﻿using CommonClass;
using I_Produce.Domains.Entity;
using I_Produce.Infrastructure.Interface;
using I_Produce.Read.API.Application.Commands.SeedWeaningCommand;
using MediatR;

namespace I_Produce.Read.API.Application.CommandHandlers.SeedWeaningHandler
{
	public class SeedWeaningQueryInfoCommandHandler : IRequestHandler<SeedWeaningQueryInfoCommand, ApiResult<SeedWeaning>>
	{
		private readonly ISeedWeaningRepository _repository;

		public SeedWeaningQueryInfoCommandHandler(ISeedWeaningRepository repository)
		{
			_repository = repository;
		}

		public async Task<ApiResult<SeedWeaning>> Handle(SeedWeaningQueryInfoCommand request, CancellationToken cancellationToken)
		{
			var result = await _repository.GetByIdAsync(request.id);

			return new ApiResult<SeedWeaning>
			{
				Code = 200,
				msg = "成功",
				Data = result
			};
		}
	}
}

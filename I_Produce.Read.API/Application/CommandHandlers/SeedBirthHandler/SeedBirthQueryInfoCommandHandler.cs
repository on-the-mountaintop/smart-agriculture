﻿using CommonClass;
using I_Produce.Domains.Entity;
using I_Produce.Infrastructure.Interface;
using I_Produce.Read.API.Application.Commands.SeedBirthCommand;
using MediatR;

namespace I_Produce.Read.API.Application.CommandHandlers.SeedBirthHandler
{
	public class SeedBirthQueryInfoCommandHandler : IRequestHandler<SeedBirthQueryInfoCommand, ApiResult<SeedBirth>>
	{
		private readonly ISeedBirthRepository _repository;

		public SeedBirthQueryInfoCommandHandler(ISeedBirthRepository seedBirthRepository)
		{
			_repository = seedBirthRepository;
		}

		public async Task<ApiResult<SeedBirth>> Handle(SeedBirthQueryInfoCommand request, CancellationToken cancellationToken)
		{
			var result = await _repository.GetByIdAsync(request.id);

			return new ApiResult<SeedBirth>
			{
				Code = 200,
				msg = "成功",
				Data = result
			};
		}
	}
}

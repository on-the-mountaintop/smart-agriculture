﻿using CommonClass;
using I_Produce.Domains.DTO;
using I_Produce.Infrastructure.Interface;
using I_Produce.Read.API.Application.Commands.ProduceCountCommand;
using MediatR;
using Newtonsoft.Json;

namespace I_Produce.Read.API.Application.CommandHandlers.ProduceCountCommandHandler
{
    public class BreedingHenStockCommandHandler : IRequestHandler<BreedingHenStockCommand, List<int>>
    {
        // 种母配种
        private readonly IBreedingMotherRepository _breedingMotherRepository;
        // 种母历史记录
        private readonly IGermlineHistoryRecordsRepository _germlineHistoryRecordsRepository;
        // 种母妊检
        private readonly IPregnancyPregnancyTestRepository _pregnancyPregnancyTestRepository;
        // 后裔转种
        private readonly IProgenyTransferRepository _progenyTransferRepository;
        // 种母分娩
        private readonly ISeedBirthRepository _seedBirthRepository;
        // 种母断奶
        private readonly ISeedWeaningRepository _seedWeaningRepository;
        // 羊只转舍
        private readonly ISheepRescissionRepository _sheepRescissionRepository;
        // 羊羔断奶
        private readonly IWeaningOfLambRepository _weaningOfLambRepository;
        private readonly Cross_services_Helper _cross_servicesHelper;
        public BreedingHenStockCommandHandler(
            IBreedingMotherRepository breedingMotherRepository,
            IGermlineHistoryRecordsRepository germlineHistoryRecordsRepository,
            IPregnancyPregnancyTestRepository pregnancyPregnancyTestRepository,
            IProgenyTransferRepository progenyTransferRepository,
            ISeedBirthRepository seedBirthRepository,
            ISeedWeaningRepository seedWeaningRepository,
            ISheepRescissionRepository sheepRescissionRepository,
            IWeaningOfLambRepository weaningOfLambRepository,
            Cross_services_Helper cross_servicesHelper)
        {
            _breedingMotherRepository = breedingMotherRepository;
            _germlineHistoryRecordsRepository = germlineHistoryRecordsRepository;
            _pregnancyPregnancyTestRepository = pregnancyPregnancyTestRepository;
            _progenyTransferRepository = progenyTransferRepository;
            _seedBirthRepository = seedBirthRepository;
            _seedWeaningRepository = seedWeaningRepository;
            _sheepRescissionRepository = sheepRescissionRepository;
            _weaningOfLambRepository = weaningOfLambRepository;
            _cross_servicesHelper = cross_servicesHelper;
        }

        public async Task<List<int>> Handle(BreedingHenStockCommand request, CancellationToken cancellationToken)
        {
           var archivesAllInfoJson= await  _cross_servicesHelper.Get("http://10.31.56.22:8000/ArchivesGetRout/api/Archives/ArchivesAllInfo");
            var archivesAllInfo = JsonConvert.DeserializeObject<List<ArchivesInfoDTO>>(archivesAllInfoJson);
            // 档案种母信息
            var motherEarNumbers = archivesAllInfo.Where(x => !x.IsDelete && x.SheepType == "种母").Select(x => x.EarNnumber);
            // 获取种母配种数据
            var breedingMothers = await _breedingMotherRepository.GetAllAsync();
            var breedingAuricles = breedingMothers.Select(x => x.Auricle);

            // 获取种母妊检数据
            var pregnancyPregnancyTests = await _pregnancyPregnancyTestRepository.GetAllAsync();
            var pregnancyAuricles = pregnancyPregnancyTests.Select(x => x.Auricle);

            // 获取种母分娩数据
            var seedBirths = await _seedBirthRepository.GetAllAsync();
            var birthAuricles = seedBirths.Select(x => x.MotherEarHorn);

            // 获取种母断奶数据
            var seedWeanings = await _seedWeaningRepository.GetAllAsync();
            var weaningAuricles = seedWeanings.Select(x => x.Auricle);
            List<int> list = new List<int>();
            // 将所有数据合并到一个集合中
            var allAuricles = motherEarNumbers.Concat(breedingAuricles).Concat(pregnancyAuricles)
                                              .Concat(birthAuricles).Concat(weaningAuricles);

            // 查询不重复的元素数量
            var distinctCount = allAuricles.Distinct().Count();
            list.Add(distinctCount);
            return list;
        }
    }
}

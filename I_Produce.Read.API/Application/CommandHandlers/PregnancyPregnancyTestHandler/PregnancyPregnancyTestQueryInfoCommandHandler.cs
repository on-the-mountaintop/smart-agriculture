﻿using CommonClass;
using I_Produce.Domains.Entity;
using I_Produce.Infrastructure.Interface;
using I_Produce.Read.API.Application.Commands.PregnancyPregnancyTestCommand;
using MediatR;

namespace I_Produce.Read.API.Application.CommandHandlers.PregnancyPregnancyTestHandler
{
	public class PregnancyPregnancyTestQueryInfoCommandHandler : IRequestHandler<PregnancyPregnancyTestQueryInfoCommand, ApiResult<PregnancyPregnancyTest>>
	{
		private readonly IPregnancyPregnancyTestRepository _repository;

		public PregnancyPregnancyTestQueryInfoCommandHandler(IPregnancyPregnancyTestRepository repository)
		{
			_repository = repository;
		}

		public async Task<ApiResult<PregnancyPregnancyTest>> Handle(PregnancyPregnancyTestQueryInfoCommand request, CancellationToken cancellationToken)
		{
			var result = await _repository.GetByIdAsync(request.id);

			return new ApiResult<PregnancyPregnancyTest>
			{
				Code = 200,
				msg = "成功",
				Data = result
			};
		}
	}
}

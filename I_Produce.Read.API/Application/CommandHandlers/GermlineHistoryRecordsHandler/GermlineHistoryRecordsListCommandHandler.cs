﻿using I_Produce.Domains.Entity;
using I_Produce.Infrastructure.Interface;
using I_Produce.Read.API.Application.Commands.GermlineHistoryRecordsCommand;
using MediatR;

namespace I_Produce.Read.API.Application.CommandHandlers.GermlineHistoryRecordsHandler
{
	public class GermlineHistoryRecordsListCommandHandler : IRequestHandler<GermlineHistoryRecordsListCommand, List<GermlineHistoryRecords>>
	{

		private readonly IGermlineHistoryRecordsRepository _germlineHistoryRecordsRepository;

		public GermlineHistoryRecordsListCommandHandler(IGermlineHistoryRecordsRepository germlineHistoryRecordsRepository)
		{
			_germlineHistoryRecordsRepository = germlineHistoryRecordsRepository;
		}

		public async Task<List<GermlineHistoryRecords>> Handle(GermlineHistoryRecordsListCommand request, CancellationToken cancellationToken)
		{
			var list = _germlineHistoryRecordsRepository.All().Result.Where(x => !x.IsDel && x.MotherEarHorn == request.motherEarHorn);

            return list.ToList();
		}
	}
}

﻿using CommonClass;
using I_Produce.Domains.Entity;
using I_Produce.Infrastructure.Interface;
using I_Produce.Read.API.Application.Commands.GermlineHistoryRecordsCommand;
using MediatR;

namespace I_Produce.Read.API.Application.CommandHandlers.GermlineHistoryRecordsHandler
{
	public class GermlineHistoryRecordsQueryInfoCommandHandler : IRequestHandler<GermlineHistoryRecordsQueryInfoCommand, ApiResult<GermlineHistoryRecords>>
    {

        private readonly IGermlineHistoryRecordsRepository _germlineHistoryRecordsRepository;

        public GermlineHistoryRecordsQueryInfoCommandHandler(IGermlineHistoryRecordsRepository germlineHistoryRecordsRepository)
        {
            _germlineHistoryRecordsRepository = germlineHistoryRecordsRepository;
        }

        public async Task<ApiResult<GermlineHistoryRecords>> Handle(GermlineHistoryRecordsQueryInfoCommand request, CancellationToken cancellationToken)
        {
            var result = await _germlineHistoryRecordsRepository.GetByIdAsync(request.id);

            return new ApiResult<GermlineHistoryRecords>
            {
                Code = 200,
                msg = "成功",
                Data = result
            };
        }
    }
}

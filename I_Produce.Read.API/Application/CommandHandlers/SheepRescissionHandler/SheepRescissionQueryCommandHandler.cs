﻿using CommonClass;
using I_Produce.Domains.DTO;
using I_Produce.Domains.Entity;
using I_Produce.Infrastructure.Interface;
using I_Produce.Read.API.Application.Commands.SheepRescissionCommand;
using MediatR;

namespace I_Produce.Read.API.Application.CommandHandlers.SheepRescissionHandler
{
	public class SheepRescissionQueryCommandHandler : IRequestHandler<SheepRescissionQueryCommand, ApiResult<PagerResult<SheepRescission>>>
    {
        private readonly ISheepRescissionRepository _repository;

        public SheepRescissionQueryCommandHandler(ISheepRescissionRepository repository)
        {
            _repository = repository;
        }

        public async Task<ApiResult<PagerResult<SheepRescission>>> Handle(SheepRescissionQueryCommand request, CancellationToken cancellationToken)
        {
            var list = _repository.All().Result.Where(x => !x.IsDel);

            if (!string.IsNullOrEmpty(request.sSurrenderDate))
            {
                list = list.Where(x => x.SurrenderDate >= DateTime.Parse(request.sSurrenderDate));
            }

            if (!string.IsNullOrEmpty(request.eSurrenderDate))
            {
                list = list.Where(x => x.SurrenderDate < DateTime.Parse(request.eSurrenderDate).AddDays(1));
            }

            if (!string.IsNullOrEmpty(request.visualEarSignal))
            {
                list = list.Where(x => x.VisualEarSignal.Contains(request.visualEarSignal));
            }

            if (request.roundType != null || !string.IsNullOrEmpty(request.roundType))
            {
                list = list.Where(x => x.RoundType.Equals(request.roundType));
            }

            if (request.transferBuilding != null || !string.IsNullOrEmpty(request.transferBuilding))
            {
                list = list.Where(x => x.TransferBuilding.Equals(request.transferBuilding));
            }

            if (request.turnBuilding != null || !string.IsNullOrEmpty(request.turnBuilding))
            {
                list = list.Where(x => x.TurnBuilding.Equals(request.turnBuilding));
            }

            if (request.rolloutField != null || !string.IsNullOrEmpty(request.rolloutField))
            {
                list = list.Where(x => x.RolloutField.Equals(request.rolloutField));
            }

            if (request.driveField != null || !string.IsNullOrEmpty(request.driveField))
            {
                list = list.Where(x => x.DriveField.Equals(request.driveField));
            }

            var total = list.Count();
            var count = (int)Math.Ceiling(total * 1.0 / request.pageSize);

            list = list.OrderByDescending(x => x.Id).Skip(request.pageSize * (request.pageIndex - 1)).Take(request.pageSize);

            return new ApiResult<PagerResult<SheepRescission>>
            {
                Code = 200,
                msg = "成功",
                Data = new PagerResult<SheepRescission>
                {
                    TotalCount = total,
                    PageCount = count,
                    Datas = list.ToList()
                }
            };
        }
    }
}

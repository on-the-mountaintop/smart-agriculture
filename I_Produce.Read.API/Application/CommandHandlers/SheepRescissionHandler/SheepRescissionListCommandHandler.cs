﻿using I_Produce.Domains.Entity;
using I_Produce.Infrastructure.Interface;
using I_Produce.Read.API.Application.Commands.SheepRescissionCommand;
using MediatR;

namespace I_Produce.Read.API.Application.CommandHandlers.SheepRescissionHandler
{
	public class SheepRescissionListCommandHandler : IRequestHandler<SheepRescissionListCommand, List<SheepRescission>>
	{
		private readonly ISheepRescissionRepository _repository;

		public SheepRescissionListCommandHandler(ISheepRescissionRepository repository)
		{
			_repository = repository;
		}

		public async Task<List<SheepRescission>> Handle(SheepRescissionListCommand request, CancellationToken cancellationToken)
		{
			var list = _repository.All().Result.Where(x => !x.IsDel);

            if (!string.IsNullOrEmpty(request.visualEarSignal))
            {
                list = list.Where(x => x.VisualEarSignal.Equals(request.visualEarSignal));
            }

            return list.ToList();
		}
	}
}

﻿using CommonClass;
using I_Produce.Domains.Entity;
using I_Produce.Infrastructure.Interface;
using I_Produce.Read.API.Application.Commands.SheepRescissionCommand;
using MediatR;

namespace I_Produce.Read.API.Application.CommandHandlers.SheepRescissionHandler
{
    public class SheepRescissionQueryInfoCommandHandler : IRequestHandler<SheepRescissionQueryInfoCommand, ApiResult<SheepRescission>>
    {
        private readonly ISheepRescissionRepository _repository;

        public SheepRescissionQueryInfoCommandHandler(ISheepRescissionRepository repository)
        {
            _repository = repository;
        }

        public async Task<ApiResult<SheepRescission>> Handle(SheepRescissionQueryInfoCommand request, CancellationToken cancellationToken)
        {
            var result = await _repository.GetByIdAsync(request.id);

            return new ApiResult<SheepRescission>
            {
                Code = 200,
                msg = "成功",
                Data = result
            };
        }
    }
}

﻿using Microsoft.AspNetCore.Mvc.Filters;

namespace I_Produce.Read.API.Extensions
{
	/// <summary>
	/// 异常过滤器
	/// </summary>
	public class MyExceptionFilter : IExceptionFilter
	{
		/// <summary>
		/// 日志
		/// </summary>
		ILogger<MyExceptionFilter> logger;

		/// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="logger"></param>
		public MyExceptionFilter(ILogger<MyExceptionFilter> logger)
		{
			this.logger = logger;
		}

		public void OnException(ExceptionContext context)
		{
			//写日志
			logger.LogError("这是全局异常过滤器" + context.Exception.Message);
			//异常解决
			context.ExceptionHandled = true;
		}
	}
}

﻿using CommonClass;
using I_Produce.Infrastructure.Impl;
using I_Produce.Infrastructure.Interface;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace I_Produce.Read.API.Extensions
{
	public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// 注册JWT
        /// </summary>
        /// <param name="services"></param>
        /// <param name="builder"></param>
        public static void AddJWT(this IServiceCollection services, WebApplicationBuilder builder)
        {
            builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    //取出私钥
                    var secretByte = Encoding.UTF8.GetBytes(builder.Configuration["Authentication:SecretKey"]);
                    options.TokenValidationParameters = new TokenValidationParameters()
                    {
                        //验证发布者
                        ValidateIssuer = true,
                        ValidIssuer = builder.Configuration["Authentication:Issuer"],
                        //验证接收者
                        ValidateAudience = true,
                        ValidAudience = builder.Configuration["Authentication:Audience"],
                        //验证是否过期
                        ValidateLifetime = true,
                        //验证秘钥
                        IssuerSigningKey = new SymmetricSecurityKey(secretByte)
                    };
                });
        }

        /// <summary>
        /// 注册服务
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddScoped<IGermlineHistoryRecordsRepository, GermlineHistoryRecordsRepository>();
            services.AddScoped<ISheepRescissionRepository, SheepRescissionRepository>();
			services.AddScoped<IBreedingMotherRepository, BreedingMotherRepository>();
			services.AddScoped<IPregnancyPregnancyTestRepository, PregnancyPregnancyTestRepository>();
			services.AddScoped<ISeedBirthRepository, SeedBirthRepository>();
			services.AddScoped<ISeedWeaningRepository, SeedWeaningRepository>();
			services.AddScoped<IWeaningOfLambRepository, WeaningOfLambRepository>();
			services.AddScoped<IProgenyTransferRepository, ProgenyTransferRepository>();
			services.AddScoped<Cross_services_Helper>();

			return services;
        }
    }
}

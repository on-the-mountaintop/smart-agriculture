﻿using I_Ear_Tag.Demoin.EarTagManagement;
using I_Ear_Tag.Infrastructure.Interface;
using I_Ear_Tag.Read.Api.Application.Command.EarTagCommand;
using MediatR;

namespace I_Ear_Tag.Read.Api.Application.CommandHandlers.EarTagHandler
{
    public class EarTagBackfillCommandHandler : IRequestHandler<EarTagBackfillCommand, EarTag>
    {
        private readonly IEarTagRepository earTagRepository;
        public EarTagBackfillCommandHandler(IEarTagRepository earTagRepository)
        {
            this.earTagRepository = earTagRepository;
        }
        /// <summary>
        /// 反填产羔耳标
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<EarTag> Handle(EarTagBackfillCommand request, CancellationToken cancellationToken)
        {
            return await earTagRepository.GetModelAsync(request.EarTagId);
        }
    }
}

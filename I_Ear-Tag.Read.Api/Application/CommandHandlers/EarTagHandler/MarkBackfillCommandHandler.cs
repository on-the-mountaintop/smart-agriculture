﻿using I_Ear_Tag.Demoin.EarTagManagement;
using I_Ear_Tag.Infrastructure.Interface;
using I_Ear_Tag.Read.Api.Application.Command.EarTagCommand;
using MediatR;

namespace I_Ear_Tag.Read.Api.Application.CommandHandlers.EarTagHandler
{
    public class MarkBackfillCommandHandler : IRequestHandler<MarkBackfillCommand, Mark>
    {
        private readonly IMarkRepository markRepository;
        public MarkBackfillCommandHandler(IMarkRepository markRepository)
        {
            this.markRepository = markRepository;
        }
        /// <summary>
        /// 反填羊只戴标信息
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Mark> Handle(MarkBackfillCommand request, CancellationToken cancellationToken)
        {
            return await markRepository.GetModelAsync(request.MarkId);
        }
    }
}

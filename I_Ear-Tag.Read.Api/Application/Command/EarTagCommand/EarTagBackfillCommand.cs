﻿using I_Ear_Tag.Demoin.EarTagManagement;
using MediatR;

namespace I_Ear_Tag.Read.Api.Application.Command.EarTagCommand
{
    public class EarTagBackfillCommand:IRequest<EarTag>
    {
        /// <summary>
        /// 耳标id
        /// </summary>
        public int EarTagId { get; set; }
    }
}

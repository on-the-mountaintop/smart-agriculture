﻿using I_Ear_Tag.Demoin.EarTagManagement;
using MediatR;

namespace I_Ear_Tag.Read.Api.Application.Command.EarTagCommand
{
    public class EarTagShowCommand:IRequest<Page<EarTag>>
    {
        /// <summary>
        /// 当前页索引
        /// </summary>
        public int pageIndex { get; set; }
        /// <summary>
        /// 每页显示数据条数
        /// </summary>
        public int pageSize { get; set; }
        /// <summary>
        /// 养只耳号
        /// </summary>
        public string? EarNumber { get; set; }
        /// <summary>
        /// 对应品种
        /// </summary>
        public Breed ?SheepBreed { get; set; }
        /// <summary>
        /// 对应出生状态
        /// </summary>
        public State ?BirthStatus { get; set; }
        
    }
}

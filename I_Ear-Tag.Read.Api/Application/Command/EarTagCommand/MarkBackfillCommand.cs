﻿using I_Ear_Tag.Demoin.EarTagManagement;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace I_Ear_Tag.Read.Api.Application.Command.EarTagCommand
{
    public class MarkBackfillCommand:IRequest<Mark>
    {
        /// <summary>
        /// 羊只戴标Id
        /// </summary>
        public int MarkId { get; set; }
    }
}

﻿using I_Ear_Tag.Demoin.EarTagManagement;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace I_Ear_Tag.Read.Api.Application.Command.EarTagCommand
{
    public class MarkShowCommand:IRequest<Page<Mark>>
    {
        /// <summary>
        /// 当前页索引
        /// </summary>
        public int pageIndex { get; set; }
        /// <summary>
        /// 每页显示数据条数
        /// </summary>
        public int pageSize { get; set; }
        /// <summary>
        /// 羊只耳号
        /// </summary>
        public string? EarNumber { get; set; }
        /// <summary>
        /// 当前电子耳号
        /// </summary>
      
        public string? OldEarTag { get; set; }
        /// <summary>
        /// 新电子耳号
        /// </summary>
      
        public string? NewEarTag { get; set; }
        /// <summary>
        /// 戴标原因
        /// </summary>
        public Cause ?Reason { get; set; }
    }
}

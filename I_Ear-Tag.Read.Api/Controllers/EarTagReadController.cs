﻿using I_Ear_Tag.Demoin.EarTagManagement;
using I_Ear_Tag.Read.Api.Application.Command.EarTagCommand;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Ear_Tag.Read.Api.Controllers
{
    /// <summary>
    /// 耳标信息接口
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class EarTagReadController : ControllerBase
    {
        private readonly IMediator mediator;
        public EarTagReadController(IMediator mediator)
        {
            this.mediator = mediator;
        }
        /// <summary>
        /// 反填产羔耳标
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<EarTag> EarTagBackfill([FromQuery]EarTagBackfillCommand command)
        {
            return await mediator.Send(command);
        }
        /// <summary>
        /// 反填羊只戴标信息
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<Mark> MarkBackfill([FromQuery]MarkBackfillCommand command)
        {
            return await mediator.Send(command);
        }
        /// <summary>
        /// 显示产羔耳标管理信息
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<Page<EarTag>> EarTagShow([FromQuery] EarTagShowCommand showCommand)
        {
            return await mediator.Send(showCommand);
        }
        /// <summary>
        /// 显示羊只戴标信息
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<Page<Mark>> MarkShow([FromQuery] MarkShowCommand showCommand)
        {
            return await mediator.Send(showCommand);
        }

    }
}

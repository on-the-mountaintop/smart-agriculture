﻿using AutoMapper;
using I_Archives.Domains.DTO;
using I_Archives.Infrastructure.Interface;
using I_Archives.Read.API.Application.Command;
using MediatR;

namespace I_Archives.Read.API.Application.CommandHandler
{
    public class ArchivesReadAllCommandHandlerL : IRequestHandler<ArchivesReadAllCommand, List<ArchivesInfoDTO>>
    {
        private readonly IArchivesRepository _archivesRepository;
        private readonly IMapper _mapper;

        public ArchivesReadAllCommandHandlerL(IArchivesRepository archivesRepository, IMapper mapper)
        {
            _archivesRepository = archivesRepository;
            _mapper = mapper;
        }

        public async Task<List<ArchivesInfoDTO>> Handle(ArchivesReadAllCommand request, CancellationToken cancellationToken)
        {
            var archivesInfoList = await _archivesRepository.GetByFuncListAsync(x => !x.IsDelete && x.Presence == "在场");
            return _mapper.Map<List<ArchivesInfoDTO>>(archivesInfoList.OrderByDescending(x => x.Id));
        }
    }
}

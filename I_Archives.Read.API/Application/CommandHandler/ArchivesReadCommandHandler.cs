﻿using AutoMapper;
using CommonClass;
using I_Archives.Domains;
using I_Archives.Domains.DTO;
using I_Archives.Infrastructure.Interface;
using I_Archives.Read.API.Application.Command;
using LinqKit;
using MediatR;
using System.Data.Entity;
using System.Linq.Expressions;

namespace I_Archives.Read.API.Application.CommandHandler
{
    public class ArchivesReadCommandHandler : IRequestHandler<ArchivesReadCommand, PageResult<ArchivesInfoDTO>>
    {
        private readonly IArchivesRepository _archivesRepository;
        private readonly IMapper _mapper;

        public ArchivesReadCommandHandler(IArchivesRepository archivesRepository, IMapper mapper)
        {
            _archivesRepository = archivesRepository;
            _mapper = mapper;
        }

        public async Task<PageResult<ArchivesInfoDTO>> Handle(ArchivesReadCommand request, CancellationToken cancellationToken)
        {
            var archives = await _archivesRepository.GetByFuncListAsync(x => !x.IsDelete);
            //var archivesInfoDTO = new List<ArchivesInfoDTO>();
            //foreach (var archive in archives)
            //{
            //    archivesInfoDTO.Add(new ArchivesInfoDTO
            //    {
            //        BirthWeight = archive.BirthWeight,
            //        CottageId = archive.CottageId,
            //        EarNnumber = archive.EarNnumber,
            //        ElectronicEarNumber = archive.ElectronicEarNumber,
            //        FatherEarNumber = archive.FatherEarNumber,
            //        GeneticGrade = archive.GeneticGrade,
            //        GrowthStage = archive.GrowthStage,
            //        Field = archive.Field,
            //        Id = archive.Id,
            //        MotherEarNumber = archive.MotherEarNumber,
            //        PregnancyTest = archive.PregnancyTest,
            //        ProductionGrade = archive.ProductionGrade,
            //        ReproductiveState = archive.ReproductiveState,
            //        Presence = archive.Presence,
            //        SalesGrade = archive.SalesGrade,
            //        SheepBir = archive.SheepBir,
            //        SheepBreed = archive.SheepBreed,
            //        SheepType = archive.SheepType,
            //        SheepGender = archive.SheepGender,
            //        StatusDays = archive.StatusDays,
            //        WeaningDate = archive.WeaningDate,
            //        WeaningWeigh = archive.WeaningWeigh,
            //        DayAge = (DateTime.Now - archive.SheepBir).Days,
            //        MonthAge = (DateTime.Now - archive.SheepBir).Days / 30,
            //        WeaningAge = archive.WeaningDate != null ? (DateTime.Now - archive.WeaningDate).Value.Days : 0
            //    });
            //}
            //var archivesDTO = archivesInfoDTO.AsQueryable();
            var archivesDTOS = _mapper.Map<List<ArchivesInfoDTO>>(archives);
            var archivesDTO = archivesDTOS.AsQueryable();

            if (!string.IsNullOrEmpty(request.EarNumber))
            {
                archivesDTO = archivesDTO.Where(a => a.EarNnumber.Contains(request.EarNumber));
            }
            if (!string.IsNullOrEmpty(request.ElectronicEarNumber))
            {
                archivesDTO = archivesDTO.Where(a => a.ElectronicEarNumber.Contains(request.ElectronicEarNumber));
            }
            if (!string.IsNullOrEmpty(request.SheepBreed))
            {
                archivesDTO = archivesDTO.Where(a => a.SheepBreed == (request.SheepBreed));
            }
            if (request.SheepGender != 2)
            {
                archivesDTO = archivesDTO.Where(a => a.SheepGender == (Convert.ToBoolean(request.SheepGender)));
            }
            if (!string.IsNullOrEmpty(request.StartSheepBir))
            {
                var startDate = DateTime.Parse(request.StartSheepBir);
                archivesDTO = archivesDTO.Where(a => a.SheepBir >= startDate);
            }
            if (!string.IsNullOrEmpty(request.EndSheepBir))
            {
                var endDate = DateTime.Parse(request.EndSheepBir);
                archivesDTO = archivesDTO.Where(a => a.SheepBir <= endDate);
            }
            if (!string.IsNullOrEmpty(request.GrowthStage))
            {
                archivesDTO = archivesDTO.Where(a => a.GrowthStage == (request.GrowthStage));
            }
            if (!string.IsNullOrEmpty(request.CottageId))
            {
                archivesDTO = archivesDTO.Where(a => a.CottageId == (request.CottageId));
            }
            if (!string.IsNullOrEmpty(request.Field))
            {
                archivesDTO = archivesDTO.Where(a => a.Field == (request.Field));
            }
            if (!string.IsNullOrEmpty(request.SheepType))
            {
                archivesDTO = archivesDTO.Where(a => a.SheepType == (request.SheepType));
            }
            if (!string.IsNullOrEmpty(request.GeneticGrade))
            {
                archivesDTO = archivesDTO.Where(a => a.GeneticGrade == (request.GeneticGrade));
            }
            if (!string.IsNullOrEmpty(request.ProductionGrade))
            {
                archivesDTO = archivesDTO.Where(a => a.ProductionGrade == (request.ProductionGrade)) ?? archivesDTO;
            }
            if (request.StartStatusDays != 0)
            {
                archivesDTO = archivesDTO.Where(a => a.StatusDays >= request.StartStatusDays);
            }
            if (request.EndStatusDays != 0)
            {
                archivesDTO = archivesDTO.Where(a => a.StatusDays <= request.EndStatusDays);
            }
            if (!string.IsNullOrEmpty(request.SalesGrade))
            {
                archivesDTO = archivesDTO.Where(a => a.SalesGrade == (request.SalesGrade));
            }
            if (!string.IsNullOrEmpty(request.ReproductiveState))
            {
                archivesDTO = archivesDTO.Where(a => a.ReproductiveState == (request.ReproductiveState));
            }
            if (!string.IsNullOrEmpty(request.Presence))
            {
                archivesDTO = archivesDTO.Where(a => a.Presence == (request.Presence));
            }
            if (!string.IsNullOrEmpty(request.FatherEarNumber))
            {
                archivesDTO = archivesDTO.Where(a => a.FatherEarNumber == (request.FatherEarNumber));
            }
            if (!string.IsNullOrEmpty(request.MotherEarNumber))
            {
                archivesDTO = archivesDTO.Where(a => a.MotherEarNumber == (request.MotherEarNumber));
            }

            var pageResult = new PageResult<ArchivesInfoDTO>();

            if (archivesDTO != null)
            {
                pageResult.TotalCount = archivesDTO.Count();
                pageResult.PageCount = (int)Math.Ceiling(pageResult.TotalCount * 1.0 / request.Size);
                pageResult.Datas = archivesDTO.OrderByDescending(a => a.Id).Skip(request.Size * (request.Index - 1)).Take(request.Size).ToList();
            }

            return pageResult;
        }
    }
}

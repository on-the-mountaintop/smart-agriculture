﻿using CommonClass.CountInfo;
using I_Archives.Infrastructure.Interface;
using I_Archives.Read.API.Application.Command;
using MediatR;

namespace I_Archives.Read.API.Application.CommandHandler
{
    public class ArchivesInventoryTrendsCommandHandler : IRequestHandler<ArchivesInventoryTrendsCommand, List<InventoryTrends>>
    {
        private readonly IArchivesRepository _archivesRepository;

        public ArchivesInventoryTrendsCommandHandler(IArchivesRepository archivesRepository)
        {
            _archivesRepository = archivesRepository;
        }

        public async Task<List<InventoryTrends>> Handle(ArchivesInventoryTrendsCommand request, CancellationToken cancellationToken)
        {
            var dateStart = DateTime.Now.AddMonths(-1);
            var dataList = await _archivesRepository.GetByFuncListAsync(x => !x.IsDelete &&x.Presence=="在场");
            Dictionary<DateTime,int> dic = new Dictionary<DateTime, int>();
            for (DateTime date = dateStart; date <= DateTime.Today; date = date.AddDays(1))
            {
                dic[date] = dataList.Where(x=>Convert.ToDateTime(x.CreateDate)<= date ).Count();
            }
            var inventoryTrendsList = dic.Select(x => new InventoryTrends()
            {
                 Date= x.Key.ToString("MM/dd"),
                 Count= x.Value
            }) .ToList();
            return inventoryTrendsList;
        }
    }
}

﻿using I_Archives.Domains;
using I_Archives.Infrastructure.Interface;
using I_Archives.Read.API.Application.Command;
using MediatR;

namespace I_Archives.Read.API.Application.CommandHandler
{
    public class ArchivesReadByIdCommandHandler : IRequestHandler<ArchivesReadByIdCommand, ArchivesInfo>
    {
        private readonly IArchivesRepository _archivesRepository;

        public ArchivesReadByIdCommandHandler(IArchivesRepository archivesRepository)
        {
            _archivesRepository = archivesRepository;
        }

        public async Task<ArchivesInfo> Handle(ArchivesReadByIdCommand request, CancellationToken cancellationToken)
        {
            return await _archivesRepository.GetByIdAsync(request.Id);
        }
    }
}

﻿using CommonClass.CountInfo;
using I_Archives.Infrastructure.Interface;
using I_Archives.Read.API.Application.Command;
using MediatR;

namespace I_Archives.Read.API.Application.CommandHandler
{
    public class ArchivesReadByCountCommandHandler : IRequestHandler<ArchivesReadByCountCommand, RealTimeStorage>
    {
        private readonly IArchivesRepository _archivesRepository;

        public ArchivesReadByCountCommandHandler(IArchivesRepository archivesRepository)
        {
            _archivesRepository = archivesRepository;
        }

        public async Task<RealTimeStorage> Handle(ArchivesReadByCountCommand request, CancellationToken cancellationToken)
        {
            var archivesInfo = await _archivesRepository.GetByFuncListAsync(x => !x.IsDelete && x.Presence == "在场");
            var countInfo = new RealTimeStorage();
            countInfo.AllCount = archivesInfo.Count();
            countInfo.MaternalCount = archivesInfo.Where(x => x.SheepType == "种母").Count();
            countInfo.MaleCount = archivesInfo.Where(x => x.SheepType == "种公").Count();
            countInfo.DescendentCount = archivesInfo.Where(x => x.SheepType == "后裔").Count();
            countInfo.RatioCount =Convert.ToDouble(countInfo.AllCount) / archivesInfo.Where(x => Convert.ToDateTime(x.CreateDate) <= DateTime.Now.AddDays(-7)).Count();
            return countInfo;
        }
    }
}

﻿using AutoMapper;
using I_Archives.Domains.DTO;
using I_Archives.Infrastructure.Interface;
using I_Archives.Read.API.Application.Command;
using MediatR;

namespace I_Archives.Read.API.Application.CommandHandler
{
    public class ArchivesReadByEarNumberCommandHandler : IRequestHandler<ArchivesReadByEarNumberCommand, ArchivesInfoDTO>
    {
        private readonly IArchivesRepository _archivesRepository;
        private readonly IMapper _mapper;

        public ArchivesReadByEarNumberCommandHandler(IArchivesRepository archivesRepository, IMapper mapper)
        {
            _archivesRepository = archivesRepository;
            _mapper = mapper;
        }

        public async Task<ArchivesInfoDTO> Handle(ArchivesReadByEarNumberCommand request, CancellationToken cancellationToken)
        {
            var archives = await _archivesRepository.GetByFuncAsync(a=>a.EarNnumber==request.EarNumber);
            return _mapper.Map<ArchivesInfoDTO>(archives);
        }
    }
}

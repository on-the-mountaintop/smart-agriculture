﻿using CommonClass.CountInfo;
using MediatR;

namespace I_Archives.Read.API.Application.Command
{
    public class ArchivesInventoryTrendsCommand:IRequest<List<InventoryTrends>>
    {
    }
}

﻿using I_Archives.Domains.DTO;
using MediatR;

namespace I_Archives.Read.API.Application.Command
{
    public class ArchivesReadAllCommand:IRequest<List<ArchivesInfoDTO>>
    {

    }
}

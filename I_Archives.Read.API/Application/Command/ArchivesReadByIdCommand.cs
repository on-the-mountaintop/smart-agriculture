﻿using I_Archives.Domains;
using MediatR;

namespace I_Archives.Read.API.Application.Command
{
    public class ArchivesReadByIdCommand:IRequest<ArchivesInfo>
    {
        public int Id { get; set; }
    }
}

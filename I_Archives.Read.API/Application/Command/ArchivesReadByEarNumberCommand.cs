﻿using I_Archives.Domains.DTO;
using MediatR;

namespace I_Archives.Read.API.Application.Command
{
    public class ArchivesReadByEarNumberCommand:IRequest<ArchivesInfoDTO>
    {
        public string? EarNumber { get; set; } // 耳号
    }
}

﻿using CommonClass;
using I_Archives.Domains;
using I_Archives.Domains.DTO;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace I_Archives.Read.API.Application.Command
{
    public class ArchivesReadCommand : IRequest<PageResult<ArchivesInfoDTO>>
    {
        public int Size { get; set; } = 15;
        public int Index { get; set; } = 1;
        /// <summary>
        /// 耳号
        /// </summary>
        public string? EarNumber { get; set; }
        /// <summary>
        /// 可视耳号
        /// </summary>
        public string? ElectronicEarNumber { get; set; }
        /// <summary>
        /// 羊只种类
        /// </summary>
        public string? SheepBreed { get; set; }
        /// <summary>
        /// 羊只性别
        /// </summary>
        public int SheepGender { get; set; } = 2;
        /// <summary>
        /// 出生日期(起始)
        /// </summary>
        public string? StartSheepBir { get; set; }
        /// <summary>
        /// 出生日期(截止)
        /// </summary>
        public string? EndSheepBir { get; set; }
        /// <summary>
        /// 在场状态
        /// </summary>
        public string? Presence { get; set; }
        /// <summary>
        /// 生长状态
        /// </summary>
        public string? GrowthStage { get; set; }
        /// <summary>
        /// 羊只类型
        /// </summary>
        public string? SheepType { get; set; }
        /// <summary>
        /// 栋舍Id
        /// </summary>
        public string? CottageId { get; set; } 
        /// <summary>
        /// 栏位Id
        /// </summary>
        public string? Field { get; set; }
        /// <summary>
        /// 繁殖状态
        /// </summary>
        public string? ReproductiveState { get; set; }
        /// <summary>
        /// 状态天数
        /// </summary>
        public int? StartStatusDays { get; set; } = 0;
        public int? EndStatusDays { get; set; } = 0;
        /// <summary>
        /// 生产等级
        /// </summary>
        public string? ProductionGrade { get; set; }
        /// <summary>
        /// 销售等级
        /// </summary>
        public string? SalesGrade { get; set; }
        /// <summary>
        /// 基因等级
        /// </summary>
        public string? GeneticGrade { get; set; }
        /// <summary>
        /// 父耳号
        /// </summary>
        public string? FatherEarNumber { get; set; }
        /// <summary>
        /// 母耳号
        /// </summary>
        public string? MotherEarNumber { get; set; }
    }
}

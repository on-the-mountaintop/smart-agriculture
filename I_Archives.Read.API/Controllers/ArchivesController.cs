﻿using CommonClass;
using CommonClass.CountInfo;
using I_Archives.Domains;
using I_Archives.Domains.DTO;
using I_Archives.Read.API.Application.Command;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace I_Archives.Read.API.Controllers
{
    /// <summary>
    /// 档案管理（Read）
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ArchivesController : ControllerBase
    {
        private readonly IMediator _mediator;
        private readonly ILogger<ArchivesController> _logger;

        public ArchivesController(IMediator mediator, ILogger<ArchivesController> logger)
        {
            _mediator = mediator;
            _logger = logger;
        }

        /// <summary>
        /// 测试
        /// </summary>
        [HttpGet]
        public void ELKTest()
        {
            try
            {
                ArchivesInfo archivesInfo = null;
                archivesInfo.EarNnumber = "";
            }
            catch (Exception)
            {
                _logger.LogError("凯哥，报错了，报错了，该修改代码了！");
                throw;
            }
        }

        [HttpGet]
        public Task<string> PollTest()
        {
            return Task.FromResult("Azure Devops 阿里云远程测试！");
        }

        /// <summary>
        /// 获取羊只品种
        /// </summary>
        /// <returns></returns>
        [HttpGet]

        public IActionResult GetSheepBreeds()
        {
            List<object> list = new List<object>();
            foreach (var item in Enum.GetValues(typeof(SheepBreeds)))
            {
                list.Add(new
                {
                    Key = item,
                    Value = item.ToString(),
                });
            }
            return Ok(list);
        }

        /// <summary>
        /// 获取羊只生长阶段
        /// </summary>
        /// <returns></returns>
        [HttpGet]

        public IActionResult GetGrowthStage()
        {
            List<object> list = new List<object>();
            foreach (var item in Enum.GetValues(typeof(GrowthStage)))
            {
                list.Add(new
                {
                    Key = item,
                    Value = item.ToString(),
                });
            }
            return Ok(list);
        }

        /// <summary>
        /// 获取羊只类型
        /// </summary>
        /// <returns></returns>
        [HttpGet]

        public IActionResult GetSheepSpecies()
        {
            List<object> list = new List<object>();
            foreach (var item in Enum.GetValues(typeof(SheepSpecies)))
            {
                list.Add(new
                {
                    Key = item,
                    Value = item.ToString(),
                });
            }
            return Ok(list);
        }

        /// <summary>
        /// 获取羊只基因等级
        /// </summary>
        /// <returns></returns>
        [HttpGet]

        public IActionResult GetGeneticGrade()
        {
            List<object> list = new List<object>();
            foreach (var item in Enum.GetValues(typeof(GeneticGrade)))
            {
                list.Add(new
                {
                    Key = item,
                    Value = item.ToString(),
                });
            }
            return Ok(list);
        }

        /// <summary>
        /// 获取羊只生产等级
        /// </summary>
        /// <returns></returns>
        [HttpGet]

        public IActionResult GetProductionGrade()
        {
            List<object> list = new List<object>();
            foreach (var item in Enum.GetValues(typeof(ProductionGrade)))
            {
                list.Add(new
                {
                    Key = item,
                    Value = item.ToString(),
                });
            }
            return Ok(list);
        }

        /// <summary>
        /// 获取羊只繁殖状态
        /// </summary>
        /// <returns></returns>
        [HttpGet]

        public IActionResult GetReproductiveState()
        {
            List<object> list = new List<object>();
            foreach (var item in Enum.GetValues(typeof(ReproductiveState)))
            {
                list.Add(new
                {
                    Key = item,
                    Value = item.ToString(),
                });
            }
            return Ok(list);
        }

        /// <summary>
        /// 获取羊只在场状态
        /// </summary>
        /// <returns></returns>
        [HttpGet]

        public IActionResult GetPresenceStatus()
        {
            List<object> list = new List<object>();
            foreach (var item in Enum.GetValues(typeof(PresenceStatus)))
            {
                list.Add(new
                {
                    Key = item,
                    Value = item.ToString(),
                });
            }
            return Ok(list);
        }

        /// <summary>
        /// 根据档案Id获取档案信息
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ArchivesInfo> ArchivesReadById([FromQuery] ArchivesReadByIdCommand command)
        {
            return await _mediator.Send(command);
        }

        /// <summary>
        /// 多条件组合查询分页档案列表
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PageResult<ArchivesInfoDTO>> ArchivesPageList([FromQuery] ArchivesReadCommand command)
        {
            return await _mediator.Send(command);
        }

        /// <summary>
        /// 根据羊只Id获取羊只系谱
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<SheepGenealogy> ArchivesSheepGenealogyRead([FromQuery] ArchivesReadGenealogyCommand command)
        {
            return await _mediator.Send(command);
        }
        /// <summary>
        /// 获取羊只档案全部数据
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<ArchivesInfoDTO>> ArchivesAllInfo([FromQuery] ArchivesReadAllCommand command)
        {
            return await _mediator.Send(command);
        }
        /// <summary>
        /// 根据羊只耳号获取羊只档案信息
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ArchivesInfoDTO> ArchivesGetByEarNumberInfo([FromQuery] ArchivesReadByEarNumberCommand command)
        {
            return await _mediator.Send(command);
        }

        /// <summary>
        /// 实时存栏统计
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<RealTimeStorage> GetRealTimeStorage([FromQuery] ArchivesReadByCountCommand command)
        {
            return await _mediator.Send(command);
        }

        /// <summary>
        /// 近30天存栏趋势统计
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<InventoryTrends>> GetInventoryTrends([FromQuery] ArchivesInventoryTrendsCommand command)
        {
            return await _mediator.Send(command);
        }
    }
}

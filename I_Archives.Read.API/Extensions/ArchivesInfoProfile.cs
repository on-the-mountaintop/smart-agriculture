﻿using AutoMapper;
using I_Archives.Domains;
using I_Archives.Domains.DTO;

namespace I_Archives.Read.API.Extensions
{
    public class ArchivesInfoProfile : Profile
    {
        public ArchivesInfoProfile()
        {
            CreateMap<ArchivesInfo, ArchivesInfoDTO>()
                .ForMember(dest => dest.DayAge, opt => opt.MapFrom(src => Convert.ToInt32((DateTime.Now - src.SheepBir).Days)))
                .ForMember(dest => dest.MonthAge, opt => opt.MapFrom(src => Convert.ToInt32((DateTime.Now - src.SheepBir).Days) / 30))
                .ForMember(dest => dest.WeaningAge, opt => opt.MapFrom(src => src.WeaningDate != null ? Convert.ToInt32((DateTime.Now - src.WeaningDate.Value).Days) : 0))
                .ReverseMap();
        }
    }
}

using I_Archives.Infrastructure;
using I_Archives.Read.API.Extensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Serilog.Events;
using Serilog;
using System.Reflection;
using I_Archives.Read.API;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

// 注册SkyWalking
//Environment.SetEnvironmentVariable("ASPNETCORE_HOSTINGSTARTUPASSEMBLIES", "SkyAPM.Agent.AspNetCore");
//Environment.SetEnvironmentVariable("SKYWALKING__SERVICENAME", "MySkyWalkingDemoTest");

// 将全局异常过滤器添加到服务容器中
builder.Services.AddControllers(options =>
{
    options.Filters.Add<GolbalExceptionFilter>();
});

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// 注册接口服务
builder.Services.AddServices();

// 注册中介者模式
builder.Services.AddMediatR(Assembly.GetExecutingAssembly());

// 注册AutoMapper
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

// 注册数据库上下文
builder.Services.AddDbContext<MyDbContext>(x => x.UseSqlServer(builder.Configuration.GetConnectionString("Authorization")));

// 注册JWT
builder.Services.AddJWT(builder);

// 注册Serilog(将 Serilog 集成到应用程序的日志记录中，并在应用程序关闭时释放 Serilog 日志记录器)
builder.Services.AddLogging(options =>
{
    options.AddSerilog(dispose: true);
});

var app = builder.Build();

// 配置ELK
var elaUri = app.Configuration["ELK:ElasticSearchUri"];

Log.Logger = new LoggerConfiguration()//创建一个新的 LoggerConfiguration 实例，用于配置日志记录器
    .Enrich.FromLogContext()//启用了从日志上下文中获取附加信息的功能。这样可以在日志中包含额外的上下文信息
    .MinimumLevel.Debug()//日志等级
    .WriteTo.Elasticsearch(//指定了将日志写入 Elasticsearch 的配置
    new Serilog.Sinks.Elasticsearch.ElasticsearchSinkOptions(new Uri(elaUri))//创建 ElasticsearchSinkOptions 对象，并传入 Elasticsearch 服务器的地址
    {
        MinimumLogEventLevel = LogEventLevel.Verbose,//表示记录所有级别的日志消息。
        AutoRegisterTemplate = true//自动注册 Elasticsearch 的模板
    }).CreateLogger();//创建并返回一个 Logger 实例,也就是我们配置好的日志记录器

// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
//    app.UseSwagger();
//    app.UseSwaggerUI();
//}

app.UseSwagger();
app.UseSwaggerUI();

app.UseCors(c => c.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Personal.Domains.User
{
    /// <summary>
    /// 审计字段表
    /// </summary>
    [Table("AuditInfo")]
    public class AuditInfo
    {
        /// <summary>
        /// 添加人
        /// </summary>
        public string? CreateBy { get; set; }

        /// <summary>
        /// 添加时间
        /// </summary>
        public DateTime CreateDate { get; set; }

        /// <summary>
        /// 修改人
        /// </summary>
        public string? UpdateBy { get; set; }

        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime UpdateDate { get; set; }

        /// <summary>
        /// 逻辑删除状态列
        /// </summary>
        public bool IsDel { get; set; } = true;
    }
}

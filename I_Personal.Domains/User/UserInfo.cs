﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Personal.Domains.User
{
    /// <summary>
    /// 个人信息表
    /// </summary>
    [Table("UserInfo")]
    public class UserInfo: AuditInfo  //继承审计字段表
    {
        /// <summary>
        /// 个人信息编号  主键
        /// </summary>
        [Key]
        public int UserId { get; set; }

        /// <summary>
        /// 用户名
        /// </summary>
        [StringLength(30)]
        public string? UserName { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        [StringLength(30)]
        public string? NickName { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        [StringLength(5)]
        public string? UserSex { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        [StringLength(10)]
        public string? Name { get; set; }

        /// <summary>
        /// 证件类型
        /// </summary>
        [StringLength(40)]
        public string? CertificateType { get; set; }

        /// <summary>
        /// 证件号码
        /// </summary>
        [StringLength(40)]
        public string? IDNumber { get; set; }

        /// <summary>
        /// 手机号
        /// </summary>
        [StringLength(16)]
        public string? UserPhone { get; set; }

        /// <summary>
        /// 所在地
        /// </summary>
        [StringLength(40)]
        public string? Location { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [StringLength(20)]
        public string? Password { get; set; }
    }
}

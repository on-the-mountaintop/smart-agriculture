using I_Produce.API.Extensions;
using I_Produce.Infrastructure;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

//配置控制器
builder.Services.AddControllers(o =>
{
	//添加自定义异常过滤器
	o.Filters.Add<MyExceptionFilter>();
});
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

//增加Xml文件,swagger文档注释
builder.Services.AddSwaggerGen(x =>
{
    string str = AppDomain.CurrentDomain.BaseDirectory + "I_Produce.API.Xml";
    x.IncludeXmlComments(str, true);
});

//注入JWT授权
builder.Services.AddJWT(builder);

// 注册接口服务
builder.Services.AddServices();

//参数类型是Assembly类型的数组 表示AddAutoMapper将在这些程序集数组里面遍历寻找所有继承了Profile类的配置文件
//在当前作用域的所有程序集里面扫描AddAutoMapper的配置文件
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

//注入HttpClient
builder.Services.AddHttpClient();

// 中介者模式
builder.Services.AddMediatR(Assembly.GetExecutingAssembly());

// 注册数据库
builder.Services.AddDbContext<EFCoreDbContext>(c =>
c.UseSqlServer(builder.Configuration.GetConnectionString("Conser")));

var app = builder.Build();

// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
    app.UseSwagger();
    app.UseSwaggerUI();
//}

app.UseCors(x => x.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod());

app.UseAuthentication();//身份认证

app.UseAuthorization();//授权

app.MapControllers();

app.Run();

﻿using CommonClass;
using I_Produce.API.Application.Commands.ProgenyTransferCommand;
using I_Produce.Infrastructure.Interface;
using MediatR;

namespace I_Produce.API.Application.CommandHandlers.ProgenyTransferHandler
{
	public class ProgenyTransferDeleteCommandHandler : IRequestHandler<ProgenyTransferDeleteCommand, ApiResult<int>>
	{
		private readonly IProgenyTransferRepository _repository;

		public ProgenyTransferDeleteCommandHandler(IProgenyTransferRepository repository)
		{
			_repository = repository;
		}

		public async Task<ApiResult<int>> Handle(ProgenyTransferDeleteCommand request, CancellationToken cancellationToken)
		{
			var progenyTransfer = await _repository.GetByIdAsync(request.id);

			if (progenyTransfer != null) { progenyTransfer.IsDel = true; }

			var result = await _repository.UpdateAsync(progenyTransfer);

			if (result > 0)
			{
				return new ApiResult<int>
				{
					Code = 200,
					msg = "成功",
					Data = result
				};
			}

			return new ApiResult<int>
			{
				Code = 201,
				msg = "失败",
				Data = result
			};
		}
	}
}

﻿using AutoMapper;
using CommonClass;
using I_Produce.API.Application.Commands.ProgenyTransferCommand;
using I_Produce.Domains.Entity;
using I_Produce.Infrastructure.Interface;
using MediatR;

namespace I_Produce.API.Application.CommandHandlers.ProgenyTransferHandler
{
	public class ProgenyTransferUpdateCommandHandler : IRequestHandler<ProgenyTransferUpdateCommand, ApiResult<int>>
	{
		private readonly IMapper _mapper;
		private readonly IProgenyTransferRepository _repository;

		public ProgenyTransferUpdateCommandHandler(IMapper mapper, IProgenyTransferRepository repository)
		{
			_mapper = mapper;
			_repository = repository;
		}

		public async Task<ApiResult<int>> Handle(ProgenyTransferUpdateCommand request, CancellationToken cancellationToken)
		{
			var progenyTransfer = _mapper.Map<ProgenyTransfer>(request);

			var result = await _repository.UpdateAsync(progenyTransfer);

			if (result > 0)
			{
				return new ApiResult<int>
				{
					Code = 200,
					msg = "成功",
					Data = result
				};
			}

			return new ApiResult<int>
			{
				Code = 201,
				msg = "失败",
				Data = result
			};
		}
	}
}

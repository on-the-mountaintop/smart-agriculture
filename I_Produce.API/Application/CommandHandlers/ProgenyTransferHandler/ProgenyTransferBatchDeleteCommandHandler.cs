﻿using CommonClass;
using I_Produce.API.Application.Commands.ProgenyTransferCommand;
using I_Produce.Infrastructure.Interface;
using MediatR;

namespace I_Produce.API.Application.CommandHandlers.ProgenyTransferHandler
{
	public class ProgenyTransferBatchDeleteCommandHandler : IRequestHandler<ProgenyTransferBatchDeleteCommand, ApiResult<int>>
	{
		private readonly IProgenyTransferRepository _repository;

		public ProgenyTransferBatchDeleteCommandHandler(IProgenyTransferRepository repository)
		{
			_repository = repository;
		}

		public async Task<ApiResult<int>> Handle(ProgenyTransferBatchDeleteCommand request, CancellationToken cancellationToken)
		{
			var idArray = request.ids.Split(',').Select(x => Convert.ToInt32(x));

			var progenyTransfer = await _repository.GetAsync(x => idArray.Contains(x.Id));

			foreach (var item in progenyTransfer)
			{
				item.IsDel = true;
			}

			var result = await _repository.UpdateRangeAsync(progenyTransfer);

			if (result > 0)
			{
				return new ApiResult<int>
				{
					Code = 200,
					msg = "成功",
					Data = result
				};
			}

			return new ApiResult<int>
			{
				Code = 201,
				msg = "失败",
				Data = result
			};
		}
	}
}

﻿using CommonClass;
using I_Produce.API.Application.Commands.PregnancyPregnancyTestCommand;
using I_Produce.Infrastructure.Interface;
using MediatR;

namespace I_Produce.API.Application.CommandHandlers.PregnancyPregnancyTestHandler
{
	public class PregnancyPregnancyTestBatchDeleteCommandHandler : IRequestHandler<PregnancyPregnancyTestBatchDeleteCommand, ApiResult<int>>
	{
		private readonly IPregnancyPregnancyTestRepository _repository;

		public PregnancyPregnancyTestBatchDeleteCommandHandler(IPregnancyPregnancyTestRepository repository)
		{
			_repository = repository;
		}

		public async Task<ApiResult<int>> Handle(PregnancyPregnancyTestBatchDeleteCommand request, CancellationToken cancellationToken)
		{

			var idArray = request.ids.Split(',').Select(x => Convert.ToInt32(x)).ToArray();

			var pregnancy = await _repository.GetAsync(x => idArray.Contains(x.Id));

			foreach (var item in pregnancy)
			{
				item.IsDel = true;
			}

			var result = await _repository.UpdateRangeAsync(pregnancy);

			if (result > 0)
			{
				return new ApiResult<int>
				{
					Code = 200,
					msg = "成功",
					Data = result
				};
			}

			return new ApiResult<int>
			{
				Code = 201,
				msg = "失败",
				Data = result
			};
		}
	}
}

﻿using CommonClass;
using I_Produce.API.Application.Commands.PregnancyPregnancyTestCommand;
using I_Produce.Infrastructure.Interface;
using MediatR;

namespace I_Produce.API.Application.CommandHandlers.PregnancyPregnancyTestHandler
{
	public class PregnancyPregnancyTestDeleteCommandHandler : IRequestHandler<PregnancyPregnancyTestDeleteCommand, ApiResult<int>>
	{
		private readonly IPregnancyPregnancyTestRepository _repository;

		public PregnancyPregnancyTestDeleteCommandHandler(IPregnancyPregnancyTestRepository repository)
		{
			_repository = repository;
		}

		public async Task<ApiResult<int>> Handle(PregnancyPregnancyTestDeleteCommand request, CancellationToken cancellationToken)
		{
			var pregnancy = await _repository.GetByIdAsync(request.id);

			if (pregnancy != null) pregnancy.IsDel = true;

			var result = await _repository.UpdateAsync(pregnancy);

			if (result > 0)
			{
				return new ApiResult<int>
				{
					Code = 200,
					msg = "成功",
					Data = result
				};
			}

			return new ApiResult<int>
			{
				Code = 201,
				msg = "失败",
				Data = result
			};
		}
	}
}

﻿using AutoMapper;
using CommonClass;
using I_Produce.API.Application.Commands.PregnancyPregnancyTestCommand;
using I_Produce.Domains.DTO;
using I_Produce.Domains.Entity;
using I_Produce.Infrastructure.Interface;
using MediatR;
using Newtonsoft.Json;

namespace I_Produce.API.Application.CommandHandlers.PregnancyPregnancyTestHandler
{
	public class PregnancyPregnancyTestCreateCommandHandler : IRequestHandler<PregnancyPregnancyTestCreateCommand, ApiResult<int>>
	{
		private readonly IMapper _mapper;
		private readonly IPregnancyPregnancyTestRepository _repository;
		private readonly IBreedingMotherRepository _motherrepository;
		private readonly IGermlineHistoryRecordsRepository _germlinerepository;
		private readonly Cross_services_Helper _httpClient;

		public PregnancyPregnancyTestCreateCommandHandler(IMapper mapper, IPregnancyPregnancyTestRepository repository, Cross_services_Helper httpClient, IBreedingMotherRepository motherrepository, IGermlineHistoryRecordsRepository germlinerepository)
		{
			_mapper = mapper;
			_repository = repository;
			_httpClient = httpClient;
			_motherrepository = motherrepository;
			_germlinerepository = germlinerepository;
		}

		public async Task<ApiResult<int>> Handle(PregnancyPregnancyTestCreateCommand request, CancellationToken cancellationToken)
		{
			var mother = await _motherrepository.GetByFuncAsync(x => x.Auricle == request.Auricle && !x.IsDel);

			if (mother == null)
			{
				return new ApiResult<int>
				{
					Code = 110,
					msg = "当前耳号没有配种信息"
				};
			}
			else
			{
				var pregnancy = _mapper.Map<PregnancyPregnancyTest>(request);

				var result = await _repository.AddAsync(pregnancy);

				if(result > 0)
				{
					var germline = await _germlinerepository.GetByFuncAsync(x => x.MotherEarHorn == request.Auricle && !x.IsDel);

					var germlineHistory = new GermlineHistoryRecords();

					germlineHistory.Id = germline.Id;
					germlineHistory.PregnancyTestDate = request.PregnancyTestDate;
					germlineHistory.PregnancyTestResult = request.PregnancyTestResult;

					await _httpClient.Put("http://10.31.56.22:8000/ProduceSetRout/api/GermlineHistoryRecords/UpdateGermlineHistoryRecordsPregnancy", germlineHistory);

					var spermatophore = await _httpClient.Get("http://10.31.56.22:8000/ArchivesGetRout/api/Archives/ArchivesGetByEarNumberInfo?EarNumber=" + request.Auricle);

					var dto = JsonConvert.DeserializeObject<BreedDTO>(spermatophore);
					var SheepBreed = dto.SheepBreed;
					var Sex = dto.SheepGender;

					var sheepRescission = new SheepRescission
					{
						RoundType = "种母妊检转舍",
						SurrenderDate = request.PregnancyTestDate,
						Breed = SheepBreed,
						Sex = Sex,
						TurnBuilding = request.CurrentBuilding,
						TransferBuilding = request.TransferBuilding,
						RolloutField = request.CurrentField,
						DriveField = request.DriveField,
						VisualEarSignal = request.Auricle
					};

					await _httpClient.Post("http://10.31.56.22:8000/ProduceSetRout/api/SheepRescission/AddSheepSheepRescission", sheepRescission);

					return new ApiResult<int>
					{
						Code = 200,
						msg = "成功",
						Data = result
					};
				}
				else
				{
					return new ApiResult<int>
					{
						Code = 201,
						msg = "失败",
						Data = result
					};
				}
			}
		}
	}
}

﻿using AutoMapper;
using CommonClass;
using I_Produce.API.Application.Commands.SeedBirthCommand;
using I_Produce.Domains.DTO;
using I_Produce.Domains.Entity;
using I_Produce.Infrastructure.Interface;
using MediatR;
using Newtonsoft.Json;

namespace I_Produce.API.Application.CommandHandlers.SeedBirthHandler
{
	public class SeedBirthCreateCommandHandler : IRequestHandler<SeedBirthCreateCommand, ApiResult<int>>
	{
		private readonly IMapper _mapper;
		private readonly ISeedBirthRepository _repository;
		private readonly IGermlineHistoryRecordsRepository _germlinerepository;
		private readonly IPregnancyPregnancyTestRepository _pregnancyPregnancyTestrepository;
		private readonly Cross_services_Helper _httpClient;

		public SeedBirthCreateCommandHandler(IMapper mapper, ISeedBirthRepository repository, IGermlineHistoryRecordsRepository germlinerepository, IPregnancyPregnancyTestRepository pregnancyPregnancyTestrepository, Cross_services_Helper httpClient)
		{
			_mapper = mapper;
			_repository = repository;
			_germlinerepository = germlinerepository;
			_pregnancyPregnancyTestrepository = pregnancyPregnancyTestrepository;
			_httpClient = httpClient;
		}

		public async Task<ApiResult<int>> Handle(SeedBirthCreateCommand request, CancellationToken cancellationToken)
		{
			var pregnancy = await _pregnancyPregnancyTestrepository.GetByFuncAsync(x => x.Auricle == request.MotherEarHorn && !x.IsDel);

			if (pregnancy == null)
			{
				return new ApiResult<int>
				{
					Code = 110,
					msg = "当前耳号没有妊检信息"
				};
			}
			else
			{
				var seedBirth = _mapper.Map<SeedBirth>(request);

				var result = await _repository.AddAsync(seedBirth);

				if (result > 0)
				{
					var germline = await _germlinerepository.GetByFuncAsync(x => x.MotherEarHorn == request.MotherEarHorn && !x.IsDel);

					var germlineHistory = new GermlineHistoryRecords();

					germlineHistory.Id = germline.Id;
					germlineHistory.DeliveryDate = request.DeliveryDate;
					germlineHistory.HealthyNumber = request.HealthyNumber;
					germlineHistory.WeakLambNumber = request.WeakLambNumber;
					germlineHistory.DeformityNumber = request.DeformityNumber;
					germlineHistory.MummyNumber = request.MummyNumber;

					await _httpClient.Put("http://10.31.56.22:8000/ProduceSetRout/api/GermlineHistoryRecords/UpdateGermlineHistoryRecordsSeedBirth", germlineHistory);

					var spermatophore = await _httpClient.Get("http://10.31.56.22:8000/ArchivesGetRout/api/Archives/ArchivesGetByEarNumberInfo?EarNumber=" + request.MotherEarHorn);

					var dto = JsonConvert.DeserializeObject<BreedDTO>(spermatophore);
					var SheepBreed = dto.SheepBreed;
					var Sex = dto.SheepGender;

					var sheepRescission = new SheepRescission
					{
						RoundType = "种母分娩转舍",
						SurrenderDate = request.DeliveryDate,
						Breed = SheepBreed,
						Sex = Sex,
						TurnBuilding = request.CurrentBuilding,
						TransferBuilding = request.TransferBuilding,
						RolloutField = request.CurrentField,
						DriveField = request.DriveField,
						VisualEarSignal = request.MotherEarHorn
					};

					await _httpClient.Post("http://10.31.56.22:8000/ProduceSetRout/api/SheepRescission/AddSheepSheepRescission", sheepRescission);

					return new ApiResult<int>
					{
						Code = 200,
						msg = "成功",
						Data = result
					};
				}
				else
				{
					return new ApiResult<int>
					{
						Code = 201,
						msg = "失败",
						Data = result
					};
				}
			}
		}
	}
}

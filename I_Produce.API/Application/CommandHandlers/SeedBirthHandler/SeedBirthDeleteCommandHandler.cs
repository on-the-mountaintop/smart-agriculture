﻿using CommonClass;
using I_Produce.API.Application.Commands.SeedBirthCommand;
using I_Produce.Infrastructure.Interface;
using MediatR;

namespace I_Produce.API.Application.CommandHandlers.SeedBirthHandler
{
	public class SeedBirthDeleteCommandHandler : IRequestHandler<SeedBirthDeleteCommand, ApiResult<int>>
	{
		private readonly ISeedBirthRepository _repository;

		public SeedBirthDeleteCommandHandler(ISeedBirthRepository repository)
		{
			_repository = repository;
		}

		public async Task<ApiResult<int>> Handle(SeedBirthDeleteCommand request, CancellationToken cancellationToken)
		{
			var seedBirth = await _repository.GetByIdAsync(request.id);

			if (seedBirth != null) seedBirth.IsDel = true;

			var result = await _repository.UpdateAsync(seedBirth);

			if (result > 0)
			{
				return new ApiResult<int>
				{
					Code = 200,
					msg = "成功",
					Data = result
				};
			}

			return new ApiResult<int>
			{
				Code = 201,
				msg = "失败",
				Data = result
			};
		}
	}
}

﻿using CommonClass;
using I_Produce.API.Application.Commands.SeedBirthCommand;
using I_Produce.Infrastructure.Interface;
using MediatR;

namespace I_Produce.API.Application.CommandHandlers.SeedBirthHandler
{
	public class SeedBirthBatchDeleteCommandHandler : IRequestHandler<SeedBirthBatchDeleteCommand, ApiResult<int>>
	{
		private readonly ISeedBirthRepository _repository;

		public SeedBirthBatchDeleteCommandHandler(ISeedBirthRepository repository)
		{
			_repository = repository;
		}

		public async Task<ApiResult<int>> Handle(SeedBirthBatchDeleteCommand request, CancellationToken cancellationToken)
		{
			var idArray = request.ids.Split(',').Select(x => Convert.ToInt32(x)).ToArray();

			var seedBirth = await _repository.GetAsync(x => idArray.Contains(x.Id));

			foreach (var item in seedBirth)
			{
				item.IsDel = true;
			}

			var result = await _repository.UpdateRangeAsync(seedBirth);

			if (result > 0)
			{
				return new ApiResult<int>
				{
					Code = 200,
					msg = "成功",
					Data = result
				};
			}

			return new ApiResult<int>
			{
				Code = 201,
				msg = "失败",
				Data = result
			};
		}
	}
}

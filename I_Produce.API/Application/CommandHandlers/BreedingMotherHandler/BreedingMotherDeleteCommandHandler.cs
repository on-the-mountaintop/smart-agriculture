﻿using CommonClass;
using I_Produce.API.Application.Commands.BreedingMotherCommand;
using I_Produce.Infrastructure.Interface;
using MediatR;

namespace I_Produce.API.Application.CommandHandlers.BreedingMotherHandler
{
	public class BreedingMotherDeleteCommandHandler : IRequestHandler<BreedingMotherDeleteCommand, ApiResult<int>>
	{
		private readonly IBreedingMotherRepository _repository;

		public BreedingMotherDeleteCommandHandler(IBreedingMotherRepository repository)
		{
			_repository = repository;
		}

		public async Task<ApiResult<int>> Handle(BreedingMotherDeleteCommand request, CancellationToken cancellationToken)
		{
			var breeing = await _repository.GetByIdAsync(request.id);

			if (breeing != null) breeing.IsDel = true;

			var result = await _repository.UpdateAsync(breeing);

			if (result > 0)
			{
				return new ApiResult<int>
				{
					Code = 200,
					msg = "成功",
					Data = result
				};
			}

			return new ApiResult<int>
			{
				Code = 201,
				msg = "失败",
				Data = result
			};
		}
	}
}

﻿using CommonClass;
using I_Produce.API.Application.Commands.BreedingMotherCommand;
using I_Produce.Infrastructure.Interface;
using MediatR;

namespace I_Produce.API.Application.CommandHandlers.BreedingMotherHandler
{
	public class BreedingMotherBatchDeleteCommandHandler : IRequestHandler<BreedingMotherBatchDeleteCommand, ApiResult<int>>
	{
		private readonly IBreedingMotherRepository _repository;

		public BreedingMotherBatchDeleteCommandHandler(IBreedingMotherRepository repository)
		{
			_repository = repository;
		}

		public async Task<ApiResult<int>> Handle(BreedingMotherBatchDeleteCommand request, CancellationToken cancellationToken)
		{
			var idArray = request.ids.Split(',').Select(x => Convert.ToInt32(x)).ToArray();

			var breeing = await _repository.GetAsync(x => idArray.Contains(x.Id));

			foreach (var item in breeing)
			{
				item.IsDel = true;
			}

			var result = await _repository.UpdateRangeAsync(breeing);

			if (result > 0)
			{
				return new ApiResult<int>
				{
					Code = 200,
					msg = "成功",
					Data = result
				};
			}

			return new ApiResult<int>
			{
				Code = 201,
				msg = "失败",
				Data = result
			};
		}
	}
}

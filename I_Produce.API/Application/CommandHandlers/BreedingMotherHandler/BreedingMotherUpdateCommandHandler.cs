﻿using AutoMapper;
using CommonClass;
using I_Produce.API.Application.Commands.BreedingMotherCommand;
using I_Produce.Domains.DTO;
using I_Produce.Domains.Entity;
using I_Produce.Infrastructure.Interface;
using MediatR;
using Newtonsoft.Json;
using System.Net.Http;

namespace I_Produce.API.Application.CommandHandlers.BreedingMotherHandler
{
	public class BreedingMotherUpdateCommandHandler : IRequestHandler<BreedingMotherUpdateCommand, ApiResult<int>>
	{
		private readonly IMapper _mapper;
		private readonly IBreedingMotherRepository _repository;
		private readonly Cross_services_Helper _httpClient;
		private readonly IGermlineHistoryRecordsRepository _germlinerepository;
		private readonly ISheepRescissionRepository _sheepRescissionrepository;

		public BreedingMotherUpdateCommandHandler(IMapper mapper, IBreedingMotherRepository repository, Cross_services_Helper httpClient, IGermlineHistoryRecordsRepository germlinerepository, ISheepRescissionRepository sheepRescissionrepository)
		{
			_mapper = mapper;
			_repository = repository;
			_httpClient = httpClient;
			_germlinerepository = germlinerepository;
			_sheepRescissionrepository = sheepRescissionrepository;
		}

		public async Task<ApiResult<int>> Handle(BreedingMotherUpdateCommand request, CancellationToken cancellationToken)
		{
			var brddding = _mapper.Map<BreedingMother>(request);

			var result = await _repository.UpdateAsync(brddding);

			if (result > 0)
			{
				var germline = await _germlinerepository.GetByFuncAsync(x => x.MotherEarHorn == request.Auricle && !x.IsDel);

				var germlineHistory = new GermlineHistoryRecords();

				germlineHistory.Id = germline.Id;
				germlineHistory.MotherEarHorn = request.Auricle;
				germlineHistory.BreedEarHorn = request.FirstWithRam;
				germlineHistory.DateOfBreeding = request.DateOfBreeding;

				await _httpClient.Put("http://10.31.56.22:8000/ProduceSetRout/api/GermlineHistoryRecords/UpdateGermlineHistoryRecordsBreedingMother", germlineHistory);

				var spermatophore = await _httpClient.Get("http://10.31.56.22:8000/ArchivesGetRout/api/Archives/ArchivesGetByEarNumberInfo?EarNumber=" + request.Auricle);

				var dto = JsonConvert.DeserializeObject<BreedDTO>(spermatophore);
				var SheepBreed = dto.SheepBreed;
				var Sex = dto.SheepGender;

				var sheepRes = await _sheepRescissionrepository.GetByFuncAsync(x => x.VisualEarSignal == request.Auricle && !x.IsDel && x.RoundType == "种母配种转舍");

				var sheepRescission = new SheepRescission
				{
					Id = sheepRes.Id,
					RoundType = "种母配种转舍",
					SurrenderDate = request.DateOfBreeding,
					Breed = SheepBreed,
					Sex = Sex,
					TurnBuilding = request.CurrentBuilding,
					TransferBuilding = request.TransferBuilding,
					RolloutField = request.CurrentField,
					DriveField = request.DriveField,
					VisualEarSignal = request.Auricle
				};

				await _httpClient.Put("http://10.31.56.22:8000/ProduceSetRout/api/SheepRescission/UpdateSheepSheepRescission", sheepRescission);

				return new ApiResult<int>
				{
					Code = 200,
					msg = "成功",
					Data = result
				};
			}

			return new ApiResult<int>
			{
				Code = 201,
				msg = "失败",
				Data = result
			};
		}
	}
}

﻿using CommonClass;
using I_Produce.API.Application.Commands.SheepRescissionCommand;
using I_Produce.Infrastructure.Interface;
using MediatR;

namespace I_Produce.API.Application.CommandHandlers.SheepRescissionHandler
{
    public class SheepRescissionDeleteCommandHandler : IRequestHandler<SheepRescissionDeleteCommand, ApiResult<int>>
    {
        private readonly ISheepRescissionRepository _repository;

        public SheepRescissionDeleteCommandHandler(ISheepRescissionRepository repository)
        {
            _repository = repository;
        }

        public async Task<ApiResult<int>> Handle(SheepRescissionDeleteCommand request, CancellationToken cancellationToken)
        {
            var sheep = await _repository.GetByIdAsync(request.id);

            if (sheep != null) { sheep.IsDel = true; }

            var result =  await _repository.UpdateAsync(sheep);

            if (result > 0)
            {
                return new ApiResult<int>
                {
                    Code = 200,
                    msg = "成功",
                    Data = result
                };
            }

            return new ApiResult<int>
            {
                Code = 201,
                msg = "失败",
                Data = result
            };
        }
    }
}

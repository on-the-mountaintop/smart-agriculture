﻿using AutoMapper;
using CommonClass;
using I_Produce.API.Application.Commands.SheepRescissionCommand;
using I_Produce.Domains.Entity;
using I_Produce.Infrastructure.Interface;
using MediatR;

namespace I_Produce.API.Application.CommandHandlers.SheepRescissionHandler
{
    public class SheepRescissionCreateCommandHandler : IRequestHandler<SheepRescissionCreateCommand, ApiResult<int>>
    {
        private readonly IMapper _mapper;
        private readonly ISheepRescissionRepository _repository;

        public SheepRescissionCreateCommandHandler(IMapper mapper, ISheepRescissionRepository repository)
        {
            _mapper = mapper;
            _repository = repository;
        }

        public async Task<ApiResult<int>> Handle(SheepRescissionCreateCommand request, CancellationToken cancellationToken)
        {
            var sheep = _mapper.Map<SheepRescission>(request);

            var result = await _repository.AddAsync(sheep);

            if (result > 0)
            {
                return new ApiResult<int>
                {
                    Code = 200,
                    msg = "成功",
                    Data = result
                };
            }

            return new ApiResult<int>
            {
                Code = 201,
                msg = "失败",
                Data = result
            };
        }
    }
}

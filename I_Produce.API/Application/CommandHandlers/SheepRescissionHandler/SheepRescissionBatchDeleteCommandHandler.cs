﻿using CommonClass;
using I_Produce.API.Application.Commands.SheepRescissionCommand;
using I_Produce.Infrastructure.Interface;
using MediatR;

namespace I_Produce.API.Application.CommandHandlers.SheepRescissionHandler
{
    public class SheepRescissionBatchDeleteCommandHandler : IRequestHandler<SheepRescissionBatchDeleteCommand, ApiResult<int>>
    {
        private readonly ISheepRescissionRepository _repository;

        public SheepRescissionBatchDeleteCommandHandler(ISheepRescissionRepository repository)
        {
            _repository = repository;
        }

        public async Task<ApiResult<int>> Handle(SheepRescissionBatchDeleteCommand request, CancellationToken cancellationToken)
        {
            var idArray = request.ids.Split(',').Select(x => Convert.ToInt32(x));

            var sheep = await _repository.GetAsync(x => idArray.Contains(x.Id));

            foreach ( var item in sheep)
            {
                item.IsDel = true;  
            }

            var result = await _repository.UpdateRangeAsync(sheep);

            if (result > 0)
            {
                return new ApiResult<int>
                {
                    Code = 200,
                    msg = "成功",
                    Data = result
                };
            }

            return new ApiResult<int>
            {
                Code = 201,
                msg = "失败",
                Data = result
            };
        }
    }
}

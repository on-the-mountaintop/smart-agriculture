﻿using AutoMapper;
using CommonClass;
using I_Produce.API.Application.Commands.WeaningOfLambCommand;
using I_Produce.Domains.Entity;
using I_Produce.Infrastructure.Interface;
using MediatR;

namespace I_Produce.API.Application.CommandHandlers.WeaningOfLambHandler
{
	public class WeaningOfLambCreateCommandHandler : IRequestHandler<WeaningOfLambCreateCommand, ApiResult<int>>
	{
		private readonly IMapper _mapper;
		private readonly IWeaningOfLambRepository _repository;

		public WeaningOfLambCreateCommandHandler(IMapper mapper, IWeaningOfLambRepository repository)
		{
			_mapper = mapper;
			_repository = repository;
		}

		public async Task<ApiResult<int>> Handle(WeaningOfLambCreateCommand request, CancellationToken cancellationToken)
		{
			var weaningOfLamb = _mapper.Map<WeaningOfLamb>(request);

			var result = await _repository.AddAsync(weaningOfLamb);

			if (result > 0)
			{
				return new ApiResult<int>
				{
					Code = 200,
					msg = "成功",
					Data = result
				};
			}

			return new ApiResult<int>
			{
				Code = 201,
				msg = "失败",
				Data = result
			};
		}
	}
}

﻿using CommonClass;
using I_Produce.API.Application.Commands.WeaningOfLambCommand;
using I_Produce.Infrastructure.Interface;
using MediatR;

namespace I_Produce.API.Application.CommandHandlers.WeaningOfLambHandler
{
	public class WeaningOfLambBatchDeleteCommandHandler : IRequestHandler<WeaningOfLambBatchDeleteCommand, ApiResult<int>>
	{
		private readonly IWeaningOfLambRepository _repository;

		public WeaningOfLambBatchDeleteCommandHandler(IWeaningOfLambRepository repository)
		{
			_repository = repository;
		}

		public async Task<ApiResult<int>> Handle(WeaningOfLambBatchDeleteCommand request, CancellationToken cancellationToken)
		{
			var idArray = request.ids.Split(',').Select(x => Convert.ToInt32(x));

			var weaningOfLamb = await _repository.GetAsync(x => idArray.Contains(x.Id));

			foreach (var item in weaningOfLamb)
			{
				item.IsDel = true;
			}

			var result = await _repository.UpdateRangeAsync(weaningOfLamb);

			if (result > 0)
			{
				return new ApiResult<int>
				{
					Code = 200,
					msg = "成功",
					Data = result
				};
			}

			return new ApiResult<int>
			{
				Code = 201,
				msg = "失败",
				Data = result
			};
		}
	}
}

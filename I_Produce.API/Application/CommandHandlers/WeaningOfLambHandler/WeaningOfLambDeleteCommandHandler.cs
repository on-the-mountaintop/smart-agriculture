﻿using CommonClass;
using I_Produce.API.Application.Commands.WeaningOfLambCommand;
using I_Produce.Domains.Entity;
using I_Produce.Infrastructure.Interface;
using MediatR;

namespace I_Produce.API.Application.CommandHandlers.WeaningOfLambHandler
{
	public class WeaningOfLambDeleteCommandHandler : IRequestHandler<WeaningOfLambDeleteCommand, ApiResult<int>>
	{
		private readonly IWeaningOfLambRepository _repository;

		public WeaningOfLambDeleteCommandHandler(IWeaningOfLambRepository repository)
		{
			_repository = repository;
		}

		public async Task<ApiResult<int>> Handle(WeaningOfLambDeleteCommand request, CancellationToken cancellationToken)
		{
			var weaningOfLamb = await _repository.GetByIdAsync(request.id);

			if (weaningOfLamb != null) { weaningOfLamb.IsDel = true; }

			var result = await _repository.UpdateAsync(weaningOfLamb);

			if (result > 0)
			{
				return new ApiResult<int>
				{
					Code = 200,
					msg = "成功",
					Data = result
				};
			}

			return new ApiResult<int>
			{
				Code = 201,
				msg = "失败",
				Data = result
			};
		}
	}
}

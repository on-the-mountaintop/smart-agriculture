﻿using AutoMapper;
using CommonClass;
using I_Produce.API.Application.Commands.SeedWeaningCommand;
using I_Produce.Domains.DTO;
using I_Produce.Domains.Entity;
using I_Produce.Infrastructure.Interface;
using MediatR;
using Newtonsoft.Json;

namespace I_Produce.API.Application.CommandHandlers.SeedWeaningHandler
{
	public class SeedWeaningUpdateCommandHandler : IRequestHandler<SeedWeaningUpdateCommand, ApiResult<int>>
	{
		private readonly IMapper _mapper;
		private readonly ISeedWeaningRepository _repository;
		private readonly ISeedBirthRepository _seedBirthrepository;
		private readonly IGermlineHistoryRecordsRepository _germlinerepository;
		private readonly Cross_services_Helper _httpClient;
		private readonly ISheepRescissionRepository _sheepRescissionrepository;

		public SeedWeaningUpdateCommandHandler(IMapper mapper,
										 ISeedWeaningRepository repository,
										 ISeedBirthRepository seedBirthrepository,
										 IGermlineHistoryRecordsRepository germlinerepository,
										 Cross_services_Helper httpClient,
										 ISheepRescissionRepository sheepRescissionrepository)
		{
			_mapper = mapper;
			_repository = repository;
			_seedBirthrepository = seedBirthrepository;
			_germlinerepository = germlinerepository;
			_httpClient = httpClient;
			_sheepRescissionrepository = sheepRescissionrepository;
		}

		public async Task<ApiResult<int>> Handle(SeedWeaningUpdateCommand request, CancellationToken cancellationToken)
		{
			var pregnancy = await _seedBirthrepository.GetByFuncAsync(x => x.MotherEarHorn == request.Auricle && !x.IsDel);

			if (pregnancy == null)
			{
				return new ApiResult<int>
				{
					Code = 110,
					msg = "当前耳号没有分娩信息"
				};
			}
			else
			{
				var seedWeaning = _mapper.Map<SeedWeaning>(request);

				var result = await _repository.UpdateAsync(seedWeaning);

				if (result > 0)
				{
					var germline = await _germlinerepository.GetByFuncAsync(x => x.MotherEarHorn == request.Auricle && !x.IsDel);

					var germlineHistory = new GermlineHistoryRecords();

					germlineHistory.Id = germline.Id;
					germlineHistory.WeaningDate = request.WeaningDate;
					germlineHistory.WeaningNumber = request.WeaningNumber;
					germlineHistory.WeaningLitterWeight = request.TotalLitterWeightWeaning;

					await _httpClient.Put("http://10.31.56.22:8000/ProduceSetRout/api/GermlineHistoryRecords/UpdateGermlineHistoryRecordsSeedWeaning", germlineHistory);

					var spermatophore = await _httpClient.Get("http://10.31.56.22:8000/ArchivesGetRout/api/Archives/ArchivesGetByEarNumberInfo?EarNumber=" + request.Auricle);

					var dto = JsonConvert.DeserializeObject<BreedDTO>(spermatophore);
					var SheepBreed = dto.SheepBreed;
					var Sex = dto.SheepGender;

					var sheepRes = await _sheepRescissionrepository.GetByFuncAsync(x => x.VisualEarSignal == request.Auricle && !x.IsDel && x.RoundType == "种母断奶转舍");

					var sheepRescission = new SheepRescission
					{
						Id = sheepRes.Id,
						RoundType = "种母断奶转舍",
						SurrenderDate = request.WeaningDate,
						Breed = SheepBreed,
						Sex = Sex,
						TurnBuilding = request.CurrentBuilding,
						TransferBuilding = request.TransferBuilding,
						RolloutField = request.CurrentField,
						DriveField = request.DriveField,
						VisualEarSignal = request.Auricle
					};

					await _httpClient.Put("http://10.31.56.22:8000/ProduceSetRout/api/SheepRescission/UpdateSheepSheepRescission", sheepRescission);

					return new ApiResult<int>
					{
						Code = 200,
						msg = "成功",
						Data = result
					};
				}
				else
				{
					return new ApiResult<int>
					{
						Code = 201,
						msg = "失败",
						Data = result
					};
				}
			}
		}
	}
}

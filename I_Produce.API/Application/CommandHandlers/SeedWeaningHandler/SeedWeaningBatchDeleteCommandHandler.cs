﻿using CommonClass;
using I_Produce.API.Application.Commands.SeedWeaningCommand;
using I_Produce.Infrastructure.Interface;
using MediatR;

namespace I_Produce.API.Application.CommandHandlers.SeedWeaningHandler
{
	public class SeedWeaningBatchDeleteCommandHandler : IRequestHandler<SeedWeaningBatchDeleteCommand, ApiResult<int>>
	{
		private readonly ISeedWeaningRepository _repository;

		public SeedWeaningBatchDeleteCommandHandler(ISeedWeaningRepository repository)
		{
			_repository = repository;
		}

		public async Task<ApiResult<int>> Handle(SeedWeaningBatchDeleteCommand request, CancellationToken cancellationToken)
		{
			var idArray = request.ids.Split(',').Select(x => Convert.ToInt32(x)).ToArray();

			var seedWeaning = await _repository.GetAsync(x => idArray.Contains(x.Id));

			foreach (var item in seedWeaning)
			{
				item.IsDel = true;
			}

			var result = await _repository.UpdateRangeAsync(seedWeaning);

			if (result > 0)
			{
				return new ApiResult<int>
				{
					Code = 200,
					msg = "成功",
					Data = result
				};
			}

			return new ApiResult<int>
			{
				Code = 201,
				msg = "失败",
				Data = result
			};
		}
	}
}

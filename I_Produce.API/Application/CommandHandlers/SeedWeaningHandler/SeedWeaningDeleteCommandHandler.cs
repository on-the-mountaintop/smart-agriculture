﻿using CommonClass;
using I_Produce.API.Application.Commands.SeedWeaningCommand;
using I_Produce.Infrastructure.Interface;
using MediatR;

namespace I_Produce.API.Application.CommandHandlers.SeedWeaningHandler
{
	public class SeedWeaningDeleteCommandHandler : IRequestHandler<SeedWeaningDeleteCommand, ApiResult<int>>
	{
		private readonly ISeedWeaningRepository _repository;

		public SeedWeaningDeleteCommandHandler(ISeedWeaningRepository repository)
		{
			_repository = repository;
		}

		public async Task<ApiResult<int>> Handle(SeedWeaningDeleteCommand request, CancellationToken cancellationToken)
		{
			var seedWeaning = await _repository.GetByIdAsync(request.id);

			if (seedWeaning != null) seedWeaning.IsDel = true;

			var result = await _repository.UpdateAsync(seedWeaning);

			if (result > 0)
			{
				return new ApiResult<int>
				{
					Code = 200,
					msg = "成功",
					Data = result
				};
			}

			return new ApiResult<int>
			{
				Code = 201,
				msg = "失败",
				Data = result
			};
		}
	}
}

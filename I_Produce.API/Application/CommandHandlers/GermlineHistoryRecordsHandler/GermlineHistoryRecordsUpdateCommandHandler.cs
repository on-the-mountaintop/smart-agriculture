﻿using AutoMapper;
using CommonClass;
using I_Produce.API.Application.Commands.GermlineHistoryRecordsCommand;
using I_Produce.Domains.Entity;
using I_Produce.Infrastructure.Interface;
using MediatR;

namespace I_Produce.API.Application.CommandHandlers.GermlineHistoryRecordsHandler
{
	public partial class GermlineHistoryRecordsUpdateCommandHandler : IRequestHandler<GermlineHistoryRecordsUpdateCommand, ApiResult<int>>
	{
		private readonly IMapper _mapper;

		private readonly IGermlineHistoryRecordsRepository _repository;

		public GermlineHistoryRecordsUpdateCommandHandler(IMapper mapper, IGermlineHistoryRecordsRepository repository)
		{
			_mapper = mapper;
			_repository = repository;
		}

		public async Task<ApiResult<int>> Handle(GermlineHistoryRecordsUpdateCommand request, CancellationToken cancellationToken)
		{
			var germline = _mapper.Map<GermlineHistoryRecords>(request);

			var result = await _repository.UpdateAsync(germline);

			if (result > 0)
			{
				return new ApiResult<int>
				{
					Code = 200,
					msg = "成功",
					Data = result
				};
			}

			return new ApiResult<int>
			{
				Code = 201,
				msg = "失败",
				Data = result
			};
		}
	}
}
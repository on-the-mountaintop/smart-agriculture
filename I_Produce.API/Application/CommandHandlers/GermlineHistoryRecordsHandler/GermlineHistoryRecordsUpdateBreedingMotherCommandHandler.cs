﻿using AutoMapper;
using CommonClass;
using I_Produce.API.Application.Commands.GermlineHistoryRecordsCommand;
using I_Produce.Infrastructure.Interface;
using MediatR;

namespace I_Produce.API.Application.CommandHandlers.GermlineHistoryRecordsHandler
{
	public class GermlineHistoryRecordsUpdateBreedingMotherCommandHandler : IRequestHandler<GermlineHistoryRecordsUpdateBreedingMotherCommand, ApiResult<int>>
	{
		private readonly IGermlineHistoryRecordsRepository _repository;

		public GermlineHistoryRecordsUpdateBreedingMotherCommandHandler(IMapper mapper, IGermlineHistoryRecordsRepository repository)
		{
			_repository = repository;
		}

		public async Task<ApiResult<int>> Handle(GermlineHistoryRecordsUpdateBreedingMotherCommand request, CancellationToken cancellationToken)
		{
			var germlineHistory = await _repository.GetByIdAsync(request.Id);

			germlineHistory.MotherEarHorn = request.MotherEarHorn;
			germlineHistory.BreedEarHorn = request.BreedEarHorn;
			germlineHistory.DateOfBreeding = request.DateOfBreeding;

			var result = await _repository.UpdateAsync(germlineHistory);

			if (result > 0)
			{
				return new ApiResult<int>
				{
					Code = 200,
					msg = "成功",
					Data = result
				};
			}

			return new ApiResult<int>
			{
				Code = 201,
				msg = "失败",
				Data = result
			};
		}
	}
}


﻿using CommonClass;
using I_Produce.API.Application.Commands.GermlineHistoryRecordsCommand;
using I_Produce.Domains.Entity;
using I_Produce.Infrastructure.Interface;
using MediatR;

namespace I_Produce.API.Application.CommandHandlers.GermlineHistoryRecordsHandler
{
    public class GermlineHistoryRecordsDeleteCommandHandler : IRequestHandler<GermlineHistoryRecordsDeleteCommand, ApiResult<int>>
    {
        private readonly IGermlineHistoryRecordsRepository _repository;

        public GermlineHistoryRecordsDeleteCommandHandler(IGermlineHistoryRecordsRepository repository)
        {
            _repository = repository;
        }

        public async Task<ApiResult<int>> Handle(GermlineHistoryRecordsDeleteCommand request, CancellationToken cancellationToken)
        {
            var germline = await _repository.GetByIdAsync(request.id);

            if (germline != null) { germline.IsDel = true; }

            var result = await _repository.UpdateAsync(germline);

            if (result > 0)
            {
                return new ApiResult<int>
                {
                    Code = 200,
                    msg = "成功",
                    Data = result
                };
            }

            return new ApiResult<int>
            {
                Code = 201,
                msg = "失败",
                Data = result
            };
        }
    }
}

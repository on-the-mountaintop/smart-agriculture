﻿using CommonClass;
using I_Produce.Domains.Entity;
using MediatR;

namespace I_Produce.API.Application.Commands.GermlineHistoryRecordsCommand
{
	public class GermlineHistoryRecordsUpdateSeedWeaningCommand : BaseSuperclass, IRequest<ApiResult<int>>
	{
		/// <summary>
		/// 自增标识列
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// 断奶日期
		/// </summary>
		public DateTime? WeaningDate { get; set; }

		/// <summary>
		/// 断奶只数
		/// </summary>
		public int? WeaningNumber { get; set; }

		/// <summary>
		/// 断奶窝重
		/// </summary>
		public decimal? WeaningLitterWeight { get; set; }
	}
}

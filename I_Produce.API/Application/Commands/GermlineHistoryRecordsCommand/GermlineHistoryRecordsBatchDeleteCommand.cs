﻿using CommonClass;
using MediatR;

namespace I_Produce.API.Application.Commands.GermlineHistoryRecordsCommand
{
    public class GermlineHistoryRecordsBatchDeleteCommand : IRequest<ApiResult<int>>
    {
        public string ids { get; set; }
    }
}

﻿using CommonClass;
using I_Produce.Domains.Entity;
using MediatR;

namespace I_Produce.API.Application.Commands.GermlineHistoryRecordsCommand
{
	public class GermlineHistoryRecordsUpdatePregnancyCommand : BaseSuperclass, IRequest<ApiResult<int>>
	{
		/// <summary>
		/// 自增标识列
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// 妊检日期
		/// </summary>
		public DateTime? PregnancyTestDate { get; set; }

		/// <summary>
		/// 妊检结果
		/// </summary>
		public string? PregnancyTestResult { get; set; }

	}
}

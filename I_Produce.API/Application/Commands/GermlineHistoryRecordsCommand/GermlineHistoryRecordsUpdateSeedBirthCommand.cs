﻿using CommonClass;
using I_Produce.Domains.Entity;
using MediatR;

namespace I_Produce.API.Application.Commands.GermlineHistoryRecordsCommand
{
	public class GermlineHistoryRecordsUpdateSeedBirthCommand : BaseSuperclass, IRequest<ApiResult<int>>
	{
		/// <summary>
		/// 自增标识列
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// 分娩日期
		/// </summary>
		public DateTime? DeliveryDate { get; set; }

		/// <summary>
		/// 健羔数
		/// </summary>
		public int? HealthyNumber { get; set; }

		/// <summary>
		/// 弱羔数
		/// </summary>
		public int? WeakLambNumber { get; set; }

		/// <summary>
		/// 畸形数
		/// </summary>
		public int? DeformityNumber { get; set; }

		/// <summary>
		/// 死胎数
		/// </summary>
		public int? NumberOfSTillbirths { get; set; }

		/// <summary>
		/// 木乃伊数
		/// </summary>
		public int? MummyNumber { get; set; }
	}
}

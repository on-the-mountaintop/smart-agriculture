﻿using CommonClass;
using MediatR;

namespace I_Produce.API.Application.Commands.GermlineHistoryRecordsCommand
{
    public class GermlineHistoryRecordsDeleteCommand : IRequest<ApiResult<int>>
    {
        public int id { get; set; }
    }
}

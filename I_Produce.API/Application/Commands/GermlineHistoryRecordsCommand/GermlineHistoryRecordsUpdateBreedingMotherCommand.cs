﻿using CommonClass;
using I_Produce.Domains.Entity;
using MediatR;

namespace I_Produce.API.Application.Commands.GermlineHistoryRecordsCommand
{
	public class GermlineHistoryRecordsUpdateBreedingMotherCommand : BaseSuperclass, IRequest<ApiResult<int>>
	{
		/// <summary>
		/// 自增标识列
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// 种母耳号
		/// </summary>
		public string? MotherEarHorn { get; set; }

		/// <summary>
		/// 种公耳号
		/// </summary>
		public string? BreedEarHorn { get; set; }

		/// <summary>
		/// 配种日期
		/// </summary>
		public DateTime? DateOfBreeding { get; set; }
	}
}

﻿using CommonClass;
using I_Produce.Domains.Entity;
using MediatR;

namespace I_Produce.API.Application.Commands.GermlineHistoryRecordsCommand
{
	public class GermlineHistoryRecordsUpdateCommand : BaseSuperclass, IRequest<ApiResult<int>>
	{
		/// <summary>
		/// 自增标识列
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// 种母耳号
		/// </summary>
		public string? MotherEarHorn { get; set; }

		/// <summary>
		/// 种公耳号
		/// </summary>
		public string? BreedEarHorn { get; set; }

		/// <summary>
		/// 配种日期
		/// </summary>
		public DateTime? DateOfBreeding { get; set; }

		/// <summary>
		/// 妊检日期
		/// </summary>
		public string? PregnancyTestDate { get; set; }

		/// <summary>
		/// 妊检结果
		/// </summary>
		public string? PregnancyTestResult { get; set; }

		/// <summary>
		/// 分娩日期
		/// </summary>
		public string? DeliveryDate { get; set; }

		/// <summary>
		/// 健羔数
		/// </summary>
		public int? HealthyNumber { get; set; }

		/// <summary>
		/// 健羔总重
		/// </summary>
		public decimal? HealthyTotalWeight { get; set; }

		/// <summary>
		/// 弱羔数
		/// </summary>
		public int? WeakLambNumber { get; set; }

		/// <summary>
		/// 畸形数
		/// </summary>
		public int? DeformityNumber { get; set; }

		/// <summary>
		/// 死胎数
		/// </summary>
		public int? NumberOfStillbirths { get; set; }

		/// <summary>
		/// 木乃伊数
		/// </summary>
		public int? MummyNumber { get; set; }

		/// <summary>
		/// 畸胎数
		/// </summary>
		public int? NumberOfFreaks { get; set; }

		/// <summary>
		/// 断奶日期
		/// </summary>
		public string? WeaningDate { get; set; }

		/// <summary>
		/// 断奶只数
		/// </summary>
		public int? WeaningNumber { get; set; }

		/// <summary>
		/// 断奶窝重
		/// </summary>
		public decimal? WeaningLitterWeight { get; set; }

		/// <summary>
		/// 当前胎次
		/// </summary>
		public int? CurrentParity { get; set; }
	}
}

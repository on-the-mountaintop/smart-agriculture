﻿using CommonClass;
using MediatR;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using I_Produce.Domains.Entity;

namespace I_Produce.API.Application.Commands.WeaningOfLambCommand
{
	public class WeaningOfLambCreateCommand : BaseSuperclass, IRequest<ApiResult<int>>
	{
		/// <summary>
		/// 羔羊耳号
		/// </summary>

		public string? LambsEar { get; set; }

		/// <summary>
		/// 断奶日龄
		/// </summary>
		public int? WeaningAge { get; set; }

		/// <summary>
		/// 断奶重
		/// </summary>
		public decimal? WeaningWeight { get; set; }

		/// <summary>
		/// 哺乳种母耳号
		/// </summary>

		public string? LamTheMotheLactatingSpeciesbsEar { get; set; }

		/// <summary>
		/// 胎次
		/// </summary>
		public int? Parity { get; set; }

		/// <summary>
		/// 断奶栋舍
		/// </summary>

		public string? WeaningHouse { get; set; }

		/// <summary>
		/// 断奶栏位
		/// </summary>

		public string? WeaningField { get; set; }

		/// <summary>
		/// 转入栋舍
		/// </summary>

		public string? TransferBuilding { get; set; }

		/// <summary>
		/// 转入栏位
		/// </summary>

		public string? DriveField { get; set; }

		/// <summary>
		/// 断奶日期
		/// </summary>
		public DateTime? WeaningDate { get; set; }
	}
}

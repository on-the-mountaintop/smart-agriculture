﻿using CommonClass;
using MediatR;

namespace I_Produce.API.Application.Commands.WeaningOfLambCommand
{
	public class WeaningOfLambBatchDeleteCommand : IRequest<ApiResult<int>>
	{

		public string ids { get; set; }
	}
}

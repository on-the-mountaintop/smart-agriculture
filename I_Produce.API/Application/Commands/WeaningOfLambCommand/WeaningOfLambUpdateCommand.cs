﻿using CommonClass;
using I_Produce.Domains.Entity;
using MediatR;

namespace I_Produce.API.Application.Commands.WeaningOfLambCommand
{
	public class WeaningOfLambUpdateCommand : BaseSuperclass, IRequest<ApiResult<int>>
	{
		/// <summary>
		/// 自增标识列
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// 羔羊耳号
		/// </summary>

		public string? LambsEar { get; set; }

		/// <summary>
		/// 断奶日龄
		/// </summary>
		public int? WeaningAge { get; set; }

		/// <summary>
		/// 断奶重
		/// </summary>
		public decimal? WeaningWeight { get; set; }

		/// <summary>
		/// 哺乳种母耳号
		/// </summary>

		public string? LamTheMotheLactatingSpeciesbsEar { get; set; }

		/// <summary>
		/// 胎次
		/// </summary>
		public int? Parity { get; set; }

		/// <summary>
		/// 断奶栋舍
		/// </summary>

		public string? WeaningHouse { get; set; }

		/// <summary>
		/// 断奶栏位
		/// </summary>

		public string? WeaningField { get; set; }

		/// <summary>
		/// 转入栋舍
		/// </summary>

		public string? TransferBuilding { get; set; }

		/// <summary>
		/// 转入栏位
		/// </summary>

		public string? DriveField { get; set; }

		/// <summary>
		/// 断奶日期
		/// </summary>
		public DateTime? WeaningDate { get; set; }
	}
}

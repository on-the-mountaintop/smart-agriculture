﻿using CommonClass;
using MediatR;

namespace I_Produce.API.Application.Commands.WeaningOfLambCommand
{
	public class WeaningOfLambDeleteCommand : IRequest<ApiResult<int>>
	{
		public int id { get; set; }
	}
}

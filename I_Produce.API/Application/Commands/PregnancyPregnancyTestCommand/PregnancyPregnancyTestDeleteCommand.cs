﻿using CommonClass;
using MediatR;

namespace I_Produce.API.Application.Commands.PregnancyPregnancyTestCommand
{
	public class PregnancyPregnancyTestDeleteCommand : IRequest<ApiResult<int>>
	{
        public int id { get; set; }
    }
}

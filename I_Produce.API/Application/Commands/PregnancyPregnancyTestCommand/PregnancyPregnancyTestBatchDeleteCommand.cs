﻿using CommonClass;
using MediatR;

namespace I_Produce.API.Application.Commands.PregnancyPregnancyTestCommand
{
	public class PregnancyPregnancyTestBatchDeleteCommand : IRequest<ApiResult<int>>
	{
		public string ids { get; set; }
	}
}

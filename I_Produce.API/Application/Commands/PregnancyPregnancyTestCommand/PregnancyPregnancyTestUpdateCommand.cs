﻿using CommonClass;
using I_Produce.Domains.Entity;
using MediatR;

namespace I_Produce.API.Application.Commands.PregnancyPregnancyTestCommand
{
	public class PregnancyPregnancyTestUpdateCommand : BaseSuperclass, IRequest<ApiResult<int>>
	{
		/// <summary>
		/// 自增标识列
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// 妊检日期
		/// </summary>
		public DateTime? PregnancyTestDate { get; set; }

		/// <summary>
		/// 耳号
		/// </summary>
		public string? Auricle { get; set; }

		/// <summary>
		/// 妊检结果
		/// </summary>
		public string? PregnancyTestResult { get; set; }

		/// <summary>
		/// 妊检方式
		/// </summary>
		public string? PregnancyTestMethod { get; set; }

		/// <summary>
		/// 同胎数
		/// </summary>
		public int? ParityNumber { get; set; }

		/// <summary>
		/// 妊检天数
		/// </summary>
		public int? PregnancyExaminationDays { get; set; }

		/// <summary>
		/// 当前栋舍
		/// </summary>
		public string? CurrentBuilding { get; set; }

		/// <summary>
		/// 当前栏位
		/// </summary>
		public string? CurrentField { get; set; }

		/// <summary>
		/// 转入栋舍
		/// </summary>
		public string? TransferBuilding { get; set; }

		/// <summary>
		/// 转入栏位
		/// </summary>
		public string? DriveField { get; set; }

		/// <summary>
		/// 负责人
		/// </summary>
		public string? PersonCharge { get; set; }

		/// <summary>
		/// 审核人
		/// </summary>
		public string? Auditor { get; set; }

		/// <summary>
		/// 审核时间
		/// </summary>
		public DateTime? AuditTime { get; set; }
	}
}

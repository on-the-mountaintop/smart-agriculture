﻿using CommonClass;
using I_Produce.Domains.Entity;
using MediatR;

namespace I_Produce.API.Application.Commands.SeedWeaningCommand
{
	public class SeedWeaningDeleteCommand : BaseSuperclass, IRequest<ApiResult<int>>
	{
		public int id { get; set; }
	}
}

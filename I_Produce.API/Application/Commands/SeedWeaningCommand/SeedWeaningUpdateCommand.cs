﻿using CommonClass;
using I_Produce.Domains.Entity;
using MediatR;

namespace I_Produce.API.Application.Commands.SeedWeaningCommand
{
	public class SeedWeaningUpdateCommand : BaseSuperclass, IRequest<ApiResult<int>>
	{
		/// <summary>
		/// 自增标识列
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// 断奶日期
		/// </summary>
		public DateTime? WeaningDate { get; set; }

		/// <summary>
		/// 耳号
		/// </summary>

		public string? Auricle { get; set; }

		/// <summary>
		/// 哺乳天数
		/// </summary>
		public int? BreastfeedingDays { get; set; }

		/// <summary>
		/// 种母重量
		/// </summary>

		public decimal? SeedWeight { get; set; }

		/// <summary>
		/// 断奶只数
		/// </summary>
		public int? WeaningNumber { get; set; }

		/// <summary>
		/// 断奶总窝重
		/// </summary>

		public decimal? TotalLitterWeightWeaning { get; set; }

		/// <summary>
		/// 当前栋舍
		/// </summary>

		public string? CurrentBuilding { get; set; }

		/// <summary>
		/// 当前栏位
		/// </summary>

		public string? CurrentField { get; set; }

		/// <summary>
		/// 转入栋舍
		/// </summary>

		public string? TransferBuilding { get; set; }

		/// <summary>
		/// 转入栏位
		/// </summary>

		public string? DriveField { get; set; }

		/// <summary>
		/// 负责人
		/// </summary>

		public string? PersonCharge { get; set; }

		/// <summary>
		/// 审核人
		/// </summary>

		public string? Auditor { get; set; }

		/// <summary>
		/// 审核时间
		/// </summary>
		public DateTime? AuditTime { get; set; }

		/// <summary>
		/// 断奶原因
		/// </summary>

		public string? WeaningReason { get; set; }
	}
}

﻿using CommonClass;
using I_Produce.Domains.Entity;
using MediatR;

namespace I_Produce.API.Application.Commands.SeedWeaningCommand
{
	public class SeedWeaningBatchDeleteCommand : BaseSuperclass, IRequest<ApiResult<int>>
	{
		public string ids { get; set; }
	}
}

﻿using CommonClass;
using MediatR;

namespace I_Produce.API.Application.Commands.SeedBirthCommand
{
	public class SeedBirthBatchDeleteCommand : IRequest<ApiResult<int>>
	{
		public string ids { get; set; }
	}
}

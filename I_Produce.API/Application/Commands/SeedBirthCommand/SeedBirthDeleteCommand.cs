﻿using CommonClass;
using MediatR;

namespace I_Produce.API.Application.Commands.SeedBirthCommand
{
	public class SeedBirthDeleteCommand : IRequest<ApiResult<int>>
	{
        public int id { get; set; }
    }
}

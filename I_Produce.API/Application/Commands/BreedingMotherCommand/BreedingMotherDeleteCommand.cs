﻿using CommonClass;
using MediatR;
using I_Produce.Domains.Entity;

namespace I_Produce.API.Application.Commands.BreedingMotherCommand
{
	public class BreedingMotherDeleteCommand : IRequest<ApiResult<int>>
	{
		public int id { get; set; }
	}
}

﻿using CommonClass;
using MediatR;
using I_Produce.Domains.Entity;

namespace I_Produce.API.Application.Commands.BreedingMotherCommand
{
	public class BreedingMotherBatchDeleteCommand : IRequest<ApiResult<int>>
	{
		public string ids { get; set; }
	}
}

﻿using CommonClass;
using MediatR;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using I_Produce.Domains.Entity;

namespace I_Produce.API.Application.Commands.BreedingMotherCommand
{
	public class BreedingMotherCreateCommand : BaseSuperclass, IRequest<ApiResult<int>>
	{
		/// <summary>
		/// 配种日期
		/// </summary>
		public DateTime? DateOfBreeding { get; set; }

		/// <summary>
		/// 上次配种日期
		/// </summary>
		public DateTime? LastDateOfBreeding { get; set; }

		/// <summary>
		/// 耳号
		/// </summary>
		public string? Auricle { get; set; }

		/// <summary>
		/// 种母品种
		/// </summary>
		public string? MotherVariety { get; set; }

		/// <summary>
		/// 首配公羊
		/// </summary>

		public string? FirstWithRam { get; set; }

		/// <summary>
		/// 种公品种
		/// </summary>

		public string? MaleBreed { get; set; }

		/// <summary>
		/// 配种方式
		/// </summary>

		public string? BreedingPattern { get; set; }

		/// <summary>
		/// 种母发情类型
		/// </summary>

		public string? SpeciesEstrusType { get; set; }

		/// <summary>
		/// 首配评分
		/// </summary>

		public string? FirstScore { get; set; }

		/// <summary>
		/// 当前栋舍
		/// </summary>

		public string? CurrentBuilding { get; set; }

		/// <summary>
		/// 当前栏位
		/// </summary>

		public string? CurrentField { get; set; }

		/// <summary>
		/// 转入栋舍
		/// </summary>

		public string? TransferBuilding { get; set; }

		/// <summary>
		/// 转入栏位
		/// </summary>

		public string? DriveField { get; set; }

		/// <summary>
		/// 繁殖状态
		/// </summary>
		public string? ReproductiveState { get; set; }

		/// <summary>
		/// 重量(公斤)
		/// </summary>

		public decimal? Weight { get; set; }

		/// <summary>
		/// 事件日期
		/// </summary>
		public DateTime? EventDate { get; set; }

		/// <summary>
		/// 负责人
		/// </summary>

		public string? PersonCharge { get; set; }

		/// <summary>
		/// 审核人
		/// </summary>

		public string? Auditor { get; set; }

		/// <summary>
		/// 审核时间
		/// </summary>
		public DateTime? AuditTime { get; set; }
	}
}

﻿using CommonClass;
using I_Produce.Domains.Entity;
using MediatR;

namespace I_Produce.API.Application.Commands.ProgenyTransferCommand
{
	public class ProgenyTransferUpdateCommand : BaseSuperclass, IRequest<ApiResult<int>>
	{
		/// <summary>
		/// 自增标识列
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// 转种日期
		/// </summary>
		public DateTime? DateConversion { get; set; }

		/// <summary>
		/// 育成羊耳号
		/// </summary>

		public string? GrowSheepEars { get; set; }

		/// <summary>
		/// 当前栋舍
		/// </summary>

		public string? CurrentBuilding { get; set; }

		/// <summary>
		/// 当前栏位
		/// </summary>

		public string? CurrentField { get; set; }

		/// <summary>
		/// 转入栋舍
		/// </summary>

		public string? TransferBuilding { get; set; }

		/// <summary>
		/// 转入栏位
		/// </summary>

		public string? DriveField { get; set; }

		/// <summary>
		/// 性别
		/// </summary>
		public bool Sex { get; set; }

		/// <summary>
		/// 转种日龄
		/// </summary>
		public int? AgeTransplanting { get; set; }
	}
}

﻿using CommonClass;
using MediatR;

namespace I_Produce.API.Application.Commands.ProgenyTransferCommand
{
	public class ProgenyTransferDeleteCommand : IRequest<ApiResult<int>>
	{
		public int id { get; set; }
	}
}

﻿using CommonClass;
using MediatR;

namespace I_Produce.API.Application.Commands.ProgenyTransferCommand
{
	public class ProgenyTransferBatchDeleteCommand : IRequest<ApiResult<int>>
	{
		public string ids { get; set; }
	}
}

﻿using CommonClass;
using I_Produce.Domains.Entity;
using MediatR;

namespace I_Produce.API.Application.Commands.SheepRescissionCommand
{
    public class SheepRescissionBatchDeleteCommand : BaseSuperclass, IRequest<ApiResult<int>>
    {
        public string ids { get; set; }
    }
}

﻿using CommonClass;
using I_Produce.Domains.Entity;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace I_Produce.API.Application.Commands.SheepRescissionCommand
{
    public class SheepRescissionCreateCommand : BaseSuperclass, IRequest<ApiResult<int>>
    {
        /// <summary>
        /// 转舍日期
        /// </summary>
        public DateTime? SurrenderDate { get; set; }

        /// <summary>
        /// 羊只耳号
        /// </summary>
        public string? VisualEarSignal { get; set; }

        /// <summary>
        /// 转舍类型
        /// </summary>
        public string? RoundType { get; set; }

        /// <summary>
        /// 转出栋舍
        /// </summary>
        public string? TurnBuilding { get; set; }

        /// <summary>
        /// 转出栏位
        /// </summary>
        public string? RolloutField { get; set; }

        /// <summary>
        /// 转入栋舍
        /// </summary>
        public string? TransferBuilding { get; set; }

        /// <summary>
        /// 转入栏位
        /// </summary>
        public string? DriveField { get; set; }

        /// <summary>
        /// 品种
        /// </summary>
        public string? Breed { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        public bool Sex { get; set; }

        /// <summary>
        /// 审核人
        /// </summary>
        public string? Auditor { get; set; }

        /// <summary>
        /// 审核时间
        /// </summary>
        public DateTime? AuditTime { get; set; }
    }

    
}

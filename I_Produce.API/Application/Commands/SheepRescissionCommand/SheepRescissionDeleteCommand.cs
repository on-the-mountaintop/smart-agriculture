﻿using CommonClass;
using I_Produce.Domains.Entity;
using MediatR;

namespace I_Produce.API.Application.Commands.SheepRescissionCommand
{
    public class SheepRescissionDeleteCommand : BaseSuperclass, IRequest<ApiResult<int>>
    {
        public int id { get; set; }
    }
}

﻿using CommonClass;
using I_Produce.API.Application.Commands.SheepRescissionCommand;
using I_Produce.Domains.Entity;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Produce.API.Controllers
{
    /// <summary>
    /// 羊只转舍API接口(写)
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class SheepRescissionController : ControllerBase
    {
        private readonly IMediator _mediator;

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="mediator"></param>
        public SheepRescissionController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// 羊只转舍的添加功能
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public Task<ApiResult<int>> AddSheepSheepRescission(SheepRescissionCreateCommand command)
        {
            return _mediator.Send(command);
        }

        /// <summary>
        /// 羊只转舍的修改功能
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        public Task<ApiResult<int>> UpdateSheepSheepRescission(SheepRescissionUpdateCommand command)
        {
            return _mediator.Send(command);
        }

        /// <summary>
        /// 羊只转舍的删除功能
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpDelete]
        public Task<ApiResult<int>> RemoveSheepSheepRescission([FromQuery] SheepRescissionDeleteCommand command)
        {
            return _mediator.Send(command);
        }

        /// <summary>
        /// 羊只转舍的批量删除功能
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpDelete]
        public Task<ApiResult<int>> BatchRemoveSheepRescission([FromQuery] SheepRescissionBatchDeleteCommand command)
        {
            return _mediator.Send(command);
        }
    }
}

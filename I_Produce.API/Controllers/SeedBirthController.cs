﻿using CommonClass;
using I_Produce.API.Application.Commands.SeedBirthCommand;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Produce.API.Controllers
{
	/// <summary>
	/// 种母分娩API接口(写)
	/// </summary>
	[Route("api/[controller]/[action]")]
	[ApiController]
	public class SeedBirthController : ControllerBase
	{
		private readonly IMediator _mediator;

		/// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="mediator"></param>
		public SeedBirthController(IMediator mediator)
		{
			_mediator = mediator;
		}

		/// <summary>
		/// 种母分娩的添加功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpPost]
		public Task<ApiResult<int>> AddSeedBirth(SeedBirthCreateCommand command)
		{
			return _mediator.Send(command);
		}

		/// <summary>
		/// 种母分娩的修改功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpPut]
		public Task<ApiResult<int>> UpdateSeedBirth(SeedBirthUpdateCommand command)
		{
			return _mediator.Send(command);
		}

		/// <summary>
		/// 种母分娩的删除功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpDelete]
		public Task<ApiResult<int>> RemoveSeedBirth([FromQuery] SeedBirthDeleteCommand command)
		{
			return _mediator.Send(command);
		}

		/// <summary>
		/// 种母分娩的批量删除功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpDelete]
		public Task<ApiResult<int>> BatchRemoveSeedBirth([FromQuery] SeedBirthBatchDeleteCommand command)
		{
			return _mediator.Send(command);
		}
	}
}

﻿using CommonClass;
using I_Produce.API.Application.Commands.SeedWeaningCommand;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Produce.API.Controllers
{
	/// <summary>
	/// 种母断奶API接口(写)
	/// </summary>
	[Route("api/[controller]/[action]")]
	[ApiController]
	public class SeedWeaningController : ControllerBase
	{
		private readonly IMediator _mediator;

		/// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="mediator"></param>
		public SeedWeaningController(IMediator mediator)
		{
			_mediator = mediator;
		}

		/// <summary>
		/// 种母断奶的添加功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpPost]
		public Task<ApiResult<int>> AddSeedWeaning(SeedWeaningCreateCommand command)
		{
			return _mediator.Send(command);
		}

		/// <summary>
		/// 种母断奶的修改功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpPut]
		public Task<ApiResult<int>> UpdateSeedWeaning(SeedWeaningUpdateCommand command)
		{
			return _mediator.Send(command);
		}

		/// <summary>
		/// 种母断奶的删除功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpDelete]
		public Task<ApiResult<int>> RemoveSeedWeaning([FromQuery] SeedWeaningDeleteCommand command)
		{
			return _mediator.Send(command);
		}

		/// <summary>
		/// 种母断奶的批量删除功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpDelete]
		public Task<ApiResult<int>> BatchRemoveSeedWeaning([FromQuery] SeedWeaningBatchDeleteCommand command)
		{
			return _mediator.Send(command);
		}
	}
}

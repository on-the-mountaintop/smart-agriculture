﻿using CommonClass;
using I_Produce.API.Application.Commands.GermlineHistoryRecordsCommand;
using I_Produce.Domains.Entity;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Produce.API.Controllers
{
	/// <summary>
	/// 种母历史记录API接口(写)
	/// </summary>
	[Route("api/[controller]/[action]")]
	[ApiController]
	public class GermlineHistoryRecordsController : ControllerBase
	{
		private readonly IMediator _mediator;

		/// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="mediator"></param>
		public GermlineHistoryRecordsController(IMediator mediator)
		{
			_mediator = mediator;
		}

		/// <summary>
		/// 种母历史记录的添加功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpPost]
		public async Task<ApiResult<int>> AddGermlineHistoryRecords(GermlineHistoryRecordsCreateCommand command)
		{
			return await _mediator.Send(command);
		}

		/// <summary>
		/// 种母历史记录的修改功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpPut]
		public async Task<ApiResult<int>> UpdateGermlineHistoryRecords(GermlineHistoryRecordsUpdateCommand command)
		{
			return await _mediator.Send(command);
		}

		/// <summary>
		/// 种母历史记录的修改妊检信息功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpPut]
		public async Task<ApiResult<int>> UpdateGermlineHistoryRecordsPregnancy(GermlineHistoryRecordsUpdatePregnancyCommand command)
		{
			return await _mediator.Send(command);
		}

		/// <summary>
		/// 种母历史记录的修改配种信息功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpPut]
		public async Task<ApiResult<int>> UpdateGermlineHistoryRecordsBreedingMother(GermlineHistoryRecordsUpdateBreedingMotherCommand command)
		{
			return await _mediator.Send(command);
		}

		/// <summary>
		/// 种母历史记录的修改分娩信息功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpPut]
		public async Task<ApiResult<int>> UpdateGermlineHistoryRecordsSeedBirth(GermlineHistoryRecordsUpdateSeedBirthCommand command)
		{
			return await _mediator.Send(command);
		}
		
		/// <summary>
		/// 种母历史记录的修改断奶信息功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpPut]
		public async Task<ApiResult<int>> UpdateGermlineHistoryRecordsSeedWeaning(GermlineHistoryRecordsUpdateSeedWeaningCommand command)
		{
			return await _mediator.Send(command);
		}

		/// <summary>
		/// 种母历史记录的删除功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpDelete]
		public async Task<ApiResult<int>> RemoveGermlineHistoryRecords([FromQuery] GermlineHistoryRecordsDeleteCommand command)
		{
			return await _mediator.Send(command);
		}

		/// <summary>
		/// 种母历史记录的批量删除功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpDelete]
		public async Task<ApiResult<int>> BatchRemoveGermlineHistoryRecords([FromQuery] GermlineHistoryRecordsBatchDeleteCommand command)
		{
			return await _mediator.Send(command);
		}
	}
}

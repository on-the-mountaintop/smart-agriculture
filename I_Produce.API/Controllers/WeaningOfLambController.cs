﻿using CommonClass;
using I_Produce.API.Application.Commands.WeaningOfLambCommand;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Produce.API.Controllers
{
	/// <summary>
	/// 羔羊断奶API接口(写)
	/// </summary>
	[Route("api/[controller]/[action]")]
	[ApiController]
	public class WeaningOfLambController : ControllerBase
	{
		private readonly IMediator _mediator;

		/// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="mediator"></param>
		public WeaningOfLambController(IMediator mediator)
		{
			_mediator = mediator;
		}

		/// <summary>
		/// 羔羊断奶的添加功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpPost]
		public async Task<ApiResult<int>> AddWeaningOfLamb(WeaningOfLambCreateCommand command)
		{
			return await _mediator.Send(command);
		}

		/// <summary>
		/// 羔羊断奶的修改功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpPut]
		public async Task<ApiResult<int>> UpdateWeaningOfLamb(WeaningOfLambUpdateCommand command)
		{
			return await _mediator.Send(command);
		}

		/// <summary>
		/// 羔羊断奶的删除功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpDelete]
		public async Task<ApiResult<int>> RemoveWeaningOfLamb([FromQuery] WeaningOfLambDeleteCommand command)
		{
			return await _mediator.Send(command);
		}

		/// <summary>
		/// 羔羊断奶的批量删除功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpDelete]
		public async Task<ApiResult<int>> BatchRemoveWeaningOfLamb([FromQuery] WeaningOfLambBatchDeleteCommand command)
		{
			return await _mediator.Send(command);
		}
	}
}

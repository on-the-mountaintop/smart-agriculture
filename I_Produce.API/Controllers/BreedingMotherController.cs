﻿using CommonClass;
using I_Produce.API.Application.Commands.BreedingMotherCommand;
using I_Produce.Domains.Entity;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Produce.API.Controllers
{
	/// <summary>
	/// 种母配种API接口(写)
	/// </summary>
	[Route("api/[controller]/[action]")]
	[ApiController]
	public class BreedingMotherController : ControllerBase
	{
		private readonly IMediator _mediator;

		/// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="mediator"></param>
		public BreedingMotherController(IMediator mediator)
		{
			_mediator = mediator;
		}

		/// <summary>
		/// 种母配种的添加功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpPost]
		public Task<ApiResult<int>> AddBreedingMother(BreedingMotherCreateCommand command)
		{
			return _mediator.Send(command);
		}

		/// <summary>
		/// 种母配种的修改功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpPut]
		public Task<ApiResult<int>> UpdateBreedingMother(BreedingMotherUpdateCommand command)
		{
			return _mediator.Send(command);
		}

		/// <summary>
		/// 种母配种的删除功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpDelete]
		public Task<ApiResult<int>> RemoveBreedingMother([FromQuery] BreedingMotherDeleteCommand command)
		{
			return _mediator.Send(command);
		}

		/// <summary>
		/// 种母配种的批量删除功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpDelete]
		public Task<ApiResult<int>> BatchRemoveBreedingMother([FromQuery] BreedingMotherBatchDeleteCommand command)
		{
			return _mediator.Send(command);
		}
	}
}

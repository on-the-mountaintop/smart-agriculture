﻿using CommonClass;
using I_Produce.API.Application.Commands.PregnancyPregnancyTestCommand;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Produce.API.Controllers
{
	/// <summary>
	/// 种母妊检API接口(写)
	/// </summary>
	[Route("api/[controller]/[action]")]
	[ApiController]
	public class PregnancyPregnancyTestController : ControllerBase
	{
		private readonly IMediator _mediator;

		/// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="mediator"></param>
		public PregnancyPregnancyTestController(IMediator mediator)
		{
			_mediator = mediator;
		}

		/// <summary>
		/// 种母妊检的添加功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpPost]
		public Task<ApiResult<int>> AddPregnancyPregnancyTest(PregnancyPregnancyTestCreateCommand command)
		{
			return _mediator.Send(command);
		}

		/// <summary>
		/// 种母妊检的修改功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpPut]
		public Task<ApiResult<int>> UpdatePregnancyPregnancyTest(PregnancyPregnancyTestUpdateCommand command)
		{
			return _mediator.Send(command);
		}

		/// <summary>
		/// 种母妊检的删除功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpDelete]
		public Task<ApiResult<int>> RemovePregnancyPregnancyTest([FromQuery] PregnancyPregnancyTestDeleteCommand command)
		{
			return _mediator.Send(command);
		}

		/// <summary>
		/// 种母妊检的批量删除功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpDelete]
		public Task<ApiResult<int>> BatchRemovePregnancyPregnancyTest([FromQuery] PregnancyPregnancyTestBatchDeleteCommand command)
		{
			return _mediator.Send(command);
		}
	}
}

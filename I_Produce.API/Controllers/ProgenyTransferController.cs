﻿using CommonClass;
using I_Produce.API.Application.Commands.ProgenyTransferCommand;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Produce.API.Controllers
{
	/// <summary>
	/// 后裔转种API接口(写)
	/// </summary>
	[Route("api/[controller]/[action]")]
	[ApiController]
	public class ProgenyTransferController : ControllerBase
	{
		private readonly IMediator _mediator;

		/// <summary>
		/// 构造函数
		/// </summary>
		/// <param name="mediator"></param>
		public ProgenyTransferController(IMediator mediator)
		{
			_mediator = mediator;
		}

		/// <summary>
		/// 后裔转种的添加功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpPost]
		public async Task<ApiResult<int>> AddProgenyTransfer(ProgenyTransferCreateCommand command)
		{
			return await _mediator.Send(command);
		}

		/// <summary>
		/// 后裔转种的修改功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpPut]
		public async Task<ApiResult<int>> UpdateProgenyTransfer(ProgenyTransferUpdateCommand command)
		{
			return await _mediator.Send(command);
		}

		/// <summary>
		/// 后裔转种的删除功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpDelete]
		public async Task<ApiResult<int>> RemoveProgenyTransfer([FromQuery] ProgenyTransferDeleteCommand command)
		{
			return await _mediator.Send(command);
		}

		/// <summary>
		/// 后裔转种的批量删除功能
		/// </summary>
		/// <param name="command"></param>
		/// <returns></returns>
		[HttpDelete]
		public async Task<ApiResult<int>> BatchRemoveProgenyTransfer([FromQuery] ProgenyTransferBatchDeleteCommand command)
		{
			return await _mediator.Send(command);
		}
	}
}

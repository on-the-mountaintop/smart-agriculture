﻿using AutoMapper;
using I_Produce.API.Application.Commands.BreedingMotherCommand;
using I_Produce.API.Application.Commands.GermlineHistoryRecordsCommand;
using I_Produce.API.Application.Commands.PregnancyPregnancyTestCommand;
using I_Produce.API.Application.Commands.ProgenyTransferCommand;
using I_Produce.API.Application.Commands.SeedBirthCommand;
using I_Produce.API.Application.Commands.SeedWeaningCommand;
using I_Produce.API.Application.Commands.SheepRescissionCommand;
using I_Produce.API.Application.Commands.WeaningOfLambCommand;
using I_Produce.Domains.Entity;

namespace I_Produce.API.Extensions
{
	public class ServiceProfile : Profile
	{
		public ServiceProfile()
		{
			CreateMap<GermlineHistoryRecords, GermlineHistoryRecordsCreateCommand>().ReverseMap();
			CreateMap<GermlineHistoryRecords, GermlineHistoryRecordsUpdateCommand>().ReverseMap();

			CreateMap<SheepRescission, SheepRescissionCreateCommand>().ReverseMap();
			CreateMap<SheepRescission, SheepRescissionUpdateCommand>().ReverseMap();

			CreateMap<BreedingMother, BreedingMotherCreateCommand>().ReverseMap();
			CreateMap<BreedingMother, BreedingMotherUpdateCommand>().ReverseMap();

			CreateMap<PregnancyPregnancyTest, PregnancyPregnancyTestCreateCommand>().ReverseMap();
			CreateMap<PregnancyPregnancyTest, PregnancyPregnancyTestUpdateCommand>().ReverseMap();

			CreateMap<SeedBirth, SeedBirthCreateCommand>().ReverseMap();
			CreateMap<SeedBirth, SeedBirthUpdateCommand>().ReverseMap();

			CreateMap<SeedWeaning, SeedWeaningCreateCommand>().ReverseMap();
			CreateMap<SeedWeaning, SeedWeaningUpdateCommand>().ReverseMap();

			CreateMap<WeaningOfLamb, WeaningOfLambCreateCommand>().ReverseMap();
			CreateMap<WeaningOfLamb, WeaningOfLambUpdateCommand>().ReverseMap();

			CreateMap<ProgenyTransfer, ProgenyTransferCreateCommand>().ReverseMap();
			CreateMap<ProgenyTransfer, ProgenyTransferUpdateCommand>().ReverseMap();
		}
	}
}

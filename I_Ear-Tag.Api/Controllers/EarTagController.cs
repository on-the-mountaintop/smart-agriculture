﻿using I_Ear_Tag.Api.Application.Command.EarTagCommand;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Ear_Tag.Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class EarTagController : ControllerBase
    {
        private readonly IMediator _mediator;
        public EarTagController(IMediator mediator)
        {
            _mediator = mediator;
        }
        /// <summary>
        /// 新增产羔耳标管理
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<int> EarTagCreate(EarTagCreateCommand command)
        {
            return await _mediator.Send(command);
        }
        /// <summary>
        /// 逻辑删除产羔耳标管理
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<int> EarTagDelete(EarTagDeleteCommand command)
        {
            return await _mediator.Send(command);
        }
        /// <summary>
        /// 批量删除耳标信息
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<int> EarTagBatchDelete([FromQuery] EarTagBatchDeleteCommand command)
        {
            return await _mediator.Send(command);
        }
        /// <summary>
        /// 修改产羔耳标管理
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<int> EarTagUpdate(EarTagUpdateCommand command)
        {
            return await _mediator.Send(command);
        }
        /// <summary>
        /// 新增羊只戴标
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<int> MarkCreate(MarkCreateCommand command)
        {
            return await _mediator.Send(command);
        }
        /// <summary>
        /// 逻辑删除羊只戴标
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<int> MarkDelete(MarkDeleteCommand command)
        {
            return await _mediator.Send(command);
        }
        /// <summary>
        /// 修改羊只戴标
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<int> MarkUpdate(MarkUpdateCommand command)
        {
            return await _mediator.Send(command);
        }
      
    }
}

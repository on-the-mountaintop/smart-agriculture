using I_Ear_Tag.Api.Extensions;
using I_Ear_Tag.Infrastructure;
using I_Ear_Tag.Infrastructure.Impl;
using I_Ear_Tag.Infrastructure.Interface;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(x =>
{
    string zhuShi = AppDomain.CurrentDomain.BaseDirectory + "I_EarTag.Api.xml";
    x.IncludeXmlComments(zhuShi, true);
});
//添加jwt授权验证
builder.Services.AddJWT(builder);
//注入数据库上下文
builder.Services.AddDbContext<EFDbContext>(x => x.UseSqlServer(builder.Configuration.GetConnectionString("Conser")));
//注入中介者模式
builder.Services.AddMediatR(Assembly.GetExecutingAssembly());

//注入AutoMapper
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

//注入IEarTagRepository接口实现
builder.Services.AddScoped<IEarTagRepository, EarTagRepository>();
// 注册IMarkRepository接口的实现
builder.Services.AddScoped<IMarkRepository, MarkRepository>();


var app = builder.Build();

// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
    
//}

app.UseSwagger();
app.UseSwaggerUI();
//app.UseAuthentication();//身份认证
app.UseAuthorization();//授权


app.MapControllers();

app.Run();

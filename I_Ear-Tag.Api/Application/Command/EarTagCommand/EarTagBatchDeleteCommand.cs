﻿using MediatR;

namespace I_Ear_Tag.Api.Application.Command.EarTagCommand
{
    /// <summary>
    /// 用户批量逻辑删除
    /// </summary>
    public class EarTagBatchDeleteCommand: IRequest<int>
    {
        /// <summary>
        /// 耳标id
        /// </summary>
        public string EarTagId { get; set; }
    }
}

﻿using I_Ear_Tag.Demoin;
using I_Ear_Tag.Demoin.EarTagManagement;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace I_Ear_Tag.Api.Application.Command.EarTagCommand
{
    public class MarkUpdateCommand: AuditField, IRequest<int>
    {
        /// <summary>
        /// 羊只戴标Id
        /// </summary>
        public int MarkId { get; set; }
        /// <summary>
        /// 羊只耳号
        /// </summary>
        [StringLength(30)]
        public string? EarNumber { get; set; }
        /// <summary>
        /// 当前电子耳号
        /// </summary>
        [StringLength(30)]
        public string? OldEarTag { get; set; }
        /// <summary>
        /// 新电子耳号
        /// </summary>
        [StringLength(30)]
        public string? NewEarTag { get; set; }
        /// <summary>
        /// 戴标原因
        /// </summary>
        public Cause Reason { get; set; }
    }
}

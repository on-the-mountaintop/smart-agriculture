﻿using MediatR;
using System.ComponentModel.DataAnnotations;

namespace I_Ear_Tag.Api.Application.Command.EarTagCommand
{
    public class EarTagDeleteCommand:IRequest<int>
    {
        /// <summary>
        /// 耳标id
        /// </summary>
        public int EarTagId { get; set; }
    }
}

﻿using I_Ear_Tag.Demoin;
using I_Ear_Tag.Demoin.EarTagManagement;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace I_Ear_Tag.Api.Application.Command.EarTagCommand
{
    public class EarTagUpdateCommand: AuditField, IRequest<int>
    {
        /// <summary>
        /// 耳标id
        /// </summary>
        public int EarTagId { get; set; }
        /// <summary>
        /// 对应出生状态
        /// </summary>
        public State BirthStatus { get; set; }
        /// <summary>
        /// 对应品种
        /// </summary>
        public Breed SheepBreed { get; set; }
        /// <summary>
        /// 添加方式
        /// </summary>
        public int InsertType { get; set; }
        /// <summary>
        /// 羊只耳号
        /// </summary>
        [StringLength(30)]
        public string? EarNumber { get; set; }
        /// <summary>
        /// 入库日期
        /// </summary>
        public DateTime? StorageDate { get; set; }
    }
}

﻿using MediatR;

namespace I_Ear_Tag.Api.Application.Command.EarTagCommand
{
    public class MarkDeleteCommand:IRequest<int>
    {
        /// <summary>
        /// 羊只戴标Id
        /// </summary>
        public int MarkId { get; set; }
    }
}

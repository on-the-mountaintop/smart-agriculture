﻿using I_Ear_Tag.Api.Application.Command.EarTagCommand;
using I_Ear_Tag.Infrastructure.Interface;
using MediatR;

namespace I_Ear_Tag.Api.Application.CommandHandlers.EarTagHandler
{
    public class EarTagDeleteCommandHandler : IRequestHandler<EarTagDeleteCommand, int>
    {
        private readonly IEarTagRepository earTagRepository;
        public EarTagDeleteCommandHandler(IEarTagRepository earTagRepository)
        {
            this.earTagRepository = earTagRepository;
        }
        /// <summary>
        /// 逻辑删除耳标管理信息
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<int> Handle(EarTagDeleteCommand request, CancellationToken cancellationToken)
        {
            var res = await earTagRepository.GetModelAsync(request.EarTagId);
            res.IsDel = false;
            return await earTagRepository.StateDelete(res);
        }
    }
}

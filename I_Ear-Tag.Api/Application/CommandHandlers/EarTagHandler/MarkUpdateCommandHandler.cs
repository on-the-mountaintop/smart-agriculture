﻿using AutoMapper;
using I_Ear_Tag.Api.Application.Command.EarTagCommand;
using I_Ear_Tag.Demoin.EarTagManagement;
using I_Ear_Tag.Infrastructure.Interface;
using MediatR;

namespace I_Ear_Tag.Api.Application.CommandHandlers.EarTagHandler
{
    public class MarkUpdateCommandHandler : IRequestHandler<MarkUpdateCommand, int>
    {
        private readonly IMarkRepository markRepository;
        private readonly IMapper mapper;
        public MarkUpdateCommandHandler(IMarkRepository markRepository, IMapper mapper)
        {
            this.markRepository = markRepository;
            this.mapper = mapper;
        }
        /// <summary>
        /// 修改羊只戴标
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<int> Handle(MarkUpdateCommand request, CancellationToken cancellationToken)
        {
            var res = mapper.Map<Mark>(request);
            return await markRepository.UpdateAsync(res);
        }
    }
}

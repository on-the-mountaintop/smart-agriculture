﻿using AutoMapper;
using I_Ear_Tag.Api.Application.Command.EarTagCommand;
using I_Ear_Tag.Demoin;
using I_Ear_Tag.Demoin.EarTagManagement;
using I_Ear_Tag.Infrastructure.Interface;
using MediatR;

namespace I_Ear_Tag.Api.Application.CommandHandlers.EarTagHandler
{
    public class EarTagCreateCommandHandler : IRequestHandler<EarTagCreateCommand, int>
    {
       
        private readonly IEarTagRepository repository;
        private readonly IMapper mapper;
        public EarTagCreateCommandHandler(IEarTagRepository repository, IMapper mapper)
        {
            this.repository = repository;
            this.mapper = mapper;
        }
        /// <summary>
        /// 新增产羔耳标管理
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<int> Handle(EarTagCreateCommand request, CancellationToken cancellationToken)
        {
            var res = mapper.Map<EarTag>(request);
            return await repository.AddAsync(res);
        }

    }
}

﻿using I_Ear_Tag.Api.Application.Command.EarTagCommand;
using I_Ear_Tag.Infrastructure.Interface;
using MediatR;

namespace I_Ear_Tag.Api.Application.CommandHandlers.EarTagHandler
{
    public class EarTagBatchDeleteCommandHandler : IRequestHandler<EarTagBatchDeleteCommand, int>
    {
        private readonly IEarTagRepository earTagRepository;
        public EarTagBatchDeleteCommandHandler(IEarTagRepository earTagRepository)
        {
            this.earTagRepository = earTagRepository;
        }
        /// <summary>
        /// 批量逻辑删除耳标信息
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<int> Handle(EarTagBatchDeleteCommand request, CancellationToken cancellationToken)
        {
            var idArray = request.EarTagId.Split(',').Select(x => Convert.ToInt32(x)).ToArray();

            var result = await earTagRepository.GetAsync(x => idArray.Contains(x.EarTagId));

            foreach (var item in result)
            {
                item.IsDel = false;
            }

            return await earTagRepository.UpdateAsync(result);
        }
    }
}

﻿using AutoMapper;
using I_Ear_Tag.Api.Application.Command.EarTagCommand;
using I_Ear_Tag.Demoin.EarTagManagement;
using I_Ear_Tag.Infrastructure.Interface;
using MediatR;

namespace I_Ear_Tag.Api.Application.CommandHandlers.EarTagHandler
{
    public class MarkCreateCommandHandler : IRequestHandler<MarkCreateCommand, int>
    {
        private readonly IMarkRepository markRepository;
        private readonly IMapper mapper;
        public MarkCreateCommandHandler(IMarkRepository markRepository, IMapper mapper)
        {
            this.markRepository = markRepository;
            this.mapper = mapper;
        }
        /// <summary>
        /// 新增羊只戴标
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<int> Handle(MarkCreateCommand request, CancellationToken cancellationToken)
        {
            //return markRepository.AddAsync(new Mark
            //{
            //    CreateBy = request.CreateBy,
            //    CreateDate = request.CreateDate,
            //    UpdateBy = request.UpdateBy,
            //    UpdateDate = request.UpdateDate,
            //    IsDel = request.IsDel,
            //    EarNumber = request.EarNumber,
            //    OldEarTag = request.OldEarTag,
            //    NewEarTag = request.NewEarTag,
            //    Reason = request.Reason,
            //});
            var res = mapper.Map<Mark>(request);
            return await markRepository.AddAsync(res);
        }
    }
}

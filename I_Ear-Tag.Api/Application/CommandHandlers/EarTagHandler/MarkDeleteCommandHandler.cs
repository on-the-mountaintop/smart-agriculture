﻿using I_Ear_Tag.Api.Application.Command.EarTagCommand;
using I_Ear_Tag.Infrastructure.Interface;
using MediatR;

namespace I_Ear_Tag.Api.Application.CommandHandlers.EarTagHandler
{
    public class MarkDeleteCommandHandler : IRequestHandler<MarkDeleteCommand, int>
    {
        private readonly IMarkRepository markRepository;
        public MarkDeleteCommandHandler(IMarkRepository markRepository)
        {
            this.markRepository = markRepository;
        }
        /// <summary>
        /// 逻辑删除羊只戴标
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<int> Handle(MarkDeleteCommand request, CancellationToken cancellationToken)
        {
            var res = await markRepository.GetModelAsync(request.MarkId);
            res.IsDel = false;
            return await markRepository.StateDelete(res);
        }
    }
}

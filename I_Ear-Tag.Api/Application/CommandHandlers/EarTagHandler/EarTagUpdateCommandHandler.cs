﻿using AutoMapper;
using I_Ear_Tag.Api.Application.Command.EarTagCommand;
using I_Ear_Tag.Demoin.EarTagManagement;
using I_Ear_Tag.Infrastructure.Interface;
using MediatR;

namespace I_Ear_Tag.Api.Application.CommandHandlers.EarTagHandler
{
    public class EarTagUpdateCommandHandler : IRequestHandler<EarTagUpdateCommand, int>
    {
        private readonly IEarTagRepository earTagRepository;
        private readonly IMapper mapper;
        public EarTagUpdateCommandHandler(IEarTagRepository earTagRepository, IMapper mapper)
        {
            this.earTagRepository = earTagRepository;
            this.mapper = mapper;
        }
        /// <summary>
        /// 修改产羔耳标管理
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<int> Handle(EarTagUpdateCommand request, CancellationToken cancellationToken)
        {
            var res = mapper.Map<EarTag>(request);
            return await earTagRepository.UpdateAsync(res);
        }
    }
}

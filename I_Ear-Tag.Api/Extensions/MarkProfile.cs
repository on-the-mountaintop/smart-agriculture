﻿using AutoMapper;
using I_Ear_Tag.Api.Application.Command.EarTagCommand;
using I_Ear_Tag.Demoin.EarTagManagement;

namespace I_Ear_Tag.Api.Extensions
{
    public class MarkProfile:Profile
    {
        public MarkProfile() 
        {
            //新增
            CreateMap<MarkCreateCommand, Mark>().ReverseMap();
            //修改
            CreateMap<MarkUpdateCommand, Mark>().ReverseMap();
        }
    }
}

﻿using AutoMapper;
using I_Ear_Tag.Api.Application.Command.EarTagCommand;
using I_Ear_Tag.Demoin.EarTagManagement;

namespace I_Ear_Tag.Api.Extensions
{
    public class EarTagProfile: Profile
    {
        public EarTagProfile() 
        {
            //新增
            CreateMap<EarTagCreateCommand, EarTag>().ReverseMap();
            //修改
            CreateMap<EarTagUpdateCommand, EarTag>().ReverseMap();
        }
    }
}

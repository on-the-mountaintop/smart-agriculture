﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Produce.Domains.DTO
{
	/// <summary>
	/// 转舍类型
	/// </summary>
	public enum RoundType
	{
		种母配种转舍 = 1,
		种母妊检转舍,
		种母分娩转舍,
		种母产羔转舍,
		种母断奶转舍,
		后备转种转舍,
		羊只期初转舍,
		羊只采购转舍,
		公羊调教转舍,
		羔羊断奶转舍,
		产羔登记转舍,
		后裔转种转舍,
		羊只转舍,
		羊只戴标转舍
	}
}

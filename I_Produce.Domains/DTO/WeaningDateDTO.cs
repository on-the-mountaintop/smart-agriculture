﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Produce.Domains.DTO
{
	public class WeaningDateDTO
	{
		/// <summary>
		/// 出生日期
		/// </summary>
		public DateTime? SheepBir { get; set; }
	}
}

﻿namespace I_Produce.Domains.DTO
{
	public enum PregnancyTestResult
	{
		空胎 = 1,
		返情,
		流产,
		怀孕,
		未孕
	}
} 

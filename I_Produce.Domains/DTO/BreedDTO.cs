﻿namespace I_Produce.Domains.DTO
{
	public class BreedDTO
	{
		public string? SheepBreed { get; set; }

		public bool SheepGender { get; set; }
	}
} 

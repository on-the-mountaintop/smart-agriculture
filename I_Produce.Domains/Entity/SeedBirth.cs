﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace I_Produce.Domains.Entity
{
	/// <summary>
	/// 种母分娩表
	/// </summary>
	[Table("SeedBirth")]
	public class SeedBirth : BaseSuperclass
	{
		/// <summary>
		/// 自增标识列
		/// </summary>
		[Key]
		public int Id { get; set; }

		/// <summary>
		/// 种母耳号
		/// </summary>
		[StringLength(50)]
		public string? MotherEarHorn { get; set; }

		/// <summary>
		/// 分娩日期
		/// </summary>
		public DateTime? DeliveryDate { get; set; }

		/// <summary>
		/// 妊娠天数
		/// </summary>
		public int? GestationDays { get; set; }

		/// <summary>
		/// 分娩难易
		/// </summary>
		[StringLength(50)]
		public string? DifficultyOfDelivery { get; set; }

		/// <summary>
		/// 母性
		/// </summary>
		[StringLength(50)]
		public string? Motherhood { get; set; }

		/// <summary>
		/// 泌乳
		/// </summary>
		[StringLength(50)]
		public string? Lactation { get; set; }

		/// <summary>
		/// 乳房炎
		/// </summary>
		[StringLength(50)]
		public string? Mastitis { get; set; }

		/// <summary>
		/// 窝总羔数
		/// </summary>
		public int? TotalNumberLambsLitter { get; set; }

		/// <summary>
		/// 窝总重(公斤)
		/// </summary>
		[Column(TypeName = "decimal(10,2)")]
		public decimal? LitterTotalWeight { get; set; }

		/// <summary>
		/// 活羔总数
		/// </summary>
		public int? LiveLambCount { get; set; }

		/// <summary>
		/// 活羔总重
		/// </summary>
		[Column(TypeName = "decimal(10,2)")]
		public decimal? TotalWeightLiveLamb { get; set; }

		/// <summary>
		/// 活羔均重
		/// </summary>
		[Column(TypeName = "decimal(10,2)")]
		public decimal? AllLiveLambsWeigh { get; set; }

		/// <summary>
		/// 死羔总数
		/// </summary>
		public int? DeadLambCount { get; set; }

		/// <summary>
		/// 死羔总重
		/// </summary>
		[Column(TypeName = "decimal(10,2)")]
		public decimal? TotalDeadLambWeight { get; set; }

		/// <summary>
		/// 死羔均重
		/// </summary>
		[Column(TypeName = "decimal(10,2)")]
		public decimal? AllDeadLambsWeigh { get; set; }

		/// <summary>
		/// 肥羊批次
		/// </summary>
		[StringLength(50)]
		public string? FatSheepLot { get; set; }

		/// <summary>
		/// 健羔数
		/// </summary>
		public int? HealthyNumber { get; set; }

		/// <summary>
		/// 弱羔数
		/// </summary>
		public int? WeakLambNumber { get; set; }

		/// <summary>
		/// 畸形数
		/// </summary>
		public int? DeformityNumber { get; set; }

		/// <summary>
		/// 死胎数
		/// </summary>
		public int? NumberOfSTillbirths { get; set; }

		/// <summary>
		/// 木乃伊数
		/// </summary>
		public int? MummyNumber { get; set; }

		/// <summary>
		/// 窝号
		/// </summary>
		[StringLength(50)]
		public string? BroodNumber { get; set; }

		/// <summary>
		/// 性别
		/// </summary>
		public bool Sex { get; set; }

		/// <summary>
		/// 当前栋舍
		/// </summary>
		[StringLength(50)]
		public string? CurrentBuilding { get; set; }

		/// <summary>
		/// 当前栏位
		/// </summary>
		[StringLength(50)]
		public string? CurrentField { get; set; }

		/// <summary>
		/// 转入栋舍
		/// </summary>
		[StringLength(50)]
		public string? TransferBuilding { get; set; }

		/// <summary>
		/// 转入栏位
		/// </summary>
		[StringLength(50)]
		public string? DriveField { get; set; }

		/// <summary>
		/// 负责人
		/// </summary>
		[StringLength(50)]
		public string? PersonCharge { get; set; }

		/// <summary>
		/// 审核人
		/// </summary>
		[StringLength(50)]
		public string? Auditor { get; set; }

		/// <summary>
		/// 审核时间
		/// </summary>
		public DateTime? AuditTime { get; set; }
	}
}

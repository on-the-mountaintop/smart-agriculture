﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace I_Produce.Domains.Entity
{
	/// <summary>
	/// 种母断奶表
	/// </summary>
	[Table("SeedWeaning")]
	public class SeedWeaning : BaseSuperclass
	{
		/// <summary>
		/// 自增标识列
		/// </summary>
		[Key]
		public int Id { get; set; }

		/// <summary>
		/// 断奶日期
		/// </summary>
		public DateTime? WeaningDate { get; set; }

		/// <summary>
		/// 耳号
		/// </summary>
		[StringLength(50)]
        public string? Auricle { get; set; }

		/// <summary>
		/// 哺乳天数
		/// </summary>
		public int? BreastfeedingDays { get; set; }

		/// <summary>
		/// 种母重量
		/// </summary>
		[Column(TypeName = "decimal(10,2)")]
        public decimal? SeedWeight { get; set; }

		/// <summary>
		/// 断奶只数
		/// </summary>
		public int? WeaningNumber { get; set; }

		/// <summary>
		/// 断奶总窝重
		/// </summary>
		[Column(TypeName = "decimal(10,2)")]
		public decimal? TotalLitterWeightWeaning { get; set; }

		/// <summary>
		/// 当前栋舍
		/// </summary>
		[StringLength(50)]
		public string? CurrentBuilding { get; set; }

		/// <summary>
		/// 当前栏位
		/// </summary>
		[StringLength(50)]
		public string? CurrentField { get; set; }

		/// <summary>
		/// 转入栋舍
		/// </summary>
		[StringLength(50)]
		public string? TransferBuilding { get; set; }

		/// <summary>
		/// 转入栏位
		/// </summary>
		[StringLength(50)]
		public string? DriveField { get; set; }

		/// <summary>
		/// 负责人
		/// </summary>
		[StringLength(50)]
		public string? PersonCharge { get; set; }

		/// <summary>
		/// 审核人
		/// </summary>
		[StringLength(50)]
		public string? Auditor { get; set; }

		/// <summary>
		/// 审核时间
		/// </summary>
		public DateTime? AuditTime { get; set; }

		/// <summary>
		/// 断奶原因
		/// </summary>
		[StringLength(50)]
		public string? WeaningReason { get; set; }
    }
}

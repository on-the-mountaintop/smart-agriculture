﻿namespace I_Produce.Domains.Entity
{
    /// <summary>
    /// 审计字段类
    /// </summary>
    public class BaseSuperclass
    {
        public bool IsDel { get; set; } = false;

        public string? CreateBy { get; set; } = "张三";

        public DateTime CreateDate { get; set; } = DateTime.Now;

        public string? UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }
    }
}

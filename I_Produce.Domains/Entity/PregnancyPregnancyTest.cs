﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static System.Net.Mime.MediaTypeNames;

namespace I_Produce.Domains.Entity
{
	/// <summary>
	/// 种母妊检表
	/// </summary>
	[Table("PregnancyPregnancyTest")]
	public class PregnancyPregnancyTest : BaseSuperclass
	{
		/// <summary>
		/// 自增标识列
		/// </summary>
		[Key]
		public int Id { get; set; }

		/// <summary>
		/// 妊检日期
		/// </summary>
		public DateTime? PregnancyTestDate { get; set; }

		/// <summary>
		/// 耳号
		/// </summary>
		[StringLength(50)]
		public string? Auricle { get; set; }

		/// <summary>
		/// 妊检结果
		/// </summary>
		[StringLength(50)]
		public string? PregnancyTestResult { get; set; }

		/// <summary>
		/// 妊检方式
		/// </summary>
		[StringLength(50)]
		public string? PregnancyTestMethod { get; set; }

		/// <summary>
		/// 同胎数
		/// </summary>
		public int? ParityNumber { get; set; }

		/// <summary>
		/// 妊检天数
		/// </summary>
		public int? PregnancyExaminationDays { get; set; }

		/// <summary>
		/// 当前栋舍
		/// </summary>
		[StringLength(50)]
		public string? CurrentBuilding { get; set; }

		/// <summary>
		/// 当前栏位
		/// </summary>
		[StringLength(50)]
		public string? CurrentField { get; set; }

		/// <summary>
		/// 转入栋舍
		/// </summary>
		[StringLength(50)]
		public string? TransferBuilding { get; set; }

		/// <summary>
		/// 转入栏位
		/// </summary>
		[StringLength(50)]
		public string? DriveField { get; set; }

		/// <summary>
		/// 负责人
		/// </summary>
		[StringLength(50)]
		public string? PersonCharge { get; set; }

		/// <summary>
		/// 审核人
		/// </summary>
		[StringLength(50)]
		public string? Auditor { get; set; }

		/// <summary>
		/// 审核时间
		/// </summary>
		public DateTime? AuditTime { get; set; }
	}
}

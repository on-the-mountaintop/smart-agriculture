﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace I_Produce.Domains.Entity
{
	/// <summary>
	/// 种母配种表
	/// </summary>
	[Table("BreedingMother")]
	public class BreedingMother : BaseSuperclass
	{
		/// <summary>
		/// 自增标识列
		/// </summary>
		[Key]
		public int Id { get; set; }

		/// <summary>
		/// 配种日期
		/// </summary>
		public DateTime? DateOfBreeding { get; set; }

		/// <summary>
		/// 上次配种日期
		/// </summary>
		public DateTime? LastDateOfBreeding { get; set; }

		/// <summary>
		/// 耳号
		/// </summary>
		[StringLength(50)]
		public string? Auricle { get; set; }

		/// <summary>
		/// 种母品种
		/// </summary>
		[StringLength(50)]
		public string? MotherVariety { get; set; }

		/// <summary>
		/// 首配公羊
		/// </summary>
		[StringLength(50)]
		public string? FirstWithRam { get; set; }

		/// <summary>
		/// 种公品种
		/// </summary>
		[StringLength(50)]
		public string? MaleBreed { get; set; }

		/// <summary>
		/// 配种方式
		/// </summary>
		[StringLength(50)]
		public string? BreedingPattern { get; set; }

		/// <summary>
		/// 种母发情类型
		/// </summary>
		[StringLength(50)]
		public string? SpeciesEstrusType { get; set; }

		/// <summary>
		/// 首配评分
		/// </summary>
		[StringLength(50)]
		public string? FirstScore { get; set; }

		/// <summary>
		/// 当前栋舍
		/// </summary>
		[StringLength(50)]
		public string? CurrentBuilding { get; set; }

		/// <summary>
		/// 当前栏位
		/// </summary>
		[StringLength(50)]
		public string? CurrentField { get; set; }

		/// <summary>
		/// 转入栋舍
		/// </summary>
		[StringLength(50)]
		public string? TransferBuilding { get; set; }

		/// <summary>
		/// 转入栏位
		/// </summary>
		[StringLength(50)]
		public string? DriveField { get; set; }

		/// <summary>
		/// 繁殖状态
		/// </summary>
		public string? ReproductiveState { get; set; }

		/// <summary>
		/// 重量(公斤)
		/// </summary>
		[Column(TypeName = "decimal(10,2)")]
		public decimal? Weight { get; set; }

		/// <summary>
		/// 事件日期
		/// </summary>
		public DateTime? EventDate { get; set; }

		/// <summary>
		/// 负责人
		/// </summary>
		[StringLength(50)]
		public string? PersonCharge { get; set; }

		/// <summary>
		/// 审核人
		/// </summary>
		[StringLength(50)]
		public string? Auditor { get; set; }

		/// <summary>
		/// 审核时间
		/// </summary>
		public DateTime? AuditTime { get; set; }
	}
}

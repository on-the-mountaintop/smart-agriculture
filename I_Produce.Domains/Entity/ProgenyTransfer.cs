﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace I_Produce.Domains.Entity
{
	/// <summary>
	/// 后裔转种表
	/// </summary>
	[Table("ProgenyTransfer")]
	public class ProgenyTransfer : BaseSuperclass
	{
		/// <summary>
		/// 自增标识列
		/// </summary>
		[Key]
		public int Id { get; set; }

		/// <summary>
		/// 转种日期
		/// </summary>
		public DateTime? DateConversion { get; set; }

		/// <summary>
		/// 育成羊耳号
		/// </summary>
		[StringLength(50)]
        public string? GrowSheepEars { get; set; }

		/// <summary>
		/// 当前栋舍
		/// </summary>
		[StringLength(50)]
		public string? CurrentBuilding { get; set; }

		/// <summary>
		/// 当前栏位
		/// </summary>
		[StringLength(50)]
		public string? CurrentField { get; set; }

		/// <summary>
		/// 转入栋舍
		/// </summary>
		[StringLength(50)]
		public string? TransferBuilding { get; set; }

		/// <summary>
		/// 转入栏位
		/// </summary>
		[StringLength(50)]
		public string? DriveField { get; set; }

		/// <summary>
		/// 性别
		/// </summary>
        public bool Sex { get; set; }

		/// <summary>
		/// 转种日龄
		/// </summary>
		public int? AgeTransplanting { get; set; }
    }
}

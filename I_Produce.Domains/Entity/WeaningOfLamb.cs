﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace I_Produce.Domains.Entity
{
	/// <summary>
	/// 羊羔断奶表
	/// </summary>
	[Table("WeaningOfLamb")]
	public class WeaningOfLamb : BaseSuperclass
	{
		/// <summary>
		/// 自增标识列
		/// </summary>
		[Key]
		public int Id { get; set; }

		/// <summary>
		/// 羔羊耳号
		/// </summary>
		[StringLength(50)]
		public string? LambsEar { get; set; }

		/// <summary>
		/// 断奶日龄
		/// </summary>
		public int? WeaningAge { get; set; }

		/// <summary>
		/// 断奶重
		/// </summary>
		[Column(TypeName = "decimal(10,2)")]
		public decimal? WeaningWeight { get; set; }

		/// <summary>
		/// 哺乳种母耳号
		/// </summary>
		[StringLength(50)]
		public string? LamTheMotheLactatingSpeciesbsEar { get; set; }

		/// <summary>
		/// 胎次
		/// </summary>
		public int? Parity { get; set; }

		/// <summary>
		/// 断奶栋舍
		/// </summary>
		[StringLength(50)]
		public string? WeaningHouse { get; set; }

		/// <summary>
		/// 断奶栏位
		/// </summary>
		[StringLength(50)]
		public string? WeaningField { get; set; }

		/// <summary>
		/// 转入栋舍
		/// </summary>
		[StringLength(50)]
		public string? TransferBuilding { get; set; }

		/// <summary>
		/// 转入栏位
		/// </summary>
		[StringLength(50)]
		public string? DriveField { get; set; }

		/// <summary>
		/// 断奶日期
		/// </summary>
		public DateTime? WeaningDate { get; set; }
	}
}

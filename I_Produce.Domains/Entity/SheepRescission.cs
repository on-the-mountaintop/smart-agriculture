﻿using CommonClass;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Produce.Domains.Entity
{
	/// <summary>
	/// 羊只转舍表
	/// </summary>
	[Table("SheepRescission")]
	public class SheepRescission : BaseSuperclass
	{
		/// <summary>
		/// 自增标识列
		/// </summary>
		[Key]
		public int Id { get; set; }

		/// <summary>
		/// 转舍日期
		/// </summary>
		public DateTime? SurrenderDate { get; set; }

		/// <summary>
		/// 羊只耳号
		/// </summary>
		[StringLength(50)]
		public string? VisualEarSignal { get; set; }

		/// <summary>
		/// 转舍类型
		/// </summary>
		[StringLength(50)]
		public string? RoundType { get; set; }

		/// <summary>
		/// 转出栋舍
		/// </summary>
		[StringLength(50)]
		public string? TurnBuilding { get; set; }

		/// <summary>
		/// 转出栏位
		/// </summary>
		[StringLength(50)]
		public string? RolloutField { get; set; }

		/// <summary>
		/// 转入栋舍
		/// </summary>
		[StringLength(50)]
		public string? TransferBuilding { get; set; }

		/// <summary>
		/// 转入栏位
		/// </summary>
		[StringLength(50)]
		public string? DriveField { get; set; }

		/// <summary>
		/// 品种
		/// </summary>
		[StringLength(50)]
		public string? Breed { get; set; }

		/// <summary>
		/// 性别
		/// </summary>
		public bool Sex { get; set; }

		/// <summary>
		/// 审核人
		/// </summary>
		[StringLength(50)]
		public string? Auditor { get; set; }

		/// <summary>
		/// 审核时间
		/// </summary>
		public DateTime? AuditTime { get; set; }
	}
}

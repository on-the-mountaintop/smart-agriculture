﻿using I_Authorization.Domains.Organization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Authorization.Infrastructure.Interface
{
    public interface IUnitsRepository:IRepository<Units>
    {
    }
}

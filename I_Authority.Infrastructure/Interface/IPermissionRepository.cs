﻿using I_Authorization.Domains.RBAC;
using I_Authorization.Infrastructure.Migrations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Authorization.Infrastructure.Interface
{
    /// <summary>
    /// 权限表的增删改查
    /// </summary>
    public interface IPermissionRepository:IRepository<PermissionInfo>
    {
    }
}

﻿using I_Authorization.Domains.RBAC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Authorization.Infrastructure.Interface
{
    /// <summary>
    /// 角色的仓储接口
    /// </summary>
    public interface IRoleRepository: IRepository<RoleInfo>
    {

    }
}

﻿using I_Authorization.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Authorization.Infrastructure.Interface
{
    /// <summary>
    /// 人员、员工的私有仓储接口
    /// </summary>
    public interface IEmployeeRepository: IRepository<EmployeeInfo>
    {
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace I_Authorization.Infrastructure
{
    /// <summary>
    /// 底层的仓储接口
    /// </summary>
    public interface IRepository<T> where T : class,new()
    {
        /// <summary>
        /// 添加信息
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        Task<int> AddAsync(T model);

        /// <summary>
        /// 修改信息
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        Task<int > UpdateAsync(T model);

        /// <summary>
        /// 单个真删除
        /// </summary>
        /// <param name="id">主键编号</param>
        /// <returns></returns>
        //添加
        //批量添加
        Task<int> AddCountAsync(List<T> model);
        //修改
        //批量修改
        Task<int> UpdateUpdAsync(List<T> list);
        //删除
        Task<int> DeleteAsync(int id);

        /// <summary>
        /// 通过主键ID查询 获取单个对象（相当于反填）
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<T> GetModelAsync(int id);

        /// <summary>
        /// 通过字符串查询 获取单个对象（通过一个字符串反填）
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<T> GetModelAsync(Expression<Func<T, bool>> expression);

        /// <summary>
        /// 批量逻辑删除
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        Task<int> AllStateDelete(List<T> list);

        /// <summary>
        /// 显示全部  没有条件
        /// </summary>
        /// <returns></returns>
        Task<List<T>> GetAllAsync();

        /// <summary>
        /// 多条件组合查询
        /// </summary>
        /// <param name="expre"></param>
        /// <returns></returns>
        // 逻辑删除
        Task<int> DeleteAsync(T info);
        // 单个查询
        Task<T> GetFindAsync(int id);
        //查询条件
        Task<List<T>> GetAsync(Expression<Func<T, bool>> expre);
    }
}

﻿using I_Authorization.Domains.Organization;
using I_Authorization.Infrastructure.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Authorization.Infrastructure.Impl
{
    //羊场
    public class Shee_YardRepository : BaseRepository<Shee_Yard>, IShee_YardRepository
    {
        public Shee_YardRepository(EFDbContext db) : base(db)
        {
        }
    }
}

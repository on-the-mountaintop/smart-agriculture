﻿using I_Authorization.Domains.RBAC;
using I_Authorization.Infrastructure.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Authorization.Infrastructure.Impl
{
    /// <summary>
    /// 角色的仓储实现
    /// </summary>
    public class RoleRepository : BaseRepository<RoleInfo>, IRoleRepository
    {
        public RoleRepository(EFDbContext _db) : base(_db)
        {
        }
    }
}

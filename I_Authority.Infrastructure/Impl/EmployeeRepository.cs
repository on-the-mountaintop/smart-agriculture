﻿using I_Authorization.Domains;
using I_Authorization.Infrastructure.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Authorization.Infrastructure.Impl
{
    /// <summary>
    /// 人员、员工的私有仓储实现
    /// </summary>
    public class EmployeeRepository : BaseRepository<EmployeeInfo>, IEmployeeRepository
    {
        public EmployeeRepository(EFDbContext _db) : base(_db)
        {
        }
    }
}

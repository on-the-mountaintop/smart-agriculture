﻿using I_Authorization.Domains.RBAC;
using I_Authorization.Infrastructure.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Authorization.Infrastructure.Impl
{
    /// <summary>
    /// 权限表的增删改查
    /// </summary>
    public class PermissionRepository : BaseRepository<PermissionInfo>, IPermissionRepository
    {
        public PermissionRepository(EFDbContext _db) : base(_db)
        {
        }
    }
}

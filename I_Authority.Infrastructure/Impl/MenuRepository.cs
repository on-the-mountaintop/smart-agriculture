﻿using I_Authorization.Domains.RBAC;
using I_Authorization.Infrastructure.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace I_Authorization.Infrastructure.Impl
{
    public class MenuRepository :BaseRepository<MenuList>, IMenuRepository
    {
        public MenuRepository(EFDbContext _db) : base(_db)
        {
        }
    }
}

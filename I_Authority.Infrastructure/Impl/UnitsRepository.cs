﻿using I_Authorization.Domains.Organization;
using I_Authorization.Infrastructure.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Authorization.Infrastructure.Impl
{
    //企业
    public class UnitsRepository : BaseRepository<Units>, IUnitsRepository
    {
        public UnitsRepository(EFDbContext db) : base(db)
        {
        }
    }
}

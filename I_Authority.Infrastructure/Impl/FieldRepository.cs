﻿using I_Authorization.Domains.Organization;
using I_Authorization.Infrastructure.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Authorization.Infrastructure.Impl
{
    //栏位
    public class FieldRepository : BaseRepository<Field>, IFieldRepository
    {
        public FieldRepository(EFDbContext db) : base(db)
        {
        }
    }
}

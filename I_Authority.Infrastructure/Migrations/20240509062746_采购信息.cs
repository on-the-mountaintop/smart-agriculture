﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace I_Authorization.Infrastructure.Migrations
{
    public partial class 采购信息 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Client",
                columns: table => new
                {
                    ClientId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ClientNumber = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    ClientName = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    ClientType = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    PrincipalTel = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    InputUnit = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    BuildingUser = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    Market = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    ServiceStaff = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    CollectionFile = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    UseOftate = table.Column<bool>(type: "bit", nullable: false),
                    PrincipalName = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    IDNumber = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    IndustryInvolved = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    Classification = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    AffiliatedUnit = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    Salesman = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    CreateBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreateDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdateBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdateDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDel = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Client", x => x.ClientId);
                });

            migrationBuilder.CreateTable(
                name: "EntityMarket",
                columns: table => new
                {
                    MarketId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MarketDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DocumentNumber = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    Client = table.Column<int>(type: "int", nullable: false),
                    SheepNumber = table.Column<int>(type: "int", nullable: false),
                    TotalPrice = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    AuditState = table.Column<bool>(type: "bit", nullable: false),
                    DepartmentId = table.Column<int>(type: "int", nullable: false),
                    SellPeople = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    ExamineBy = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    MarketAddress = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    ExamineDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    FinanceBy = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    FinanceDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    StashBy = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    StashDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreateBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreateDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdateBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdateDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDel = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EntityMarket", x => x.MarketId);
                });

            migrationBuilder.CreateTable(
                name: "EntityPurchase",
                columns: table => new
                {
                    PurId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DocumentNumber = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    PurDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    PruTytpe = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    Supplier = table.Column<int>(type: "int", nullable: false),
                    Digest = table.Column<int>(type: "int", nullable: false),
                    Earbugles = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    CottageName = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    FieldName = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    PurSex = table.Column<bool>(type: "bit", nullable: false),
                    Phase = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    Buyer = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    Stash = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    GoodName = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    Package = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    MeasuringUnit = table.Column<int>(type: "int", nullable: false),
                    AdjustNmber = table.Column<int>(type: "int", nullable: false),
                    PayNumber = table.Column<int>(type: "int", nullable: false),
                    QuantityNumber = table.Column<int>(type: "int", nullable: false),
                    AveragePrice = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Price = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    PriceDiscount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    PriceAdjust = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    PricePay = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    TotalWeight = table.Column<int>(type: "int", nullable: false),
                    CreateBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreateDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdateBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdateDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDel = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EntityPurchase", x => x.PurId);
                });

            migrationBuilder.CreateTable(
                name: "Supplier",
                columns: table => new
                {
                    SupplierId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SupplierName = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    SupplierNumber = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    SupplierType = table.Column<bool>(type: "bit", nullable: false),
                    PrincipalTelephone = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    InputUnit = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    BuildingUser = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    Market = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    ServiceStaff = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    CollectionFile = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    UseOftate = table.Column<bool>(type: "bit", nullable: false),
                    PrincipalName = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    IDNumber = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    Location = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    IndustryInvolved = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    AffiliatedUnit = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    Salesman = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    SupplierImg = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    DateOfCooperation = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreateBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreateDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    UpdateBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdateDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDel = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Supplier", x => x.SupplierId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Client");

            migrationBuilder.DropTable(
                name: "EntityMarket");

            migrationBuilder.DropTable(
                name: "EntityPurchase");

            migrationBuilder.DropTable(
                name: "Supplier");
        }
    }
}

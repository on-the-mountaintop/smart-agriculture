﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace I_Authorization.Infrastructure.Migrations
{
    public partial class permission : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Operation",
                table: "PermissionInfo",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RoleId",
                table: "PermissionInfo",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Operation",
                table: "PermissionInfo");

            migrationBuilder.DropColumn(
                name: "RoleId",
                table: "PermissionInfo");
        }
    }
}

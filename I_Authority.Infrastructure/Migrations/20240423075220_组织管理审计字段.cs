﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace I_Authorization.Infrastructure.Migrations
{
    public partial class 组织管理审计字段 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CreateBy",
                table: "Units",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreateDate",
                table: "Units",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDel",
                table: "Units",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdateBy",
                table: "Units",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdateDate",
                table: "Units",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreateBy",
                table: "Shee_Yard",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreateDate",
                table: "Shee_Yard",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDel",
                table: "Shee_Yard",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdateBy",
                table: "Shee_Yard",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdateDate",
                table: "Shee_Yard",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreateBy",
                table: "Field",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreateDate",
                table: "Field",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDel",
                table: "Field",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdateBy",
                table: "Field",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdateDate",
                table: "Field",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreateBy",
                table: "Department",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreateDate",
                table: "Department",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDel",
                table: "Department",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdateBy",
                table: "Department",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdateDate",
                table: "Department",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CreateBy",
                table: "Cottage",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreateDate",
                table: "Cottage",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDel",
                table: "Cottage",
                type: "bit",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UpdateBy",
                table: "Cottage",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdateDate",
                table: "Cottage",
                type: "datetime2",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreateBy",
                table: "Units");

            migrationBuilder.DropColumn(
                name: "CreateDate",
                table: "Units");

            migrationBuilder.DropColumn(
                name: "IsDel",
                table: "Units");

            migrationBuilder.DropColumn(
                name: "UpdateBy",
                table: "Units");

            migrationBuilder.DropColumn(
                name: "UpdateDate",
                table: "Units");

            migrationBuilder.DropColumn(
                name: "CreateBy",
                table: "Shee_Yard");

            migrationBuilder.DropColumn(
                name: "CreateDate",
                table: "Shee_Yard");

            migrationBuilder.DropColumn(
                name: "IsDel",
                table: "Shee_Yard");

            migrationBuilder.DropColumn(
                name: "UpdateBy",
                table: "Shee_Yard");

            migrationBuilder.DropColumn(
                name: "UpdateDate",
                table: "Shee_Yard");

            migrationBuilder.DropColumn(
                name: "CreateBy",
                table: "Field");

            migrationBuilder.DropColumn(
                name: "CreateDate",
                table: "Field");

            migrationBuilder.DropColumn(
                name: "IsDel",
                table: "Field");

            migrationBuilder.DropColumn(
                name: "UpdateBy",
                table: "Field");

            migrationBuilder.DropColumn(
                name: "UpdateDate",
                table: "Field");

            migrationBuilder.DropColumn(
                name: "CreateBy",
                table: "Department");

            migrationBuilder.DropColumn(
                name: "CreateDate",
                table: "Department");

            migrationBuilder.DropColumn(
                name: "IsDel",
                table: "Department");

            migrationBuilder.DropColumn(
                name: "UpdateBy",
                table: "Department");

            migrationBuilder.DropColumn(
                name: "UpdateDate",
                table: "Department");

            migrationBuilder.DropColumn(
                name: "CreateBy",
                table: "Cottage");

            migrationBuilder.DropColumn(
                name: "CreateDate",
                table: "Cottage");

            migrationBuilder.DropColumn(
                name: "IsDel",
                table: "Cottage");

            migrationBuilder.DropColumn(
                name: "UpdateBy",
                table: "Cottage");

            migrationBuilder.DropColumn(
                name: "UpdateDate",
                table: "Cottage");
        }
    }
}

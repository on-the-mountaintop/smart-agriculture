﻿//using I_Authority.Domains;
using I_Authorization.Domains;
using I_Authorization.Domains.Organization;
using I_Authorization.Domains.RBAC;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Authorization.Infrastructure
{
    public class EFDbContext : DbContext
    {
        public EFDbContext(DbContextOptions<EFDbContext> options) : base(options)
        {
        }
        //实体


        public DbSet<Units> Units { get; set; }
        public DbSet<Department> Department { get; set; }
        public DbSet<Shee_Yard> Shee_Yard { get; set; }
        public DbSet<Field> Field { get; set; }
        public DbSet<Cottage> Cottage { get; set; }








        public DbSet <EmployeeInfo> EmployeeInfo { get; set; }
        public DbSet <RoleInfo> RoleInfo { get; set; }
        public DbSet <PermissionInfo> PermissionInfo { get; set; }
        public DbSet <RolePermissionRelation> RolePermissionRelation { get; set; }


        public DbSet <MenuList> MenuLists { get; set; }

        public DbSet <EntityPurchase> EntityPurchase { get; set; }
        public DbSet <EntityMarket> EntityMarket { get; set; }
        public DbSet <Client> Client { get; set; }
        public DbSet <Supplier> Supplier { get; set; }

    }
}

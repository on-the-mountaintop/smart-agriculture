﻿using I_Beginning.Domains.BeginningManagement;
using I_Beginning.Infrastructure.Interface;
using I_Beginning.Read.API.Application.Command.BeginningCommand;
using MediatR;

namespace I_Beginning.Read.API.Application.CommandHandlers.BeginningHandler
{
    public class BeginningBackfillCommandHandler : IRequestHandler<BeginningBackfillCommand, Beginning>
    {
        private readonly IBeginningRepository beginningRepository;
        public BeginningBackfillCommandHandler(IBeginningRepository beginningRepository)
        {
            this.beginningRepository = beginningRepository;
        }
        /// <summary>
        /// 反填期初信息
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>

        public async Task<Beginning> Handle(BeginningBackfillCommand request, CancellationToken cancellationToken)
        {
            return await beginningRepository.GetModelAsync(request.BeginningId);
        }
    }
}

﻿using I_Beginning.Domains.BeginningManagement;
using I_Beginning.Infrastructure.Interface;
using I_Beginning.Read.API.Application.Command.BeginningCommand;
using MediatR;

namespace I_Beginning.Read.API.Application.CommandHandlers.BeginningHandler
{
    public class BeginningShowCommandHandler : IRequestHandler<BeginningShowCommand, Page<Beginning>>
    {
        private readonly IBeginningRepository beginningRepository;
        public BeginningShowCommandHandler(IBeginningRepository beginningRepository)
        {
            this.beginningRepository = beginningRepository;
        }
        /// <summary>
        /// 显示期初信息
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<Page<Beginning>> Handle(BeginningShowCommand request, CancellationToken cancellationToken)
        {
            var list = await beginningRepository.GetAsync(x => x.IsDel == true);
            if (request.Code != null)
            {
                list = await beginningRepository.GetAsync(x => x.Code == request.Code&&!x.IsDel==false);
            }
            int total = list.Count();
            int pageCount = (int)Math.Ceiling((total * 1.0) / request.pageSize);
            list = list.Skip((request.pageIndex - 1) * request.pageSize).Take(request.pageSize).ToList();
            return new Page<Beginning>
            {
                total = total,
                pageCount = pageCount,
                Data = list
            };
        }
    }
}

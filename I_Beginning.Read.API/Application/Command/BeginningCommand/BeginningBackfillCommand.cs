﻿using I_Beginning.Domains.BeginningManagement;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace I_Beginning.Read.API.Application.Command.BeginningCommand
{
    public class BeginningBackfillCommand:IRequest<Beginning>
    {
        /// <summary>
        /// 期初ID
        /// </summary>
        public int BeginningId { get; set; }
    }
}

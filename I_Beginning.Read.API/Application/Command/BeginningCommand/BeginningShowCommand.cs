﻿using I_Beginning.Domains.BeginningManagement;
using MediatR;

namespace I_Beginning.Read.API.Application.Command.BeginningCommand
{
    public class BeginningShowCommand:IRequest<Page<Beginning>>
    {
        /// <summary>
        /// 当前页索引
        /// </summary>
        public int pageIndex { get; set; }
        /// <summary>
        /// 每页显示数据条数
        /// </summary>
        public int pageSize { get; set; }
        /// <summary>
        /// 种公 种母 后裔标识列
        /// </summary>
        public int ?Code { get; set; }

    }
}

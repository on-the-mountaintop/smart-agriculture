﻿using I_Beginning.Domains.BeginningManagement;
using I_Beginning.Read.API.Application.Command.BeginningCommand;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Beginning.Read.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class BeginningReadController : ControllerBase
    {
        private readonly IMediator mediator;
        public BeginningReadController(IMediator mediator)
        {
            this.mediator = mediator;
        }
        /// <summary>
        /// 反填期初信息
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet]

        public async Task<Beginning> BeginningBackfill([FromQuery] BeginningBackfillCommand command)
        {
            return await mediator.Send(command);
        }
        /// <summary>
        /// 显示期初信息
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<Page<Beginning>> BeginningShow([FromQuery] BeginningShowCommand command)
        {
            return await mediator.Send(command);
        }

    }
}

﻿using I_Beginning.Domains.BeginningManagement;
using I_Beginning.Infrastructure.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace I_Beginning.Infrastructure.Impl
{
    public class BeginningRepository : BaseRepostitory<Beginning>, IBeginningRepository
    {
        public BeginningRepository(EFDbContext context) : base(context)
        {

        }
        public async Task<int> AddAsync(Beginning t)
        {
            return await base.AddAsync(t);
        }

        public async Task<int> AddAsync(List<Beginning> t)
        {
            return await base.AddAsync(t);
        }

        public async Task<int> DeleteAsync(int id)
        {
            return await base.DeleteAsync(id);
        }

        public async Task<int> DeleteAsync(Expression<Func<Beginning, bool>> expression)
        {
            return await base.DeleteAsync(expression);
        }

        public async Task<List<Beginning>> GetAllAsync()
        {
            return await base.GetAllAsync();
        }

        public async Task<List<Beginning>> GetAsync(Expression<Func<Beginning, bool>> expre)
        {
            return await base.GetAsync(expre);
        }

        public async Task<Beginning> GetModelAsync(int id)
        {
            return await base.GetModelAsync(id);
        }

        public async Task<Beginning> GetModelAsync(Expression<Func<Beginning, bool>> expression)
        {
            return await base.GetModelAsync(expression);
        }

        public async Task<int> StateDelete(Beginning t)
        {
            return await base.StateDelete(t);
        }

        public async Task<int> UpdateAsync(Beginning t)
        {
            return await base.UpdateAsync(t);
        }
    }
}

﻿using I_Beginning.Domains.BeginningManagement;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Beginning.Infrastructure
{
    public class EFDbContext:DbContext
    {
        public EFDbContext(DbContextOptions<EFDbContext> options) : base(options) { }
        /// <summary>
        /// 期初信息表
        /// </summary>
        public DbSet<Beginning> Beginning {  get; set; }
    }
}

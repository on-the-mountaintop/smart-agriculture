﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace I_Beginning.Infrastructure.Migrations
{
    public partial class zhaozimings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CreateBy",
                table: "Beginning",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreateDate",
                table: "Beginning",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDel",
                table: "Beginning",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "UpdateBy",
                table: "Beginning",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "UpdateDate",
                table: "Beginning",
                type: "datetime2",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreateBy",
                table: "Beginning");

            migrationBuilder.DropColumn(
                name: "CreateDate",
                table: "Beginning");

            migrationBuilder.DropColumn(
                name: "IsDel",
                table: "Beginning");

            migrationBuilder.DropColumn(
                name: "UpdateBy",
                table: "Beginning");

            migrationBuilder.DropColumn(
                name: "UpdateDate",
                table: "Beginning");
        }
    }
}

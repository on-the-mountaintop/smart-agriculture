﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace I_Beginning.Infrastructure.Migrations
{
    public partial class zhaoziming : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Beginning",
                columns: table => new
                {
                    BeginningId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EarNumber = table.Column<string>(type: "nvarchar(40)", maxLength: 40, nullable: true),
                    Buildings = table.Column<string>(type: "nvarchar(40)", maxLength: 40, nullable: true),
                    Columns = table.Column<string>(type: "nvarchar(40)", maxLength: 40, nullable: true),
                    SheepNumber = table.Column<int>(type: "int", nullable: false),
                    Weight = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Code = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Beginning", x => x.BeginningId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Beginning");
        }
    }
}

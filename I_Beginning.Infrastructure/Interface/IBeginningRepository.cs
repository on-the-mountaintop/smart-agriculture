﻿using I_Beginning.Domains.BeginningManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Beginning.Infrastructure.Interface
{
    public interface IBeginningRepository: IRepository<Beginning>
    {
    }
}

using CommonClass;
using I_Archives.API.Extensions;
using I_Archives.Infrastructure;
using IGeekFan.AspNetCore.Knife4jUI;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

// 配置swagger版本
builder.Services.AddSwaggerGen(options =>
{
    foreach (FieldInfo field in typeof(ApiVersionInfo).GetFields())
    {
        options.SwaggerDoc(field.Name, new OpenApiInfo
        {
            Version = field.Name,
            Title = $"智慧农业-写入",
            Description = $"API描述，{field.Name}版本",
        });

        var xml = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
        options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xml));
    }
});

// 注册接口服务
builder.Services.AddServices();

// 注册AutoMapper
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

// 注册数据库上下文
builder.Services.AddDbContext<MyDbContext>(x =>
{
    x.UseSqlServer(builder.Configuration.GetConnectionString("Authorization"));
});

// 注册JWT
builder.Services.AddJWT(builder);

// 注册中介者模式
builder.Services.AddMediatR(Assembly.GetExecutingAssembly());

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI();

//// 配置swagger版本(开发环境)
//if (app.Environment.IsDevelopment())
//{
//    app.UseSwagger();
//    // 配置swagger版本
//    app.UseSwaggerUI(options =>
//    {
//        foreach (FieldInfo field in typeof(ApiVersionInfo).GetFields())
//        {
//            options.SwaggerEndpoint($"/swagger/{field.Name}/swagger.json", $"{field.Name}");
//        }
//    });

//    // 小刀UI
//    app.UseKnife4UI(options =>
//    {
//        foreach (FieldInfo field in typeof(ApiVersionInfo).GetFields())
//        {
//            options.RoutePrefix = string.Empty;
//            var xml = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
//            options.SwaggerEndpoint($"/swagger/{field.Name}/swagger.json", $"{field.Name}");
//        }
//    });
//}


// 跨域
app.UseCors(c =>
{
    c.AllowAnyOrigin();
    c.AllowAnyHeader();
    c.AllowAnyMethod();
});

app.UseAuthorization();

app.MapControllers();

app.Run();

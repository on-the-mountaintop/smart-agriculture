﻿using AutoMapper;
using I_Archives.API.Application.Command;
using I_Archives.Domains;
namespace I_Archives.API.Extensions
{
    public class ArchivesProfile : Profile
    {
        public ArchivesProfile()
        {
            CreateMap<ArchivesInsertCommand, ArchivesInfo>().ReverseMap();
            CreateMap<ArchivesUpdateCommand, ArchivesInfo>().ReverseMap();
        }
    }
}

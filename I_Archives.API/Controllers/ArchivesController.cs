﻿using CommonClass;
using CommonClass.CountInfo;
using I_Archives.API.Application.Command;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Reflection;

namespace I_Archives.API.Controllers
{
    /// <summary>
    /// 档案管理
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    [ApiExplorerSettings(GroupName = nameof(ApiVersionInfo.羊只档案管理))]
    public class ArchivesController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ArchivesController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// 档案新增
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<int> ArchivesInsert(ArchivesInsertCommand command)
        {
            return await _mediator.Send(command);
        }

        /// <summary>
        /// 档案修改
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<int> ArchivesUpdate(ArchivesUpdateCommand command)
        {
            return await _mediator.Send(command);
        }

        /// <summary>
        /// 档案在场状态
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<int> ArchivesUpdateStatus(ArchivesUpdateStatusCommand command)
        {
            return await _mediator.Send(command);
        }

        /// <summary>
        /// 档案删除
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<int> ArchivesDelete([FromQuery]ArchivesDeleteCommand command)
        {
            return await _mediator.Send(command);
        }
    }
}

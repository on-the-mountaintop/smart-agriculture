﻿using AutoMapper;
using I_Archives.API.Application.Command;
using I_Archives.Domains;
using I_Archives.Infrastructure.Interface;
using MediatR;

namespace I_Archives.API.Application.CommandHandler
{
    public class ArchivesUpdateCommandHandler : IRequestHandler<ArchivesUpdateCommand, int>
    {
        private readonly IMapper _mapper;
        private readonly IArchivesRepository _archivesRepository;

        public ArchivesUpdateCommandHandler(IMapper mapper, IArchivesRepository archivesRepository)
        {
            _mapper = mapper;
            _archivesRepository = archivesRepository;
        }

        public async Task<int> Handle(ArchivesUpdateCommand request, CancellationToken cancellationToken)
        {
            var archivesInfo  = _mapper.Map<ArchivesInfo>(request);
            return await _archivesRepository.UpdateAsync(archivesInfo);
        }
    }
}

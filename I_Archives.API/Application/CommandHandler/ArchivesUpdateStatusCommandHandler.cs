﻿using I_Archives.API.Application.Command;
using I_Archives.Infrastructure.Interface;
using MediatR;

namespace I_Archives.API.Application.CommandHandler
{
    public class ArchivesUpdateStatusCommandHandler : IRequestHandler<ArchivesUpdateStatusCommand, int>
    {
        private readonly IArchivesRepository _archivesRepository;

        public ArchivesUpdateStatusCommandHandler(IArchivesRepository archivesRepository)
        {
            _archivesRepository = archivesRepository;
        }

        public async Task<int> Handle(ArchivesUpdateStatusCommand request, CancellationToken cancellationToken)
        {
            var archivesInfo = await _archivesRepository.GetByFuncAsync(x => x.EarNnumber == request.EarNumber);
            archivesInfo.Presence = request.PresenceStatus;
            return await _archivesRepository.UpdateAsync(archivesInfo);
        }
    }
}

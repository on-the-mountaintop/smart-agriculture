﻿using AutoMapper;
using I_Archives.API.Application.Command;
using I_Archives.Domains;
using I_Archives.Infrastructure.Impls;
using I_Archives.Infrastructure.Interface;
using MediatR;

namespace I_Archives.API.Application.CommandHandler
{
    public class ArchivesInsertCommandHandler : IRequestHandler<ArchivesInsertCommand, int>
    {
        private readonly IMapper _mapper;
        private readonly IArchivesRepository _archivesRepository;

        public ArchivesInsertCommandHandler(IMapper mapper,IArchivesRepository archivesRepository)
        {
            _mapper = mapper;
            _archivesRepository = archivesRepository;
        }

        public async Task<int> Handle(ArchivesInsertCommand request, CancellationToken cancellationToken)
        {
            request.CreateBy = "张三";
            request.CreateDate = DateTime.Now.ToString();
            var archivesInfo = _mapper.Map<ArchivesInfo>(request);
            return await _archivesRepository.AddAsync(archivesInfo);
        }
    }
}

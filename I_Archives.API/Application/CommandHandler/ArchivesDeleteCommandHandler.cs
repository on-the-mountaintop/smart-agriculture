﻿using AutoMapper;
using I_Archives.API.Application.Command;
using I_Archives.Infrastructure.Interface;
using MediatR;

namespace I_Archives.API.Application.CommandHandler
{
    public class ArchivesDeleteCommandHandler : IRequestHandler<ArchivesDeleteCommand, int>
    {
        private readonly IMapper _mapper;
        private readonly IArchivesRepository _archivesRepository;

        public ArchivesDeleteCommandHandler(IMapper mapper, IArchivesRepository archivesRepository)
        {
            _mapper = mapper;
            _archivesRepository = archivesRepository;
        }

        public async Task<int> Handle(ArchivesDeleteCommand request, CancellationToken cancellationToken)
        {
            var archivesInfo = await _archivesRepository.GetByIdAsync(request.Id);
            archivesInfo.IsDelete = true;
            return await _archivesRepository.UpdateAsync(archivesInfo);
        }
    }
}

﻿using MediatR;

namespace I_Archives.API.Application.Command
{
    public class ArchivesUpdateStatusCommand: IRequest<int>
    {
        public string? EarNumber { get; set; }
        public string? PresenceStatus { get; set; }
    }
}

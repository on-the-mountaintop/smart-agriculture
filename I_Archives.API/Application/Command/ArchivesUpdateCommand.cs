﻿using CommonClass;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace I_Archives.API.Application.Command
{
    public class ArchivesUpdateCommand : AuditInfo, IRequest<int>
    {
        /// <summary>
        /// 档案Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 可视耳号
        /// </summary>
        public string? EarNnumber { get; set; }
        /// <summary>
        /// 电子耳号
        /// </summary>
        public string? ElectronicEarNumber { get; set; }
        /// <summary>
        /// 羊只种类
        /// </summary>
        public string? SheepBreed { get; set; }
        /// <summary>
        /// 羊只性别
        /// </summary>
        public bool SheepGender { get; set; }
        /// <summary>
        /// 出生日期
        /// </summary>
        public DateTime SheepBir { get; set; }
        /// <summary>
        /// 在场状态
        /// </summary>
        public string? Presence { get; set; }
        /// <summary>
        /// 生长状态
        /// </summary>
        public string? GrowthStage { get; set; }
        /// <summary>
        /// 羊只类型
        /// </summary>
        public string? SheepType { get; set; }
        /// <summary>
        /// 栋舍Id
        /// </summary>
        public string? CottageId { get; set; }
        /// <summary>
        /// 栏位Id
        /// </summary>
        public string? Field { get; set; }
        /// <summary>
        /// 繁殖状态
        /// </summary>
        public string? ReproductiveState { get; set; }
        /// <summary>
        /// 是否妊检
        /// </summary>
        public bool? PregnancyTest { get; set; }
        /// <summary>
        /// 状态天数
        /// </summary>
        public int? StatusDays { get; set; }
        /// <summary>
        /// 生产等级
        /// </summary>
        public string? ProductionGrade { get; set; }
        /// <summary>
        /// 销售等级
        /// </summary>
        public string? SalesGrade { get; set; }
        /// <summary>
        /// 基因等级
        /// </summary>
        public string? GeneticGrade { get; set; }
        /// <summary>
        /// 出生重量
        /// </summary>
        public decimal? BirthWeight { get; set; }
        /// <summary>
        /// 断奶重量
        /// </summary>
        public decimal? WeaningWeigh { get; set; }
        /// <summary>
        /// 断奶日期
        /// </summary>
        public DateTime? WeaningDate { get; set; }
        /// <summary>
        /// 父耳号
        /// </summary>
        public string? FatherEarNumber { get; set; }
        /// <summary>
        /// 母耳号
        /// </summary>
        public string? MotherEarNumber { get; set; }
        /// <summary>
        /// 出生状态
        /// </summary>
        public string? StateBirth { get; set; }
        /// <summary>
        /// 出生场地
        /// </summary>
        public string? PlaceBirth { get; set; }
        /// <summary>
        /// 入场日期
        /// </summary>
        public DateTime? InDate { get; set; }
        /// <summary>
        /// 档案备注
        /// </summary>
        public string? ArchivesRemark { get; set; }
        /// <summary>
        /// 档案图片
        /// </summary>
        public string? ArchivesPicture { get; set; }
    }
}

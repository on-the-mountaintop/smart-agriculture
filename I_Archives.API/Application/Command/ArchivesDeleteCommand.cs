﻿using MediatR;

namespace I_Archives.API.Application.Command
{
    public class ArchivesDeleteCommand:IRequest<int>
    {
        public int Id {  get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonClass
{
    /// <summary>
    /// 基因等级
    /// </summary>
    public enum GeneticGrade
    {
        后备级 = 1,
        核心级,
        生产级,
        未定级,
    }
}

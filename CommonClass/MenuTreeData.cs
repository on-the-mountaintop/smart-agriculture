﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonClass
{
    public class MenuTreeData<T>
    {
        public int Id {  get; set; }
        public string? MenuName { get; set; }
        public string? MenuURL { get; set; }
        public List<T>? Children { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace CommonClass
{
	public class Cross_services_Helper
	{
		private readonly IHttpClientFactory httpClient;

		public Cross_services_Helper(IHttpClientFactory httpClient)
		{
			this.httpClient = httpClient;
		}
		/// <summary>
		/// get请求修改
		/// </summary>
		/// <param name="requestUrl"></param>
		/// <returns></returns>
		public async Task<string> Get(string requestUrl)
		{
			try
			{
			//修改相关IP号和对应的控制器名称,以及对应的方法名
			http://10.31.56.10:5185/api/ControllerName
				 //通过httpclient调用 别的Api服务的方法 返回

				var httpclients = httpClient.CreateClient();

				//在头部中加入一个token进行验证
				var response = httpclients.GetAsync(requestUrl).Result;
				//通过httpclient 来访问httpURL
				//通过回调函数的IsSuceessStatuesCode判断是否成功
				if (response.IsSuccessStatusCode)
				{
					var message = response.Content.ReadAsStringAsync().Result;
					return message;
				}
				return string.Empty;

			}
			catch (HttpRequestException ex)
			{
				return "Http请求异常" + ex.Message;
				throw;
			}
			catch (TaskCanceledException ex)
			{
				// 处理连接超时异常
				return "Http连接超时" + ex.Message;
				throw;
			}

		}
		/// <summary>
		/// 添加请求
		/// </summary>
		/// <param name="requestUrl"></param>
		/// <param name="obj"></param>
		/// <param name="token"></param>
		/// <returns></returns>
		public async Task<string> Post(string requestUrl, object obj)
		{
			try
			{
				var httepclient = httpClient.CreateClient();

				var jsonobj = JsonSerializer.Serialize(obj);
				var context = new StringContent(jsonobj, Encoding.UTF8, "application/json");
				var response = httepclient.PostAsync(requestUrl, context).Result;
				if (response.IsSuccessStatusCode)
				{
					var message = response.Content.ReadAsStringAsync().Result;
					return message;
				}
				string words = "状态码" + (int)response.StatusCode;
				return words;
			}
			catch (HttpRequestException ex)
			{
				return ("Http请求异常" + ex.Message);
				throw;
			}
			catch (TaskCanceledException ex)
			{
				// 处理连接超时异常
				return ("Http连接超时" + ex.Message);
				throw;
			}
		}
		/// <summary>
		/// put请求
		/// </summary>
		/// <param name="requestUrl">请求地址</param>
		/// <param name="obj">对象</param>
		/// <returns></returns>

		public async Task<string> Put(string requestUrl, object obj)
		{
			try
			{
				var httepclient = httpClient.CreateClient();

				var jsonobj = JsonSerializer.Serialize(obj);
				var context = new StringContent(jsonobj, Encoding.UTF8, "application/json");
				var response = httepclient.PutAsync(requestUrl, context).Result;
				if (response.IsSuccessStatusCode)
				{
					var message = response.Content.ReadAsStringAsync().Result;
					return message;
				}
				string words = "状态码" + (int)response.StatusCode;
				return words;
			}
			catch (HttpRequestException ex)
			{
				return ("Http请求异常" + ex.Message);
				throw;
			}
			catch (TaskCanceledException ex)
			{
				// 处理连接超时异常
				return ("Http连接超时" + ex.Message);
				throw;
			}
		}
		/// <summary>
		/// Delete删除请求
		/// </summary>
		/// <param name="requestUrl"></param>
		/// <returns></returns>
		public async Task<string> Delete(string requestUrl)
		{
			try
			{
			//修改相关IP号和对应的控制器名称,以及对应的方法名
			http://10.31.56.10:5185/api/ControllerName
				 //通过httpclient调用 别的Api服务的方法 返回

				var httpclients = httpClient.CreateClient();

				//在头部中加入一个token进行验证
				var response = httpclients.DeleteAsync(requestUrl).Result;
				//通过httpclient 来访问httpURL
				//通过回调函数的IsSuceessStatuesCode判断是否成功
				if (response.IsSuccessStatusCode)
				{
					var message = response.Content.ReadAsStringAsync().Result;
					return message;
				}
				string words = "状态码" + (int)response.StatusCode;
				return words;

			}
			catch (HttpRequestException ex)
			{
				return ("Http请求异常" + ex.Message);
				throw;
			}
			catch (TaskCanceledException ex)
			{
				// 处理连接超时异常
				return ("Http连接超时" + ex.Message);
				throw;
			}

		}
	}
}

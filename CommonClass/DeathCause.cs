﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonClass
{
    public enum DeathCause
    {
        腹泻 = 1,
        肺炎 = 2,
        产前瘫 = 3,
        产后瘫 = 4,
        酸中毒 = 5,
        应激猝死 = 6,
        梭卷 = 7,
        尿结石 = 8,
        发热 = 9,
        脱肛 = 10,
        黄疸 = 11,
        猝死 = 12,
    }
}

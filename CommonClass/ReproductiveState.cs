﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonClass
{
    /// <summary>
    /// 繁殖状态
    /// </summary>
    public enum ReproductiveState
    {
        后备空怀 = 1,
        返情空怀,
        未孕空怀,
        流产空怀,
        空胎空怀,
        断奶空怀,
        妊娠,
        哺乳
    }
}

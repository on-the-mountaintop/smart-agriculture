﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonClass
{
    public enum EliminationCause
    {
        子宫脱 = 1,
        子宫炎,
        肥胖,
        消瘦,
        精液质量差,
        拒配,
        无乳,
        屡配不孕,
        老龄,
        习惯性流产,
        空胎,
        其他
    }
}

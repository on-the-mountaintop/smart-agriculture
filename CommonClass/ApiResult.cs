﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonClass
{
    public class ApiResult<T>
    {
        public int Code {  get; set; }
        public string? msg {  get; set; }
        public T? Data {  get; set; }
    }
}

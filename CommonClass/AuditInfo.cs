﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonClass
{
    /// <summary>
    /// 审计信息表
    /// </summary>
    public class AuditInfo
    {
        public string? CreateBy {  get; set; }
        public string? CreateDate {  get; set; }
        public string? UpdateBy {  get; set; }
        public string? UpdateDate {  get; set; }
        public bool IsDelete {  get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonClass
{
    public class PageResult<T>
    {
        public int TotalCount {  get; set; }
        public int PageCount {  get; set; }
        public List<T>? Datas {  get; set; }
    }
}

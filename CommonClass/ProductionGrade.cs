﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonClass
{
    /// <summary>
    /// 生产等级
    /// </summary>
    public enum ProductionGrade
    {
        后备群 = 1,
        生产群,
        核心群,
        未定级,
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonClass.CountInfo
{
    public class DeadTaoTrend
    {
        public string? Date { get; set;}
        public int? DeathCount { get; set;}
        public int? ElinationCount { get; set; }
    }
}

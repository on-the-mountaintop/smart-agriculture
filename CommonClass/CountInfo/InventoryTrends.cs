﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonClass.CountInfo
{
    /// <summary>
    /// 仅30日存栏趋势
    /// </summary>
    public class InventoryTrends
    {
        public string? Date { get; set;}
        public int? Count { get; set; }
    }
}

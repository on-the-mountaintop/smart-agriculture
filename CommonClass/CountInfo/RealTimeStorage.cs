﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonClass.CountInfo
{
    /// <summary>
    /// 实时存栏
    /// </summary>
    public class RealTimeStorage
    {
        /// <summary>
        /// 总存栏
        /// </summary>
        public int? AllCount { get; set; }
        /// <summary>
        /// 种公存栏
        /// </summary>
        public int? MaleCount { get; set; }
        /// <summary>
        /// 种母存栏
        /// </summary>
        public int? MaternalCount { get; set; }
        /// <summary>
        /// 后裔存栏
        /// </summary>
        public int? DescendentCount { get; set; }
        /// <summary>
        /// 上周比率
        /// </summary>
        public double? RatioCount { get; set; }
    }
}

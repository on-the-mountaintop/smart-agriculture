﻿namespace CommonClass
{
    /// <summary>
    /// 羊只品种
    /// </summary>
    public enum SheepBreeds
    {
        杜泊 = 1,
        澳洲白,
        萨福克,
        杜湖,
        杜杜湖,
        萨杜湖,
        澳湖,
        湖羊,
        澳杜湖,
    }
}

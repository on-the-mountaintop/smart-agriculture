﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonClass
{
    /// <summary>
    /// 生长状态
    /// </summary>
    public enum GrowthStage
    {
        乳羊 = 1,
        保育,
        后背公羊, 
        后备母羊,
        后背羊
    }
}

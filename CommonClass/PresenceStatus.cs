﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonClass
{
    /// <summary>
    /// 在场状态
    /// </summary>
    public enum PresenceStatus
    {
        在场 = 1,
        待入场,
        销售离场,
        死亡离场,
        淘汰已离场,
        调拨离场,
        死羔未入场
    }
}

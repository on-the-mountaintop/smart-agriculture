﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Ear_Tag.Demoin
{
    /// <summary>
    /// 审计表
    /// </summary>
    public class AuditField
    {
        /// <summary>
        /// 创建人
        /// </summary>
        public string? CreateBy { get; set; }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateDate { get; set; }
        /// <summary>
        /// 修改人
        /// </summary>
        public string? UpdateBy { get; set; }
        /// <summary>
        /// 修改时间
        /// </summary>
        public DateTime? UpdateDate { get; set; }
        /// <summary>
        /// 删除标识
        /// </summary>
        public bool IsDel { get; set; } = true;
    }
}

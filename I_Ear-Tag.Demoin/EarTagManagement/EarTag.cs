﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace I_Ear_Tag.Demoin.EarTagManagement
{
    /// <summary>
    /// 耳标管理表
    /// </summary>
    [Table("EarTag")]
    public class EarTag: AuditField
    {
        /// <summary>
        /// 耳标id
        /// </summary>
        [Key]
        public int EarTagId { get; set; }
        /// <summary>
        /// 对应出生状态
        /// </summary>
        public State BirthStatus { get; set; }
        /// <summary>
        /// 对应品种
        /// </summary>
        public Breed SheepBreed { get; set; }
        /// <summary>
        /// 添加方式
        /// </summary>
        public int InsertType { get; set; }
        /// <summary>
        /// 羊只耳号
        /// </summary>
        [StringLength(30)]
        public string? EarNumber { get; set; }
        /// <summary>
        /// 入库日期
        /// </summary>
        public DateTime? StorageDate { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Ear_Tag.Demoin.EarTagManagement
{
    /// <summary>
    /// 对应品种
    /// </summary>
    public enum Breed
    {
        杜泊=1,
        澳洲白,
        萨福克,
        杜湖,
        杜杜湖,
        萨杜湖,
        澳湖,
        湖羊,
        澳杜湖,
    }
}

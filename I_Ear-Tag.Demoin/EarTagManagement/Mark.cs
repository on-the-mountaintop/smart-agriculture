﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Ear_Tag.Demoin.EarTagManagement
{
    /// <summary>
    /// 羊只戴标
    /// </summary>
    [Table("Mark")]
    public class Mark: AuditField
    {
        /// <summary>
        /// 羊只戴标Id
        /// </summary>
        [Key]
        public int MarkId { get; set; }
        /// <summary>
        /// 羊只耳号
        /// </summary>
        [StringLength(30)]
        public string? EarNumber { get; set; }
        /// <summary>
        /// 当前电子耳号
        /// </summary>
        [StringLength(30)]
        public string? OldEarTag { get; set; }
        /// <summary>
        /// 新电子耳号
        /// </summary>
        [StringLength(30)]
        public string? NewEarTag { get; set; }
        /// <summary>
        /// 戴标原因
        /// </summary>
        public Cause Reason { get; set; }

    }
}

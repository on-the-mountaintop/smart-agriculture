﻿using I_Organization.Domains;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Organization.Infrastructure
{
    //数据库上下文
    public class EFDbContext : DbContext
    {
        public EFDbContext(DbContextOptions<EFDbContext> options) : base(options)
        {

        }
        //羊场表
        public DbSet<Sheepwalk> Sheepwalk { get; set; }
    }
}

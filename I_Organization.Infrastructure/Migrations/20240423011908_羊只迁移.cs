﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace I_Organization.Infrastructure.Migrations
{
    public partial class 羊只迁移 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Sheepwalk",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SheepCode = table.Column<int>(type: "int", nullable: false),
                    SheepFull = table.Column<int>(type: "int", nullable: false),
                    SheepAbbre = table.Column<int>(type: "int", nullable: false),
                    SheepScale = table.Column<int>(type: "int", nullable: false),
                    ProviceId = table.Column<int>(type: "int", nullable: false),
                    CityId = table.Column<int>(type: "int", nullable: false),
                    CountyId = table.Column<int>(type: "int", nullable: false),
                    SheepAddress = table.Column<int>(type: "int", nullable: false),
                    SheepAccounts = table.Column<int>(type: "int", nullable: false),
                    Uid = table.Column<int>(type: "int", nullable: false),
                    DepartId = table.Column<int>(type: "int", nullable: false),
                    SheepState = table.Column<int>(type: "int", nullable: false),
                    CreateBy = table.Column<int>(type: "int", nullable: false),
                    CreateDate = table.Column<int>(type: "int", nullable: false),
                    UpdateBy = table.Column<int>(type: "int", nullable: false),
                    UpdateDate = table.Column<int>(type: "int", nullable: false),
                    IsDel = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sheepwalk", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Sheepwalk");
        }
    }
}

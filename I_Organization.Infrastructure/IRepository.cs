﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace I_Organization.Infrastructure
{//仓库
    public interface IRepository<T>where T : class,new()
    {
        //添加
        Task<int> AddAsync(T model);
        //批量添加
        Task<int> AddCountAsync(List<T> model);
        //修改
        Task<int> UpdateAsync(T model);
        //批量修改
        Task<int> UpdateUpdAsync(List<T> list);
        //删除
        Task<int> DeleteAsync(int id);
        // 逻辑删除
        Task<int> DeleteAsync(T info);
        //批量删除
        Task<int> DelCount(Expression<Func<T, bool>> expression);

        Task<List<T>> GetAllAsync();
        //真删
        Task<int> LoDelete(T info);
        // 单个查询
        Task<T> GetModelAsync(int id);
        //查询条件
        Task<List<T>> GetAsync(Expression<Func<T, bool>> expre);
        Task<T> GetModelAsync(Expression<Func<T, bool>> expre);
        //查询IQueryable异步
        Task<IQueryable<T>> GetAll();
        //多条件查询
        Task<T> GetAsyncs(Expression<Func<T, bool>> expre);

    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace I_Archives.Infrastructure.Migrations
{
    public partial class 档案新增图片 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ArchivesPicture",
                table: "archives_info",
                type: "nvarchar(300)",
                maxLength: 300,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ArchivesPicture",
                table: "archives_info");
        }
    }
}

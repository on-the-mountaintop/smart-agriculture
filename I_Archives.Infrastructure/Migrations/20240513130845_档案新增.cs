﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace I_Archives.Infrastructure.Migrations
{
    public partial class 档案新增 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ArchivesRemark",
                table: "archives_info",
                type: "nvarchar(300)",
                maxLength: 300,
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "InDate",
                table: "archives_info",
                type: "datetime2",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PlaceBirth",
                table: "archives_info",
                type: "nvarchar(30)",
                maxLength: 30,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ArchivesRemark",
                table: "archives_info");

            migrationBuilder.DropColumn(
                name: "InDate",
                table: "archives_info");

            migrationBuilder.DropColumn(
                name: "PlaceBirth",
                table: "archives_info");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace I_Archives.Infrastructure.Migrations
{
    public partial class 档案新增出生状态 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "StateBirth",
                table: "archives_info",
                type: "nvarchar(30)",
                maxLength: 30,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StateBirth",
                table: "archives_info");
        }
    }
}

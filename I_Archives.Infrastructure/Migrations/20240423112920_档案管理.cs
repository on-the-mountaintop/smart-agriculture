﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace I_Archives.Infrastructure.Migrations
{
    public partial class 档案管理 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "archives_info",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EarNnumber = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    ElectronicEarNumber = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: false),
                    SheepBreed = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    SheepGender = table.Column<bool>(type: "bit", nullable: false),
                    SheepBir = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Presence = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    GrowthStage = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    SheepType = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    CottageId = table.Column<int>(type: "int", nullable: false),
                    Field = table.Column<int>(type: "int", nullable: false),
                    ReproductiveState = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    PregnancyTest = table.Column<bool>(type: "bit", maxLength: 100, nullable: true),
                    StatusDays = table.Column<int>(type: "int", nullable: true),
                    ProductionGrade = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    SalesGrade = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    GeneticGrade = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    BirthWeight = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    WeaningWeigh = table.Column<decimal>(type: "decimal(18,2)", nullable: true),
                    WeaningDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    FatherEarNumber = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    MotherEarNumber = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    CreateBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreateDate = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdateBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdateDate = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDelete = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_archives_info", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "archives_info");
        }
    }
}

﻿using I_Archives.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Archives.Infrastructure.Interface
{
    public interface IArchivesRepository:IRepository<ArchivesInfo>
    {
    }
}

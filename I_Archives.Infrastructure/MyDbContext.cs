﻿using I_Archives.Domains;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Archives.Infrastructure
{
    public class MyDbContext : DbContext
    {
        public MyDbContext(DbContextOptions<MyDbContext> options) : base(options)
        {
        }
        /// <summary>
        /// 档案管理
        /// </summary>
        public virtual DbSet<ArchivesInfo> ArchivesInfos { get; set; }
    }
}

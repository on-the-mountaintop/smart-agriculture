﻿using I_Archives.Domains;
using I_Archives.Infrastructure.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace I_Archives.Infrastructure.Impls
{
    public class ArchivesRepository : BaseRepository<ArchivesInfo>,IArchivesRepository
    {
        public ArchivesRepository(MyDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<int> AddAsync(ArchivesInfo entity)
        {
            return await base.AddAsync(entity);
        }
        public async Task<int> UpdateAsync(ArchivesInfo entity)
        {
            return await base.UpdateAsync(entity);
        }
        public async Task<ArchivesInfo> GetByIdAsync(int id)
        {
            return await base.GetByIdAsync(id);
        }
        public async Task<ArchivesInfo> GetByFuncAsync(Expression<Func<ArchivesInfo, bool>> handler)
        {
            return await base.GetByFuncAsync(handler);
        }
        public async Task<List<ArchivesInfo>> GetByFuncListAsync(Expression<Func<ArchivesInfo, bool>> handler)
        {
            return await base.GetByFuncListAsync(handler);
        }
        public async Task<List<ArchivesInfo>> GetAllAsync()
        {
            return await base.GetAllAsync();
        }
    }
}

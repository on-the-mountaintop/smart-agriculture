﻿using I_Produce.Domains.Entity;
using I_Produce.Infrastructure.Interface;

namespace I_Produce.Infrastructure.Impl
{
	public class PregnancyPregnancyTestRepository : BaseRepository<PregnancyPregnancyTest>, IPregnancyPregnancyTestRepository
	{
		public PregnancyPregnancyTestRepository(EFCoreDbContext dbContext) : base(dbContext)
		{
		}
	}
}

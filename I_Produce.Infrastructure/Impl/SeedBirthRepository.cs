﻿using I_Produce.Domains.Entity;
using I_Produce.Infrastructure.Interface;

namespace I_Produce.Infrastructure.Impl
{
	public class SeedBirthRepository : BaseRepository<SeedBirth>, ISeedBirthRepository
	{
		public SeedBirthRepository(EFCoreDbContext dbContext) : base(dbContext)
		{
		}
	}
}

﻿using I_Produce.Domains.Entity;
using I_Produce.Infrastructure.Interface;

namespace I_Produce.Infrastructure.Impl
{
	public class SeedWeaningRepository : BaseRepository<SeedWeaning>, ISeedWeaningRepository
	{
		public SeedWeaningRepository(EFCoreDbContext dbContext) : base(dbContext)
		{
		}
	}
}

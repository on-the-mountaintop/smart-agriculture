﻿using I_Produce.Domains.Entity;
using I_Produce.Infrastructure.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Produce.Infrastructure.Impl
{
    public class GermlineHistoryRecordsRepository : BaseRepository<GermlineHistoryRecords>, IGermlineHistoryRecordsRepository
    {
        public GermlineHistoryRecordsRepository(EFCoreDbContext dbContext) : base(dbContext)
        {
        }
    }
}

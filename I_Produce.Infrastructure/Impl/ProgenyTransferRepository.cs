﻿using I_Produce.Domains.Entity;
using I_Produce.Infrastructure.Interface;

namespace I_Produce.Infrastructure.Impl
{
	public class ProgenyTransferRepository : BaseRepository<ProgenyTransfer>, IProgenyTransferRepository
	{
		public ProgenyTransferRepository(EFCoreDbContext dbContext) : base(dbContext)
		{
		}
	}
}

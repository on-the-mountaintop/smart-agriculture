﻿using I_Produce.Domains.Entity;
using I_Produce.Infrastructure.Interface;

namespace I_Produce.Infrastructure.Impl
{
	public class WeaningOfLambRepository : BaseRepository<WeaningOfLamb>, IWeaningOfLambRepository
	{
		public WeaningOfLambRepository(EFCoreDbContext dbContext) : base(dbContext)
		{
		}
	}
}

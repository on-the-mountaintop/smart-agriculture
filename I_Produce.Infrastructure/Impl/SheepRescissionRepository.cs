﻿using I_Produce.Domains.Entity;
using I_Produce.Infrastructure.Interface;

namespace I_Produce.Infrastructure.Impl
{
	public class SheepRescissionRepository : BaseRepository<SheepRescission>, ISheepRescissionRepository
	{
		public SheepRescissionRepository(EFCoreDbContext dbContext) : base(dbContext)
		{
		}
	}
}

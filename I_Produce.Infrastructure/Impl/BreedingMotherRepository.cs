﻿using I_Produce.Domains.Entity;
using I_Produce.Infrastructure.Interface;

namespace I_Produce.Infrastructure.Impl
{
	public class BreedingMotherRepository : BaseRepository<BreedingMother>, IBreedingMotherRepository
	{
		public BreedingMotherRepository(EFCoreDbContext dbContext) : base(dbContext)
		{
		}
	}
}

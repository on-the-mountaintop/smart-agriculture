﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace I_Produce.Infrastructure.Migrations
{
    public partial class _050701 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BreedingMother",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateOfBreeding = table.Column<DateTime>(type: "datetime2", nullable: true),
                    LastDateOfBreeding = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Auricle = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    MotherVariety = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    FirstWithRam = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    MaleBreed = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    BreedingPattern = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    SpeciesEstrusType = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    FirstScore = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    CurrentBuilding = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    CurrentField = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    TransferBuilding = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    DriveField = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    ReproductiveState = table.Column<int>(type: "int", nullable: true),
                    Weight = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    EventDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    PersonCharge = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Auditor = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    AuditTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDel = table.Column<bool>(type: "bit", nullable: false),
                    CreateBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdateDate = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BreedingMother", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BreedingMother");
        }
    }
}

﻿// <auto-generated />
using System;
using I_Produce.Infrastructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace I_Produce.Infrastructure.Migrations
{
    [DbContext(typeof(EFCoreDbContext))]
    [Migration("20240425030315_04-25-1")]
    partial class _04251
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "6.0.29")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder, 1L, 1);

            modelBuilder.Entity("I_Produce.Domains.Entity.GermlineHistoryRecords", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<string>("BreedEarHorn")
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("CreateBy")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("CreateDate")
                        .HasColumnType("datetime2");

                    b.Property<int>("CurrentParity")
                        .HasColumnType("int");

                    b.Property<DateTime>("DateOfBreeding")
                        .HasColumnType("datetime2");

                    b.Property<int>("DeformityNumber")
                        .HasColumnType("int");

                    b.Property<DateTime>("DeliveryDate")
                        .HasColumnType("datetime2");

                    b.Property<int>("HealthyNumber")
                        .HasColumnType("int");

                    b.Property<decimal>("HealthyTotalWeight")
                        .HasColumnType("decimal(10,2)");

                    b.Property<bool>("IsDel")
                        .HasColumnType("bit");

                    b.Property<string>("MotherEarHorn")
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<int>("MummyNumber")
                        .HasColumnType("int");

                    b.Property<int>("NumberOfFreaks")
                        .HasColumnType("int");

                    b.Property<int>("NumberOfStillbirths")
                        .HasColumnType("int");

                    b.Property<DateTime>("PregnancyTestDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("PregnancyTestResult")
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("UpdateBy")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime?>("UpdateDate")
                        .HasColumnType("datetime2");

                    b.Property<int>("WeakLambNumber")
                        .HasColumnType("int");

                    b.Property<DateTime>("WeaningDate")
                        .HasColumnType("datetime2");

                    b.Property<decimal>("WeaningLitterWeight")
                        .HasColumnType("decimal(10,2)");

                    b.Property<int>("WeaningNumber")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.ToTable("GermlineHistoryRecords");
                });

            modelBuilder.Entity("I_Produce.Domains.Entity.SheepRescission", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"), 1L, 1);

                    b.Property<DateTime?>("AuditTime")
                        .HasColumnType("datetime2");

                    b.Property<string>("Auditor")
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<int>("Breed")
                        .HasColumnType("int");

                    b.Property<string>("CreateBy")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime>("CreateDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("DriveField")
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<bool>("IsDel")
                        .HasColumnType("bit");

                    b.Property<string>("RolloutField")
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("RoundType")
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<bool>("Sex")
                        .HasColumnType("bit");

                    b.Property<DateTime>("SurrenderDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("TransferBuilding")
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("TurnBuilding")
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("UpdateBy")
                        .HasColumnType("nvarchar(max)");

                    b.Property<DateTime?>("UpdateDate")
                        .HasColumnType("datetime2");

                    b.Property<string>("VisualEarSignal")
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.HasKey("Id");

                    b.ToTable("SheepRescission");
                });
#pragma warning restore 612, 618
        }
    }
}

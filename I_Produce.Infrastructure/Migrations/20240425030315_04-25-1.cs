﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace I_Produce.Infrastructure.Migrations
{
    public partial class _04251 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "GermlineHistoryRecords",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MotherEarHorn = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    BreedEarHorn = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    DateOfBreeding = table.Column<DateTime>(type: "datetime2", nullable: false),
                    PregnancyTestDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    PregnancyTestResult = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    DeliveryDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    HealthyNumber = table.Column<int>(type: "int", nullable: false),
                    HealthyTotalWeight = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    WeakLambNumber = table.Column<int>(type: "int", nullable: false),
                    DeformityNumber = table.Column<int>(type: "int", nullable: false),
                    NumberOfStillbirths = table.Column<int>(type: "int", nullable: false),
                    MummyNumber = table.Column<int>(type: "int", nullable: false),
                    NumberOfFreaks = table.Column<int>(type: "int", nullable: false),
                    WeaningDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    WeaningNumber = table.Column<int>(type: "int", nullable: false),
                    WeaningLitterWeight = table.Column<decimal>(type: "decimal(10,2)", nullable: false),
                    CurrentParity = table.Column<int>(type: "int", nullable: false),
                    IsDel = table.Column<bool>(type: "bit", nullable: false),
                    CreateBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdateDate = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GermlineHistoryRecords", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SheepRescission",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    SurrenderDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    VisualEarSignal = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    RoundType = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    TurnBuilding = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    RolloutField = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    TransferBuilding = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    DriveField = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Breed = table.Column<int>(type: "int", nullable: false),
                    Sex = table.Column<bool>(type: "bit", nullable: false),
                    Auditor = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    AuditTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDel = table.Column<bool>(type: "bit", nullable: false),
                    CreateBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdateDate = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SheepRescission", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GermlineHistoryRecords");

            migrationBuilder.DropTable(
                name: "SheepRescission");
        }
    }
}

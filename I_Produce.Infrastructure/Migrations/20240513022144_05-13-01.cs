﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace I_Produce.Infrastructure.Migrations
{
    public partial class _051301 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SeedBirth",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MotherEarHorn = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    DeliveryDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    GestationDays = table.Column<int>(type: "int", nullable: true),
                    DifficultyOfDelivery = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Motherhood = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Lactation = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Mastitis = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    TotalNumberLambsLitter = table.Column<int>(type: "int", nullable: true),
                    LitterTotalWeight = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    LiveLambCount = table.Column<int>(type: "int", nullable: true),
                    TotalWeightLiveLamb = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    AllLiveLambsWeigh = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    DeadLambCount = table.Column<int>(type: "int", nullable: true),
                    TotalDeadLambWeight = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    AllDeadLambsWeigh = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    FatSheepLot = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    HealthyNumber = table.Column<int>(type: "int", nullable: true),
                    WeakLambNumber = table.Column<int>(type: "int", nullable: true),
                    DeformityNumber = table.Column<int>(type: "int", nullable: true),
                    NumberOfSTillbirths = table.Column<int>(type: "int", nullable: true),
                    MummyNumber = table.Column<int>(type: "int", nullable: true),
                    BroodNumber = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Sex = table.Column<bool>(type: "bit", nullable: false),
                    CurrentBuilding = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    CurrentField = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    TransferBuilding = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    DriveField = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    PersonCharge = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Auditor = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    AuditTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDel = table.Column<bool>(type: "bit", nullable: false),
                    CreateBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdateDate = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SeedBirth", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "SeedWeaning",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    WeaningDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Auricle = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    BreastfeedingDays = table.Column<int>(type: "int", nullable: true),
                    SeedWeight = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    WeaningNumber = table.Column<int>(type: "int", nullable: true),
                    TotalLitterWeightWeaning = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    CurrentBuilding = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    CurrentField = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    TransferBuilding = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    DriveField = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    PersonCharge = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Auditor = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    AuditTime = table.Column<DateTime>(type: "datetime2", nullable: true),
                    WeaningReason = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    IsDel = table.Column<bool>(type: "bit", nullable: false),
                    CreateBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdateDate = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SeedWeaning", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SeedBirth");

            migrationBuilder.DropTable(
                name: "SeedWeaning");
        }
    }
}

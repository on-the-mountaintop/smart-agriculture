﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace I_Produce.Infrastructure.Migrations
{
    public partial class _051205 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "PregnancyTestResult",
                table: "PregnancyPregnancyTest",
                type: "bit",
                maxLength: 50,
                nullable: false,
                defaultValue: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(50)",
                oldMaxLength: 50,
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "PregnancyTestResult",
                table: "PregnancyPregnancyTest",
                type: "nvarchar(50)",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(bool),
                oldType: "bit",
                oldMaxLength: 50);
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace I_Produce.Infrastructure.Migrations
{
    public partial class _051501 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ProgenyTransfer",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DateConversion = table.Column<DateTime>(type: "datetime2", nullable: true),
                    GrowSheepEars = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    CurrentBuilding = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    CurrentField = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    TransferBuilding = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    DriveField = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Sex = table.Column<bool>(type: "bit", nullable: false),
                    AgeTransplanting = table.Column<int>(type: "int", nullable: true),
                    IsDel = table.Column<bool>(type: "bit", nullable: false),
                    CreateBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdateDate = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProgenyTransfer", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WeaningOfLamb",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    LambsEar = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    WeaningAge = table.Column<int>(type: "int", nullable: true),
                    WeaningWeight = table.Column<decimal>(type: "decimal(10,2)", nullable: true),
                    LamTheMotheLactatingSpeciesbsEar = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    Parity = table.Column<int>(type: "int", nullable: true),
                    WeaningHouse = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    WeaningField = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    TransferBuilding = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    DriveField = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true),
                    WeaningDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    IsDel = table.Column<bool>(type: "bit", nullable: false),
                    CreateBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreateDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    UpdateBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdateDate = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WeaningOfLamb", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProgenyTransfer");

            migrationBuilder.DropTable(
                name: "WeaningOfLamb");
        }
    }
}

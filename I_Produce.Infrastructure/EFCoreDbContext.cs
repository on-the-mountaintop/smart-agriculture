﻿using I_Produce.Domains.Entity;
using Microsoft.EntityFrameworkCore;

namespace I_Produce.Infrastructure
{
    public class EFCoreDbContext : DbContext
    {
        public EFCoreDbContext(DbContextOptions<EFCoreDbContext> options) : base(options)
        {
        }

        /// <summary>
        /// 种母历史记录
        /// </summary>
        public DbSet<GermlineHistoryRecords> GermlineHistoryRecords { get; set; }

        /// <summary>
        /// 羊只转舍
        /// </summary>
        public DbSet<SheepRescission> SheepRescission { get; set; }

        /// <summary>
        /// 种母配种
        /// </summary>
        public DbSet<BreedingMother> BreedingMother { get; set; }

        /// <summary>
        /// 种母妊检
        /// </summary>
        public DbSet<PregnancyPregnancyTest> PregnancyPregnancyTest { get; set; }

        /// <summary>
        /// 种母分娩
        /// </summary>
        public DbSet<SeedBirth> SeedBirth { get; set; }

        /// <summary>
        /// 种母断奶
        /// </summary>
        public DbSet<SeedWeaning> SeedWeaning { get; set; }

        /// <summary>
        /// 羊羔断奶
        /// </summary>
        public DbSet<WeaningOfLamb> WeaningOfLamb { get; set; }

        /// <summary>
        /// 后裔转种
        /// </summary>
        public DbSet<ProgenyTransfer> ProgenyTransfer { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace I_Produce.Infrastructure
{
    public interface IRepository<T> where T : class, new()
    {
        Task<int> AddAsync(T entity);

        Task<int> UpdateAsync(T entity);

        Task<int> UpdateRangeAsync(List<T> values);

        Task<T> GetByIdAsync(int id);

        Task<IQueryable<T>> All();

        Task<T> GetByFuncAsync(Expression<Func<T, bool>> expre);

        Task<List<T>> GetAllAsync();

        Task<List<T>> GetAsync(Expression<Func<T, bool>> expre);

        Task<int> DeleteAsync(T entity);

        Task<int> BatchDeleteAsync(List<T> roleInfos);
    }
}

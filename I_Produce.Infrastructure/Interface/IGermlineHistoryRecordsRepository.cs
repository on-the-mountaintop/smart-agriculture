﻿using I_Produce.Domains.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Produce.Infrastructure.Interface
{
    public interface IGermlineHistoryRecordsRepository : IRepository<GermlineHistoryRecords>
    {
    }
}

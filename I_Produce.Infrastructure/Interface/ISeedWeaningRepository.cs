﻿using I_Produce.Domains.Entity;

namespace I_Produce.Infrastructure.Interface
{
	public interface ISeedWeaningRepository : IRepository<SeedWeaning>
	{

	}
}

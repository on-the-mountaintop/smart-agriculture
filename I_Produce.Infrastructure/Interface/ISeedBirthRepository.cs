﻿using I_Produce.Domains.Entity;

namespace I_Produce.Infrastructure.Interface
{
	public interface ISeedBirthRepository : IRepository<SeedBirth>
    {

    }
}

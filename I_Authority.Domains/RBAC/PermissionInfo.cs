﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Authorization.Domains.RBAC
{
    /// <summary>
    /// 权限表
    /// </summary>
    [Table("PermissionInfo")]
    public class PermissionInfo: AuditInfo
    {
        /// <summary>
        /// 权限编号  主键
        /// </summary>
        [Key]
        public int PermissionId { get; set; }

        /// <summary>
        /// 功能名称
        /// </summary>
        [StringLength(30)]
        public string? FunctionName { get; set; }

        /// <summary>
        /// 操作
        /// </summary>
        [StringLength(50)]
        public string? Operation { get; set; }

        /// <summary>
        /// 外键  角色编号
        /// </summary>
        public int RoleId { get; set; }
    }
}

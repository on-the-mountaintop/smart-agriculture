﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Authorization.Domains.RBAC
{
    /// <summary>
    /// 角色表
    /// </summary>
    [Table("RoleInfo")]
    public class RoleInfo: AuditInfo
    {
        /// <summary>
        /// 角色编号 主键
        /// </summary>
        [Key]
        public int RoleId { get; set; }

        /// <summary>
        /// 角色名称
        /// </summary>
        [StringLength(20)]
        public string? RoleName { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [StringLength(40)]
        public string? RoleRemark { get; set; }

        /// <summary>
        /// 角色描述
        /// </summary>
        [StringLength(30)]
        public string? RoleDescription { get; set; }
    }
}

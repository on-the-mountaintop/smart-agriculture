﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Authorization.Domains.RBAC
{
    public class ShowPermissionRoleDTO
    {
        /// <summary>
        /// 权限编号
        /// </summary>
        public int PermissionId { get; set; }

        /// <summary>
        /// 功能名称
        /// </summary>
        
        public string? FunctionName { get; set; }

        /// <summary>
        /// 操作
        /// </summary>
        public string? Operation { get; set; }

        /// <summary>
        /// 外键  角色编号
        /// </summary>
        public int RoleId { get; set; }

        /// <summary>
        /// 角色名称
        /// </summary>
        public string? RoleName { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        public string? RoleRemark { get; set; }

        /// <summary>
        /// 角色描述
        /// </summary>
        public string? RoleDescription { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Authorization.Domains.RBAC
{
    [Table("menu_info")]
    public class MenuList
    {
        [Key]
        /// <summary>
        /// 菜单Id
        /// </summary>
        public int Id {  get; set; }
        /// <summary>
        /// 菜单名称
        /// </summary>
        [StringLength(30)]
        public string? MenuName {  get; set; }
        /// <summary>
        /// 菜单URL
        /// </summary>
        [StringLength(300)]
        public string? URL {  get; set; }
        /// <summary>
        /// 操作
        /// </summary>
        [StringLength(50)]
        public string? Operation { get; set; }

        /// <summary>
        /// 父级Id
        /// </summary>
        public int ParentId {  get; set; }

    }
}

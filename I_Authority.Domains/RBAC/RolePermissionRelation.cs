﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Authorization.Domains.RBAC
{
    /// <summary>
    /// 角色权限关系表
    /// </summary>
    [Table("RolePermissionRelation")]
    public class RolePermissionRelation
    {
        /// <summary>
        /// 角色权限关系编号   主键
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// 外键  角色编号
        /// </summary>
        public int RoleId { get; set; }

        /// <summary>
        /// 外键  权限编号
        /// </summary>
        public int PermissionId { get; set; }
    }
}

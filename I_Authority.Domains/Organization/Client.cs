﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Authorization.Domains.Organization
{
    /// <summary>
    /// 客户信息表
    /// </summary>
    [Table("Client")]
    public class Client : Audit_field
    {
        [Key]
        public int ClientId { get; set; } //客户Id
        [StringLength(30)]
        public string? ClientNumber { get; set; } //har(20) 客户编号
        [StringLength(30)]
        public string? ClientName { get; set; } //ar(20) 客户名称
        [StringLength(30)]
        public string? ClientType { get; set; } //ar(20) 客户类型
        [StringLength(30)]
        public string? PrincipalTel { get; set; } //archar(20) 负责人电话
        [StringLength(30)]
        public string? InputUnit { get; set; } //ar(20) 录入单位
        [StringLength(30)]
        public string? BuildingUser { get; set; } //archar(20) 使用单位
        [StringLength(30)]
        public string? Market { get; set; } // 所属市场
        [StringLength(30)]
        public string? ServiceStaff { get; set; } //archar(20) 服务人员
        [StringLength(30)]
        public string? CollectionFile { get; set; } //archar(20) 归集档案
        public bool UseOftate { get; set; } //使用状态
        [StringLength(30)]
        public string? PrincipalName { get; set; } //char(20) 负责人姓名
        [StringLength(30)]
        public string? IDNumber { get; set; } //ar(20) 身份证
        [StringLength(30)]
        public string? IndustryInvolved { get; set; } //   varchar(20) 所属行业
        [StringLength(30)]
        public string? Classification { get; set; } //archar(20) 客户级别
        [StringLength(30)]
        public string? AffiliatedUnit { get; set; } //archar(20) 所属单位
        [StringLength(30)]
        public string? Salesman { get; set; } //ar(20) 业务员
    }
}

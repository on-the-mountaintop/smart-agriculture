﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Authorization.Domains.Organization
{
    /// <summary>
    /// 羊场表
    /// </summary>
    [Table("Shee_Yard")]
    public class Shee_Yard: Audit_field
    {
        [Key]
        public int SheepId { get; set; }             //主键标识
        [StringLength(30)]
        public string? SheepCode { get; set; }	     //羊场编码（不唯一）
        [StringLength(30)]
        public string? SheepFull { get; set; }	     //羊场全称
        [StringLength(30)]
        public string? SheepAbbre { get; set; }       //羊场简称
        [StringLength(30)]
        public string? SheepScale { get; set; }       //羊场规模
        [StringLength(30)]
        public string? SheepAddress { get; set; }       //详细地址(省市县全部)
        public DateTime? SheeAccounts { get; set; }   //建账日期
        public int Uid { get; set; }       //负责人（外键）
        public int DepartId { get; set; }       //管理部门（部门外键）
        public bool SheepState { get; set; }      //是否启用
    }
}

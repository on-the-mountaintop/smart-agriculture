﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Authorization.Domains.Organization
{
    /// <summary>
    /// 个体销售表
    /// </summary>
    [Table("EntityMarket")]
    public class EntityMarket : Audit_field
    {
        [Key]
        public int? MarketId { get; set; }  //销售编号
        public DateTime? MarketDate { get; set; }  // 销售日期
        [StringLength(30)]
        public string? DocumentNumber { get; set; }  //单据号
        public int Client { get; set; }  //客户
        public int SheepNumber { get; set; }  //只数
        public decimal TotalPrice { get; set; }  //总成交价（元）
        public bool AuditState { get; set; }  //审核状态
        public int DepartmentId { get; set; }  //部门
        [StringLength(30)]
        public string? SellPeople { get; set; }  //销售员
        [StringLength(30)]
        public string? ExamineBy { get; set; }  //审核人
        [StringLength(30)]
        public string? MarketAddress { get; set; }  //收货地址
        public DateTime ExamineDate { get; set; }  //审核时间
        [StringLength(30)]
        public string? FinanceBy { get; set; }  //财务审核人
        public DateTime FinanceDate { get; set; }  //财务审核时间
        [StringLength(30)]
        public string? StashBy { get; set; }  //仓库审核人
        public DateTime StashDate { get; set; }  //仓库审核时间
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Authorization.Domains.Organization
{
    /// <summary>
    /// 栋舍表
    /// </summary>
    [Table("Cottage")]
    public class Cottage:Audit_field
    {
        [Key]
        public int CottageId { get; set; }        //主键标识
        [StringLength(30)]
        public string? CottageType { get; set; }        //栋舍类型
        [StringLength(30)]
        public string? CottageName { get; set; }        //栋舍名称
        [StringLength(30)]
        public string? Principal { get; set; }        //负责人（外键）
        [StringLength(30)]
        public string? CottageProduction { get; set; }         //生产人员
        public bool CottageState { get; set; }         //状态（启用、禁用）
        [StringLength(30)]
        public string? CottageRemark { get; set; }         //备注
    }

}

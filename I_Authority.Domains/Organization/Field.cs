﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Authorization.Domains.Organization
{
    /// <summary>
    /// 栏位表
    /// </summary>
    [Table("Field")]
    public class Field: Audit_field
    {
        [Key]
        public int FieId { get; set; }            //主键标识
        public int CottageId { get; set; }            //栋舍编号（外键）
        [StringLength(30)]
        public string? FieName { get; set; }         //栏位名称
    }
}

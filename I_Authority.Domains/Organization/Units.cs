﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace I_Authorization.Domains.Organization
{
    [Table("Units")]
    public class Units: Audit_field
    {
        [Key]
        public int UnitId { get; set; }   //主键标识
        [StringLength(30)]
        public string? UnitFull { get; set; }   //archar（30）	企业名称
        [StringLength(200)]
        public string? UnitLOGO { get; set; }   // nvarchar（30）	企业LOGO图片
        [StringLength(30)]
        public string? TradeLable { get; set; }   // nvarchar（30）	行业标签
        [StringLength(30)]
        public string? UnitAddress { get; set; }   // nvarchar（100）	详细地址
        [StringLength(30)]
        public string? UnitPhone { get; set; }   //rchar（20）	企业电话
        [StringLength(30)]
        public string? UnitCredit { get; set; }   // nvarchar（30）	统一社会信用代码
        public bool UnitState { get; set; }   // bit 停用企业
        [StringLength(30)]
        public string? UnitPrinc { get; set; }   //varchar（30）	负责人
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace I_Authorization.Domains.Organization
{
    /// <summary>
    /// 个体采购信息表
    /// </summary>
    [Table("EntityPurchase")]
    public class EntityPurchase : Audit_field
    {
        [Key]
        public int PurId { get; set; }// 羊只采购编号
        [StringLength(30)]
        public string? DocumentNumber { get; set; }//   单据号
        public DateTime PurDate { get; set; } = DateTime.Now; // 采购日期
        [StringLength(30)]
        public string? PruTytpe { get; set; }// 类别
        public int Supplier { get; set; }//  供应商
        public int Digest { get; set; }//  摘要
        [StringLength(30)]
        public string? Earbugles { get; set; }//  耳号/批次
        [StringLength(30)]
        public string? CottageName { get; set; }//    栋舍
        [StringLength(30)]
        public string? FieldName { get; set; }//    转入栏位
        public bool PurSex { get; set; }//  性别
        [StringLength(30)]
        public string? Phase { get; set; }//   阶段
        [StringLength(30)]
        public string? Buyer { get; set; }//   采购员
        [StringLength(30)]
        public string? Stash { get; set; }//    仓库
        [StringLength(30)]
        public string? GoodName { get; set; }//   商品名称
        [StringLength(30)]
        public string? Package { get; set; }//   标包
        public int? MeasuringUnit { get; set; }//  计量单位
        public int? AdjustNmber { get; set; }//  调整数量
        public int? PayNumber { get; set; }//  结算数量
        public int? QuantityNumber { get; set; }//  入库数量
        public decimal? AveragePrice { get; set; }//  只均单价
        public decimal? Price { get; set; }//  金额
        public decimal? PriceDiscount { get; set; }//  单位折扣
        public decimal? PriceAdjust { get; set; }// 调整金额
        public decimal? PricePay { get; set; }//  结算金额
        public int? TotalWeight { get; set; }// 总重量(公斤)
    }
}

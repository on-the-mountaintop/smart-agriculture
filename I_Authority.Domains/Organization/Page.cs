﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Authorization.Domains.Organization
{
    /// <summary>
    /// 分页方法
    /// </summary>
    public class Page<T>
    {
        public List<T>? Data { get; set; }
        public int tatalCount { get; set; }
        public int pageCount { get; set; }
    }
}

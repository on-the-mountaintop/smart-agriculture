﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace I_Authorization.Domains.Organization
{
    [Table("Department")]
    public class Department: Audit_field
    {
        [Key]
        public int DepartId { get; set; }   //键标识
        public int DepartCode { get; set; }   //部门编号（唯一）
        [StringLength(20)]
        public string? DepartName { get; set; }   //部门名称
        public int DepartState { get; set; }   //部门状态（启用、停用）
        [StringLength(20)]
        public string? Departtype { get; set; }   //部门类型(下拉框死值)
        public int DepartRank { get; set; }   //部门序号（级别）
        public int ParentId { get; set; }   //级编号
        [StringLength(100)]
        public string? DepartArea { get; set; }   //部门区域
        [StringLength(100)]
        public string? DepartmentHead { get; set; }   // 部门负责人
        public string? CreateBy { get; set; } //nvarchar（20）	添加人
        public DateTime? CreateDate { get; set; } //datetime 添加时间
        public string? UpdateBy { get; set; } //nvarchar（20）	修改人
        public DateTime? UpdateDate { get; set; } //datetime 修改时间
        public bool? IsDel { get; set; } = false; //bool 确认删除

    }
}

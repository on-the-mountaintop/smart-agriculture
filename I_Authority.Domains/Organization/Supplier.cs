﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Authorization.Domains.Organization
{
    /// <summary>
    ///供应商细信息列表 
    /// </summary>
    [Table("Supplier")]
    public class Supplier : Audit_field
    {
        [Key]
        public int SupplierId { get; set; } //供应商
        [StringLength(30)]
        public string? SupplierName { get; set; }//供应商名称
        [StringLength(30)]
        public string? SupplierNumber { get; set; }// 供应商编号
        public bool SupplierType { get; set; }//供应商类型
        [StringLength(30)]
        public string? PrincipalTelephone { get; set; }// 负责人电话
        [StringLength(30)]
        public string? InputUnit { get; set; }// 录入单位
        [StringLength(30)]
        public string? BuildingUser { get; set; }// 使用单位
        [StringLength(30)]
        public string? Market { get; set; }// 所属市场
        [StringLength(30)]
        public string? ServiceStaff { get; set; }//服务人员
        [StringLength(30)]
        public string? CollectionFile { get; set; }//归集档案
        public bool UseOftate { get; set; }//用状态
        [StringLength(30)]
        public string? PrincipalName { get; set; }//负责人姓名
        [StringLength(30)]
        public string? IDNumber { get; set; }//身份证
        [StringLength(30)]
        public string? Location { get; set; }//所在地
        [StringLength(30)]
        public string? IndustryInvolved { get; set; }//所属行业
        [StringLength(30)]
        public string? AffiliatedUnit { get; set; }//所属单位
        [StringLength(30)]
        public string? Salesman { get; set; }// 业务员
        [StringLength(30)]
        public string? SupplierImg { get; set; }//供应商头像
        public DateTime DateOfCooperation { get; set; }//合作日期
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Authorization.Domains
{
    /// <summary>
    /// 审计字段
    /// </summary>
    public class AuditInfo
    {
        /// <summary>
        /// 添加人
        /// </summary>
        public string? CreateBy { get; set; }
        public DateTime? CreateDate { get; set; }
        public string? UpdateBy { get; set; }
        public DateTime? UpdateDate { get; set; }

        public bool? IsDel { get; set; } = true;
    }
}

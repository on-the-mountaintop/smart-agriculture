﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Authorization.Domains
{
    public class Audit_field
    {
        public string? CreateBy    {get;set;} //nvarchar（20）	添加人
        public DateTime? CreateDate  {get;set;} //datetime 添加时间
        public string? UpdateBy    {get;set;} //nvarchar（20）	修改人
        public DateTime? UpdateDate  {get;set;} //datetime 修改时间
        public bool? IsDel { get; set; } = false; //bool 确认删除
    }
    }

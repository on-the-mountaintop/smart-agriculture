﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Authorization.Domains.Purchase
{
    /// <summary>
    /// 个体采购  羊只采购表
    /// </summary>
    [Table("PurchaseInfo")]
    public class PurchaseInfo
    {
        /// <summary>
        /// 采购编号  主键
        /// </summary>
        [Key]
        public int PurchaseId { get; set; }
    }
}

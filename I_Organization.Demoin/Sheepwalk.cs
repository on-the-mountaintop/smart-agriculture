﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Organization.Domains
{
    /// <summary>
    /// 羊场表
    /// </summary>
    [Table("Sheepwalk")]
    public class Sheepwalk
    {
        [Key]
        public int Id            { get; set; }
        public string? SheepCode     { get; set; }	//羊场编码（不唯一）
        public string? SheepFull     { get; set; }//羊场全称
        public string? SheepAbbre    { get; set; }//羊场简称
        public int SheepScale    { get; set; } //羊场规模
        public int ProviceId     { get; set; } //所在区域（省表）
        public int CityId        { get; set; }  //所在区域（市表）
        public int CountyId      { get; set; }   //所在区域（县表）
        public string? SheepAddress  { get; set; }	//详细地址
        public DateTime SheepAccounts { get; set; }   //建账日期
        public int Uid           { get; set; }   //负责人（外键）
        public int DepartId      { get; set; }   //管理部门（部门外键）
        public bool SheepState    { get; set; }   //是否启用
        public string? CreateBy      { get; set; }   //添加人
        public int CreateDate    { get; set; }   //添加时间
        public string? UpdateBy      { get; set; }   //修改人
        public int UpdateDate    { get; set; }   //修改时间
        public int IsDel         { get; set; }   //确认删除
    }
}

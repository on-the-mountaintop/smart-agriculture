﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace I_Ear_Tag.Infrastructure
{
   public interface IRepository<T> where T : class,new()
    {
        /// <summary>
        /// 添加
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        Task<int> AddAsync(T t);
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        Task<int> UpdateAsync(T t);
        /// <summary>
        /// 通过主键查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<T> GetModelAsync(int id);
        /// <summary>
        /// 通过条件找到对应实体单条数据
        /// </summary>
        /// <param name="expression">表达式</param>
        /// <returns>单条数据</returns>
        Task<T> GetModelAsync(Expression<Func<T, bool>> expression);
        /// <summary>
        /// 显示
        /// </summary>
        /// <returns></returns>
        Task<List<T>> GetAllAsync();
        /// <summary>
        /// 查询带条件
        /// </summary>
        /// <param name="expre"></param>
        /// <returns></returns>
        Task<List<T>> GetAsync(Expression<Func<T, bool>> expre);
        /// <summary>
        /// 删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<int> DeleteAsync(int id);
        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="expression">表达式</param>
        /// <returns></returns>
        Task<int> DeleteAsync(Expression<Func<T, bool>> expression);
        /// <summary>
        /// 批量添加
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        Task<int> AddAsync(List<T> t);
        /// <summary>
        /// 批量修改
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        Task<int> UpdateAsync(List<T> t);
        /// <summary>
        /// 逻辑删除
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<int> StateDelete(T t);
    }
}

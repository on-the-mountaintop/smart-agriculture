﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace I_Ear_Tag.Infrastructure.Migrations
{
    public partial class zhaoqianan : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EarTag",
                columns: table => new
                {
                    EarTagId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BirthStatus = table.Column<int>(type: "int", nullable: false),
                    SheepBreed = table.Column<int>(type: "int", nullable: false),
                    InsertType = table.Column<int>(type: "int", nullable: false),
                    EarNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    StorageDate = table.Column<DateTime>(type: "datetime2", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EarTag", x => x.EarTagId);
                });

            migrationBuilder.CreateTable(
                name: "Mark",
                columns: table => new
                {
                    MarkId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EarNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    OldEarTag = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NewEarTag = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Reason = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mark", x => x.MarkId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "EarTag");

            migrationBuilder.DropTable(
                name: "Mark");
        }
    }
}

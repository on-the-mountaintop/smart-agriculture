﻿using I_Ear_Tag.Demoin.EarTagManagement;
using Microsoft.EntityFrameworkCore;

namespace I_Ear_Tag.Infrastructure
{
    public class EFDbContext:DbContext
    {
        public EFDbContext(DbContextOptions<EFDbContext> options) : base(options) { }
        /// <summary>
        /// 耳标管理表
        /// </summary>
        public DbSet<EarTag> EarTag {  get; set; }
        /// <summary>
        /// 羊只戴标
        /// </summary>
        public DbSet<Mark> Mark {  get; set; }
    }
}

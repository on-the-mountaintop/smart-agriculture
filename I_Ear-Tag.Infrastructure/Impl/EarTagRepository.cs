﻿using I_Ear_Tag.Demoin.EarTagManagement;
using I_Ear_Tag.Infrastructure.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace I_Ear_Tag.Infrastructure.Impl
{
    public class EarTagRepository : BaseRepostitory<EarTag> ,IEarTagRepository
    {
        public EarTagRepository(EFDbContext context) : base(context)
        {
        }

        public async Task<int> AddAsync(EarTag t)
        {
           return await base.AddAsync(t);
        }

        public async Task<int> AddAsync(List<EarTag> t)
        {
           return await base.AddAsync(t);
        }

        public async Task<int> DeleteAsync(int id)
        {
            return await base.DeleteAsync(id);
        }

        public async Task<int> DeleteAsync(Expression<Func<EarTag, bool>> expression)
        {
            return await base.DeleteAsync(expression);
        }

        public async Task<List<EarTag>> GetAllAsync()
        {
            return await base.GetAllAsync();
        }

        public async Task<List<EarTag>> GetAsync(Expression<Func<EarTag, bool>> expre)
        {
            return await base.GetAsync(expre);
        }

        public async Task<EarTag> GetModelAsync(int id)
        {
            return await base.GetModelAsync(id);
        }

        public async Task<EarTag> GetModelAsync(Expression<Func<EarTag, bool>> expression)
        {
          return await base.GetModelAsync(expression);
        }

        public async Task<int> StateDelete(EarTag t)
        {
            return await base.StateDelete(t);
        }

        public async Task<int> UpdateAsync(EarTag t)
        {
            return await base.UpdateAsync(t);
        }
    }
}

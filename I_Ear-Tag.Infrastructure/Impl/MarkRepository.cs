﻿using I_Ear_Tag.Demoin.EarTagManagement;
using I_Ear_Tag.Infrastructure.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace I_Ear_Tag.Infrastructure.Impl
{
    public class MarkRepository : BaseRepostitory<Mark>, IMarkRepository
    {
        public MarkRepository(EFDbContext context) : base(context)
        {
        }

        public async Task<int> AddAsync(Mark t)
        {
           return await base.AddAsync(t);
        }

        public async Task<int> AddAsync(List<Mark> t)
        {
            return await base.AddAsync(t);
        }

        public async Task<int> DeleteAsync(int id)
        {
            return await base.DeleteAsync(id);
        }

        public async Task<int> DeleteAsync(Expression<Func<Mark, bool>> expression)
        {
            return await base.DeleteAsync(expression);
        }

        public async Task<List<Mark>> GetAllAsync()
        {
            return await base.GetAllAsync();
        }

        public async Task<List<Mark>> GetAsync(Expression<Func<Mark, bool>> expre)
        {
           return await base.GetAsync(expre);
        }

        public async Task<Mark> GetModelAsync(int id)
        {
           return await base.GetModelAsync(id);
        }

        public async Task<Mark> GetModelAsync(Expression<Func<Mark, bool>> expression)
        {
            return await base.GetModelAsync(expression);
        }

        public async Task<int> StateDelete(Mark t)
        {
            return await base.StateDelete(t);
        }

        public async Task<int> UpdateAsync(Mark t)
        {
            return await base.UpdateAsync(t);
        }
    }
}

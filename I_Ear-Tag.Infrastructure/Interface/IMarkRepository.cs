﻿using I_Ear_Tag.Demoin.EarTagManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Ear_Tag.Infrastructure.Interface
{
    public interface IMarkRepository: IRepository<Mark>
    {
    }
}

﻿using I_Authorization.Domains.Organization;
using I_Authorization.Infrastructure.Interface;
using I_Authorization.Services.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Authorization.Services.Service
{
    public class UnitsService: IUnitsService
    {
        private readonly IUnitsRepository _units;

        public UnitsService(IUnitsRepository units)
        {
            _units = units;
        }
        public async Task<int> UnitsCreate(Units info)
        {
            var res = new Units
            {
                UnitFull = info.UnitFull,
                UnitLOGO = info.UnitLOGO,
                TradeLable = info.TradeLable,
                UnitAddress = info.UnitAddress,
                UnitPhone = info.UnitPhone,
                UnitCredit = info.UnitCredit,
                UnitState = false,
                UnitPrinc = info.UnitPrinc
            };
            return await _units.AddAsync(res);
        }
    }
}

﻿using I_SheepManagement.Domains;
using I_SheepManagement.Infrastructure.Interface;
using I_SheepManagement.Read.API.Application.Command;
using MediatR;

namespace I_SheepManagement.Read.API.Application.CommandHandler
{
    public class DeathManageReadByIdCommandHandler : IRequestHandler<DeathManageReadByIdCommand, DeathManagement>
    {
        private readonly IDeathManagementRepository _sheepManagementRepository;

        public DeathManageReadByIdCommandHandler(IDeathManagementRepository sheepManagementRepository)
        {
            _sheepManagementRepository = sheepManagementRepository;
        }

        public async Task<DeathManagement> Handle(DeathManageReadByIdCommand request, CancellationToken cancellationToken)
        {
            return await _sheepManagementRepository.GetByFuncAsync(x => x.EarNumber == request.EarNumber);
        }
    }
}

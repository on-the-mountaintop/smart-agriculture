﻿using I_SheepManagement.Domains;
using I_SheepManagement.Infrastructure.Interface;
using I_SheepManagement.Read.API.Application.Command;
using MediatR;

namespace I_SheepManagement.Read.API.Application.CommandHandler
{
    public class EliminationManageReadByIdCommandHandler : IRequestHandler<EliminationManageReadByIdCommand, EliminationManagement>
    {
        private readonly IEliminationManagementRepository _eliminationManagementRepository;

        public EliminationManageReadByIdCommandHandler(IEliminationManagementRepository eliminationManagementRepository)
        {
            _eliminationManagementRepository = eliminationManagementRepository;
        }

        public async Task<EliminationManagement> Handle(EliminationManageReadByIdCommand request, CancellationToken cancellationToken)
        {
            return await _eliminationManagementRepository.GetByFuncAsync(x => x.EarNumber == request.EarNumber);
        }
    }
}

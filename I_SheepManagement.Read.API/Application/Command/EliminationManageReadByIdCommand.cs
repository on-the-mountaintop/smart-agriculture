﻿using I_SheepManagement.Domains;
using MediatR;

namespace I_SheepManagement.Read.API.Application.Command
{
    public class EliminationManageReadByIdCommand:IRequest<EliminationManagement>
    {
        public string? EarNumber { get; set; }
    }
}

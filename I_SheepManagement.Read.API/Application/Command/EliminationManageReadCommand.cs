﻿using CommonClass;
using I_SheepManagement.Domains;
using MediatR;

namespace I_SheepManagement.Read.API.Application.Command
{
    public class EliminationManageReadCommand:IRequest<PageResult<EliminationManagement>>
    {
        public int Size { get; set; }
        public int Index { get; set; }
        /// <summary>
        /// 淘汰日期(起始)
        /// </summary>
        public string? StartSheepElimination { get; set; }
        /// <summary>
        /// 淘汰日期(截止)
        /// </summary>
        public string? EndSheepElimination { get; set; }
        /// <summary>
        /// 羊只耳号
        /// </summary>
        public string? EarNumber { get; set; }
        /// <summary>
        /// 淘汰原因
        /// </summary>
        public string? EliminationCause { get; set; }
        /// <summary>
        /// 是否离场
        /// </summary>
        public string? IsLeave { get; set; }
    }
}

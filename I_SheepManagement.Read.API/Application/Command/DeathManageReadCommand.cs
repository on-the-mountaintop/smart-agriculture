﻿using CommonClass;
using I_SheepManagement.Domains;
using MediatR;

namespace I_SheepManagement.Read.API.Application.Command
{
    public class DeathManageReadCommand:IRequest<PageResult<DeathManagement>>
    {
        public int Size { get; set; }
        public int Index { get; set; }
        /// <summary>
        /// 死亡日期(起始)
        /// </summary>
        public string? StartSheepDeath { get; set; }
        /// <summary>
        /// 死亡日期(截止)
        /// </summary>
        public string? EndSheepDeath { get; set; }
        /// <summary>
        /// 羊只耳号
        /// </summary>
        public string? EarNumber { get; set; }
        /// <summary>
        /// 死亡原因
        /// </summary>
        public string? DeathCause { get; set; }
    }
}

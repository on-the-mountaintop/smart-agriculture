﻿using I_SheepManagement.Domains;
using MediatR;

namespace I_SheepManagement.Read.API.Application.Command
{
    public class DeathManageReadByIdCommand:IRequest<DeathManagement>
    {
        public string? EarNumber { get; set; }
    }
}

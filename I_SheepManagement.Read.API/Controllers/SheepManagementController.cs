﻿using CommonClass;
using CommonClass.CountInfo;
using I_SheepManagement.Domains;
using I_SheepManagement.Read.API.Application.Command;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace I_SheepManagement.Read.API.Controllers
{
    /// <summary>
    /// 羊只管理
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class SheepManagementController : ControllerBase
    {
        private readonly IMediator _mediator;

        public SheepManagementController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// 获取死亡原因
        /// </summary>
        /// <returns></returns>
        [HttpGet]

        public IActionResult GetDeathCause()
        {
            List<object> list = new List<object>();
            foreach (var item in Enum.GetValues(typeof(DeathCause)))
            {
                list.Add(new
                {
                    Key = item,
                    Value = item.ToString(),
                });
            }
            return Ok(list);
        }

        /// <summary>
        /// 获取淘汰原因
        /// </summary>
        /// <returns></returns>
        [HttpGet]

        public IActionResult GetEliminationCause()
        {
            List<object> list = new List<object>();
            foreach (var item in Enum.GetValues(typeof(EliminationCause)))
            {
                list.Add(new
                {
                    Key = item,
                    Value = item.ToString(),
                });
            }
            return Ok(list);
        }


        [HttpGet]
        public async Task<PageResult<DeathManagement>> GetPageList([FromQuery] DeathManageReadCommand command)
        {
            return await _mediator.Send(command);
        }

        [HttpGet]
        public async Task<DeathManagement> GetById([FromQuery] DeathManageReadByIdCommand command)
        {
            return await _mediator.Send(command);
        }

        [HttpGet]
        public async Task<PageResult<EliminationManagement>> GetEliminationPageList([FromQuery] EliminationManageReadCommand command)
        {
            return await _mediator.Send(command);
        }

        [HttpGet]
        public async Task<EliminationManagement> GetEliminationById([FromQuery] EliminationManageReadByIdCommand command)
        {
            return await _mediator.Send(command);
        }

        [HttpGet]
        public async Task<List<DeadTaoTrend>> GetDeadTaoTrend([FromQuery] SheepCountReadCommand command)
        {
            return await _mediator.Send(command);
        }
    }
}

﻿using AutoMapper;
using I_SheepManagement.API.Application.Commands;
using I_SheepManagement.Domains;
using I_SheepManagement.Infrastructure.Interface;
using MediatR;

namespace I_SheepManagement.API.Application.CommandHandlers
{
    public class EliminationManagementBatchDeleteCommandHandler : IRequestHandler<EliminationManagementBatchDeleteCommand, int>
    {
        private readonly IEliminationManagementRepository _eliminationManagementRepository;
        private readonly IMapper _mapper;

        public EliminationManagementBatchDeleteCommandHandler(IEliminationManagementRepository eliminationManagementRepository, IMapper mapper)
        {
            _eliminationManagementRepository = eliminationManagementRepository;
            _mapper = mapper;
        }

        public async Task<int> Handle(EliminationManagementBatchDeleteCommand request, CancellationToken cancellationToken)
        {
            var res = 0;
            var ids = request.Ids.Split(',') ;
            foreach (var id in ids)
            {
                var eliminationManagement = await _eliminationManagementRepository.GetByIdAsync(int.Parse(id));
                eliminationManagement.IsDelete = true;
                res += await _eliminationManagementRepository.UpdateAsync(eliminationManagement);
            }
            return res;
        }
    }
}

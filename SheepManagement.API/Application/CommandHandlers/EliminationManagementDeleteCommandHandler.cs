﻿using AutoMapper;
using I_SheepManagement.API.Application.Commands;
using I_SheepManagement.Domains;
using I_SheepManagement.Infrastructure.Interface;
using MediatR;

namespace I_SheepManagement.API.Application.CommandHandlers
{
    public class EliminationManagementDeleteCommandHandler : IRequestHandler<EliminationManagementDeleteCommand, int>
    {
        private readonly IEliminationManagementRepository _eliminationManagementRepository;
        private readonly IMapper _mapper;

        public EliminationManagementDeleteCommandHandler(IEliminationManagementRepository eliminationManagementRepository, IMapper mapper)
        {
            _eliminationManagementRepository = eliminationManagementRepository;
            _mapper = mapper;
        }

        public async Task<int> Handle(EliminationManagementDeleteCommand request, CancellationToken cancellationToken)
        {
            var eliminationManagement = await _eliminationManagementRepository.GetByIdAsync(request.Id);
            eliminationManagement.IsDelete = true;
            return await _eliminationManagementRepository.UpdateAsync(eliminationManagement);
        }
    }
}

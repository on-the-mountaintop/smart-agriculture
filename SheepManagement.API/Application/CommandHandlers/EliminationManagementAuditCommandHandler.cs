﻿using I_SheepManagement.API.Application.Commands;
using I_SheepManagement.Infrastructure.Impls;
using I_SheepManagement.Infrastructure.Interface;
using MediatR;

namespace I_SheepManagement.API.Application.CommandHandlers
{
    public class EliminationManagementAuditCommandHandler : IRequestHandler<EliminationManagementAuditCommand, int>
    {
        private readonly IEliminationManagementRepository _eliminationManagementRepository;

        public EliminationManagementAuditCommandHandler(IEliminationManagementRepository eliminationManagementRepository)
        {
            _eliminationManagementRepository = eliminationManagementRepository;
        }

        public async Task<int> Handle(EliminationManagementAuditCommand request, CancellationToken cancellationToken)
        {
            var res = 0;
            var ids = request.Ids.Split(',');
            foreach (var id in ids)
            {
                var eliminationInfo = await _eliminationManagementRepository.GetByIdAsync(int.Parse(id));
                if (eliminationInfo != null)
                {
                    //eliminationInfo.ExamineBy = auditForSheepManage.ExamineBy;
                    //eliminationInfo.ExamineDate = auditForSheepManage.ExamineDate;
                    eliminationInfo.ExamineBy = "王五";
                    eliminationInfo.ExamineDate = DateTime.Now;
                    res += await _eliminationManagementRepository.UpdateAsync(eliminationInfo);
                }
            }
            return res;
        }
    }
}

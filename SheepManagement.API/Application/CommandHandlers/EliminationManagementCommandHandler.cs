﻿using AutoMapper;
using I_SheepManagement.API.Application.Commands;
using I_SheepManagement.Domains;
using I_SheepManagement.Infrastructure.Interface;
using MediatR;

namespace I_SheepManagement.API.Application.CommandHandlers
{
    public class EliminationManagementCommandHandler : IRequestHandler<EliminationManagementCommand, int>
    {
        private readonly IEliminationManagementRepository _eliminationManagementRepository;
        private readonly IMapper _mapper;

        public EliminationManagementCommandHandler(IEliminationManagementRepository eliminationManagementRepository, IMapper mapper)
        {
            _eliminationManagementRepository = eliminationManagementRepository;
            _mapper = mapper;
        }

        public async Task<int> Handle(EliminationManagementCommand request, CancellationToken cancellationToken)
        {
            return await _eliminationManagementRepository.AddAsync(_mapper.Map<EliminationManagement>(request));
        }
    }
}

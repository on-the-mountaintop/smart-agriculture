﻿using I_SheepManagement.API.Application.Commands;
using I_SheepManagement.Infrastructure.Interface;
using MediatR;

namespace I_SheepManagement.API.Application.CommandHandlers
{
    public class DeathManagementAuditCommandHandler : IRequestHandler<DeathManagementAuditCommand, int>
    {
        private readonly IDeathManagementRepository _deathManagementRepository;

        public DeathManagementAuditCommandHandler(IDeathManagementRepository deathManagementRepository)
        {
            _deathManagementRepository = deathManagementRepository;
        }

        public async Task<int> Handle(DeathManagementAuditCommand request, CancellationToken cancellationToken)
        {
            var res = 0;
            var ids = request.Ids.Split(',') ;
            foreach (var id in ids)
            {
                var deathInfo = await _deathManagementRepository.GetByIdAsync(int.Parse(id));
                if (deathInfo != null)
                {
                    //deathInfo.ExamineBy = deathManagementAudit.ExamineBy;
                    //deathInfo.ExamineDate = deathManagementAudit.ExamineDate;
                    deathInfo.ExamineBy = "王五";
                    deathInfo.ExamineDate = DateTime.Now;
                    res += await _deathManagementRepository.UpdateAsync(deathInfo);
                }
            }
            return res;
        }
    }
}

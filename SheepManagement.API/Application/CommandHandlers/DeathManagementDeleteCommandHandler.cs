﻿using I_SheepManagement.API.Application.Commands;
using I_SheepManagement.Infrastructure.Interface;
using MediatR;

namespace I_SheepManagement.API.Application.CommandHandlers
{
    public class DeathManagementDeleteCommandHandler : IRequestHandler<DeathManagementDeleteCommand, int>
    {
        private readonly IDeathManagementRepository _sheepManagementRepository;

        public DeathManagementDeleteCommandHandler(IDeathManagementRepository sheepManagementRepository)
        {
            _sheepManagementRepository = sheepManagementRepository;
        }

        public async Task<int> Handle(DeathManagementDeleteCommand request, CancellationToken cancellationToken)
        {
            var deathManagementInfo = await _sheepManagementRepository.GetByIdAsync(request.Id);
            deathManagementInfo.IsDelete = true;
            return await _sheepManagementRepository.UpdateAsync(deathManagementInfo);
        }
    }
}

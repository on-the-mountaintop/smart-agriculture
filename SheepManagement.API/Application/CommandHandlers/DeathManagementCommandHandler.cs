﻿using AutoMapper;
using I_SheepManagement.API.Application.Commands;
using I_SheepManagement.Domains;
using I_SheepManagement.Infrastructure.Interface;
using MediatR;

namespace I_SheepManagement.API.Application.CommandHandlers
{
    public class DeathManagementCommandHandler : IRequestHandler<DeathManagementCommand, int>
    {
        private readonly IDeathManagementRepository _sheepManagementRepository;
        private readonly IMapper _mapper;

        public DeathManagementCommandHandler(IDeathManagementRepository sheepManagementRepository, IMapper mapper)
        {
            _sheepManagementRepository = sheepManagementRepository;
            _mapper = mapper;
        }

        public async Task<int> Handle(DeathManagementCommand request, CancellationToken cancellationToken)
        {
            return await _sheepManagementRepository.AddAsync(_mapper.Map<DeathManagement>(request));
        }
    }
}

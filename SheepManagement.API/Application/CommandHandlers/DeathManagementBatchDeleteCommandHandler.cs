﻿using I_SheepManagement.API.Application.Commands;
using I_SheepManagement.Infrastructure.Interface;
using MediatR;

namespace I_SheepManagement.API.Application.CommandHandlers
{
    public class DeathManagementBatchDeleteCommandHandler : IRequestHandler<DeathManagementBatchDeleteCommand, int>
    {
        private readonly IDeathManagementRepository _sheepManagementRepository;

        public DeathManagementBatchDeleteCommandHandler(IDeathManagementRepository sheepManagementRepository)
        {
            _sheepManagementRepository = sheepManagementRepository;
        }

        public async Task<int> Handle(DeathManagementBatchDeleteCommand request, CancellationToken cancellationToken)
        {
            var res = 0;
            var ids = request.Ids.Split(',');
            foreach (var id in ids)
            {
                var deathManagementInfo = await _sheepManagementRepository.GetByIdAsync(int.Parse(id));
                deathManagementInfo.IsDelete = true;
                res += await _sheepManagementRepository.UpdateAsync(deathManagementInfo);
            }
            return res;
        }
    }
}

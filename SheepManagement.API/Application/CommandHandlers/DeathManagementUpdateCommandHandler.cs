﻿using AutoMapper;
using I_SheepManagement.API.Application.Commands;
using I_SheepManagement.Domains;
using I_SheepManagement.Infrastructure.Interface;
using MediatR;

namespace I_SheepManagement.API.Application.CommandHandlers
{
    public class DeathManagementUpdateCommandHandler : IRequestHandler<DeathManagementUpdateCommand, int>
    {
        private readonly IDeathManagementRepository _sheepManagementRepository;
        private readonly IMapper _mapper;

        public DeathManagementUpdateCommandHandler(IDeathManagementRepository sheepManagementRepository, IMapper mapper)
        {
            _sheepManagementRepository = sheepManagementRepository;
            _mapper = mapper;
        }

        public async Task<int> Handle(DeathManagementUpdateCommand request, CancellationToken cancellationToken)
        {
            var deathManagementInfo =  _mapper.Map<DeathManagement>(request);
            return await _sheepManagementRepository.UpdateAsync(deathManagementInfo);
        }
    }
}

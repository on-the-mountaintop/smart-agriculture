﻿using MediatR;
using System.ComponentModel.DataAnnotations;

namespace I_SheepManagement.API.Application.Commands
{
    public class EliminationManagementBatchDeleteCommand : IRequest<int>
    {
        public string? Ids { get; set; }
    }
}

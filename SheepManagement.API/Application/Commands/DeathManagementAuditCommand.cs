﻿using CommonClass;
using MediatR;

namespace I_SheepManagement.API.Application.Commands
{
    public class DeathManagementAuditCommand:IRequest<int>
    {
        public string? Ids { get; set;}
    }
}

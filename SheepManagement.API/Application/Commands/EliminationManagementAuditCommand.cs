﻿using CommonClass;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace I_SheepManagement.API.Application.Commands
{
    public class EliminationManagementAuditCommand:IRequest<int>
    {
        public string? Ids { get; set; }
    }
}

﻿using MediatR;

namespace I_SheepManagement.API.Application.Commands
{
    public class DeathManagementDeleteCommand:IRequest<int>
    {
        public int Id { get; set; }
    }
}

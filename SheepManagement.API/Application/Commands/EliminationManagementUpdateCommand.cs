﻿using MediatR;
using System.ComponentModel.DataAnnotations;

namespace I_SheepManagement.API.Application.Commands
{
    public class EliminationManagementUpdateCommand:IRequest<int>
    {
        public int Id { get; set; }
        /// <summary>
        /// 羊只耳号
        /// </summary>
        [StringLength(30)]
        public string? EarNumber { get; set; }
        /// <summary>
        /// 淘汰日期
        /// </summary>
        public DateTime? EliminationDate { get; set; }
        /// <summary>
        /// 出生日期
        /// </summary>
        public DateTime? SheepBir { get; set; }
        /// <summary>
        /// 淘汰原因
        /// </summary>
        [StringLength(300)]
        public string? EliminationCause { get; set; }
        /// <summary>
        /// 淘汰日龄
        /// </summary>
        public int? EliminationDayAge { get; set; }
        /// <summary>
        /// 淘汰月龄
        /// </summary>
        public int? EliminationMonthAge { get; set; }
        /// <summary>
        /// 是否离场
        /// </summary>
        [StringLength(300)]
        public string? IsLeave { get; set; }
        /// <summary>
        /// 羊只品种
        /// </summary>
        [StringLength(30)]
        public string? SheepBreed { get; set; }
        /// <summary>
        /// 羊只性别
        /// </summary>
        public bool? SheepGender { get; set; }
        /// <summary>
        /// 处理措施
        /// </summary>
        [StringLength(300)]
        public string? EliminationMeasures { get; set; }
        /// <summary>
        /// 繁殖状态
        /// </summary>
        [StringLength(100)]
        public string? ReproductiveState { get; set; }
        /// <summary>
        /// 栋舍Id
        /// </summary>
        public string? CottageId { get; set; }
        /// <summary>
        /// 栏位Id
        /// </summary>
        public string? Field { get; set; }
        /// <summary>
        /// 审核人
        /// </summary>
        [StringLength(30)]
        public string? ExamineBy { get; set; }
        /// <summary>
        /// 审核时间
        /// </summary>
        public DateTime? ExamineDate { get; set; }
        /// <summary>
        /// 单据号
        /// </summary>
        [StringLength(300)]
        public string? DocumentNumber { get; set; }
    }
}

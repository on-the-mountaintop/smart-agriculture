﻿using MediatR;

namespace I_SheepManagement.API.Application.Commands
{
    public class DeathManagementBatchDeleteCommand:IRequest<int>
    {
        public string? Ids { get; set; }
    }
}

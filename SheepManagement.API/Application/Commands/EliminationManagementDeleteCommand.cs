﻿using MediatR;
using System.ComponentModel.DataAnnotations;

namespace I_SheepManagement.API.Application.Commands
{
    public class EliminationManagementDeleteCommand : IRequest<int>
    {
        public int Id { get; set; }
    }
}

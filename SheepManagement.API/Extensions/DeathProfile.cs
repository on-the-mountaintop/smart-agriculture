﻿using AutoMapper;
using I_SheepManagement.API.Application.Commands;
using I_SheepManagement.Domains;

namespace I_SheepManagement.API.Extensions
{
    public class DeathProfile:Profile
    {
        public DeathProfile()
        {
            CreateMap<DeathManagementCommand,DeathManagement>().ReverseMap();
            CreateMap<DeathManagementUpdateCommand,DeathManagement>().ReverseMap();
        }
    }
}

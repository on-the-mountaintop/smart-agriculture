﻿using AutoMapper;
using I_SheepManagement.API.Application.Commands;
using I_SheepManagement.Domains;

namespace I_SheepManagement.API.Extensions
{
    public class EliminationProfile: Profile
    {
        public EliminationProfile()
        {
            CreateMap<EliminationManagementCommand, EliminationManagement>().ReverseMap();
            CreateMap<EliminationManagementUpdateCommand, EliminationManagement>().ReverseMap();
        }
    }
}

﻿using CommonClass;
using I_SheepManagement.API.Application.Commands;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_SheepManagement.API.Controllers
{
    /// <summary>
    /// 羊只管理
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    [ApiExplorerSettings(GroupName = nameof(ApiVersionInfo.羊只死淘管理))]
    public class SheepManagementController : ControllerBase
    {
        private readonly IMediator _mediator;

        public SheepManagementController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// 羊只死亡管理添加
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<int> CreateSheepManagement(DeathManagementCommand command)
        {
            return await _mediator.Send(command);
        }

        /// <summary>
        /// 羊只死亡管理修改
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<int> UpdateSheepManagement(DeathManagementUpdateCommand command)
        {
            return await _mediator.Send(command);
        }

        /// <summary>
        /// 羊只死亡管理审核
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<int> AudifSheepManagement(DeathManagementAuditCommand command)
        {
            return await _mediator.Send(command);
        }

        /// <summary>
        /// 羊只死亡管理删除
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<int> DeleteSheepManagement([FromQuery]DeathManagementDeleteCommand command)
        {
            return await _mediator.Send(command);
        }

        /// <summary>
        /// 羊只死亡管理批量删除
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<int> BatchDeleteSheepManagement([FromQuery] DeathManagementBatchDeleteCommand command)
        {
            return await _mediator.Send(command);
        }

        /// <summary>
        /// 羊只淘汰管理添加
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<int> CreateEliminationManagement(EliminationManagementUpdateCommand command)
        {
            return await _mediator.Send(command);
        }

        /// <summary>
        /// 羊只淘汰管理修改
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<int> UpdateEliminationManagement(EliminationManagementUpdateCommand command)
        {
            return await _mediator.Send(command);
        }

        /// <summary>
        /// 羊只淘汰管理审核
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<int> AuditEliminationManagement(EliminationManagementAuditCommand command)
        {
            return await _mediator.Send(command);
        }

        /// <summary>
        /// 羊只淘汰管理删除
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<int> DeleteEliminationManagement([FromQuery] EliminationManagementDeleteCommand command)
        {
            return await _mediator.Send(command);
        }

        /// <summary>
        /// 羊只淘汰管理批量删除
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<int> BatchDeleteEliminationManagement([FromQuery] EliminationManagementBatchDeleteCommand command)
        {
            return await _mediator.Send(command);
        }
    }
}

﻿using I_SheepManagement.Domains;
using I_SheepManagement.Infrastructure.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace I_SheepManagement.Infrastructure.Impls
{
    public class DeathManagementRepository :BaseRepository<DeathManagement>, IDeathManagementRepository
    {
        public DeathManagementRepository(MyDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<int> AddAsync(DeathManagement entity)
        {
           return await base.AddAsync(entity);
        }

        public async Task<List<DeathManagement>> GetAllAsync()
        {
            return await base.GetAllAsync();
        }

        public async Task<DeathManagement> GetByFuncAsync(Expression<Func<DeathManagement, bool>> handler)
        {
            return await base.GetByFuncAsync(handler);
        }

        public async Task<List<DeathManagement>> GetByFuncListAsync(Expression<Func<DeathManagement, bool>> handler)
        {
            return await base.GetByFuncListAsync(handler);
        }

        public async Task<DeathManagement> GetByIdAsync(int id)
        {
            return await base.GetByIdAsync(id);
        }

        public async Task<int> UpdateAsync(DeathManagement entity)
        {
            return await base.UpdateAsync(entity);
        }
    }
}

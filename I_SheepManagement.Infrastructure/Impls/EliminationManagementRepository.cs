﻿using I_SheepManagement.Domains;
using I_SheepManagement.Infrastructure.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace I_SheepManagement.Infrastructure.Impls
{
    public class EliminationManagementRepository : BaseRepository<EliminationManagement>, IEliminationManagementRepository
    {
        public EliminationManagementRepository(MyDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<int> AddAsync(EliminationManagement entity)
        {
            return await base.AddAsync(entity);
        }

        public async Task<List<EliminationManagement>> GetAllAsync()
        {
            return await base.GetAllAsync();
        }

        public async Task<EliminationManagement> GetByFuncAsync(Expression<Func<EliminationManagement, bool>> handler)
        {
            return await base.GetByFuncAsync(handler);
        }

        public async Task<List<EliminationManagement>> GetByFuncListAsync(Expression<Func<EliminationManagement, bool>> handler)
        {
            return await base.GetByFuncListAsync(handler);
        }

        public async Task<EliminationManagement> GetByIdAsync(int id)
        {
            return await base.GetByIdAsync(id);
        }

        public async Task<int> UpdateAsync(EliminationManagement entity)
        {
            return await base.UpdateAsync(entity);  
        }
    }
}

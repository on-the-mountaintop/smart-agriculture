﻿using I_SheepManagement.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_SheepManagement.Infrastructure.Interface
{
    public interface IDeathManagementRepository:IRepository<DeathManagement>
    {
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace I_SheepManagement.Infrastructure.Migrations
{
    public partial class 羊只管理之死亡管理 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DeathManagements",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EarNumber = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    Deathdate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeathCause = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true),
                    SheepBreed = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    SheepGender = table.Column<bool>(type: "bit", nullable: true),
                    ReproductiveState = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true),
                    CottageId = table.Column<int>(type: "int", nullable: false),
                    Field = table.Column<int>(type: "int", nullable: false),
                    ExamineBy = table.Column<string>(type: "nvarchar(30)", maxLength: 30, nullable: true),
                    ExamineDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    DocumentNumber = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true),
                    CreateBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreateDate = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdateBy = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UpdateDate = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsDelete = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DeathManagements", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DeathManagements");
        }
    }
}

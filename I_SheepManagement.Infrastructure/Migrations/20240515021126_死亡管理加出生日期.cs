﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace I_SheepManagement.Infrastructure.Migrations
{
    public partial class 死亡管理加出生日期 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "SheepBir",
                table: "DeathManagements",
                type: "datetime2",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SheepBir",
                table: "DeathManagements");
        }
    }
}

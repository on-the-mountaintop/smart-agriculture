﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace I_SheepManagement.Infrastructure.Migrations
{
    public partial class 死亡日龄月龄 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DeathDayAge",
                table: "DeathManagements",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DeathMonthAge",
                table: "DeathManagements",
                type: "int",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeathDayAge",
                table: "DeathManagements");

            migrationBuilder.DropColumn(
                name: "DeathMonthAge",
                table: "DeathManagements");
        }
    }
}

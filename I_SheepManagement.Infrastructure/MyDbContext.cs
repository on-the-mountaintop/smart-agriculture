﻿using I_SheepManagement.Domains;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_SheepManagement.Infrastructure
{
    public class MyDbContext : DbContext
    {
        public MyDbContext(DbContextOptions<MyDbContext> options) : base(options)
        {
        }
        /// <summary>
        /// 死亡管理
        /// </summary>
        public virtual DbSet<DeathManagement> DeathManagements { get; set; }
        /// <summary>
        /// 淘汰管理
        /// </summary>
        public virtual DbSet<EliminationManagement> EliminationManagements { get; set; }
    }
}

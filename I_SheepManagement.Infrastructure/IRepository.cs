﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace I_SheepManagement.Infrastructure
{
    public interface IRepository<T> where T : class
    {
        Task<int> AddAsync(T entity);
        Task<int> UpdateAsync(T entity);
        Task<T> GetByIdAsync(int id);
        Task<T> GetByFuncAsync(Expression<Func<T, bool>> handler);
        Task<List<T>> GetByFuncListAsync(Expression<Func<T, bool>> handler);
        Task<List<T>> GetAllAsync();
    }
}

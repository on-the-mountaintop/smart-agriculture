﻿using AutoMapper;
using I_Authorization.API.Application.Commons.CottageCommand;
using I_Authorization.API.Application.Commons.Units;
using I_Authorization.Domains.Organization;

namespace I_Authorization.API.Extensions
{
    public class CottagePrufile : Profile
    {
        public CottagePrufile() 
        {
            CreateMap<CottageCreateCommand, Cottage>().ReverseMap();
            CreateMap<CottageUpdCommand, Cottage>().ReverseMap();
        }
    }
}

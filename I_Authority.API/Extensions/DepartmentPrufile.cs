﻿using AutoMapper;
using I_Authorization.API.Application.Commons.DepartmentCommand;
using I_Authorization.Domains.Organization;


namespace I_Authorization.API.Extensions
{
    public class DepartmentPrufile: Profile
    {
        public DepartmentPrufile() 
        {
            CreateMap<DepartmentCreateCommand, Department>().ReverseMap();
            CreateMap<DepartmentUpdCommand, Department>().ReverseMap();
        }
    }
}

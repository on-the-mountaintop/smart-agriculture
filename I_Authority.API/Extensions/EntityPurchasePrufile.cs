﻿using AutoMapper;
using I_Authorization.API.Application.Commons.DepartmentCommand;
using I_Authorization.API.Application.Commons.EntityPurchaseCommand;
using I_Authorization.Domains.Organization;

namespace I_Authorization.API.Extensions
{
    public class EntityPurchasePrufile : Profile
    {
        public EntityPurchasePrufile() 
        {
            CreateMap<EntityPurchaseCreateCommand, EntityPurchase>().ReverseMap();
        }
    }
}

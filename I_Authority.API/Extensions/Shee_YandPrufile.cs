﻿using AutoMapper;
using I_Authorization.API.Application.Commons.Shee_YandCommand;
using I_Authorization.API.Application.Commons.Units;
using I_Authorization.Domains.Organization;

namespace I_Authorization.API.Extensions
{
    public class Shee_YandPrufile : Profile
    {
        public Shee_YandPrufile() 
        {
            CreateMap<Shee_YandCreateCommand, Shee_Yard>().ReverseMap(); 
            CreateMap<Shee_YandUpdCommand, Shee_Yard>().ReverseMap(); 
        }
    }
}

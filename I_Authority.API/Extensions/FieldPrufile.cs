﻿using AutoMapper;
using I_Authorization.API.Application.Commons.FieldCommand;
using I_Authorization.API.Application.Commons.Units;
using I_Authorization.Domains.Organization;

namespace I_Authorization.API.Extensions
{
    public class FieldPrufile : Profile
    {
        public FieldPrufile() 
        {
            CreateMap<FieldCreateCommand, Field>().ReverseMap();
        }
    }
}

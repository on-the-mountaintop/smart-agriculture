﻿using AutoMapper;
using I_Authorization.API.Application.Commons.Units;
using I_Authorization.Domains.Organization;

namespace I_Authorization.API.Extensions
{
    public class UnitsPrufile : Profile
    {
        public UnitsPrufile() 
        {
            CreateMap<UnitsCreateCommand, Units>().ReverseMap();
            CreateMap<UnitsUpdCommand, Units>().ReverseMap();
        }
    }
}

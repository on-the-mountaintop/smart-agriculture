using I_Authority.API.Extensions;
using I_Authorization.Infrastructure;
using I_Authorization.Infrastructure.Impl;
using I_Authorization.Infrastructure.Interface;
using MediatR;
using Microsoft.EntityFrameworkCore;
using NLog.Web;
using System.Reflection;


var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(x =>
{
    string xml = AppDomain.CurrentDomain.BaseDirectory + "I_Authorization.API.xml";
    x.IncludeXmlComments(xml, true);
});

//日志
builder.Host.UseNLog();

//中介者模式
builder.Services.AddMediatR(Assembly.GetExecutingAssembly());

//注入企业仓储
builder.Services.AddScoped<IUnitsRepository, UnitsRepository>();
//注入部门仓储
builder.Services.AddScoped<IDepartmentRepository, DepartmentRepository>();
//注入羊场
builder.Services.AddScoped<IShee_YardRepository, Shee_YardRepository>();

//栋舍
builder.Services.AddScoped<ICottageRepository, CottageRepository>();
//注入栏位
builder.Services.AddScoped<IFieldRepository, FieldRepository>();

builder.Services.AddScoped<IEmployeeRepository, EmployeeRepository>();   //注入人员表的私有的仓储接口以及实现

builder.Services.AddScoped<IRoleRepository, RoleRepository>();   //注入角色表的私有的仓储接口以及实现

builder.Services.AddScoped<IPermissionRepository, PermissionRepository>();  //注入权限表的私有的仓储接口以及实现


builder.Services.AddScoped<IEntityPurchaseRepository, EntityPurchaseRepository>();

//数据库上下文
builder.Services.AddDbContext<EFDbContext>(option => option.UseSqlServer(builder.Configuration.GetConnectionString("Authorization")));

//AutoMapper注入
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

//文件注释
builder.Services.AddSwaggerGen(c =>
{
    string comm = AppDomain.CurrentDomain.BaseDirectory + "I_Authorization.API.xml";
    c.IncludeXmlComments(comm, true);
});

builder.Services.AddJWT(builder);

var app = builder.Build();

//静态路由
app.UseStaticFiles();

//跨域
app.UseCors(c => c.AllowAnyHeader().AllowAnyOrigin().AllowAnyMethod());

// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
    app.UseSwagger();
    app.UseSwaggerUI();
//}

app.UseAuthorization();

app.UseStaticFiles();
app.UseCors(x => x.AllowAnyHeader().AllowAnyOrigin().AllowAnyMethod());

app.MapControllers();

app.Run();

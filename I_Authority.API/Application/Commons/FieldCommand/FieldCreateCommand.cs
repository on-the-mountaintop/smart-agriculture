﻿using I_Authorization.Domains;
using I_Authorization.Domains.Organization;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace I_Authorization.API.Application.Commons.FieldCommand
{
    public class FieldCreateCommand:Audit_field, IRequest<int>
    {
        public int CottageId { get; set; }            //栋舍编号（外键）
        [StringLength(30)]
        public string? FieName { get; set; }         //栏位名称
    }
}

﻿using I_Authorization.Domains;
using MediatR;

namespace I_Authorization.API.Application.Commons.Shee_YandCommand
{
    public class Shee_YandDelCommand:IRequest<int>
    {
        public int SheepId { get; set; }             //主键标识
    }
}

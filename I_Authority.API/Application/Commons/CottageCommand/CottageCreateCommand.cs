﻿using I_Authorization.Domains;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace I_Authorization.API.Application.Commons.CottageCommand
{
    public class CottageCreateCommand: Audit_field, IRequest<int>
    {
        [StringLength(30)]
        public string? CottageType { get; set; }        //栋舍类型
        [StringLength(30)]
        public string? CottageName { get; set; }        //栋舍名称
        [StringLength(30)]
        public string? Principal { get; set; }        //负责人（外键）
        [StringLength(30)]
        public string? CottageProduction { get; set; }         //生产人员
        public bool CottageState { get; set; }         //状态（启用、禁用）
        [StringLength(30)]
        public string? CottageRemark { get; set; }         //备注
    }
}

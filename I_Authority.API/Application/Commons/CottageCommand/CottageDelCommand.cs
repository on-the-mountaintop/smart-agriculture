﻿using MediatR;

namespace I_Authorization.API.Application.Commons.CottageCommand
{
    public class CottageDelCommand:IRequest<int>
    {
        public int CottageId { get; set; }        //主键标识
    }
}

﻿using I_Authorization.Domains;
using MediatR;

namespace I_Authorization.API.Application.Commons.Units
{
    /// <summary>
    /// 企业删除
    /// </summary>
    public class UnitsDelCommand : IRequest<int>
    {
        public int UnitId { get; set; }   //主键标识
    }
}

﻿using I_Authorization.Domains;
using I_Authorization.Infrastructure;
using System.ComponentModel.DataAnnotations;
using MediatR;

namespace I_Authorization.API.Application.Commons.Units
{
    /// <summary>
    /// 添加企业变量
    /// </summary>
    public class UnitsCreateCommand : Audit_field, IRequest<int>
    {

        public string? UnitFull { get; set; }   //企业名称

        public string? UnitLOGO { get; set; }   // 企业LOGO图片

        public string? TradeLable { get; set; }   //行业标签

        public string? UnitAddress { get; set; }   //详细地址

        public string? UnitPhone { get; set; }   //	企业电话

        public string? UnitCredit { get; set; }   // 统一社会信用代码
        public bool UnitState { get; set; }   // 停用企业

        public string? UnitPrinc { get; set; }   //负责人
    }
}

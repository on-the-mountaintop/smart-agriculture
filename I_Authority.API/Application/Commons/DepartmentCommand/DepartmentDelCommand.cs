﻿using MediatR;

namespace I_Authorization.API.Application.Commons.DepartmentCommand
{
    public class DepartmentDelCommand: IRequest<int>
    {
        public int DepartId { get; set; }   //键标识
    }
}

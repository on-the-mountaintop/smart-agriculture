﻿using I_Authorization.Domains;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace I_Authorization.API.Application.Commands.Role
{
    /// <summary>
    /// 中介者模式  角色单个假删除
    /// </summary>
    public class RoleFalseDeleteCommand:IRequest<int>
    {
        /// <summary>
        /// 角色编号 主键
        /// </summary>
        public int RoleId { get; set; }
    }
}

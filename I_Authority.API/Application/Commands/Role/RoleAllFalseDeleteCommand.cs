﻿using MediatR;

namespace I_Authorization.API.Application.Commands.Role
{
    /// <summary>
    /// 角色的中介者模式批量逻辑删除
    /// </summary>
    public class RoleAllFalseDeleteCommand:IRequest<int>
    {
        public string roleId { get; set; }
    }
}

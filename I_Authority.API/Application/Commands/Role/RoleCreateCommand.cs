﻿using I_Authorization.Domains;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace I_Authorization.API.Application.Commands.Role
{
    /// <summary>
    /// 角色的中介者模式添加
    /// </summary>
    public class RoleCreateCommand : AuditInfo, IRequest<int>
    {
        /// <summary>
        /// 角色名称
        /// </summary>
        [StringLength(20)]
        public string? RoleName { get; set; }

        /// <summary>
        /// 备注
        /// </summary>
        [StringLength(40)]
        public string? RoleRemark { get; set; }

        /// <summary>
        /// 角色描述
        /// </summary>
        [StringLength(30)]
        public string? RoleDescription { get; set; }
    }
}

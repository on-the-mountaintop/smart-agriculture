﻿using I_Authorization.Domains;
using MediatR;

namespace I_Authorization.API.Application.Commands.Role
{
    /// <summary>
    /// 角色的真删除方法
    /// </summary>
    public class RoleTrueDeleteCommand : AuditInfo, IRequest<int>
    {
        /// <summary>
        /// 角色的主键
        /// </summary>
        public int RoleId { get; set; }
    }
}

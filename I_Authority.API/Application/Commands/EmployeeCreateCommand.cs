﻿using I_Authorization.Domains;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace I_Authorization.API.Application.Commands
{
    /// <summary>
    /// 人员 员工终结者模式添加
    /// </summary>
    public class EmployeeCreateCommand: AuditInfo,IRequest<int>
    {
        /// <summary>
        /// 姓名
        /// </summary>
        [StringLength(20)]
        public string? EmployeeName { get; set; }

        /// <summary>
        /// 性别
        /// </summary>
        [StringLength(4)]
        public string? EmployeeSex { get; set; }

        /// <summary>
        /// 证件类型 （固定值下拉框）
        /// </summary>
        [StringLength(20)]
        public string? CertificateId { get; set; }

        /// <summary>
        /// 联系电话
        /// </summary>
        [StringLength(20)]
        public string? EmployeePhone { get; set; }

        /// <summary>
        /// 证件号码
        /// </summary>
        [StringLength(30)]
        public string? IDNumber { get; set; }

        /// <summary>
        /// 所在地
        /// </summary>
        [StringLength(30)]
        public string? Location { get; set; }

        /// <summary>
        /// 单位编号  外键
        /// </summary>
        public int UnitId { get; set; }

        /// <summary>
        /// 员工状态  （固定下拉框）
        /// </summary>
        [StringLength(20)]
        public string? EmployeeState { get; set; }

        /// <summary>
        /// 部门编号 外键
        /// </summary>
        public int DepartId { get; set; }

        /// <summary>
        /// 入职日期
        /// </summary>
        public DateTime EntryDate { get; set; } = DateTime.Now;

        /// <summary>
        /// 职务下拉框
        /// </summary>
        [StringLength(20)]
        public string? Position { get; set; }

        /// <summary>
        /// 岗位下拉框
        /// </summary>
        [StringLength(20)]
        public string? Office { get; set; }

        /// <summary>
        /// 人事级别下拉框
        /// </summary>
        [StringLength(20)]
        public string? PersonnelLevel { get; set; }

        /// <summary>
        /// 账号 （用户名）
        /// </summary>
        [StringLength(20)]
        public string? Account { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [StringLength(20)]
        public string? Password { get; set; }

        /// <summary>
        /// 角色编号  外键
        /// </summary>
        public int RoleId { get; set; }
    }
}

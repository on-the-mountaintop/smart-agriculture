﻿using I_Authorization.Domains;
using MediatR;

namespace I_Authorization.API.Application.Commands
{
    /// <summary>
    /// 人员、员工的单个逻辑删除
    /// </summary>
    public class EmployeeFalseDeleteCommand: IRequest<int>
    {
        /// <summary>
        /// 人员、员工编号
        /// </summary>
        public int EmployeeId { get; set; }
    }
}

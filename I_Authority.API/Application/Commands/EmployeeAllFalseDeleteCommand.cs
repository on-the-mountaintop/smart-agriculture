﻿using MediatR;

namespace I_Authorization.API.Application.Commands
{
    /// <summary>
    /// 人员、员工的批量逻辑删除
    /// </summary>
    public class EmployeeAllFalseDeleteCommand:IRequest<int>
    {
        /// <summary>
        /// 人员、员工主键
        /// </summary>
        public string EmployeeId { get; set; }
    }
}

﻿using I_Authorization.Domains;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace I_Authorization.API.Application.Commands.Permission
{
    /// <summary>
    /// 中介者添加权限信息
    /// </summary>
    public class PermissionCreateCommand: AuditInfo,IRequest<int>
    {
        /// <summary>
        /// 功能名称
        /// </summary>
        [StringLength(30)]
        public string? FunctionName { get; set; }

        /// <summary>
        /// 操作
        /// </summary>
        [StringLength(50)]
        public string? Operation { get; set; }

        /// <summary>
        /// 外键  角色编号
        /// </summary>
        public int RoleId { get; set; }
    }
}

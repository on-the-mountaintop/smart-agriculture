﻿using I_Authorization.Domains;
using MediatR;

namespace I_Authorization.API.Application.Commands.Permission
{
    /// <summary>
    /// 权限真删除
    /// </summary>
    public class PermissionTrueDeleteCommand: IRequest<int>
    {
        /// <summary>
        /// 主键  权限编号
        /// </summary>
        public int PermissionId { get; set; }
    }
}

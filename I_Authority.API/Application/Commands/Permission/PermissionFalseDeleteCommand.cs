﻿using I_Authorization.Domains;
using MediatR;

namespace I_Authorization.API.Application.Commands.Permission
{
    /// <summary>
    /// 权限表的单个逻辑删除
    /// </summary>
    public class PermissionFalseDeleteCommand: IRequest<int>
    {
        /// <summary>
        /// 主键  权限编号
        /// </summary>
        public int PermissionId { get; set; }
    }
}

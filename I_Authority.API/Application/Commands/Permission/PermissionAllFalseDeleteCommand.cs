﻿using MediatR;

namespace I_Authorization.API.Application.Commands.Permission
{
    /// <summary>
    /// 权限表的批量逻辑删除
    /// </summary>
    public class PermissionAllFalseDeleteCommand: IRequest<int>
    {
        public string permissionId { get; set; }
    }
}

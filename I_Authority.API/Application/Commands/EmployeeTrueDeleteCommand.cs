﻿using MediatR;

namespace I_Authorization.API.Application.Commands
{
    /// <summary>
    /// 人员 员工的单个真删除
    /// </summary>
    public class EmployeeTrueDeleteCommand:IRequest<int>
    {
        /// <summary>
        /// 人员  员工编号
        /// </summary>
        public int EmployeeId { get; set; }
    }
}

﻿using AutoMapper;
using I_Authorization.API.Application.Commons.Units;
using I_Authorization.Domains.Organization;
using I_Authorization.Infrastructure.Interface;
using MediatR;

namespace I_Authorization.API.Application.CommandHandlers.UnitsHandlers
{
    public class UnitsUpdCommandHandler : IRequestHandler<UnitsUpdCommand, int>
    {
        private readonly IUnitsRepository _units;
        private readonly IMapper _mapper;

        public UnitsUpdCommandHandler(IUnitsRepository units, IMapper mapper)
        {
            _units = units;
            _mapper = mapper;
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<int> Handle(UnitsUpdCommand request, CancellationToken cancellationToken)
        {
            var res = _mapper.Map<Units>(request);
            return await _units.UpdateAsync(res);
        }
    }
}

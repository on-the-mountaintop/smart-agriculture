﻿using AutoMapper;
using I_Authorization.API.Application.Commons.Units;
using I_Authorization.Domains.Organization;
using I_Authorization.Infrastructure;
using I_Authorization.Infrastructure.Interface;
using MediatR;

namespace I_Authorization.API.Application.CommandHandlers.UnitsHandlers
{
    /// <summary>
    /// 单位中介者
    /// </summary>
    public class UnitsCreateCommandHandlerL : IRequestHandler<UnitsCreateCommand, int>
    {
        private readonly IUnitsRepository _units;
        private readonly IMapper _mapper;

        /// <summary>
        /// 注入
        /// </summary>
        /// <param name="units"></param>
        public UnitsCreateCommandHandlerL(IUnitsRepository units, IMapper mapper)
        {
            _units = units;
            _mapper = mapper;
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<int> Handle(UnitsCreateCommand request, CancellationToken cancellationToken)
        {

            var res = _mapper.Map<Units>(request);
            res.IsDel = false;
            return await _units.AddAsync(res);
        }
    }
}

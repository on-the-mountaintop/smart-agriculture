﻿using AutoMapper;
using I_Authorization.API.Application.Commons.Units;
using I_Authorization.Domains;
using I_Authorization.Domains.Organization;
using I_Authorization.Infrastructure.Interface;
using MediatR;

namespace I_Authorization.API.Application.CommandHandlers.UnitsHandlers
{
    /// <summary>
    /// 企业删除
    /// </summary>
    public class UnitsDelCommandHandler : IRequestHandler<UnitsDelCommand, int>
    {
        private readonly IUnitsRepository _units;
        private readonly IMapper _mapper;
        /// <summary>
        /// 注入
        /// </summary>
        /// <param name="units"></param>
        /// <param name="mapper"></param>
        public UnitsDelCommandHandler(IUnitsRepository units, IMapper mapper)
        {
            _units = units;
            _mapper = mapper;
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<int> Handle(UnitsDelCommand request, CancellationToken cancellationToken)
        {
            var res = await _units.GetFindAsync(request.UnitId);
            res.IsDel = true;
            return await _units.DeleteAsync(res);
        }
    }
}

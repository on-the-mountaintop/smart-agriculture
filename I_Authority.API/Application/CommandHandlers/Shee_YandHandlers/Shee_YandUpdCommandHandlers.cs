﻿using AutoMapper;
using I_Authorization.API.Application.Commons.Shee_YandCommand;
using I_Authorization.Domains.Organization;
using I_Authorization.Infrastructure.Interface;
using MediatR;

namespace I_Authorization.API.Application.CommandHandlers.Shee_YandHandlers
{
    public class Shee_YandUpdCommandHandlers : IRequestHandler<Shee_YandUpdCommand, int>
    {
        private readonly IShee_YardRepository _shee_Yard;
        private readonly IMapper _mapper;

        public Shee_YandUpdCommandHandlers(IShee_YardRepository shee_Yard, IMapper mapper)
        {
            _shee_Yard = shee_Yard;
            _mapper = mapper;
        }
        public async Task<int> Handle(Shee_YandUpdCommand request, CancellationToken cancellationToken)
        {
            var res = _mapper.Map<Shee_Yard>(request);
            return await _shee_Yard.UpdateAsync(res);
        }
    }
}

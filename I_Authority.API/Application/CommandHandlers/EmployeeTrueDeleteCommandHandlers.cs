﻿using I_Authorization.API.Application.Commands;
using I_Authorization.Infrastructure.Interface;
using MediatR;

namespace I_Authorization.API.Application.CommandHandlers
{
    /// <summary>
    /// 人员 员工的单个真删除
    /// </summary>
    public class EmployeeTrueDeleteCommandHandlers : IRequestHandler<EmployeeTrueDeleteCommand, int>
    {
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeTrueDeleteCommandHandlers(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        /// <summary>
        /// 人员、员工的单个真删除
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<int> Handle(EmployeeTrueDeleteCommand request, CancellationToken cancellationToken)
        {
            var list = _employeeRepository.DeleteAsync(request.EmployeeId);
            return await list;
            //throw new NotImplementedException();
        }
    }
}

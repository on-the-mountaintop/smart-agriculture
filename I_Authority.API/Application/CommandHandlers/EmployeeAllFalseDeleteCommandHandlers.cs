﻿using I_Authorization.API.Application.Commands;
using I_Authorization.Infrastructure.Interface;
using MediatR;

namespace I_Authorization.API.Application.CommandHandlers
{
    /// <summary>
    /// 人员、员工的批量逻辑删除
    /// </summary>
    public class EmployeeAllFalseDeleteCommandHandlers : IRequestHandler<EmployeeAllFalseDeleteCommand, int>
    {
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeAllFalseDeleteCommandHandlers(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        /// <summary>
        /// 人员、员工的批量逻辑删除
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<int> Handle(EmployeeAllFalseDeleteCommand request, CancellationToken cancellationToken)
        {
            //throw new NotImplementedException();
            var idarry= request.EmployeeId.Split(',').Select(x => Convert.ToInt32(x)).ToArray();

            var result = await _employeeRepository.GetAsync(x => idarry.Contains(x.EmployeeId));

            foreach (var item in result)
            {
                item.IsDel = false;
            }

            return await _employeeRepository.AllStateDelete(result);
        }
    }
}

﻿using AutoMapper;
using I_Authorization.API.Application.Commons.FieldCommand;
using I_Authorization.API.Application.Commons.Shee_YandCommand;
using I_Authorization.Domains.Organization;
using I_Authorization.Infrastructure.Interface;
using MediatR;

namespace I_Authorization.API.Application.CommandHandlers.FieldHandlers
{
    public class FieldCreateCommandHandlers : IRequestHandler<FieldCreateCommand, int>
    {
        private readonly IFieldRepository _field;
        private readonly IMapper _mapper;

        public FieldCreateCommandHandlers(IFieldRepository field, IMapper mapper)
        {
            _field = field;
            _mapper = mapper;
        }

        public async Task<int> Handle(FieldCreateCommand request, CancellationToken cancellationToken)
        {
            var res = _mapper.Map<Field>(request);
            return await _field.AddAsync(res);
        }
    }
}

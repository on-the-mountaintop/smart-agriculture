﻿using I_Authorization.API.Application.Commands;
using I_Authorization.Domains;
using I_Authorization.Infrastructure.Interface;
using MediatR;

namespace I_Authorization.API.Application.CommandHandlers
{
    /// <summary>
    /// 人员、员工的中介者修改
    /// </summary>
    public class EmployeeUpdateCommandHandler : IRequestHandler<EmployeeUpdateCommand, int>
    {
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeUpdateCommandHandler(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }
        public async Task<int> Handle(EmployeeUpdateCommand request, CancellationToken cancellationToken)
        {
            //throw new NotImplementedException();
            var code = await _employeeRepository.UpdateAsync(new EmployeeInfo
            {
                EmployeeId = request.EmployeeId,
                EmployeeName = request.EmployeeName,
                EmployeeSex = request.EmployeeSex,
                CertificateId = request.CertificateId,
                EmployeePhone = request.EmployeePhone,
                IDNumber = request.IDNumber,
                Location = request.Location,
                UnitId = request.UnitId,
                EmployeeState = request.EmployeeState,
                DepartId = request.DepartId,
                EntryDate = request.EntryDate,
                Position = request.Position,
                Office = request.Office,
                PersonnelLevel = request.PersonnelLevel,
                Account = request.Account,
                Password = request.Password,
                RoleId = request.RoleId,
                CreateBy = request.CreateBy,
                CreateDate = request.CreateDate,
                UpdateBy = request.UpdateBy,
                UpdateDate = request.UpdateDate,
                IsDel = request.IsDel,
            });
            return code;
        }
    }
}

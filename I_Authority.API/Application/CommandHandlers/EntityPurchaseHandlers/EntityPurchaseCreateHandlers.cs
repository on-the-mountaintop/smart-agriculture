﻿using AutoMapper;
using I_Authorization.API.Application.Commons.EntityPurchaseCommand;
using I_Authorization.API.Application.Commons.Units;
using I_Authorization.Domains.Organization;
using I_Authorization.Infrastructure.Interface;
using MediatR;

namespace I_Authorization.API.Application.CommandHandlers.EntityPurchaseHandlers
{
    public class EntityPurchaseCreateHandlers : IRequestHandler<EntityPurchaseCreateCommand, int>
    {
        private readonly IEntityPurchaseRepository _entityPurchase;
        private readonly IMapper _mapper;

        public EntityPurchaseCreateHandlers(IEntityPurchaseRepository entityPurchase, IMapper mapper)
        {
            _entityPurchase = entityPurchase;
            _mapper = mapper;
        }

        public async Task<int> Handle(EntityPurchaseCreateCommand request, CancellationToken cancellationToken)
        {
            var res = _mapper.Map<EntityPurchase>(request);
            res.IsDel = false;
            return await _entityPurchase.AddAsync(res);
        }
    }
}

﻿using AutoMapper;
using I_Authorization.API.Application.Commons.CottageCommand;
using I_Authorization.Domains.Organization;
using I_Authorization.Infrastructure.Interface;
using MediatR;

namespace I_Authorization.API.Application.CommandHandlers.CottageHandlers
{
    /// <summary>
    /// 栋舍添加
    /// </summary>
    public class CottageCreateCommandHandlers : IRequestHandler<CottageCreateCommand, int>
    {
        private readonly ICottageRepository _cottage;
        private readonly IMapper _mapper;
        /// <summary>
        /// 注入
        /// </summary>
        /// <param name="cottage"></param>
        /// <param name="mapper"></param>
        public CottageCreateCommandHandlers(ICottageRepository cottage, IMapper mapper)
        {
            _cottage = cottage;
            _mapper = mapper;
        }
        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public Task<int> Handle(CottageCreateCommand request, CancellationToken cancellationToken)
        {
            var res = _mapper.Map<Cottage>(request);
            return _cottage.AddAsync(res);
        }
    }
}

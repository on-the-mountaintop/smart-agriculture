﻿using AutoMapper;
using I_Authorization.API.Application.Commons.CottageCommand;
using I_Authorization.Infrastructure.Interface;
using MediatR;
using NLog.Config;

namespace I_Authorization.API.Application.CommandHandlers.CottageHandlers
{
    public class CottageDelCommandHandlers : IRequestHandler<CottageDelCommand, int>
    {
        private readonly ICottageRepository _cottage;
        private readonly IMapper _mapper;
        /// <summary>
        /// 注入
        /// </summary>
        /// <param name="cottage"></param>
        /// <param name="mapper"></param>
        public CottageDelCommandHandlers(ICottageRepository cottage, IMapper mapper)
        {
            _cottage = cottage;
            _mapper = mapper;
        }
        public async Task<int> Handle(CottageDelCommand request, CancellationToken cancellationToken)
        {
            var res = await _cottage.GetFindAsync(request.CottageId);
            res.IsDel = true;
            return await _cottage.UpdateAsync(res);
        }
    }
}

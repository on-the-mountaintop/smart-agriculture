﻿using AutoMapper;
using I_Authorization.API.Application.Commons.CottageCommand;
using I_Authorization.Domains.Organization;
using I_Authorization.Infrastructure.Interface;
using MediatR;

namespace I_Authorization.API.Application.CommandHandlers.CottageHandlers
{
    public class CottageUpdCommandHandlers : IRequestHandler<CottageUpdCommand, int>
    {
        private readonly ICottageRepository _cottage;
        private readonly IMapper _mapper;
        /// <summary>
        /// 注入
        /// </summary>
        /// <param name="cottage"></param>
        /// <param name="mapper"></param>
        public CottageUpdCommandHandlers(ICottageRepository cottage, IMapper mapper)
        {
            _cottage = cottage;
            _mapper = mapper;
        }
        public async Task<int> Handle(CottageUpdCommand request, CancellationToken cancellationToken)
        {
            var res = _mapper.Map<Cottage>(request);
            return await _cottage.UpdateAsync(res);
        }
    }
}

﻿using I_Authorization.API.Application.Commands.Role;
using I_Authorization.Infrastructure.Interface;
using MediatR;

namespace I_Authorization.API.Application.CommandHandlers.Role
{
    /// <summary>
    /// 中介者模式 角色单个假删除
    /// </summary>
    public class RoleFalseDeleteCommandHandlers : IRequestHandler<RoleFalseDeleteCommand, int>
    {
        private readonly IRoleRepository _roleRepository;

        public RoleFalseDeleteCommandHandlers(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;
        }
        public async Task<int> Handle(RoleFalseDeleteCommand request, CancellationToken cancellationToken)
        {
            var list = await _roleRepository.GetModelAsync(request.RoleId);
            list.IsDel = false;
            return await _roleRepository.DeleteAsync(list);
            //throw new NotImplementedException();
        }
    }
}

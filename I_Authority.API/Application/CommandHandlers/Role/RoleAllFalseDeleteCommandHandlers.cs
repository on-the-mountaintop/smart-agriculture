﻿using I_Authorization.API.Application.Commands.Role;
using I_Authorization.Infrastructure.Impl;
using I_Authorization.Infrastructure.Interface;
using MediatR;

namespace I_Authorization.API.Application.CommandHandlers.Role
{
    public class RoleAllFalseDeleteCommandHandlers : IRequestHandler<RoleAllFalseDeleteCommand, int>
    {
        private readonly IRoleRepository _roleRepository;

        public RoleAllFalseDeleteCommandHandlers(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;
        }

        /// <summary>
        /// 角色的中介者模式批量逻辑删除
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<int> Handle(RoleAllFalseDeleteCommand request, CancellationToken cancellationToken)
        {
            var idarry = request.roleId.Split(',').Select(x => Convert.ToInt32(x)).ToArray();

            var result = await _roleRepository.GetAsync(x => idarry.Contains(x.RoleId));

            foreach (var item in result)
            {
                item.IsDel = false;
            }

            return await _roleRepository.AllStateDelete(result);
        }
    }
}

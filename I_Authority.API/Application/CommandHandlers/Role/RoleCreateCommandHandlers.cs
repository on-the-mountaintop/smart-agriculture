﻿using I_Authorization.API.Application.Commands.Role;
using I_Authorization.Domains.RBAC;
using I_Authorization.Infrastructure.Interface;
using MediatR;

namespace I_Authorization.API.Application.CommandHandlers.Role
{
    /// <summary>
    /// 角色的中介者添加
    /// </summary>
    public class RoleCreateCommandHandlers : IRequestHandler<RoleCreateCommand, int>
    {
        private readonly IRoleRepository _roleRepository;

        public RoleCreateCommandHandlers(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;
        }
        public async Task<int> Handle(RoleCreateCommand request, CancellationToken cancellationToken)
        {
            return await _roleRepository.AddAsync(new RoleInfo
            {
                CreateBy = request.CreateBy,
                CreateDate = request.CreateDate,
                UpdateBy = request.UpdateBy,
                UpdateDate = request.UpdateDate,
                IsDel = request.IsDel,
                RoleName = request.RoleName,
                RoleRemark = request.RoleRemark,
                RoleDescription = request.RoleDescription
            });
            //throw new NotImplementedException();
        }
    }
}

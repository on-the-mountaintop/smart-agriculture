﻿using I_Authorization.API.Application.Commands.Role;
using I_Authorization.Domains.RBAC;
using I_Authorization.Infrastructure.Interface;
using MediatR;

namespace I_Authorization.API.Application.CommandHandlers.Role
{
    /// <summary>
    /// 角色的中介者模式修改
    /// </summary>
    public class RoleUpdateCommandHandlers : IRequestHandler<RoleUpdateCommand, int>
    {
        private readonly IRoleRepository _roleRepository;

        public RoleUpdateCommandHandlers(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;
        }
        public async Task<int> Handle(RoleUpdateCommand request, CancellationToken cancellationToken)
        {
            return await _roleRepository.UpdateAsync(new RoleInfo
            {
                RoleId = request.RoleId,
                RoleName = request.RoleName,
                RoleRemark = request.RoleRemark,
                RoleDescription = request.RoleDescription,
                CreateBy = request.CreateBy,
                CreateDate = request.CreateDate,
                UpdateBy = request.UpdateBy,
                UpdateDate = request.UpdateDate,
            });
            //throw new NotImplementedException();
        }
    }
}

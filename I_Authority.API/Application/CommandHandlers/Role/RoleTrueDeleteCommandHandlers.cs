﻿using I_Authorization.API.Application.Commands.Role;
using I_Authorization.Infrastructure.Interface;
using MediatR;

namespace I_Authorization.API.Application.CommandHandlers.Role
{
    /// <summary>
    /// 角色 中介者模式真删除
    /// </summary>
    public class RoleTrueDeleteCommandHandlers : IRequestHandler<RoleTrueDeleteCommand, int>
    {
        private readonly IRoleRepository _roleRepository;

        public RoleTrueDeleteCommandHandlers(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;
        }
        public async Task<int> Handle(RoleTrueDeleteCommand request, CancellationToken cancellationToken)
        {
            var list = await _roleRepository.DeleteAsync(request.RoleId);
            return list;
            //throw new NotImplementedException();
        }
    }
}

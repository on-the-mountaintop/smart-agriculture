﻿using I_Authorization.API.Application.Commands;
using I_Authorization.Domains;
using I_Authorization.Infrastructure.Interface;
using MediatR;

namespace I_Authorization.API.Application.CommandHandlers
{
    /// <summary>
    /// 人员、员工的单个逻辑删除
    /// </summary>
    public class EmployeeFalseDeleteCommandHandlers : IRequestHandler<EmployeeFalseDeleteCommand, int>
    {
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeFalseDeleteCommandHandlers(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        /// <summary>
        /// 人员 员工的单个逻辑删除
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<int> Handle(EmployeeFalseDeleteCommand request, CancellationToken cancellationToken)
        {
            //throw new NotImplementedException();
            var info = await _employeeRepository.GetModelAsync(request.EmployeeId);
            info.IsDel = false;
            var list = await _employeeRepository.DeleteAsync(info);
            return list;
        }
    }
}

﻿using I_Authorization.API.Application.Commands;
using I_Authorization.Domains;
using I_Authorization.Domains.Organization;
using I_Authorization.Infrastructure.Interface;
using MediatR;
using System.Security.Principal;

namespace I_Authorization.API.Application.CommandHandlers
{
    /// <summary>
    /// 人员、员工终结者模式添加
    /// </summary>
    public class EmployeeCreateCommandHandlers:IRequestHandler<EmployeeCreateCommand, int>
    {
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeCreateCommandHandlers(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public async Task<int> Handle(EmployeeCreateCommand request, CancellationToken cancellationToken)
        {
            //throw new NotImplementedException();
            return await _employeeRepository.AddAsync(new EmployeeInfo
            {
                EmployeeName = request.EmployeeName,
                EmployeeSex = request.EmployeeSex,
                CertificateId = request.CertificateId,
                EmployeePhone = request.EmployeePhone,
                IDNumber = request.IDNumber,
                Location = request.Location,
                UnitId = request.UnitId,
                EmployeeState = request.EmployeeState,
                DepartId = request.DepartId,
                EntryDate = request.EntryDate,
                Position = request.Position,
                Office = request.Office,
                PersonnelLevel = request.PersonnelLevel,
                Account = request.Account,
                Password = request.Password,
                RoleId = request.RoleId,
                CreateBy = request.CreateBy,
                CreateDate = request.CreateDate,
                UpdateBy = request.UpdateBy,
                UpdateDate = request.UpdateDate,
                IsDel= request.IsDel,
            });
        }
    }
}

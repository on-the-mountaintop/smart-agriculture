﻿using I_Authorization.API.Application.Commands.Permission;
using I_Authorization.Domains.RBAC;
using I_Authorization.Infrastructure.Interface;
using I_Authorization.Infrastructure.Migrations;
using MediatR;

namespace I_Authorization.API.Application.CommandHandlers.Permission
{
    /// <summary>
    /// 中介者权限修改
    /// </summary>
    public class PermissionUpdateCommandHandlers : IRequestHandler<PermissionUpdateCommand, int>
    {
        private readonly IPermissionRepository _permissionRepository;

        public PermissionUpdateCommandHandlers(IPermissionRepository permissionRepository)
        {
            _permissionRepository = permissionRepository;
        }

        public async Task<int> Handle(PermissionUpdateCommand request, CancellationToken cancellationToken)
        {
            return await _permissionRepository.UpdateAsync(new PermissionInfo
            {
                PermissionId=request.PermissionId,
                FunctionName = request.FunctionName,
                Operation = request.Operation,
                RoleId = request.RoleId,
                CreateBy = request.CreateBy,
                CreateDate = request.CreateDate,
                UpdateBy = request.UpdateBy,
                UpdateDate = request.UpdateDate,
                IsDel = request.IsDel,
            });
            //throw new NotImplementedException();
        }
    }
}

﻿using I_Authorization.API.Application.Commands.Permission;
using I_Authorization.Infrastructure.Interface;
using MediatR;

namespace I_Authorization.API.Application.CommandHandlers.Permission
{
    /// <summary>
    /// 权限表的单个逻辑删除
    /// </summary>
    public class PermissionFalseDeleteCommandHandlers : IRequestHandler<PermissionFalseDeleteCommand, int>
    {
        private readonly IPermissionRepository _permissionRepository;

        public PermissionFalseDeleteCommandHandlers(IPermissionRepository permissionRepository)
        {
            _permissionRepository = permissionRepository;
        }

        /// <summary>
        /// 权限表的单个逻辑删除方法
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<int> Handle(PermissionFalseDeleteCommand request, CancellationToken cancellationToken)
        {
            //throw new NotImplementedException();
            var query = await _permissionRepository.GetModelAsync(request.PermissionId);
            query.IsDel = false;
            return await _permissionRepository.DeleteAsync(query);
        }
    }
}

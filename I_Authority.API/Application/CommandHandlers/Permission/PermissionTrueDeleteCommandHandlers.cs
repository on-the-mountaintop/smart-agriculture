﻿using I_Authorization.API.Application.Commands.Permission;
using I_Authorization.Infrastructure.Interface;
using MediatR;

namespace I_Authorization.API.Application.CommandHandlers.Permission
{
    /// <summary>
    /// 权限的单个真删除
    /// </summary>
    public class PermissionTrueDeleteCommandHandlers : IRequestHandler<PermissionTrueDeleteCommand, int>
    {
        private readonly IPermissionRepository _permissionRepository;

        public PermissionTrueDeleteCommandHandlers(IPermissionRepository permissionRepository)
        {
            _permissionRepository = permissionRepository;
        }

        /// <summary>
        /// 权限的单个真删除
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<int> Handle(PermissionTrueDeleteCommand request, CancellationToken cancellationToken)
        {
            //throw new NotImplementedException();
            var list = _permissionRepository.DeleteAsync(request.PermissionId);
            return await list;
        }
    }
}

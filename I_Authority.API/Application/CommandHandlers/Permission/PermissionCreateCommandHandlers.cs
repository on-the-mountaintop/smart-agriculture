﻿using I_Authorization.API.Application.Commands.Permission;
using I_Authorization.Domains.RBAC;
using I_Authorization.Infrastructure.Interface;
using MediatR;

namespace I_Authorization.API.Application.CommandHandlers.Permission
{
    /// <summary>
    /// 中介者添加权限信息
    /// </summary>
    public class PermissionCreateCommandHandlers : IRequestHandler<PermissionCreateCommand, int>
    {
        private readonly IPermissionRepository _permissionRepository;

        public PermissionCreateCommandHandlers(IPermissionRepository permissionRepository)
        {
            _permissionRepository = permissionRepository;
        }
        public async Task<int> Handle(PermissionCreateCommand request, CancellationToken cancellationToken)
        {
            return await _permissionRepository.AddAsync(new PermissionInfo
            {
                FunctionName = request.FunctionName,
                Operation = request.Operation,
                RoleId = request.RoleId,
                CreateBy = request.CreateBy,
                CreateDate = request.CreateDate,
                UpdateBy = request.UpdateBy,
                UpdateDate = request.UpdateDate,
                IsDel = request.IsDel,
            });
            //throw new NotImplementedException();
        }
    }
}

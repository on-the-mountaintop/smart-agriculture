﻿using I_Authorization.API.Application.Commands.Permission;
using I_Authorization.Infrastructure.Impl;
using I_Authorization.Infrastructure.Interface;
using MediatR;

namespace I_Authorization.API.Application.CommandHandlers.Permission
{
    /// <summary>
    /// 权限表的批量逻辑删除
    /// </summary>
    public class PermissionAllFalseDeleteCommandHandlers: IRequestHandler<PermissionAllFalseDeleteCommand,int>
    {
        private readonly IPermissionRepository _permissionRepository;

        public PermissionAllFalseDeleteCommandHandlers(IPermissionRepository permissionRepository)
        {
            _permissionRepository = permissionRepository;
        }

        /// <summary>
        /// 权限表的批量逻辑删除
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<int> Handle(PermissionAllFalseDeleteCommand request, CancellationToken cancellationToken)
        {
            var idarry = request.permissionId.Split(',').Select(x => Convert.ToInt32(x)).ToArray();

            var result = await _permissionRepository.GetAsync(x => idarry.Contains(x.PermissionId));

            foreach (var item in result)
            {
                item.IsDel = false;
            }

            return await _permissionRepository.AllStateDelete(result);
        }
    }
}

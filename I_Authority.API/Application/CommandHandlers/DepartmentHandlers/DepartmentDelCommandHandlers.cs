﻿using AutoMapper;
using I_Authorization.API.Application.Commons.DepartmentCommand;
using I_Authorization.Domains.Organization;
using I_Authorization.Infrastructure.Interface;
using MediatR;

namespace I_Authorization.API.Application.CommandHandlers.DepartmentHandlers
{
    public class DepartmentDelCommandHandlers : IRequestHandler<DepartmentDelCommand, int>
    {
        private readonly IDepartmentRepository _department;
        private readonly IMapper _mapper;

        public DepartmentDelCommandHandlers(IDepartmentRepository department, IMapper mapper)
        {
            _department = department;
            _mapper = mapper;
        }

        public async Task<int> Handle(DepartmentDelCommand request, CancellationToken cancellationToken)
        {
            var res = await _department.GetFindAsync(request.DepartId);
            res.IsDel = true;
            return await _department.UpdateAsync(res);
        }
    }
}

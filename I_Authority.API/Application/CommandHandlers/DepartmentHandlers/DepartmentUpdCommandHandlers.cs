﻿using AutoMapper;
using I_Authorization.API.Application.Commons.DepartmentCommand;
using I_Authorization.Domains.Organization;
using I_Authorization.Infrastructure.Interface;
using MediatR;

namespace I_Authorization.API.Application.CommandHandlers.DepartmentHandlers
{
    ///部门中介者添加
    public class DepartmentUpdCommandHandlers : IRequestHandler<DepartmentUpdCommand, int>
    {
        private readonly IDepartmentRepository _department;
        private readonly IMapper _mapper;

        public DepartmentUpdCommandHandlers(IDepartmentRepository department, IMapper mapper)
        {
            _department = department;
            _mapper = mapper;
        }

        public async Task<int> Handle(DepartmentUpdCommand request, CancellationToken cancellationToken)
        {
            var res = _mapper.Map<Department>(request);
            return await _department.UpdateAsync(res);
        }
    }
}

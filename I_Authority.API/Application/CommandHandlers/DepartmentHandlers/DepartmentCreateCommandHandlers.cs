﻿using AutoMapper;
using I_Authorization.API.Application.Commons.DepartmentCommand;
using I_Authorization.Domains.Organization;
using I_Authorization.Infrastructure.Interface;
using MediatR;

namespace I_Authorization.API.Application.CommandHandlers.DepartmentHandlers
{
    ///部门中介者添加
    public class DepartmentCreateCommandHandlers : IRequestHandler<DepartmentCreateCommand, int>
    {
        private readonly IDepartmentRepository _department;
        private readonly IMapper _mapper;
        /// <summary>
        /// 注入
        /// </summary>
        /// <param name="department"></param>
        /// <param name="mapper"></param>
        public DepartmentCreateCommandHandlers(IDepartmentRepository department, IMapper mapper)
        {
            _department = department;
            _mapper = mapper;
        }
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<int> Handle(DepartmentCreateCommand request, CancellationToken cancellationToken)
        {
            var res = _mapper.Map<Department>(request);
            res.IsDel = false;
            return await _department.AddAsync(res);
        }
    }
}

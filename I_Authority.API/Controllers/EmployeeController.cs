﻿using I_Authorization.API.Application.Commands;
using I_Authorization.API.Application.Commands.Role;
using I_Authorization.Domains;
using I_Authorization.Infrastructure.Interface;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Authorization.API.Controllers
{
    /// <summary>
    /// 人员 员工控制器
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        //private readonly IEmployeeRepository _employeeRepository;  //人员、员工
        //private readonly IRoleRepository _roleRepository;  //角色
        private readonly IMediator _mediator;

        public EmployeeController(IEmployeeRepository employeeRepository, IMediator mediator, IRoleRepository roleRepository)
        {
            //_employeeRepository = employeeRepository;
            _mediator = mediator;
            //_roleRepository = roleRepository;
        }

        /// <summary>
        /// 中介者模式添加人员、员工信息
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<int> AddEmployee(EmployeeCreateCommand m)
        {
            return await _mediator.Send(m);
        }

        /// <summary>
        /// 中介者模式修改人员、员工信息
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<int> UpdateEmployee(EmployeeUpdateCommand m)
        {
            return await _mediator.Send(m);
        }


        /// <summary>
        /// 中介者模式添加角色信息
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<int> AddRole(RoleCreateCommand m)
        {
            return await _mediator.Send(m);
        }

        /// <summary>
        /// 中介者模式修改角色信息
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<int> UpdateRole(RoleUpdateCommand m)
        {
            return await _mediator.Send(m);
        }

        /// <summary>
        /// 角色单个真删除
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<int> TrueDelete(RoleTrueDeleteCommand m)
        {
            return await _mediator.Send(m);
        }

        /// <summary>
        /// 角色单个逻辑删除
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<int> FalseDelete([FromQuery]RoleFalseDeleteCommand m)
        {
            return await _mediator.Send(m);
        }

        //人员 员工的单个真删除   单个逻辑删除

        /// <summary>
        /// 人员 员工的单个真删除
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<int> TrueDeleteEmployee([FromQuery] EmployeeTrueDeleteCommand m)
        {
            return await _mediator.Send(m);
        }

        /// <summary>
        /// 人员、员工的单个逻辑删
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<int> FalseDeleteEmployee([FromQuery] EmployeeFalseDeleteCommand m)
        {
            return await _mediator.Send(m);
        }

        /// <summary>
        /// 人员、员工的批量逻辑删除
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<int> AllFalseDeleteEmployee([FromQuery]EmployeeAllFalseDeleteCommand m)
        {
            return await _mediator.Send(m);
        }

        /// <summary>
        /// 角色的批量逻辑删除
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<int> AllFalseDeleteRole([FromQuery] RoleAllFalseDeleteCommand m)
        {
            return await _mediator.Send(m); 
        }
    }
}

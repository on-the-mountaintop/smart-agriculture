﻿using I_Authorization.API.Application.Commons.DepartmentCommand;
using I_Authorization.API.Application.Commons.Units;
using I_Authorization.Domains.Organization;
using I_Authorization.Infrastructure.Interface;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Authorization.API.Controllers
{
    /// <summary>
    /// 企业
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class UnitsController : ControllerBase
    {
        private readonly IMediator mediator;
        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="mediator"></param>
        public UnitsController( IMediator mediator)
        {
            this.mediator = mediator;
        }
        /// <summary>
        /// 企业公司添加
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<int> UnitsCreate(UnitsCreateCommand command)
        {
            return await mediator.Send(command);
        }
        /// <summary>
        /// 企业公司删除
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<int> UnitsDel([FromQuery] UnitsDelCommand command)
        {
            return await mediator.Send(command);
        }
        /// <summary>
        /// 企业公司修改
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<int> UnitsUpd(UnitsUpdCommand command)
        {
            return await mediator.Send(command);
        }
       
    }
}

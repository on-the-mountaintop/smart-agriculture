﻿using I_Authorization.API.Application.Commons.CottageCommand;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Authorization.API.Controllers
{
    /// <summary>
    /// 栋舍
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CottageController : ControllerBase
    {
        private readonly IMediator _mediator;
        /// <summary>
        /// 注入
        /// </summary>
        /// <param name="mediator"></param>
        public CottageController(IMediator mediator)
        {
            _mediator = mediator;
        }
        /// <summary>
        /// 栋舍添加
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<int> CreateCottage(CottageCreateCommand command) 
        {
            return await _mediator.Send(command);
        }
        /// <summary>
        /// 栋舍删除
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<int> DelCottage([FromQuery] CottageDelCommand command)
        {
            return await _mediator.Send(command);
        }
        /// <summary>
        /// 栋舍修改
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<int> UpdCottage(CottageUpdCommand command)
        {
            return await _mediator.Send(command);
        }

    }
}

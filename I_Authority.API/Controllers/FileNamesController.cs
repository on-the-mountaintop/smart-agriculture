﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace I_Authorization.API.Controllers
{
    /// <summary>
    /// 图片控制器
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class FileNamesController : ControllerBase
    {
        /// <summary>
        /// 图片添加
        /// </summary>
        /// <param name="file">图片路径</param>
        /// <returns>返回图片路径</returns>
        [HttpPost]
        public IActionResult FileNameURL(IFormFile file)
        {

            string FileNameUrl = Directory.GetCurrentDirectory() + @"\wwwroot\img\" + file.FileName;

            using (FileStream sf = new FileStream(FileNameUrl, FileMode.Create))
            {
                file.CopyTo(sf);
                sf.Flush();
            }
            var json = JsonConvert.SerializeObject(file.FileName);
            return Ok(json);
        }
    }
}

﻿using I_Authorization.API.Application.Commons.DepartmentCommand;
using I_Authorization.API.Application.Commons.Shee_YandCommand;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Authorization.API.Controllers
{
    /// <summary>
    /// 部门
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class DepartmentController : ControllerBase
    {
        private readonly IMediator _mediator;
        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="mediator"></param>
        public DepartmentController(IMediator mediator)
        {
            _mediator = mediator;
        }
        /// <summary>
        /// 部门添加
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<int> DepartmentCreate(DepartmentCreateCommand command)
        {
            return await _mediator.Send(command);
        }

        /// <summary>
        /// 部门逻辑删除
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<int> Shee_YandDel([FromQuery] DepartmentDelCommand command)
        {
            return await _mediator.Send(command);
        }

        /// <summary>
        /// 部门修改
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<int> UnitsUpd(DepartmentUpdCommand command)
        {
            return await _mediator.Send(command);
        }
    }
}

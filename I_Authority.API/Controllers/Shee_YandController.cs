﻿using AutoMapper;
using I_Authorization.API.Application.Commons.Shee_YandCommand;
using I_Authorization.API.Application.Commons.Units;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Authorization.API.Controllers
{
    /// <summary>
    /// 羊场
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class Shee_YandController : ControllerBase
    {
        private readonly IMediator _mediator;

        /// <summary>
        /// 实现
        /// </summary>
        /// <param name="mediator"></param>
        public Shee_YandController(IMediator mediator)
        {
            _mediator = mediator;
        }
        /// <summary>
        /// 羊场添加
        /// </summary>
        [HttpPost]
        public async Task<int> Shee_YandCreate(Shee_YandCreateCommand command) 
        {
            return await _mediator.Send(command);
        }
        /// <summary>
        /// 羊场逻辑删除
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<int> Shee_YandDel([FromQuery] Shee_YandDelCommand command) 
        {
            return await _mediator.Send(command);
        }
        /// <summary>
        /// 羊场修改
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<int> UnitsUpd(Shee_YandUpdCommand command)
        {
            return await _mediator.Send(command);
        }
    }
}

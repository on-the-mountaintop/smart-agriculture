﻿using I_Authorization.API.Application.Commands.Permission;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata;

namespace I_Authorization.API.Controllers
{
    /// <summary>
    /// 权限控制器
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class PermissionController : ControllerBase
    {
        private readonly IMediator _mediator;

        public PermissionController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// 中介者添加权限信息
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<int> AddPermissionInfo(PermissionCreateCommand m)
        {
            return await _mediator.Send(m);
        }

        /// <summary>
        /// 修改中介者权限信息
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<int> UpdatePermissionInfo(PermissionUpdateCommand m)
        {
            return await _mediator.Send(m);
        }

        /// <summary>
        /// 中介者模式单个权限真删除
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<int> TrueDelete([FromQuery]PermissionTrueDeleteCommand m)
        {
            return await _mediator.Send(m);
        }

        /// <summary>
        /// 中介者模式单个权限逻辑删除
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<int> FalseDelete([FromQuery] PermissionFalseDeleteCommand m)
        {
            return await _mediator.Send(m);
        }

        /// <summary>
        /// 中介者模式的批量逻辑删除
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<int> AllFalseDelete([FromQuery]PermissionAllFalseDeleteCommand m)
        {
            return await _mediator.Send(m);
        }
    }
}

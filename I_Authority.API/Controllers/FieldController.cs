﻿using I_Authorization.API.Application.Commons.FieldCommand;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Authorization.API.Controllers
{
    /// <summary>
    /// 栏位
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class FieldController : ControllerBase
    {
        private readonly IMediator _mediator;

        public FieldController(IMediator mediator)
        {
            _mediator = mediator;
        }
        /// <summary>
        /// 栏位添加
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public Task<int> CreateField(FieldCreateCommand command) 
        {
            return _mediator.Send(command);
        }
    }
}

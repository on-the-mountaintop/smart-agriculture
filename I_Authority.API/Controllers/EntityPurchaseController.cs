﻿using I_Authorization.API.Application.Commons.CottageCommand;
using I_Authorization.API.Application.Commons.EntityPurchaseCommand;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Authorization.API.Controllers
{
    /// <summary>
    /// 羊只采购
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class EntityPurchaseController : ControllerBase
    {
        private readonly IMediator _mediator;

        public EntityPurchaseController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// 羊只采购新增
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<int> EntityPurchaseCottage(EntityPurchaseCreateCommand command)
        {
            return await _mediator.Send(command);
        }
    }
}

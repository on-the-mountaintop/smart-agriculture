﻿using CommonClass;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace I_Archives.Domains
{
    /// <summary>
    /// 档案信息表
    /// </summary>
    [Table("archives_info")]
    public class ArchivesInfo: AuditInfo
    {
        /// <summary>
        /// 档案Id
        /// </summary>
        [Key]
        public int Id { get; set; }
        /// <summary>
        /// 可视耳号
        /// </summary>
        [Required]
        [StringLength(30)]
        public string? EarNnumber { get; set; }
        /// <summary>
        /// 电子耳号
        /// </summary>
        [Required]
        [StringLength(30)]
        public string? ElectronicEarNumber { get; set; }
        /// <summary>
        /// 羊只种类
        /// </summary>
        [StringLength(30)]
        public string? SheepBreed { get; set; }
        /// <summary>
        /// 羊只性别
        /// </summary>
        public bool SheepGender { get; set; }
        /// <summary>
        /// 出生日期
        /// </summary>
        public DateTime SheepBir { get; set; }
        /// <summary>
        /// 在场状态
        /// </summary>
        [StringLength(100)]
        public string? Presence { get; set; }
        /// <summary>
        /// 生长状态
        /// </summary>
        [StringLength(100)]
        public string? GrowthStage { get; set; }
        /// <summary>
        /// 羊只类型
        /// </summary>
        [StringLength(100)]
        public string? SheepType { get;set;}
        /// <summary>
        /// 栋舍Id
        /// </summary>
        public string? CottageId {  get; set; }
        /// <summary>
        /// 栏位Id
        /// </summary>
        public string? Field {  get; set; }
        /// <summary>
        /// 繁殖状态
        /// </summary>
        [StringLength(100)]
        public string? ReproductiveState { get; set; }
        /// <summary>
        /// 是否妊检
        /// </summary>
        [StringLength(100)]
        public bool? PregnancyTest {  get; set; }
        /// <summary>
        /// 状态天数
        /// </summary>
        public int? StatusDays {  get; set; }
        /// <summary>
        /// 生产等级
        /// </summary>
        [StringLength(100)]
        public string? ProductionGrade { get; set; }
        /// <summary>
        /// 销售等级
        /// </summary>
        [StringLength(100)]
        public string? SalesGrade { get; set; }
        /// <summary>
        /// 基因等级
        /// </summary>
        [StringLength(100)]
        public string? GeneticGrade { get; set; }
        /// <summary>
        /// 出生重量
        /// </summary>
        public decimal? BirthWeight { get; set; }
        /// <summary>
        /// 断奶重量
        /// </summary>
        public decimal? WeaningWeigh { get; set; }
        /// <summary>
        /// 断奶日期
        /// </summary>
        public DateTime? WeaningDate { get; set; }
        /// <summary>
        /// 父耳号
        /// </summary>
        [StringLength(30)]
        public string? FatherEarNumber {  get; set; }
        /// <summary>
        /// 母耳号
        /// </summary>
        [StringLength(30)]
        public string? MotherEarNumber { get; set; }
        /// <summary>
        /// 出生状态
        /// </summary>
        [StringLength(30)]
        public string? StateBirth { get; set; }
        /// <summary>
        /// 出生场地
        /// </summary>
        [StringLength(30)]
        public string? PlaceBirth { get; set; }
        /// <summary>
        /// 入场日期
        /// </summary>
        public DateTime? InDate { get; set; }
        /// <summary>
        /// 档案备注
        /// </summary>
        [StringLength(300)]
        public string? ArchivesRemark { get; set; }
        /// <summary>
        /// 档案图片
        /// </summary>
        [StringLength(2000)]
        public string? ArchivesPicture { get; set; }
    }
}

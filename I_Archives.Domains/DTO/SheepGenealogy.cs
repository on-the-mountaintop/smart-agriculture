﻿using System.ComponentModel.DataAnnotations;

namespace I_Archives.Domains.DTO
{
    public class SheepGenealogy
    {
        /// <summary>
        /// 自身耳号
        /// </summary>
        public string? EarNumber {  get; set; }
        /// <summary>
        /// 父耳号
        /// </summary>
        public string? FatherEarNumber { get; set; }
        /// <summary>
        /// 母耳号
        /// </summary>
        public string? MotherEarNumber { get; set; }
        /// <summary>
        /// 祖父耳号
        /// </summary>
        public string? GrandFatherEarNumber { get; set; }
        /// <summary>
        /// 祖母耳号
        /// </summary>
        public string? GrandMotherEarNumber { get; set; }
        /// <summary>
        /// 祖父耳号
        /// </summary>
        public string? GrandPaFatherEarNumber { get; set; }
        /// <summary>
        /// 祖母耳号
        /// </summary>
        public string? GrandPaMotherEarNumber { get; set; }
    }
}

﻿using I_Authorization.Domains;
using MediatR;

namespace I_Authorization.Read.API.Application.Commands.Employee
{
    /// <summary>
    /// 中介者登录
    /// </summary>
    public class EmployeeRegCommand:IRequest<UserDataDTO>
    {
        public string? Account { get; set; }
        public string? Password { get; set; }
    }
}

﻿using I_Authorization.Domains;
using I_Authorization.Domains.Organization;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace I_Authorization.Read.API.Application.Commands.Employee
{
    /// <summary>
    /// 人员显示
    /// </summary>
    public class EmployeeShowCommand:IRequest<Page<EmployeeInfo>>
    {
        /// <summary>
        /// 主键 自增
        /// </summary>
        //[Key]
        //public int EmployeeId { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        [StringLength(20)]
        public string? EmployeeName { get; set; }

        /// <summary>
        /// 员工状态  （固定下拉框）
        /// </summary>
        [StringLength(20)]
        public string? EmployeeState { get; set; }

        /// <summary>
        /// 部门编号 外键
        /// </summary>
        public int? DepartId { get; set; }






        public int pageIndex { get; set; }
        public int pageSize { get; set; }



    }
}

﻿using I_Authorization.Domains;
using MediatR;

namespace I_Authorization.Read.API.Application.Commands.Employee
{
    /// <summary>
    /// 人员反填
    /// </summary>
    public class EmployeeBackInfoCommand:IRequest<EmployeeInfo>
    {
        /// <summary>
        /// 主键 自增
        /// </summary>
        public int EmployeeId { get; set; }
    }
}

﻿using CommonClass;
using I_Authorization.Domains.RBAC;
using MediatR;

namespace I_Authorization.Read.API.Application.Commands.Menu
{
    public class MenuReadCommand:IRequest<List<MenuTreeData<MenuList>>>
    {
        public int ParentId {  get; set; }
    }
}

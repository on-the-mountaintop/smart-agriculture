﻿using I_Authorization.Domains.RBAC;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace I_Authorization.Read.API.Application.Commands.Role
{
    /// <summary>
    /// 根据主键反填   中介者反填信息
    /// </summary>
    public class RoleFindBackCommand:IRequest<RoleInfo>
    {
        /// <summary>
        /// 角色编号 主键
        /// </summary>
        public int RoleId { get; set; }
    }
}

﻿using CommonClass;
using I_Authorization.Domains;
using I_Authorization.Domains.Organization;
using I_Authorization.Domains.RBAC;
using MediatR;
using System.IO;

namespace I_Authorization.Read.API.Application.Commands.Role
{
    /// <summary>
    /// 角色分页显示
    /// </summary>
    public class RoleShowCommand : IRequest<PageResult<RoleInfo>>
    {
        /// <summary>
        /// 当前页索引
        /// </summary>
        public int pageIndex { get; set; }

        /// <summary>
        /// 页容量
        /// </summary>
        public int pageSize { get; set; }

        /// <summary>
        /// 角色名称
        /// </summary>
        public string? RoleName { get; set; }
    }
}

﻿using I_Authorization.Domains;
using I_Authorization.Domains.Organization;
using I_Authorization.Domains.RBAC;
using MediatR;

namespace I_Authorization.Read.API.Application.Commands.Permission
{
    /// <summary>
    /// 中介者模式的权限显示以及分页
    /// </summary>
    public class PermissionShowPageCommand:IRequest<Page<ShowPermissionRoleDTO>>
    {
        /// <summary>
        /// 页索引
        /// </summary>
        public int pageIndex { get; set; }

        /// <summary>
        /// 页容量
        /// </summary>
        public int pageSize { get; set; }


        /// <summary>
        /// 功能名称
        /// </summary>
        public string? FunctionName { get; set; }

        /// <summary>
        /// 功能编号  外键
        /// </summary>
        public int? RoleId { get; set; }
    }
}

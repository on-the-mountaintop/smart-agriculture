﻿using I_Authorization.Domains.RBAC;
using I_Authorization.Infrastructure.Migrations;
using MediatR;

namespace I_Authorization.Read.API.Application.Commands.Permission
{
    /// <summary>
    /// 权限反填
    /// </summary>
    public class PermissionBackCommand:IRequest<PermissionInfo>
    {
        /// <summary>
        /// 权限主键
        /// </summary>
        public int PermissionId { get; set; }
    }
}

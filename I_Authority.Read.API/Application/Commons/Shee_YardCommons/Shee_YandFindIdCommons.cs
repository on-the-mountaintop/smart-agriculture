﻿using I_Authorization.Domains.Organization;
using MediatR;

namespace I_Authorization.Read.API.Application.Commons.Shee_YardCommons
{
    public class Shee_YandFindIdCommons:IRequest<Shee_Yard>
    {
        public int SheepId { get; set; }             //主键标识
    }
}

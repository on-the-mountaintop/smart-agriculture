﻿using I_Authorization.Domains.Organization;
using MediatR;

namespace I_Authorization.Read.API.Application.Commons.Shee_YardCommons
{
    public class Shee_YandShowCommons:IRequest<Page<Shee_Yard>>
    {
        public int page { get; set; }
        public int size { get; set; }
        public string? SheepFull { get; set; }       //羊场全称
    }
}

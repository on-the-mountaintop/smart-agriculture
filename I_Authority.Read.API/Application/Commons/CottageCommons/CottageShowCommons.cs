﻿using I_Authorization.Domains.Organization;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace I_Authorization.Read.API.Application.Commons.CottageCommons
{
    public class CottageShowCommons:IRequest<Page<Cottage>>
    {
        public int page { get; set; }
        public int size { get; set; }
        [StringLength(30)]
        public string? CottageName { get; set; }        //栋舍名称
    }
}

﻿using I_Authorization.Domains.Organization;
using MediatR;
using System.ComponentModel.DataAnnotations;

namespace I_Authorization.Read.API.Application.Commons.CottageCommons
{
    public class CottageDepartListCommons : IRequest<List<SelectList>>
    {
        public int? Id { get; set; }
    }
}

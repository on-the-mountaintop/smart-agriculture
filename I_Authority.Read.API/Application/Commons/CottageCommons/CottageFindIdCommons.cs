﻿using I_Authorization.Domains.Organization;
using MediatR;

namespace I_Authorization.Read.API.Application.Commons.CottageCommons
{
    public class CottageFindIdCommons:IRequest<Cottage>
    {
        public int CottageId { get; set; }      //主键标识
    }
}

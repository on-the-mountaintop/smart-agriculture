﻿using I_Authorization.Domains.Organization;
using MediatR;

namespace I_Authorization.Read.API.Application.Commons.DepartmentCommons
{
    public class DepartmentFindIdCommons : IRequest<Department>
    {
        public int DepartId { get; set; }   //键标识
    }
}

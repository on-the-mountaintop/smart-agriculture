﻿using CommonClass;
using I_Authorization.Domains;
using I_Authorization.Domains.Organization;
using MediatR;
using NLog.Web.LayoutRenderers;

namespace I_Authorization.Read.API.Application.Commons.DepartmentCommons
{
    public class DepartmentShowCommons : IRequest<Page<Department>>
    {
        public int page { get; set; }
        public int size { get; set; }
        public string? DepartName { get; set; }
    }
}

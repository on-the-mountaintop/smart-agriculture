﻿using I_Authorization.Domains.Organization;
using I_Authorization.Infrastructure.Interface;
using I_Authorization.Read.API.Application.Commons.DepartmentCommons;
using MediatR;

namespace I_Authorization.Read.API.Application.CommandHandlers.DepartmentHandlers
{
    public class DepartmentFindIdCommonsHandlers : IRequestHandler<DepartmentFindIdCommons, Department>
    {
        private readonly IDepartmentRepository _department;

        public DepartmentFindIdCommonsHandlers(IDepartmentRepository _department)
        {
            this._department = _department;
        }

        public async Task<Department> Handle(DepartmentFindIdCommons request, CancellationToken cancellationToken)
        {
            return await _department.GetFindAsync(request.DepartId);
        }
    }
}

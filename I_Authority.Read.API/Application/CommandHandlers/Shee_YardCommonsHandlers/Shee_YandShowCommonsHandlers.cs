﻿using I_Authorization.Domains.Organization;
using I_Authorization.Infrastructure.Impl;
using I_Authorization.Infrastructure.Interface;
using I_Authorization.Read.API.Application.Commons.Shee_YardCommons;
using MediatR;
using Microsoft.AspNetCore.Server.HttpSys;

namespace I_Authorization.Read.API.Application.CommandHandlers.Shee_YardCommonsHandlers
{
    public class Shee_YandShowCommonsHandlers : IRequestHandler<Shee_YandShowCommons, Page<Shee_Yard>>
    {
        private readonly IShee_YardRepository _shee_Yard;

        public Shee_YandShowCommonsHandlers(IShee_YardRepository shee_Yard)
        {
            _shee_Yard = shee_Yard;
        }

        public async Task<Page<Shee_Yard>> Handle(Shee_YandShowCommons request, CancellationToken cancellationToken)
        {
            var List = await _shee_Yard.GetAsync(c=>c.IsDel==false);

            if (!string.IsNullOrEmpty(request.SheepFull) || request.SheepFull != null)
            {
                List = List.Where(c => c.SheepFull.Contains(request.SheepFull)).ToList();
            }
            int totalCount = List.Count(); 
            int pageCount = (int)Math.Ceiling(totalCount * 1.0 / request.size);
            List = List.OrderByDescending(c=>c.SheepId).Skip(request.size * (request.page - 1)).Take(request.size).ToList();

            return new Page<Shee_Yard>
            {
                tatalCount = totalCount,
                pageCount = pageCount,
                Data = List
            };
        }
    }
}

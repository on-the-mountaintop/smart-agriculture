﻿using AutoMapper;
using I_Authorization.Domains.Organization;
using I_Authorization.Infrastructure.Interface;
using I_Authorization.Read.API.Application.Commons.Shee_YardCommons;
using MediatR;

namespace I_Authorization.Read.API.Application.CommandHandlers.Shee_YardCommonsHandlers
{
    public class Shee_YandFindIdCommonsHandlers : IRequestHandler<Shee_YandFindIdCommons, Shee_Yard>
    {
        private readonly IShee_YardRepository _shee_Yard;
        public Shee_YandFindIdCommonsHandlers(IShee_YardRepository shee_Yard)
        {
            _shee_Yard = shee_Yard;
        }
        public async Task<Shee_Yard> Handle(Shee_YandFindIdCommons request, CancellationToken cancellationToken)
        {
            return await _shee_Yard.GetFindAsync(request.SheepId);
        }
    }
}

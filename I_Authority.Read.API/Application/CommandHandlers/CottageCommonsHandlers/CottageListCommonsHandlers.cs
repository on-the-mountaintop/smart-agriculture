﻿using I_Authorization.Domains.Organization;
using I_Authorization.Infrastructure.Interface;
using I_Authorization.Read.API.Application.Commons.CottageCommons;
using MediatR;

namespace I_Authorization.Read.API.Application.CommandHandlers.CottageCommonsHandlers
{
    public class CottageListCommonsHandlers : IRequestHandler<CottageListCommons, List<SelectList>>
    {
        private readonly ICottageRepository _cottage;
        public CottageListCommonsHandlers(ICottageRepository cottage)
        {
            _cottage = cottage;
           
        }
        public async Task<List<SelectList>> Handle(CottageListCommons request, CancellationToken cancellationToken)
        {
            var info =  await _cottage.GetAsync(c => c.IsDel == false);
            List<SelectList> res = new List<SelectList>();
            foreach ( var item in info) 
            {
                res.Add(new SelectList
                {
                    Id = item.CottageId,
                    name = item.CottageName,
                });
            }
            return res;
        }
    }
}

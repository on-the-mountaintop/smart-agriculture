﻿using I_Authorization.Domains.Organization;
using I_Authorization.Infrastructure.Interface;
using I_Authorization.Read.API.Application.Commons.CottageCommons;
using MediatR;

namespace I_Authorization.Read.API.Application.CommandHandlers.CottageCommonsHandlers
{
    public class CottageDeparCommonsHandlers : IRequestHandler<CottageDepartListCommons, List<SelectList>>
    {
        private readonly IDepartmentRepository _department;

        public CottageDeparCommonsHandlers(IDepartmentRepository department)
        {
            _department = department;
        }

        public async Task<List<SelectList>> Handle(CottageDepartListCommons request, CancellationToken cancellationToken)
        {
            var info = await _department.GetAsync(c => c.IsDel == false);
            List<SelectList> res = new List<SelectList>();
            foreach (var item in info)
            {
                res.Add(new SelectList
                {
                    Id = item.DepartId,
                    name = item.DepartName,
                });
            }
            return res;
        }
    }
}

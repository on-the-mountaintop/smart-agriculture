﻿using I_Authorization.Domains.Organization;
using I_Authorization.Infrastructure.Interface;
using I_Authorization.Read.API.Application.Commons.CottageCommons;
using MediatR;

namespace I_Authorization.Read.API.Application.CommandHandlers.CottageCommonsHandlers
{
    public class CottageUnitsCommonsHandlers : IRequestHandler<CottageUnitsListCommons, List<SelectList>>
    {
        private readonly IUnitsRepository _units;

        public CottageUnitsCommonsHandlers(IUnitsRepository units)
        {
            _units = units;
        }

        public async Task<List<SelectList>> Handle(CottageUnitsListCommons request, CancellationToken cancellationToken)
        {
            var info = await  _units.GetAsync(c => c.IsDel == false);
            List<SelectList> res = new List<SelectList>();
            foreach (var item in info)
            {
                res.Add(new SelectList
                {
                    Id = item.UnitId,
                    name = item.UnitFull,
                });
            }
            return res;
        }
    }
}

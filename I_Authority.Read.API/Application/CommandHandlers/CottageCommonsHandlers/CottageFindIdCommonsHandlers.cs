﻿using I_Authorization.Domains.Organization;
using I_Authorization.Infrastructure.Interface;
using I_Authorization.Read.API.Application.Commons.CottageCommons;
using MediatR;

namespace I_Authorization.Read.API.Application.CommandHandlers.CottageCommonsHandlers
{
    public class CottageFindIdCommonsHandlers : IRequestHandler<CottageFindIdCommons, Cottage>
    {
        private readonly ICottageRepository _cottage;

        public CottageFindIdCommonsHandlers(ICottageRepository cottage)
        {
            _cottage = cottage;
        }

        public async Task<Cottage> Handle(CottageFindIdCommons request, CancellationToken cancellationToken)
        {
            return await _cottage.GetFindAsync(request.CottageId);
        }
    }
}

﻿using I_Authorization.Domains.Organization;
using I_Authorization.Infrastructure.Interface;
using I_Authorization.Read.API.Application.Commons.CottageCommons;
using MediatR;

namespace I_Authorization.Read.API.Application.CommandHandlers.CottageCommonsHandlers
{
    public class FieidListCommonsHandlers : IRequestHandler<FieidListCommons, List<SelectList>>
    {
        private readonly IFieldRepository _field;

        public FieidListCommonsHandlers(IFieldRepository field)
        {
            _field = field;
        }
         
        public async Task<List<SelectList>> Handle(FieidListCommons request, CancellationToken cancellationToken)
        {
            var info = await _field.GetAsync(c => c.CottageId.Equals(request.Id) && c.IsDel == false);
            List<SelectList> res = new List<SelectList>();
            foreach (var item in info)
            {
                res.Add(new SelectList
                {
                    Id = item.FieId,
                    name = item.FieName,
                });
            }
            return res;
        }
    }
}

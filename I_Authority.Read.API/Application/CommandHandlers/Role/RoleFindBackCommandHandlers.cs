﻿using I_Authorization.Domains.RBAC;
using I_Authorization.Infrastructure.Interface;
using I_Authorization.Read.API.Application.Commands.Role;
using MediatR;

namespace I_Authorization.Read.API.Application.CommandHandlers.Role
{
    /// <summary>
    /// 中介者角色反填
    /// </summary>
    public class RoleFindBackCommandHandlers : IRequestHandler<RoleFindBackCommand, RoleInfo>
    {

        private readonly IRoleRepository _roleRepository;

        public RoleFindBackCommandHandlers(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;
        }

        /// <summary>
        /// 中介者角色反填
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<RoleInfo> Handle(RoleFindBackCommand request, CancellationToken cancellationToken)
        {
            return await _roleRepository.GetModelAsync(request.RoleId);
            //throw new NotImplementedException();
        }
    }
}

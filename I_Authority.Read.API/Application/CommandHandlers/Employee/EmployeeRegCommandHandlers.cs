﻿using I_Authorization.Domains;
using I_Authorization.Infrastructure.Interface;
using I_Authorization.Read.API.Application.Commands.Employee;
using MediatR;
using System.Security.Principal;

namespace I_Authorization.Read.API.Application.CommandHandlers.Employee
{
    public class EmployeeRegCommandHandlers : IRequestHandler<EmployeeRegCommand, UserDataDTO>
    {
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeRegCommandHandlers(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        /// <summary>
        /// 中介者登录
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<UserDataDTO> Handle(EmployeeRegCommand request, CancellationToken cancellationToken)
        {
            //var list = await _employeeRepository.GetModelAsync(x => x.Account == request.Account && x.Password == request.Password);
            //return list;

            var list = await _employeeRepository.GetAsync(x => x.Account == request.Account && x.Password == request.Password);
            UserDataDTO userDataDTO = new UserDataDTO();
            foreach (var item in list)
            {
                var info = new UserDataDTO
                {
                    Account = request.Account,
                    Password = request.Password,
                    
                };
                return info;
            };

            if (list.Count != 0)
            {
                return userDataDTO;
            }
            else
            {
                return null;
            }

            //throw new NotImplementedException();
        }
    }
}

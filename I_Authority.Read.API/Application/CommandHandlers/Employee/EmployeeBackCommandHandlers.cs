﻿using I_Authorization.Domains;
using I_Authorization.Infrastructure.Interface;
using I_Authorization.Read.API.Application.Commands.Employee;
using MediatR;

namespace I_Authorization.Read.API.Application.CommandHandlers.Employee
{
    /// <summary>
    /// 人员反填
    /// </summary>
    public class EmployeeBackCommandHandlers : IRequestHandler<EmployeeBackInfoCommand, EmployeeInfo>
    {
        private readonly IEmployeeRepository _employeeRepository;

        public EmployeeBackCommandHandlers(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        

        /// <summary>
        /// 反填人员信息
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public async Task<EmployeeInfo> Handle(EmployeeBackInfoCommand request, CancellationToken cancellationToken)
        {
            //throw new NotImplementedException();
            var list = _employeeRepository.GetModelAsync(request.EmployeeId);
            return await list;
        }
    }
}

﻿using I_Authorization.Domains.RBAC;
using I_Authorization.Infrastructure.Interface;
using I_Authorization.Read.API.Application.Commands.Permission;
using MediatR;

namespace I_Authorization.Read.API.Application.CommandHandlers.Permission
{
    /// <summary>
    /// 权限反填
    /// </summary>
    public class PermissionBackCommandHandlers : IRequestHandler<PermissionBackCommand, PermissionInfo>
    {
        private readonly IPermissionRepository _permissionRepository;

        public PermissionBackCommandHandlers(IPermissionRepository permissionRepository)
        {
            _permissionRepository = permissionRepository;
        }

        /// <summary>
        /// 权限反填
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<PermissionInfo> Handle(PermissionBackCommand request, CancellationToken cancellationToken)
        {
            //throw new NotImplementedException();
            var list = _permissionRepository.GetModelAsync(request.PermissionId);
            return await list;
        }

        //public async Task<int> Handle(PermissionBackCommand request, CancellationToken cancellationToken)
        //{
        //    var list = _permissionRepository.GetModelAsync(request.PermissionId);
        //    return await list;
        //    //throw new NotImplementedException();
        //}
    }
}

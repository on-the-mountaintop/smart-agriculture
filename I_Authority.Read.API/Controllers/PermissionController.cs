﻿using I_Authorization.Domains;
using I_Authorization.Domains.Organization;
using I_Authorization.Domains.RBAC;
using I_Authorization.Read.API.Application.Commands.Permission;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Authorization.Read.API.Controllers
{
    /// <summary>
    /// 权限控制器
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class PermissionController : ControllerBase
    {
        private readonly IMediator _mediator;

        public PermissionController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// 权限的分页显示
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<Page<ShowPermissionRoleDTO>> ShowPagePermission([FromQuery]PermissionShowPageCommand m)
        {
            return await _mediator.Send(m);
        }

        /// <summary>
        /// 反填
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<PermissionInfo> BackPermissionInfo([FromQuery]PermissionBackCommand m)
        {
            return await _mediator.Send(m);
        }
    }
}

﻿using CommonClass;
using I_Authorization.Domains;
using I_Authorization.Domains.Organization;
using I_Authorization.Domains.RBAC;
using I_Authorization.Read.API.Application.Commands.Employee;
using I_Authorization.Read.API.Application.Commands.Role;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Authorization.Read.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class RoleShowController : ControllerBase
    {
        private readonly IMediator mediator;

        public RoleShowController(IMediator _mediator)
        {
            mediator = _mediator;
        }

        /// <summary>
        /// 显示角色分页
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        [HttpGet]

        public async Task<PageResult<RoleInfo>> ShowPageRoleInfo([FromQuery]RoleShowCommand m)
        {
            return await mediator.Send(m);
        }

        /// <summary>
        /// 反填角色方法
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<RoleInfo> BackRoleInfo([FromQuery] RoleFindBackCommand m)
        {
            return await mediator.Send(m);
        }

        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<UserDataDTO> RegEmployeeInfo(EmployeeRegCommand m)
        {
            return await mediator.Send(m);
        }

        /// <summary>
        /// 人员、员工信息的显示、分页信息
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<Page<EmployeeInfo>> ShowPageEmployeeInfo([FromQuery]EmployeeShowCommand m)
        {
            return await mediator.Send(m);
        }

        /// <summary>
        /// 通过字符串查询单个对象 反填
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<EmployeeInfo> BackEmployeeInfo([FromQuery] EmployeeBackInfoCommand m)
        {
            return await mediator.Send(m);
        }
    }
}

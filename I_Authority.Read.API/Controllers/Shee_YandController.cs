﻿using I_Authorization.Domains.Organization;
using I_Authorization.Read.API.Application.Commons.Shee_YardCommons;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Authorization.Read.API.Controllers
{
    /// <summary>
    /// 羊场
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class Shee_YandController : ControllerBase
    {
        private readonly IMediator _mediator;
        /// <summary>
        ///构造函数
        /// </summary>
        /// <param name="mediator"></param>
        public Shee_YandController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// 管理部门列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult AdministrativeType()
        {
            List<object> list = new List<object>();
            foreach (var item in Enum.GetValues<Administrative>())
            {
                list.Add(new
                {
                    value = item,
                    text = item.ToString()
                });
            }
            
            return Ok(list);
        }
        /// <summary>
        /// 羊场列表
        /// </summary>
        /// <param name="commons"></param>
        /// <returns></returns>
        [HttpGet]
        public  async Task<Page<Shee_Yard>> GetShowShee_Yard([FromQuery] Shee_YandShowCommons commons) 
        {
            return await  _mediator.Send(commons);
        }

        /// <summary>
        /// 通过ID获取羊场数据
        /// </summary>
        /// <param name="commons"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<Shee_Yard> FindShee_Yard([FromQuery] Shee_YandFindIdCommons commons) 
        {
            return await _mediator.Send(commons);
        } 
    }
}

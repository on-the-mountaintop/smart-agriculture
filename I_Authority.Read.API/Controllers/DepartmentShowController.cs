﻿using I_Authorization.Domains.Organization;
using I_Authorization.Read.API.Application.Commons.DepartmentCommons;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Authorization.Read.API.Controllers
{
    /// <summary>
    /// 企业
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class DepartmentShowController : ControllerBase
    {
        private readonly IMediator mediator;
        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="mediator"></param>
        public DepartmentShowController(IMediator mediator)
        {
            this.mediator = mediator;
        }
        /// <summary>
        /// 部门列表
        /// </summary>
        /// <param name="command">页数 条数</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<Page<Department>> ArchivesPageList([FromQuery] DepartmentShowCommons command)
        {
            return await mediator.Send(command);
        }
        /// <summary>
        /// 根据Id获取部门数据
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<Department> ArchivesReadById([FromQuery] DepartmentFindIdCommons command)
        {
            return await mediator.Send(command);
        }
       
    }
}

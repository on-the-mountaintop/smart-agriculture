﻿using I_Authorization.Domains.Organization;
using I_Authorization.Read.API.Application.Commons.CottageCommons;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Authorization.Read.API.Controllers
{
    /// <summary>
    /// 栋舍
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CottageController : ControllerBase
    {
        private readonly IMediator _mediator;
        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="mediator"></param>
        public CottageController(IMediator mediator)
        {
            _mediator = mediator;
        }
        /// <summary>
        /// 栋舍类型列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult CottageType() 
        {
            List<object> list = new List<object>();
            foreach (var item in Enum.GetValues<CottType>())
            {
                list.Add(new
                {
                    value = item,
                    text = item.ToString()
                });
            }
            return Ok(list);
        }
        /// <summary>
        /// 负责人列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult PrincipalType()
        {
            List<object> list = new List<object>();
            foreach (var item in Enum.GetValues<Principal>())
            {
                list.Add(new
                {
                    value = item,
                    text = item.ToString()
                });
            }
            return Ok(list);
        }
        /// <summary>
        /// 栋舍列表
        /// </summary>
        /// <param name="commons"></param>
        /// <returns></returns>
        [HttpGet]
        public Task<Page<Cottage>> PageShowCottage([FromQuery] CottageShowCommons commons)
        {
            return _mediator.Send(commons);
        }
        /// <summary>
        /// 通过栋舍ID拿到栋舍信息
        /// </summary>
        /// <param name="commons"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<Cottage>FindIDCottage([FromQuery] CottageFindIdCommons commons)
        {
            return await _mediator.Send(commons);
        }

        /// <summary>
        /// 栋舍下拉列表
        /// </summary>
        /// <param name="commons"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<SelectList>> CootList([FromQuery] CottageListCommons commons)
        {
            return await _mediator.Send(commons);
        }

        /// <summary>
        /// 栏位下拉列表
        /// </summary>
        /// <param name="commons"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<SelectList>> FieidList([FromQuery] FieidListCommons commons)
        {
            return await _mediator.Send(commons);
        }
        /// <summary>
        /// 公司下拉列表
        /// </summary>
        /// <param name="commons"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<SelectList>> UnitsList([FromQuery] CottageUnitsListCommons commons)
        {
            return await _mediator.Send(commons);
        }
        /// <summary>
        /// 部门下拉列表
        /// </summary>
        /// <param name="commons"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<SelectList>> DeparList([FromQuery] CottageDepartListCommons commons)
        {
            return await _mediator.Send(commons);
        }
    }
}

﻿using CommonClass;
using I_Authorization.Domains.RBAC;
using I_Authorization.Read.API.Application.Commands.Menu;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace I_Authorization.Read.API.Controllers
{
    /// <summary>
    /// 菜单管理
    /// </summary>
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class MenuController : ControllerBase
    {
        private readonly IMediator _mediator;
        
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="mediator"></param>
        public MenuController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// 获取菜单列表
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<MenuTreeData<MenuList>>> GetMenuList([FromQuery]MenuReadCommand command)
        {
            return await _mediator.Send(command);
        }
    }
}
